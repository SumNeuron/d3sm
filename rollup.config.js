import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';


import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/entry.js',
  external: ['d3', 'd3-selection'],

  output: {
    name: pkg.name,
    exports: 'named',
    extend: true,
    globals: {
      d3: 'd3',
      'd3-selection': "d3-selection"
    },
  },
  plugins: [
    babel({
      exclude: 'node_modules/**',
      externalHelpers: true,
      runtimeHelpers: true,
    }),
    nodeResolve({
      mainFields: [
        // 'module',
        'main', 'jsnext'
      ]
    }),
    commonjs({
      // non-CommonJS modules will be ignored, but you can also
      // specifically include/exclude files
      include: 'node_modules/**',  // Default: undefined

      // search for files other than .js files (must already
      // be transpiled by a previous plugin!)
      extensions: [ '.js', '.coffee' ],  // Default: [ '.js' ]

      namedExports: {
        // 'node_modules/d3/dist/d3.node.js': [
        //   'selection', 'select', 'interpolateRgbBasis',
        //   'median', 'min', 'max', 'scaleLinear', 'easeExp', 'extent',
        //   'easeSin', 'selectAll', 'keys', 'interpolateRgb', 'descending', 'line',
        //   'curveBasis', 'histogram', 'ascending',
        //    'mouse', 'event',
        //    'sourceEvent', 'start'
        // ],
        // 'node_modules/d3-selection/dist/d3-selection.js': [
        //   'mouse',
        //   'event'
        // ]
        // 'node_modules/d3/d3.js': ['selection', 'mouse', 'select'],
        // 'node_modules/d3-selection/src/selection/index.js': ['selection']
      },

      // sometimes you have to leave require statements
      // unconverted. Pass an array containing the IDs
      // or a `id => boolean` function. Only use this
      // option if you know what you're doing!
      ignore: [ 'conditional-runtime-dependency' ]
    })

  ],
  onwarn: function ( message ) {
    if (message.code === 'CIRCULAR_DEPENDENCY') {
      return;
    }
    console.error(message);
  }
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
