function code(codeFilePath, codeTagId) {
  var reader = new XMLHttpRequest() || new ActiveXObject('MSXML2.XMLHTTP');

  function read() {
    reader.open('get', codeFilePath, true)
    reader.onreadystatechange = dumpContents;
    reader.send(null)
  }

  function dumpContents() {
    if (reader.readyState==4) {
      var el = document.getElementById(codeTagId);
      el.innerHTML = reader.responseText
      highlight()
    }
  }

  function highlight() {
    var el = document.getElementById(codeTagId);
    hljs.highlightBlock(el)
    // hljs.lineNumbersBlock(el)
  }

  var codeFile = {}
  codeFile.read = read;
  codeFile.dumpContents = dumpContents;
  return codeFile

}
