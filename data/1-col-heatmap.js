hmData = {
  "cell30": {
    "experimentId": "GSE68189_fibroblast-of-lung_mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -6.2226386938325,
    "padj": 2.1088142155574902e-23,
    "baseMean": 61.9213576289632,
    "baseMean_GroupA": 122.391407251482,
    "baseMean_GroupB": 1.45130800644418,
    "lfcSE": 0.613252124956384,
    "stat": -10.1469500725743,
    "pvalue": 3.41881026372532e-24,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_742e4dbf6fe1e11da3ee964d91c894d6_5b158790f1a71",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_742e4dbf6fe1e11da3ee964d91c894d6",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell21": {
    "experimentId": "GSE68189_fibroblast-of-lung_midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -9.37387168033694,
    "padj": 7.97831900235951e-20,
    "baseMean": 69.2709232381526,
    "baseMean_GroupA": 138.541846476305,
    "baseMean_GroupB": 0,
    "lfcSE": 1.01255840714789,
    "stat": -9.25761083426353,
    "pvalue": 2.09057556746319e-20,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_midbrain-cell_none_1",
      "folderName": "DE__GSE68189_de7a43dad8718dbf59fa00ae423fa9e9_5b1587934ddbb",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_de7a43dad8718dbf59fa00ae423fa9e9",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell20": {
    "experimentId": "GSE50064_MCF-10A-cell_HCT-116-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.03466516653887,
    "padj": 0.000132299860961102,
    "baseMean": 190.816274984812,
    "baseMean_GroupA": 256.980780827328,
    "baseMean_GroupB": 124.651769142296,
    "lfcSE": 0.246515603809422,
    "stat": -4.19715892442554,
    "pvalue": 0.000027028427208916,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell",
      "group_B": "HCT-116-cell",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell_HCT-116-cell_none_1",
      "folderName": "DE__GSE50064_1917a99d0f258f13f79a380bbc5ac2cb_5b0fcaaf6b812",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE50064_1917a99d0f258f13f79a380bbc5ac2cb",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "MCF-10A-cell vs HCT-116-cell -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell23": {
    "experimentId": "GSE62776_reprogramming-intermediates-of-hiF-T-cell_hIPSCs-from-reprogrammed-hiF-T-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -2.9950835712129997,
    "padj": 0.0475076739384759,
    "baseMean": 18.6150191435003,
    "baseMean_GroupA": 59.9205108590766,
    "baseMean_GroupB": 2.05341278081007,
    "lfcSE": 1.18527943354801,
    "stat": -2.5269008188621997,
    "pvalue": 0.0115073995011776,
    "comparisonDetails": {
      "group_A": "reprogramming-intermediates-of-hiF-T-cell",
      "group_B": "hIPSCs-from-reprogrammed-hiF-T-cell",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_reprogramming-intermediates-of-hiF-T-cell_hIPSCs-from-reprogrammed-hiF-T-cell_none_1",
      "folderName": "DE__GSE62776_95d1a8bfe63268e0dba41683f4c6037b_5b1589f6b9108",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE62776_95d1a8bfe63268e0dba41683f4c6037b",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "reprogramming-intermediates-of-hiF-T-cell vs hIPSCs-from-reprogrammed-hiF-T-cell -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell22": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_H9-cell:embryonic-stem-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -4.59691086931101,
    "padj": 0.000019606731786956703,
    "baseMean": 63.4932757375456,
    "baseMean_GroupA": 122.782036031139,
    "baseMean_GroupB": 4.20451544395197,
    "lfcSE": 1.03849072849252,
    "stat": -4.42653048620272,
    "pvalue": 0.00000957608538590158,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "H9-cell:embryonic-stem-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_H9-cell:embryonic-stem-cell_none_1",
      "folderName": "DE__GSE68189_2a5873fc97203d7e21eb72138adb5b0a_5b1587c1156b7",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_2a5873fc97203d7e21eb72138adb5b0a",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs H9-cell:embryonic-stem-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell25": {
    "experimentId": "GSE37686_embryonic-stem-cell_ES-derived-Retinal-pigment-epithelium_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 4.27201692897152,
    "padj": 0.00541804123440497,
    "baseMean": 8.84011289650136,
    "baseMean_GroupA": 0.61535534669606,
    "baseMean_GroupB": 17.0648704463067,
    "lfcSE": 1.40418676185875,
    "stat": 3.04234240416607,
    "pvalue": 0.0023474470235307,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "ES-derived-Retinal-pigment-epithelium",
      "datasetId": "GSE37686",
      "covariates": "none",
      "experimentId": "GSE37686_embryonic-stem-cell_ES-derived-Retinal-pigment-epithelium_none_1",
      "folderName": "DE__GSE37686_61ef8b3661853c09edd06b7ed0ec284e_5b0fcaa4416e3",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE37686_61ef8b3661853c09edd06b7ed0ec284e",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs ES-derived-Retinal-pigment-epithelium -- GSE37686",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell24": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -6.2226386938325,
    "padj": 2.1088142155574902e-23,
    "baseMean": 61.9213576289632,
    "baseMean_GroupA": 122.391407251482,
    "baseMean_GroupB": 1.45130800644418,
    "lfcSE": 0.613252124956384,
    "stat": -10.1469500725743,
    "pvalue": 3.41881026372532e-24,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_99e0059a42c40e4792ca2be63b414828_5b1587c5739c5",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_99e0059a42c40e4792ca2be63b414828",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell27": {
    "experimentId": "GSE62776_dox-days-0-10_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.75111790941546,
    "padj": 0.00263671733813016,
    "baseMean": 17.3477039597667,
    "baseMean_GroupA": 27.8365230104297,
    "baseMean_GroupB": 1.6144753837722199,
    "lfcSE": 1.02126989894703,
    "stat": -3.67299370448794,
    "pvalue": 0.000239725391634311,
    "comparisonDetails": {
      "group_A": "dox-days-0-10",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-10_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_1b6433c32038f7fc1c2dda7f8755f91a_5b158a1a306c2",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_1b6433c32038f7fc1c2dda7f8755f91a",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-10 vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell26": {
    "experimentId": "GSE58127_A549-cell_immortal-human-B-cell-line-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -6.96972687159003,
    "padj": 0.00469823823894185,
    "baseMean": 30.3573897784674,
    "baseMean_GroupA": 60.7147795569348,
    "baseMean_GroupB": 0,
    "lfcSE": 1.96558160125058,
    "stat": -3.54588528258283,
    "pvalue": 0.000391296510488027,
    "comparisonDetails": {
      "group_A": "A549-cell",
      "group_B": "immortal-human-B-cell-line-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_A549-cell_immortal-human-B-cell-line-cell_none_1",
      "folderName": "DE__GSE58127_d99a6b540971b73d2612ab8796522945_5b0fcb247f19a",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_d99a6b540971b73d2612ab8796522945",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "A549-cell vs immortal-human-B-cell-line-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell29": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.39118486858574,
    "padj": 0.000943275327539548,
    "baseMean": 257.545500626694,
    "baseMean_GroupA": 373.868742432201,
    "baseMean_GroupB": 141.222258821187,
    "lfcSE": 0.360017657929595,
    "stat": -3.86421287385242,
    "pvalue": 0.00011144799827906,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_b50454cdda6c430ee297f656dc3165a6_5b0fcad724fee",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_b50454cdda6c430ee297f656dc3165a6",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell28": {
    "experimentId": "GSE62776_ssea3-_tra-1-60+_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -2.59943791366055,
    "padj": 0.0401887639247702,
    "baseMean": 12.4291508523923,
    "baseMean_GroupA": 25.5391864568192,
    "baseMean_GroupB": 3.6891271161076302,
    "lfcSE": 0.992145560922037,
    "stat": -2.62001667501772,
    "pvalue": 0.00879254675277191,
    "comparisonDetails": {
      "group_A": "ssea3-",
      "group_B": "tra-1-60+",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_ssea3-_tra-1-60+_none_1",
      "folderName": "DE__GSE62776_5915161c69d32a1c139d4d30462ea221_5b158a2519235",
      "comparisonCondition": "marker",
      "experimentHash": "GSE62776_5915161c69d32a1c139d4d30462ea221",
      "genome": "hg38",
      "allConditions": [
        "marker"
      ]
    },
    "xkey": "ssea3- vs tra-1-60+ -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell0": {
    "experimentId": "GSE58127_HEK293_A549-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 6.80566266865795,
    "padj": 0.000275654109160269,
    "baseMean": 34.3371299049544,
    "baseMean_GroupA": 0.333134808818314,
    "baseMean_GroupB": 85.3431225491586,
    "lfcSE": 1.54303687555737,
    "stat": 4.41056385395822,
    "pvalue": 0.0000103101796672932,
    "comparisonDetails": {
      "group_A": "HEK293",
      "group_B": "A549-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_HEK293_A549-cell_none_1",
      "folderName": "DE__GSE58127_dc2f42c7539259047f087525468d6f2f_5b0fcb18150f4",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_dc2f42c7539259047f087525468d6f2f",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HEK293 vs A549-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell1": {
    "experimentId": "GSE68189_fibroblast-of-lung_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.79742589662322,
    "padj": 4.92514358696472e-27,
    "baseMean": 37.8035627949757,
    "baseMean_GroupA": 103.959797686183,
    "baseMean_GroupB": 0,
    "lfcSE": 0.803121751128083,
    "stat": -10.9540376465538,
    "pvalue": 6.35502398318028e-28,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_fa2e391d3a6802daf2f7453bd4234594_5b1587981a308",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_fa2e391d3a6802daf2f7453bd4234594",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell4": {
    "experimentId": "GSE68189_fibroblast-of-lung_embryonic-stem-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -4.59691086931101,
    "padj": 0.000019606731786956703,
    "baseMean": 63.4932757375456,
    "baseMean_GroupA": 122.782036031139,
    "baseMean_GroupB": 4.20451544395197,
    "lfcSE": 1.03849072849252,
    "stat": -4.42653048620272,
    "pvalue": 0.00000957608538590158,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "embryonic-stem-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_embryonic-stem-cell_none_1",
      "folderName": "DE__GSE68189_bc9f33ce5db1292d8acec8e5ee11b5c5_5b15878c91ada",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_bc9f33ce5db1292d8acec8e5ee11b5c5",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs embryonic-stem-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell10": {
    "experimentId": "GSE62776_dox-days-0-10_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -2.6366995770507797,
    "padj": 0.00245113125769884,
    "baseMean": 19.41837033534,
    "baseMean_GroupA": 33.6977039383261,
    "baseMean_GroupB": 5.13903673235394,
    "lfcSE": 0.686238519545994,
    "stat": -3.84224945401663,
    "pvalue": 0.000121911836824811,
    "comparisonDetails": {
      "group_A": "dox-days-0-10",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-10_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_78085df089de6c2fee21a88b9109fe36_5b158a1877ed5",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_78085df089de6c2fee21a88b9109fe36",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-10 vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell32": {
    "experimentId": "GSE62776_no-dox_dox-days-0-14_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -5.15540961578232,
    "padj": 1.19115071370713e-16,
    "baseMean": 150.332162776201,
    "baseMean_GroupA": 361.698631022664,
    "baseMean_GroupB": 9.42118394522576,
    "lfcSE": 0.587068038915077,
    "stat": -8.78162201660595,
    "pvalue": 1.61133811427604e-18,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-14",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-14_none_1",
      "folderName": "DE__GSE62776_d1de9fbbb00fd9a44e67428470f246fe_5b1589fea0a90",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_d1de9fbbb00fd9a44e67428470f246fe",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-14 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell5": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -9.37387168033694,
    "padj": 7.97831900235951e-20,
    "baseMean": 69.2709232381526,
    "baseMean_GroupA": 138.541846476305,
    "baseMean_GroupB": 0,
    "lfcSE": 1.01255840714789,
    "stat": -9.25761083426353,
    "pvalue": 2.09057556746319e-20,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:midbrain-cell_none_1",
      "folderName": "DE__GSE68189_ffee1ecefaa2710b163f3ee44d80d2bd_5b1587c79ba7b",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_ffee1ecefaa2710b163f3ee44d80d2bd",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell31": {
    "experimentId": "GSE50064_HCT-116-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 0.942952550116248,
    "padj": 0.0470877754366495,
    "baseMean": 259.67205694395,
    "baseMean_GroupA": 174.785270671563,
    "baseMean_GroupB": 344.558843216337,
    "lfcSE": 0.312034271129362,
    "stat": 3.02195187311756,
    "pvalue": 0.00251150475112822,
    "comparisonDetails": {
      "group_A": "HCT-116-cell:control:0-hN/A",
      "group_B": "HCT-116-p53-/-:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_b78a5bd2bb34d95bde7263dd245851fc_5b0fcaeea0252",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_b78a5bd2bb34d95bde7263dd245851fc",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "HCT-116-cell:control:0-hN/A vs HCT-116-p53-/-:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell2": {
    "experimentId": "GSE68189_fibroblast-of-lung_forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.42752701751858,
    "padj": 3.2796866184148498e-16,
    "baseMean": 62.4510365981004,
    "baseMean_GroupA": 124.64905810873,
    "baseMean_GroupB": 0.253015087471031,
    "lfcSE": 1.01460931403416,
    "stat": -8.30617943374686,
    "pvalue": 9.883274504342919e-17,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_forebrain-cell_none_1",
      "folderName": "DE__GSE68189_0b08194eb7ac9486483b3f18a59afaf8_5b15878ec6413",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_0b08194eb7ac9486483b3f18a59afaf8",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell12": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.15049242880557,
    "padj": 0.00696310642428488,
    "baseMean": 253.758991027815,
    "baseMean_GroupA": 350.836256234805,
    "baseMean_GroupB": 156.681725820825,
    "lfcSE": 0.350089853470426,
    "stat": -3.28627755817767,
    "pvalue": 0.00101520921396086,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_ee07f7537e648df24e9673ddaae3d8b4_5b0fcad8a4043",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_ee07f7537e648df24e9673ddaae3d8b4",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell34": {
    "experimentId": "GSE50064_HCT-116-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 1.1663337049821,
    "padj": 0.0391687872179181,
    "baseMean": 236.90232969708,
    "baseMean_GroupA": 139.582995425528,
    "baseMean_GroupB": 334.221663968632,
    "lfcSE": 0.382153363944892,
    "stat": 3.05200428681896,
    "pvalue": 0.00227318854389703,
    "comparisonDetails": {
      "group_A": "HCT-116-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_db7e14084df526f7bad36bd63b85a074_5b0fcafc2d1d5",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_db7e14084df526f7bad36bd63b85a074",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "HCT-116-cell:irradiation:24-hN/A vs HCT-116-p53-/-:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell3": {
    "experimentId": "GSE68189_fibroblast-of-lung_hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.99971837046482,
    "padj": 2.11401691643762e-18,
    "baseMean": 61.0126334752684,
    "baseMean_GroupA": 122.025266950537,
    "baseMean_GroupB": 0,
    "lfcSE": 1.01072786549189,
    "stat": -8.90419536032573,
    "pvalue": 5.37746751399077e-19,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_5179e043e1016fb7108d9053c513b1a6_5b15879574325",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_5179e043e1016fb7108d9053c513b1a6",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell11": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-14_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.04744956712667,
    "padj": 0.000014555485734934702,
    "baseMean": 46.0492007476017,
    "baseMean_GroupA": 98.5339983697705,
    "baseMean_GroupB": 11.0593356661558,
    "lfcSE": 0.615345372833932,
    "stat": -4.95242135825585,
    "pvalue": 7.329568096432039e-7,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-14",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-14_none_1",
      "folderName": "DE__GSE62776_878c58cbfb81c7eb3710c2d819915187_5b158a0764258",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_878c58cbfb81c7eb3710c2d819915187",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-14 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell33": {
    "experimentId": "GSE62776_no-dox_dox-days-0-10_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.60474697156577,
    "padj": 1.27065120265123e-12,
    "baseMean": 161.591158941589,
    "baseMean_GroupA": 361.229419901354,
    "baseMean_GroupB": 28.4989849684117,
    "lfcSE": 0.477857379348264,
    "stat": -7.54356242542948,
    "pvalue": 4.5730377473309e-14,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-10",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-10_none_1",
      "folderName": "DE__GSE62776_93860658856b17818835209e2c747b48_5b1589fbd2160",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_93860658856b17818835209e2c747b48",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-10 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell8": {
    "experimentId": "GSE68189_hFL1s_ESC-derived-cell-line_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.3630795537141,
    "padj": 1.23434843853603e-46,
    "baseMean": 18.398349204579,
    "baseMean_GroupA": 122.636973827872,
    "baseMean_GroupB": 0.26989274835419,
    "lfcSE": 0.57025804335607,
    "stat": -14.6654302401346,
    "pvalue": 1.0733464682922e-48,
    "comparisonDetails": {
      "group_A": "hFL1s",
      "group_B": "ESC-derived-cell-line",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s_ESC-derived-cell-line_none_1",
      "folderName": "DE__GSE68189_935b100102477f60aeb5ef00f01de428_5b158773d1d92",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE68189_935b100102477f60aeb5ef00f01de428",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "hFL1s vs ESC-derived-cell-line -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell14": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.79742589662322,
    "padj": 4.92514358696472e-27,
    "baseMean": 37.8035627949757,
    "baseMean_GroupA": 103.959797686183,
    "baseMean_GroupB": 0,
    "lfcSE": 0.803121751128083,
    "stat": -10.9540376465538,
    "pvalue": 6.35502398318028e-28,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_68f343d185f80e0779d411c84cbe5ccb_5b1587ccc1165",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_68f343d185f80e0779d411c84cbe5ccb",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell36": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.50850182663323,
    "padj": 0.00182735346508214,
    "baseMean": 220.66960537050699,
    "baseMean_GroupA": 327.50146341306,
    "baseMean_GroupB": 113.837747327954,
    "lfcSE": 0.411839285021326,
    "stat": -3.66284101953779,
    "pvalue": 0.000249433357022337,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_f396670c21dc63b11467a204ad8c03d7_5b0fcada1bd17",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_f396670c21dc63b11467a204ad8c03d7",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell9": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.99971837046482,
    "padj": 2.11401691643762e-18,
    "baseMean": 61.0126334752684,
    "baseMean_GroupA": 122.025266950537,
    "baseMean_GroupB": 0,
    "lfcSE": 1.01072786549189,
    "stat": -8.90419536032573,
    "pvalue": 5.37746751399077e-19,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_b63c4aabbc983a6385f95cd15e7ff347_5b1587c9c4b30",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_b63c4aabbc983a6385f95cd15e7ff347",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell13": {
    "experimentId": "GSE68189_neuronal-stem-cell_fibroblast-of-lung_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 8.72226979771276,
    "padj": 1.85720196114278e-13,
    "baseMean": 65.4651567702637,
    "baseMean_GroupA": 0,
    "baseMean_GroupB": 114.564024347961,
    "lfcSE": 1.15897930099643,
    "stat": 7.52582016798215,
    "pvalue": 5.2390406916946604e-14,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "fibroblast-of-lung",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_fibroblast-of-lung_none_1",
      "folderName": "DE__GSE68189_4592909453e832f3d1808d4377fd0d44_5b15877cf390b",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_4592909453e832f3d1808d4377fd0d44",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs fibroblast-of-lung -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell35": {
    "experimentId": "GSE62776_no-dox_dox-days-0-5_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.68139070840347,
    "padj": 0.0000138535717825748,
    "baseMean": 147.906718235849,
    "baseMean_GroupA": 278.351060937471,
    "baseMean_GroupB": 17.4623755342272,
    "lfcSE": 0.712556949735671,
    "stat": -5.16645120052385,
    "pvalue": 2.38580455497844e-7,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-5",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-5_none_1",
      "folderName": "DE__GSE62776_4b921f629cc3e0cc3f8a900a4643a411_5b1589fa08461",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_4b921f629cc3e0cc3f8a900a4643a411",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-5 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell6": {
    "experimentId": "GSE50064_HCT-116-cell_HCT-116-p53-/-_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 0.818538349534168,
    "padj": 0.000670002921481065,
    "baseMean": 229.688738822827,
    "baseMean_GroupA": 164.794293006238,
    "baseMean_GroupB": 294.583184639417,
    "lfcSE": 0.19889940367089,
    "stat": 4.11533837923701,
    "pvalue": 0.0000386611820015423,
    "comparisonDetails": {
      "group_A": "HCT-116-cell",
      "group_B": "HCT-116-p53-/-",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell_HCT-116-p53-/-_none_1",
      "folderName": "DE__GSE50064_c3826dd254288cacad78b995061d179d_5b0fcab5ccd1e",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE50064_c3826dd254288cacad78b995061d179d",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HCT-116-cell vs HCT-116-p53-/- -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell16": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -5.41306722276627,
    "padj": 0.0000723131550772105,
    "baseMean": 38.2906489216073,
    "baseMean_GroupA": 75.1540776476556,
    "baseMean_GroupB": 1.42722019555903,
    "lfcSE": 1.18714218424756,
    "stat": -4.55974633417413,
    "pvalue": 0.00000512154480990815,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_21859766c108845e22c092297a8c24ba_5b158a0b45902",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_21859766c108845e22c092297a8c24ba",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell38": {
    "experimentId": "GSE62776_no-dox_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -7.38101668462907,
    "padj": 3.78512273813257e-9,
    "baseMean": 129.930459035775,
    "baseMean_GroupA": 258.698717651249,
    "baseMean_GroupB": 1.16220042030147,
    "lfcSE": 1.14987936567042,
    "stat": -6.41894872191716,
    "pvalue": 1.3721861571590502e-10,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_6f013662efd370468517efd4fbe7221b_5b158a02472d8",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_6f013662efd370468517efd4fbe7221b",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell7": {
    "experimentId": "GSE36147_leiomyosarcoma_liposarcoma_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 2.22195883732388,
    "padj": 0.00769681865030531,
    "baseMean": 22.1086452143847,
    "baseMean_GroupA": 6.98443798536397,
    "baseMean_GroupB": 37.2328524434054,
    "lfcSE": 0.714785501483516,
    "stat": 3.10856730125649,
    "pvalue": 0.00187996823297922,
    "comparisonDetails": {
      "group_A": "leiomyosarcoma",
      "group_B": "liposarcoma",
      "datasetId": "GSE36147",
      "covariates": "none",
      "experimentId": "GSE36147_leiomyosarcoma_liposarcoma_none_1",
      "folderName": "DE__GSE36147_d3bf6479f2709ec61d813494b6146ecf_5b0fca9e3e9ca",
      "comparisonCondition": "disease",
      "experimentHash": "GSE36147_d3bf6479f2709ec61d813494b6146ecf",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "leiomyosarcoma vs liposarcoma -- GSE36147",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell15": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-10_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.49520401309735,
    "padj": 0.0191696173624941,
    "baseMean": 60.4583531697031,
    "baseMean_GroupA": 99.9387281267913,
    "baseMean_GroupB": 34.1381031983109,
    "lfcSE": 0.517606449519464,
    "stat": -2.88868891507335,
    "pvalue": 0.00386851576047693,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-10",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-10_none_1",
      "folderName": "DE__GSE62776_cccaf4a843f602569bf6351e41bdae70_5b158a057e4fb",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_cccaf4a843f602569bf6351e41bdae70",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-10 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell37": {
    "experimentId": "GSE62776_no-dox_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -6.24082347436968,
    "padj": 2.08000829431281e-11,
    "baseMean": 143.434764453158,
    "baseMean_GroupA": 352.396337171301,
    "baseMean_GroupB": 4.12704930772947,
    "lfcSE": 0.850324520672382,
    "stat": -7.33934318327648,
    "pvalue": 2.14644590567907e-13,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_95da4ee91dc912aa8d74a59f097484c4_5b158a0072817",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_95da4ee91dc912aa8d74a59f097484c4",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell18": {
    "experimentId": "GSE68189_hFL1s_H9-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -4.59691086931101,
    "padj": 0.000019606731786956703,
    "baseMean": 63.4932757375456,
    "baseMean_GroupA": 122.782036031139,
    "baseMean_GroupB": 4.20451544395197,
    "lfcSE": 1.03849072849252,
    "stat": -4.42653048620272,
    "pvalue": 0.00000957608538590158,
    "comparisonDetails": {
      "group_A": "hFL1s",
      "group_B": "H9-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s_H9-cell_none_1",
      "folderName": "DE__GSE68189_d1abc33fe06c71a470aceb143aaff764_5b15876e21b65",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE68189_d1abc33fe06c71a470aceb143aaff764",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "hFL1s vs H9-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell17": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -4.19546846367291,
    "padj": 0.000011344071619984,
    "baseMean": 41.8939522251046,
    "baseMean_GroupA": 97.3390309070007,
    "baseMean_GroupB": 4.93056643717383,
    "lfcSE": 0.837312052812044,
    "stat": -5.01063904381022,
    "pvalue": 5.424958492511289e-7,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_8f4fa4992180d5585c71a24b730dddd6_5b158a098315e",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_8f4fa4992180d5585c71a24b730dddd6",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell39": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -8.42752701751858,
    "padj": 3.2796866184148498e-16,
    "baseMean": 62.4510365981004,
    "baseMean_GroupA": 124.64905810873,
    "baseMean_GroupB": 0.253015087471031,
    "lfcSE": 1.01460931403416,
    "stat": -8.30617943374686,
    "pvalue": 9.883274504342919e-17,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:forebrain-cell_none_1",
      "folderName": "DE__GSE68189_9e625d8892a819450d2db06053fc5dc9_5b1587c34e0a4",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_9e625d8892a819450d2db06053fc5dc9",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell19": {
    "experimentId": "GSE58127_C8166-cell_A549-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 6.20399964234678,
    "padj": 0.0180559554586613,
    "baseMean": 24.187521458042,
    "baseMean_GroupA": 0,
    "baseMean_GroupB": 48.3750429160839,
    "lfcSE": 1.92639938964466,
    "stat": 3.22051578488673,
    "pvalue": 0.00127960145726441,
    "comparisonDetails": {
      "group_A": "C8166-cell",
      "group_B": "A549-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_C8166-cell_A549-cell_none_1",
      "folderName": "DE__GSE58127_b06b66f92a1810c253677fb9ebfe1084_5b0fcb1e2170d",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_b06b66f92a1810c253677fb9ebfe1084",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "C8166-cell vs A549-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  }
}
