hmData = {
  "cell121": {
    "experimentId": "GSE68189_ESC-derived-cell-line:hindbrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.18141738399271,
    "padj": 0.00104627207940374,
    "baseMean": 819.630433689586,
    "baseMean_GroupA": 442.753247572804,
    "baseMean_GroupB": 1034.98882575632,
    "lfcSE": 0.305627035773845,
    "stat": 3.86555260401415,
    "pvalue": 0.000110837955443684,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:hindbrain-cell",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:hindbrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_c77dbfb30dfca7aa8b046f78aa9c6664_5b1587f139e39",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_c77dbfb30dfca7aa8b046f78aa9c6664",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:hindbrain-cell vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell122": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.44671257130667,
    "padj": 2.27931902450465e-14,
    "baseMean": 25.9390328686374,
    "baseMean_GroupA": 9.13732114835591,
    "baseMean_GroupB": 76.3441680294818,
    "lfcSE": 0.450047659987895,
    "stat": 7.65855014422112,
    "pvalue": 1.8804381952163398e-14,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "group_B": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_05a37d88331bc9f9caa94bb186519ebe_5b1588ecd143c",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_05a37d88331bc9f9caa94bb186519ebe",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq vs heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell120": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-H37Rv:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.431782894803095,
    "padj": 0.00472108098022986,
    "baseMean": 317069.19857181,
    "baseMean_GroupA": 269753.473202635,
    "baseMean_GroupB": 364384.923940985,
    "lfcSE": 0.133933853195831,
    "stat": 3.22385180818896,
    "pvalue": 0.00126478833337245,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-H37Rv:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-H37Rv:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_e62f1e66d57429237ef8f44317eb3bf3_5b1585b2b3eee",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_e62f1e66d57429237ef8f44317eb3bf3",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-H37Rv:48hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell129": {
    "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.673908763898737,
    "padj": 0.0000372156269438146,
    "baseMean": 269277.395592627,
    "baseMean_GroupA": 207151.948372374,
    "baseMean_GroupB": 331402.842812881,
    "lfcSE": 0.144579131934339,
    "stat": 4.66117588951076,
    "pvalue": 0.00000314407882801193,
    "comparisonDetails": {
      "group_A": "salmonella-typhimurium-strain-keller:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_eedb7d353734663b8024c88e4f748a36_5b15863d6fdf1",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_eedb7d353734663b8024c88e4f748a36",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "salmonella-typhimurium-strain-keller:48hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell127": {
    "experimentId": "GSE14362_size-range-<50nt_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -6.89089358064319,
    "padj": 3.1746549491649295e-49,
    "baseMean": 3344.04445572827,
    "baseMean_GroupA": 4079.76426833431,
    "baseMean_GroupB": 33.3052990011289,
    "lfcSE": 0.460304766083466,
    "stat": -14.970285098881,
    "pvalue": 1.14827944969795e-50,
    "comparisonDetails": {
      "group_A": "size-range-<50nt",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_size-range-<50nt_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_4621298f14feb26ba80406302fce1633_5b0ebbc70258a",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_4621298f14feb26ba80406302fce1633",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "size-range-<50nt vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell128": {
    "experimentId": "GSE64142_staphloccocus-epidermidis:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.700368992794821,
    "padj": 0.00514965762912344,
    "baseMean": 272568.425999809,
    "baseMean_GroupA": 355292.60296426,
    "baseMean_GroupB": 217418.974690175,
    "lfcSE": 0.212620729802845,
    "stat": -3.2939826396243,
    "pvalue": 0.000987785990241069,
    "comparisonDetails": {
      "group_A": "staphloccocus-epidermidis:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_staphloccocus-epidermidis:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_342ef4fcff8265c9cc798057b5a03eb3_5b15864e9b395",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_342ef4fcff8265c9cc798057b5a03eb3",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "staphloccocus-epidermidis:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell125": {
    "experimentId": "GSE62776_no-dox_dox-days-0-5_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.68139070840347,
    "padj": 0.0000138535717825748,
    "baseMean": 147.906718235849,
    "baseMean_GroupA": 278.351060937471,
    "baseMean_GroupB": 17.4623755342272,
    "lfcSE": 0.712556949735671,
    "stat": -5.16645120052385,
    "pvalue": 2.38580455497844e-7,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-5",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-5_none_1",
      "folderName": "DE__GSE62776_4b921f629cc3e0cc3f8a900a4643a411_5b1589fa08461",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_4b921f629cc3e0cc3f8a900a4643a411",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-5 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell126": {
    "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.4505784525117,
    "padj": 0.000031073476619018,
    "baseMean": 2631025.96673142,
    "baseMean_GroupA": 3863436.41734436,
    "baseMean_GroupB": 1398615.51611848,
    "lfcSE": 0.307047160675953,
    "stat": -4.72428551144492,
    "pvalue": 0.0000023092579602536898,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:control:0-hN/A",
      "group_B": "HCT-116-cell:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_ed6fda7e2cc0166e9e769d77ae6bd839_5b0fcacd14612",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_ed6fda7e2cc0166e9e769d77ae6bd839",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:control:0-hN/A vs HCT-116-cell:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell123": {
    "experimentId": "GSE72183_Cells_Extracellular-Vesicles_gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.27704608988456,
    "padj": 0.0000921210969718656,
    "baseMean": 1284.37354290666,
    "baseMean_GroupA": 749.625555135065,
    "baseMean_GroupB": 1832.83301754419,
    "lfcSE": 0.306157403208182,
    "stat": 4.17120760923161,
    "pvalue": 0.0000302989654492183,
    "comparisonDetails": {
      "group_A": "Cells",
      "group_B": "Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "gender",
      "experimentId": "GSE72183_Cells_Extracellular-Vesicles_gender_1",
      "folderName": "DE__GSE72183_31da69c5bee14ddf80e6acb6b31b21ac_5b1692b1526ad",
      "comparisonCondition": "tissue-part",
      "experimentHash": "GSE72183_31da69c5bee14ddf80e6acb6b31b21ac",
      "genome": "hg38",
      "allConditions": [
        "tissue-part"
      ]
    },
    "xkey": "Cells vs Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell124": {
    "experimentId": "GSE72183_female-organism:second-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.88391301386087,
    "padj": 0.0467219781473761,
    "baseMean": 1929.08791529783,
    "baseMean_GroupA": 786.143600555544,
    "baseMean_GroupB": 3072.03223004011,
    "lfcSE": 0.836870732342915,
    "stat": 2.2511398009901002,
    "pvalue": 0.0243766842508049,
    "comparisonDetails": {
      "group_A": "female-organism:second-void-urine:Cells",
      "group_B": "male-organism:second-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_female-organism:second-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_79368362ad86ea9125f2025234544b47_5b15898005efb",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_79368362ad86ea9125f2025234544b47",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "female-organism:second-void-urine:Cells vs male-organism:second-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell132": {
    "experimentId": "GSE66334_exercise_normal-physical-activity_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.892149231966326,
    "padj": 0.0177125519125302,
    "baseMean": 31679.1871585303,
    "baseMean_GroupA": 21792.9209132851,
    "baseMean_GroupB": 41565.4534037756,
    "lfcSE": 0.297230393710764,
    "stat": 3.0015410632416,
    "pvalue": 0.00268616807795497,
    "comparisonDetails": {
      "group_A": "exercise",
      "group_B": "normal-physical-activity",
      "datasetId": "GSE66334",
      "covariates": "none",
      "experimentId": "GSE66334_exercise_normal-physical-activity_none_1",
      "folderName": "DE__GSE66334_e80a90f67507e27b4d1ebc0aa866bc4e_5b0eb565739d9",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE66334_e80a90f67507e27b4d1ebc0aa866bc4e",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "exercise vs normal-physical-activity -- GSE66334",
    "ykey": "hsa-let-7a-5p"
  },
  "cell133": {
    "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 4.29205325725998,
    "padj": 0.000797779019361772,
    "baseMean": 1204.1825437097,
    "baseMean_GroupA": 228.818041848196,
    "baseMean_GroupB": 2911.07042196732,
    "lfcSE": 1.24641210151505,
    "stat": 3.4435266249765,
    "pvalue": 0.000574180055457607,
    "comparisonDetails": {
      "group_A": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_dfc808a98306b93e64fb05b812ec267d_5b1588bb0e194",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_dfc808a98306b93e64fb05b812ec267d",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell130": {
    "experimentId": "GSE45722_sample-c:bioo-scientific-kit_sample-b:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.48294362135145,
    "padj": 1.0706686098277e-19,
    "baseMean": 2755.55653701044,
    "baseMean_GroupA": 1448.10028064017,
    "baseMean_GroupB": 4063.01279338072,
    "lfcSE": 0.160233484789632,
    "stat": 9.2548921550223,
    "pvalue": 2.14446782962565e-20,
    "comparisonDetails": {
      "group_A": "sample-c:bioo-scientific-kit",
      "group_B": "sample-b:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:bioo-scientific-kit_sample-b:NEB-kit_none_1",
      "folderName": "DE__GSE45722_264bad3f942c886bf59f0abda3acf163_5b0eb5471b977",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_264bad3f942c886bf59f0abda3acf163",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:bioo-scientific-kit vs sample-b:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell131": {
    "experimentId": "GSE68189_fibroblast-of-lung_embryonic-stem-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.00250941706356,
    "padj": 8.93249038738169e-12,
    "baseMean": 99435.8197272562,
    "baseMean_GroupA": 193269.362025992,
    "baseMean_GroupB": 5602.2774285206,
    "lfcSE": 0.714142298918686,
    "stat": -7.00491964225908,
    "pvalue": 2.47127632046572e-12,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "embryonic-stem-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_embryonic-stem-cell_none_1",
      "folderName": "DE__GSE68189_bc9f33ce5db1292d8acec8e5ee11b5c5_5b15878c91ada",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_bc9f33ce5db1292d8acec8e5ee11b5c5",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs embryonic-stem-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell138": {
    "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.86573275968863,
    "padj": 0.0000353228904151466,
    "baseMean": 12534.7741081153,
    "baseMean_GroupA": 19891.6996519517,
    "baseMean_GroupB": 5177.84856427895,
    "lfcSE": 0.381619609617201,
    "stat": -4.88898555700564,
    "pvalue": 0.00000101356930889947,
    "comparisonDetails": {
      "group_A": "DKS-8:wild-type-KRAS-allele:exosome",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
      "folderName": "DE__GSE67004_d6e6953916dd999b78922b6ba844da1d_5b1586986982c",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_d6e6953916dd999b78922b6ba844da1d",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKS-8:wild-type-KRAS-allele:exosome vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell139": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.39347651435268,
    "padj": 0.000012150592009639301,
    "baseMean": 2581665.64302442,
    "baseMean_GroupA": 3747409.45768963,
    "baseMean_GroupB": 1415921.82835921,
    "lfcSE": 0.284640345794333,
    "stat": -4.89556921547424,
    "pvalue": 9.80215825987707e-7,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_ee07f7537e648df24e9673ddaae3d8b4_5b0fcad8a4043",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_ee07f7537e648df24e9673ddaae3d8b4",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell136": {
    "experimentId": "GSE37686_embryonic-stem-cell_partially-differentiated-ESC_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.5005687696062902,
    "padj": 0.0274690585733609,
    "baseMean": 2169.30767170214,
    "baseMean_GroupA": 2942.47054534482,
    "baseMean_GroupB": 1009.56336123813,
    "lfcSE": 0.555664862998033,
    "stat": -2.70049245422884,
    "pvalue": 0.00692369076005825,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "partially-differentiated-ESC",
      "datasetId": "GSE37686",
      "covariates": "none",
      "experimentId": "GSE37686_embryonic-stem-cell_partially-differentiated-ESC_none_1",
      "folderName": "DE__GSE37686_2f775f4646b24bf2286666404e114156_5b0fcaa22fc30",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE37686_2f775f4646b24bf2286666404e114156",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs partially-differentiated-ESC -- GSE37686",
    "ykey": "hsa-let-7a-5p"
  },
  "cell137": {
    "experimentId": "GSE72183_female-organism:second-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.3695191380164298,
    "padj": 0.00603435752535616,
    "baseMean": 4308.45630891277,
    "baseMean_GroupA": 1332.4196210579,
    "baseMean_GroupB": 7284.49299676764,
    "lfcSE": 0.763943768401756,
    "stat": 3.10169312981463,
    "pvalue": 0.00192417311484328,
    "comparisonDetails": {
      "group_A": "female-organism:second-void-urine:Cells",
      "group_B": "female-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_female-organism:second-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_c63f6b40b627ed774d213f2c06f06cf6_5b15896c6dd96",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_c63f6b40b627ed774d213f2c06f06cf6",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "female-organism:second-void-urine:Cells vs female-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell134": {
    "experimentId": "GSE68189_hFL1s_ESC-derived-cell-line_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -6.79997416306838,
    "padj": 7.66002418642655e-18,
    "baseMean": 29994.3909980126,
    "baseMean_GroupA": 193542.036165834,
    "baseMean_GroupB": 5601.65925142489,
    "lfcSE": 0.761427902707691,
    "stat": -8.93055552454434,
    "pvalue": 4.23874856165896e-19,
    "comparisonDetails": {
      "group_A": "hFL1s",
      "group_B": "ESC-derived-cell-line",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s_ESC-derived-cell-line_none_1",
      "folderName": "DE__GSE68189_935b100102477f60aeb5ef00f01de428_5b158773d1d92",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE68189_935b100102477f60aeb5ef00f01de428",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "hFL1s vs ESC-derived-cell-line -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell135": {
    "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_size-range-50-200nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.15560035265298,
    "padj": 0.00451530056259994,
    "baseMean": 1009.89949399934,
    "baseMean_GroupA": 416.570758105509,
    "baseMean_GroupB": 1899.89259784008,
    "lfcSE": 0.719758798175008,
    "stat": 2.99489267532212,
    "pvalue": 0.00274541423096354,
    "comparisonDetails": {
      "group_A": "CIP-treatment-prior-to-TAP",
      "group_B": "size-range-50-200nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_size-range-50-200nt_none_1",
      "folderName": "DE__GSE14362_77af1f2196e41159578354446b1ff239_5b0ebb9e3a2f5",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_77af1f2196e41159578354446b1ff239",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "CIP-treatment-prior-to-TAP vs size-range-50-200nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell143": {
    "experimentId": "GSE14362_cap-captured-eluate-with-TAP_cap-captured-eluate-with-heat_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.04929790380532,
    "padj": 0.0243580443119027,
    "baseMean": 19.1468785862535,
    "baseMean_GroupA": 7.23077983652506,
    "baseMean_GroupB": 31.0629773359819,
    "lfcSE": 0.830117080273296,
    "stat": 2.46868538487443,
    "pvalue": 0.013561038854499,
    "comparisonDetails": {
      "group_A": "cap-captured-eluate-with-TAP",
      "group_B": "cap-captured-eluate-with-heat",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_cap-captured-eluate-with-TAP_cap-captured-eluate-with-heat_none_1",
      "folderName": "DE__GSE14362_bda0a33dca811c6430adab2d2110f4e1_5b0ebbd70b8a7",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_bda0a33dca811c6430adab2d2110f4e1",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "cap-captured-eluate-with-TAP vs cap-captured-eluate-with-heat -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell144": {
    "experimentId": "GSE68189_H9-cell_ESC-derived-cell-line_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.7410047999361797,
    "padj": 0.0000028564038824112597,
    "baseMean": 1439.15964568947,
    "baseMean_GroupA": 5359.65889064901,
    "baseMean_GroupB": 5513.1489446188,
    "lfcSE": 0.547603137043559,
    "stat": -5.00545854199178,
    "pvalue": 5.57292068685293e-7,
    "comparisonDetails": {
      "group_A": "H9-cell",
      "group_B": "ESC-derived-cell-line",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell_ESC-derived-cell-line_none_1",
      "folderName": "DE__GSE68189_794b10433de355c1a5312223dbbcff51_5b1587798f141",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE68189_794b10433de355c1a5312223dbbcff51",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "H9-cell vs ESC-derived-cell-line -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell141": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.507849870550634,
    "padj": 0.0290744291627904,
    "baseMean": 316642.775520767,
    "baseMean_GroupA": 270570.525555211,
    "baseMean_GroupB": 385751.150469102,
    "lfcSE": 0.192903690558577,
    "stat": 2.63266021028468,
    "pvalue": 0.00847190564943149,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A",
      "group_B": "staphloccocus-epidermidis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
      "folderName": "DE__GSE64142_5c41ce443c6e16511d90a71c5c8d63eb_5b15860289512",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_5c41ce443c6e16511d90a71c5c8d63eb",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A vs staphloccocus-epidermidis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell142": {
    "experimentId": "GSE64142_non-infected:48hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.426769868157831,
    "padj": 0.00158966666492969,
    "baseMean": 242254.701018819,
    "baseMean_GroupA": 277991.511143852,
    "baseMean_GroupB": 206517.890893785,
    "lfcSE": 0.118825807307103,
    "stat": -3.59155875166792,
    "pvalue": 0.000328706052348624,
    "comparisonDetails": {
      "group_A": "non-infected:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:48hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_d5d059ddeccf21a44beba269dac5a927_5b1584a0c09ef",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_d5d059ddeccf21a44beba269dac5a927",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:48hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell140": {
    "experimentId": "GSE68189_neuronal-stem-cell_fibroblast-of-lung_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 8.40255490236978,
    "padj": 0,
    "baseMean": 103383.95705232,
    "baseMean_GroupA": 532.793024165849,
    "baseMean_GroupB": 180522.330073435,
    "lfcSE": 0.138827657596761,
    "stat": 60.5250787042441,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "fibroblast-of-lung",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_fibroblast-of-lung_none_1",
      "folderName": "DE__GSE68189_4592909453e832f3d1808d4377fd0d44_5b15877cf390b",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_4592909453e832f3d1808d4377fd0d44",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs fibroblast-of-lung -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell149": {
    "experimentId": "GSE14362_size-range-50-200nt_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.99807844602485,
    "padj": 2.96333825446912e-13,
    "baseMean": 471.696825864131,
    "baseMean_GroupA": 929.887207023877,
    "baseMean_GroupB": 13.5064447043841,
    "lfcSE": 0.781410018382163,
    "stat": -7.67596819201693,
    "pvalue": 1.6417386451352502e-14,
    "comparisonDetails": {
      "group_A": "size-range-50-200nt",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_size-range-50-200nt_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_ef19dc8abd72d652139543fb84174863_5b0ebbd3e270b",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_ef19dc8abd72d652139543fb84174863",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "size-range-50-200nt vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell147": {
    "experimentId": "GSE62776_reprogramming-intermediates-of-hiF-T-cell_hIPSCs-from-reprogrammed-hiF-T-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.6288709351915305,
    "padj": 0.0000551203943172612,
    "baseMean": 137859.285698283,
    "baseMean_GroupA": 151857.382525161,
    "baseMean_GroupB": 4877.36584294617,
    "lfcSE": 1.00070093327184,
    "stat": -4.62562867814786,
    "pvalue": 0.00000373463855488269,
    "comparisonDetails": {
      "group_A": "reprogramming-intermediates-of-hiF-T-cell",
      "group_B": "hIPSCs-from-reprogrammed-hiF-T-cell",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_reprogramming-intermediates-of-hiF-T-cell_hIPSCs-from-reprogrammed-hiF-T-cell_none_1",
      "folderName": "DE__GSE62776_95d1a8bfe63268e0dba41683f4c6037b_5b1589f6b9108",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE62776_95d1a8bfe63268e0dba41683f4c6037b",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "reprogramming-intermediates-of-hiF-T-cell vs hIPSCs-from-reprogrammed-hiF-T-cell -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell148": {
    "experimentId": "GSE68189_embryonic-stem-cell_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.08134393594588,
    "padj": 0.000680309676822971,
    "baseMean": 2252.96373749635,
    "baseMean_GroupA": 4406.13718738752,
    "baseMean_GroupB": 1022.5789089871,
    "lfcSE": 0.577449637124656,
    "stat": -3.60437309530524,
    "pvalue": 0.000312907293215733,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_embryonic-stem-cell_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_038b5874cf81189a76a9bcd867c50488_5b1587a4e63d6",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_038b5874cf81189a76a9bcd867c50488",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell145": {
    "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.3811912549442,
    "padj": 2.5863732616789696e-11,
    "baseMean": 64038.1506132955,
    "baseMean_GroupA": 107561.901587022,
    "baseMean_GroupB": 20514.3996395689,
    "lfcSE": 0.345980415406458,
    "stat": -6.88244521628998,
    "pvalue": 5.88337943231765e-12,
    "comparisonDetails": {
      "group_A": "DKS-8:wild-type-KRAS-allele:cell",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
      "folderName": "DE__GSE67004_224d73e650d572d39afd8c861bf01de1_5b158694b1aac",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_224d73e650d572d39afd8c861bf01de1",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKS-8:wild-type-KRAS-allele:cell vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell146": {
    "experimentId": "GSE69825_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.01656690014082,
    "padj": 5.010009193639149e-22,
    "baseMean": 6774.90269479393,
    "baseMean_GroupA": 9069.93553131575,
    "baseMean_GroupB": 4479.86985827212,
    "lfcSE": 0.1036450360009,
    "stat": -9.80815810736939,
    "pvalue": 1.03846709551698e-22,
    "comparisonDetails": {
      "group_A": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_04cbac9470674be6182191526e5070df_5b1588f6efa09",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_04cbac9470674be6182191526e5070df",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell154": {
    "experimentId": "GSE64142_Mycobacterium-bovis-BCG:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.432571291183066,
    "padj": 0.00487600636344834,
    "baseMean": 282139.26635012,
    "baseMean_GroupA": 324337.562366044,
    "baseMean_GroupB": 239940.970334195,
    "lfcSE": 0.126277169265375,
    "stat": -3.4255700670166602,
    "pvalue": 0.000613510669085194,
    "comparisonDetails": {
      "group_A": "Mycobacterium-bovis-BCG:4hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-bovis-BCG:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_02b05f60b8402dce4c3882648a841ef0_5b1584c3c4f04",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_02b05f60b8402dce4c3882648a841ef0",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-bovis-BCG:4hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell155": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.502747478680063,
    "padj": 2.12634678296245e-8,
    "baseMean": 10041.9410489206,
    "baseMean_GroupA": 8282.01040810844,
    "baseMean_GroupB": 11801.8716897327,
    "lfcSE": 0.0826290320406445,
    "stat": 6.08439269181764,
    "pvalue": 1.16933687051481e-9,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-a:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:NEB-kit_none_1",
      "folderName": "DE__GSE45722_ff2625822e4a7c2d7d09b94430262798_5b0eb5268a657",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_ff2625822e4a7c2d7d09b94430262798",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-a:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell152": {
    "experimentId": "GSE68189_neuronal-stem-cell_mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.90001956699778,
    "padj": 5.489764284087148e-254,
    "baseMean": 16423.9066344685,
    "baseMean_GroupA": 473.339930328629,
    "baseMean_GroupB": 28386.8316625734,
    "lfcSE": 0.172633232686331,
    "stat": 34.1766152159006,
    "pvalue": 5.382121847144257e-256,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_d99da64c6af9403db6546848e674c7e0_5b1587836bcaa",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_d99da64c6af9403db6546848e674c7e0",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell153": {
    "experimentId": "GSE72183_male-organism:first-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.70954006070673,
    "padj": 0.00871825813075426,
    "baseMean": 2678.16243585452,
    "baseMean_GroupA": 653.863115677412,
    "baseMean_GroupB": 4702.46175603163,
    "lfcSE": 0.879246072087257,
    "stat": 3.08166296867782,
    "pvalue": 0.00205847761420587,
    "comparisonDetails": {
      "group_A": "male-organism:first-void-urine:Cells",
      "group_B": "female-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:first-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_8a219c6e0e5f6f394b4a9f3f37c293b8_5b158949a7a2f",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_8a219c6e0e5f6f394b4a9f3f37c293b8",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:first-void-urine:Cells vs female-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell150": {
    "experimentId": "GSE67004_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.7194834099878102,
    "padj": 3.33743078988365e-12,
    "baseMean": 75883.9729350888,
    "baseMean_GroupA": 131920.573025135,
    "baseMean_GroupB": 19847.3728450431,
    "lfcSE": 0.377918336430813,
    "stat": -7.19595517823116,
    "pvalue": 6.202500343632969e-13,
    "comparisonDetails": {
      "group_A": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
      "folderName": "DE__GSE67004_a46d9ee5b4f014e4adb23faf9baebca1_5b15869b6b39d",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_a46d9ee5b4f014e4adb23faf9baebca1",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell151": {
    "experimentId": "GSE58127_A549-cell_immortal-human-B-cell-line-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -6.96972687159003,
    "padj": 0.00469823823894185,
    "baseMean": 30.3573897784674,
    "baseMean_GroupA": 60.7147795569348,
    "baseMean_GroupB": 0,
    "lfcSE": 1.96558160125058,
    "stat": -3.54588528258283,
    "pvalue": 0.000391296510488027,
    "comparisonDetails": {
      "group_A": "A549-cell",
      "group_B": "immortal-human-B-cell-line-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_A549-cell_immortal-human-B-cell-line-cell_none_1",
      "folderName": "DE__GSE58127_d99a6b540971b73d2612ab8796522945_5b0fcb247f19a",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_d99a6b540971b73d2612ab8796522945",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "A549-cell vs immortal-human-B-cell-line-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell158": {
    "experimentId": "GSE68189_neuronal-stem-cell_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.35523890591658,
    "padj": 0.000858241213213221,
    "baseMean": 806.629794570841,
    "baseMean_GroupA": 379.053252926614,
    "baseMean_GroupB": 989.876883846938,
    "lfcSE": 0.358744024227009,
    "stat": 3.77773235062726,
    "pvalue": 0.000158262829225557,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_bf7064c71e927f6f87345e79d6ca8884_5b15878a603ea",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_bf7064c71e927f6f87345e79d6ca8884",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell159": {
    "experimentId": "GSE50064_HCT-116-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 1.1663337049821,
    "padj": 0.0391687872179181,
    "baseMean": 236.90232969708,
    "baseMean_GroupA": 139.582995425528,
    "baseMean_GroupB": 334.221663968632,
    "lfcSE": 0.382153363944892,
    "stat": 3.05200428681896,
    "pvalue": 0.00227318854389703,
    "comparisonDetails": {
      "group_A": "HCT-116-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_db7e14084df526f7bad36bd63b85a074_5b0fcafc2d1d5",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_db7e14084df526f7bad36bd63b85a074",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "HCT-116-cell:irradiation:24-hN/A vs HCT-116-p53-/-:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell156": {
    "experimentId": "GSE62776_tra-1-60-_tra-1-60+_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.1461787295465298,
    "padj": 0.000030277411454217202,
    "baseMean": 24576.8570386426,
    "baseMean_GroupA": 40690.5863763277,
    "baseMean_GroupB": 8463.12770095743,
    "lfcSE": 0.421093778047585,
    "stat": -5.09667642086131,
    "pvalue": 3.45668423452402e-7,
    "comparisonDetails": {
      "group_A": "tra-1-60-",
      "group_B": "tra-1-60+",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_tra-1-60-_tra-1-60+_none_1",
      "folderName": "DE__GSE62776_06dcf345d27d6c25f2af8c07fe326098_5b158a2ab3a1e",
      "comparisonCondition": "marker",
      "experimentHash": "GSE62776_06dcf345d27d6c25f2af8c07fe326098",
      "genome": "hg38",
      "allConditions": [
        "marker"
      ]
    },
    "xkey": "tra-1-60- vs tra-1-60+ -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell157": {
    "experimentId": "GSE14362_size-range-<50nt_rRNA-depleted_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.4530295469736,
    "padj": 4.09211819385401e-9,
    "baseMean": 3294.52908772483,
    "baseMean_GroupA": 4332.25341290715,
    "baseMean_GroupB": 181.356112177863,
    "lfcSE": 0.715015521718298,
    "stat": -6.22787815329133,
    "pvalue": 4.72794532357635e-10,
    "comparisonDetails": {
      "group_A": "size-range-<50nt",
      "group_B": "rRNA-depleted",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_size-range-<50nt_rRNA-depleted_none_1",
      "folderName": "DE__GSE14362_c160a5310946f2ea6ceb577f073ca7d3_5b0fca5822318",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_c160a5310946f2ea6ceb577f073ca7d3",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "size-range-<50nt vs rRNA-depleted -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell206": {
    "experimentId": "GSE62776_dox-days-0-5_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.60756458260723,
    "padj": 0.0123174749201644,
    "baseMean": 95293.8570130232,
    "baseMean_GroupA": 195372.286804899,
    "baseMean_GroupB": 28574.9038184393,
    "lfcSE": 0.787816675752741,
    "stat": -3.30986213273,
    "pvalue": 0.000933419347590467,
    "comparisonDetails": {
      "group_A": "dox-days-0-5",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-5_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_88bbb99c487a4733f9f54cbb45b0a242_5b158a11235ff",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_88bbb99c487a4733f9f54cbb45b0a242",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-5 vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell207": {
    "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.51739964614711,
    "padj": 5.00724310813451e-43,
    "baseMean": 9637.88571729593,
    "baseMean_GroupA": 24627.2769266476,
    "baseMean_GroupB": 1072.51931195214,
    "lfcSE": 0.32307008005053,
    "stat": -13.9827236413863,
    "pvalue": 1.9873441153941997e-44,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:mesenchymal-cell",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_e02cc1dee4139ab6cfd532a698948eef_5b1587e958da7",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_e02cc1dee4139ab6cfd532a698948eef",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:mesenchymal-cell vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell204": {
    "experimentId": "GSE68085_healthy-tissue_diseased-tissue_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.96485863334166,
    "padj": 1.2466802719729699e-25,
    "baseMean": 8458.89809646347,
    "baseMean_GroupA": 25855.6936408114,
    "baseMean_GroupB": 6600.98789269817,
    "lfcSE": 0.184293157782029,
    "stat": -10.6615929586793,
    "pvalue": 1.53937911843619e-26,
    "comparisonDetails": {
      "group_A": "healthy-tissue",
      "group_B": "diseased-tissue",
      "datasetId": "GSE68085",
      "covariates": "none",
      "experimentId": "GSE68085_healthy-tissue_diseased-tissue_none_1",
      "folderName": "DE__GSE68085_170e8db42c3b9b54c4321719f31d56d0_5b15871f6800c",
      "comparisonCondition": "tissue-phenotype",
      "experimentHash": "GSE68085_170e8db42c3b9b54c4321719f31d56d0",
      "genome": "hg38",
      "allConditions": [
        "tissue-phenotype"
      ]
    },
    "xkey": "healthy-tissue vs diseased-tissue -- GSE68085",
    "ykey": "hsa-let-7a-5p"
  },
  "cell205": {
    "experimentId": "GSE72183_female-organism:first-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.20276909992077,
    "padj": 0.0259962906642777,
    "baseMean": 3974.13558426735,
    "baseMean_GroupA": 1342.46221020945,
    "baseMean_GroupB": 6605.80895832526,
    "lfcSE": 0.868352317106773,
    "stat": 2.53672277545142,
    "pvalue": 0.0111895523748745,
    "comparisonDetails": {
      "group_A": "female-organism:first-void-urine:Cells",
      "group_B": "female-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_female-organism:first-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_7bb1488a84660ba1498402a306ec7279_5b1589286ad27",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_7bb1488a84660ba1498402a306ec7279",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "female-organism:first-void-urine:Cells vs female-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell202": {
    "experimentId": "GSE14362_alternative-primers_size-range-<50nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 4.3906258283979,
    "padj": 4.38847586714179e-53,
    "baseMean": 4557.18033251877,
    "baseMean_GroupA": 260.055501898614,
    "baseMean_GroupB": 5512.09696154547,
    "lfcSE": 0.282150941090422,
    "stat": 15.56126593599,
    "pvalue": 1.33433387852284e-54,
    "comparisonDetails": {
      "group_A": "alternative-primers",
      "group_B": "size-range-<50nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_alternative-primers_size-range-<50nt_none_1",
      "folderName": "DE__GSE14362_a9d42a6629b6777d164837e4693ad7dd_5b0ebb86b63a6",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_a9d42a6629b6777d164837e4693ad7dd",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "alternative-primers vs size-range-<50nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell203": {
    "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.01712273231954,
    "padj": 1.02662087314725e-8,
    "baseMean": 165.326502892703,
    "baseMean_GroupA": 265.15591372941,
    "baseMean_GroupB": 15.5823866376433,
    "lfcSE": 0.647355393307857,
    "stat": -6.20543641691597,
    "pvalue": 5.454532153344e-10,
    "comparisonDetails": {
      "group_A": "CIP-treatment-prior-to-TAP",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_61d4df07d2bfa19d32e9f0e66559cf5d_5b0ebba10c68c",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_61d4df07d2bfa19d32e9f0e66559cf5d",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "CIP-treatment-prior-to-TAP vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell200": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.501633889097258,
    "padj": 0.00970386416074051,
    "baseMean": 260533.295046303,
    "baseMean_GroupA": 305746.170703443,
    "baseMean_GroupB": 215320.419389162,
    "lfcSE": 0.155528791686577,
    "stat": -3.2253442186328902,
    "pvalue": 0.00125821289541805,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_2fceee7f601369823059b1ae6f3b0b0e_5b1585c94d0b0",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_2fceee7f601369823059b1ae6f3b0b0e",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell201": {
    "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.69254859898964,
    "padj": 0.00728215252918802,
    "baseMean": 169018.339347453,
    "baseMean_GroupA": 78695.9312349102,
    "baseMean_GroupB": 259340.747459996,
    "lfcSE": 0.570121601799333,
    "stat": 2.96875016425947,
    "pvalue": 0.00299013599243577,
    "comparisonDetails": {
      "group_A": "DKO-1:mutant-KRAS-allele:exosome",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_none_1",
      "folderName": "DE__GSE67004_3c86501c3cd1d5103df25d8643b03408_5b15868ccfcba",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_3c86501c3cd1d5103df25d8643b03408",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKO-1:mutant-KRAS-allele:exosome vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell208": {
    "experimentId": "GSE68189_forebrain-cell_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.51727406168862,
    "padj": 7.63338822149939e-16,
    "baseMean": 737.037939330322,
    "baseMean_GroupA": 178.081792121721,
    "baseMean_GroupB": 1056.44145202095,
    "lfcSE": 0.296510562462137,
    "stat": 8.48966067443236,
    "pvalue": 2.07241309180979e-17,
    "comparisonDetails": {
      "group_A": "forebrain-cell",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_forebrain-cell_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_9147ddab1f6d56bf1eed1f6efbafc2e2_5b1587af04129",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_9147ddab1f6d56bf1eed1f6efbafc2e2",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "forebrain-cell vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell209": {
    "experimentId": "GSE71008_healthy_prostate-cancer_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.286339112620777,
    "padj": 0.0489534148831182,
    "baseMean": 4155.95520359266,
    "baseMean_GroupA": 4504.66396799638,
    "baseMean_GroupB": 3671.63747525416,
    "lfcSE": 0.112782198621591,
    "stat": -2.53886797846092,
    "pvalue": 0.0111211781124905,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "prostate-cancer",
      "datasetId": "GSE71008",
      "covariates": "age-gender",
      "experimentId": "GSE71008_healthy_prostate-cancer_age-gender_1",
      "folderName": "DE__GSE71008_d5eaf49ec1fb42c87a8abfe03827b624_5b1691f5f2ad8",
      "comparisonCondition": "disease",
      "experimentHash": "GSE71008_d5eaf49ec1fb42c87a8abfe03827b624",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs prostate-cancer -- GSE71008",
    "ykey": "hsa-let-7a-5p"
  },
  "cell210": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.19377572730824,
    "padj": 6.2962011915698e-14,
    "baseMean": 16.4915525373997,
    "baseMean_GroupA": 10.7030593249194,
    "baseMean_GroupB": 33.8570321748405,
    "lfcSE": 0.292185501082181,
    "stat": 7.50816080600525,
    "pvalue": 5.99638208720934e-14,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_4b0380a509679969d19181b40632cbb6_5b1588f095086",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_4b0380a509679969d19181b40632cbb6",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell0": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.985394435451848,
    "padj": 0.0029348945563363,
    "baseMean": 2625655.15258902,
    "baseMean_GroupA": 3494158.16016643,
    "baseMean_GroupB": 1757152.1450116,
    "lfcSE": 0.278457255611205,
    "stat": -3.53876372619179,
    "pvalue": 0.000402005496229853,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-p53-/-:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_caf9e0ed2c86ecbe57089cf8b58b49d2_5b0fcadbc6e4b",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_caf9e0ed2c86ecbe57089cf8b58b49d2",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-p53-/-:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell1": {
    "experimentId": "GSE14362_alternative-primers_no-TAP-treatment_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.5993084194972305,
    "padj": 1.23656787129078e-9,
    "baseMean": 12859.888202046,
    "baseMean_GroupA": 394.994642564465,
    "baseMean_GroupB": 21169.8172417003,
    "lfcSE": 0.842971905010239,
    "stat": 6.64234286601665,
    "pvalue": 3.08735736590341e-11,
    "comparisonDetails": {
      "group_A": "alternative-primers",
      "group_B": "no-TAP-treatment",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_alternative-primers_no-TAP-treatment_none_1",
      "folderName": "DE__GSE14362_7b1467ee1a3a2b1b565c4868dc7ecb12_5b0ebb7c32a30",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_7b1467ee1a3a2b1b565c4868dc7ecb12",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "alternative-primers vs no-TAP-treatment -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell4": {
    "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.37914010374634,
    "padj": 8.9903657224281e-11,
    "baseMean": 52643.4425292471,
    "baseMean_GroupA": 88411.3479953406,
    "baseMean_GroupB": 16875.5370631535,
    "lfcSE": 0.355050669442286,
    "stat": -6.70084669177921,
    "pvalue": 2.0721535133431498e-11,
    "comparisonDetails": {
      "group_A": "DKO-1:mutant-KRAS-allele:cell",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:cell_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome_none_1",
      "folderName": "DE__GSE67004_d368b353866096606b40792e319e2f03_5b15868743387",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_d368b353866096606b40792e319e2f03",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKO-1:mutant-KRAS-allele:cell vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:exosome -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell98": {
    "experimentId": "GSE14362_no-TAP-treatment_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -9.07688642473399,
    "padj": 1.6557505738693098e-77,
    "baseMean": 7936.63266575296,
    "baseMean_GroupA": 13212.0614501909,
    "baseMean_GroupB": 23.4894890959992,
    "lfcSE": 0.479041662268352,
    "stat": -18.9480104543585,
    "pvalue": 4.585966544466719e-80,
    "comparisonDetails": {
      "group_A": "no-TAP-treatment",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_no-TAP-treatment_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_f5be2b4d2b44ee62226875a98ba50818_5b0ebbb6191e4",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_f5be2b4d2b44ee62226875a98ba50818",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "no-TAP-treatment vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell217": {
    "experimentId": "GSE45722_sample-b:NEB-kit_sample-b:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.984401220468947,
    "padj": 0.00143412769887921,
    "baseMean": 3540.92181390739,
    "baseMean_GroupA": 4712.17575473867,
    "baseMean_GroupB": 2369.6678730761,
    "lfcSE": 0.281752866407552,
    "stat": -3.49384633782225,
    "pvalue": 0.000476114975299954,
    "comparisonDetails": {
      "group_A": "sample-b:NEB-kit",
      "group_B": "sample-b:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-b:NEB-kit_sample-b:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_558a7edc9a8fe4dcfdfaf21d1e441880_5b0eb54a1cedd",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_558a7edc9a8fe4dcfdfaf21d1e441880",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-b:NEB-kit vs sample-b:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell5": {
    "experimentId": "GSE14362_no-TAP-treatment_rRNA-depleted_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.7051466804505395,
    "padj": 0.0000230696220385607,
    "baseMean": 6151.37279028048,
    "baseMean_GroupA": 12123.1571872029,
    "baseMean_GroupB": 179.588393358034,
    "lfcSE": 1.20397349549934,
    "stat": -4.73859823474299,
    "pvalue": 0.0000021520169812090198,
    "comparisonDetails": {
      "group_A": "no-TAP-treatment",
      "group_B": "rRNA-depleted",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_no-TAP-treatment_rRNA-depleted_none_1",
      "folderName": "DE__GSE14362_40ea829676b09a3133f6a50f19939024_5b0ebbb0bdc73",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_40ea829676b09a3133f6a50f19939024",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "no-TAP-treatment vs rRNA-depleted -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell97": {
    "experimentId": "GSE68189_forebrain-cell_hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.37859494741144,
    "padj": 1.0242792015475001e-17,
    "baseMean": 393.016379904913,
    "baseMean_GroupA": 217.510948584693,
    "baseMean_GroupB": 568.521811225134,
    "lfcSE": 0.15475511965364,
    "stat": 8.90823483253346,
    "pvalue": 5.18514782526118e-19,
    "comparisonDetails": {
      "group_A": "forebrain-cell",
      "group_B": "hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_forebrain-cell_hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_7dcbc333c6d66619e9f347852ed62696_5b1587abe973e",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_7dcbc333c6d66619e9f347852ed62696",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "forebrain-cell vs hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell218": {
    "experimentId": "GSE68189_fibroblast-of-lung_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -7.25872993843716,
    "padj": 4.870552111575659e-126,
    "baseMean": 60259.887908102,
    "baseMean_GroupA": 163846.653399906,
    "baseMean_GroupB": 1067.45048421406,
    "lfcSE": 0.301838783672367,
    "stat": -24.0483673109291,
    "pvalue": 8.682648076662887e-128,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_fa2e391d3a6802daf2f7453bd4234594_5b1587981a308",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_fa2e391d3a6802daf2f7453bd4234594",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell2": {
    "experimentId": "GSE34137_healthy_squamous-cell-carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.24706369147928,
    "padj": 0.00113464786271062,
    "baseMean": 2846.94238641933,
    "baseMean_GroupA": 3566.40902963245,
    "baseMean_GroupB": 688.542456779976,
    "lfcSE": 0.591964731775487,
    "stat": -3.79594183717606,
    "pvalue": 0.000147083982203228,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "squamous-cell-carcinoma",
      "datasetId": "GSE34137",
      "covariates": "none",
      "experimentId": "GSE34137_healthy_squamous-cell-carcinoma_none_1",
      "folderName": "DE__GSE34137_bbb6b961d9a383c0fcf2233926ed4b47_5b0eb961d4a8a",
      "comparisonCondition": "disease",
      "experimentHash": "GSE34137_bbb6b961d9a383c0fcf2233926ed4b47",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs squamous-cell-carcinoma -- GSE34137",
    "ykey": "hsa-let-7a-5p"
  },
  "cell215": {
    "experimentId": "GSE62776_no-dox_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.927807164146,
    "padj": 0.000145882104994098,
    "baseMean": 207444.838358082,
    "baseMean_GroupA": 391842.023268759,
    "baseMean_GroupB": 23047.6534474051,
    "lfcSE": 0.903127973049593,
    "stat": -4.34911472278173,
    "pvalue": 0.000013668819653657801,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_6f013662efd370468517efd4fbe7221b_5b158a02472d8",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_6f013662efd370468517efd4fbe7221b",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell3": {
    "experimentId": "GSE45722_sample-a:illumina-kit_sample-b:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.10974295372891,
    "padj": 3.58978863330718e-19,
    "baseMean": 2888.24953046808,
    "baseMean_GroupA": 1828.21718099689,
    "baseMean_GroupB": 3948.28187993926,
    "lfcSE": 0.12152809045061,
    "stat": 9.13157566793102,
    "pvalue": 6.75094578801052e-20,
    "comparisonDetails": {
      "group_A": "sample-a:illumina-kit",
      "group_B": "sample-b:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:illumina-kit_sample-b:NEB-kit_none_1",
      "folderName": "DE__GSE45722_155ae9b9b5a5388d5683cac302106d71_5b0eb53796b50",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_155ae9b9b5a5388d5683cac302106d71",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:illumina-kit vs sample-b:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell99": {
    "experimentId": "GSE38916_cells_extracellular-vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -6.20875913298927,
    "padj": 6.21044818370537e-9,
    "baseMean": 68952.6188548761,
    "baseMean_GroupA": 136371.396961544,
    "baseMean_GroupB": 1533.84074820774,
    "lfcSE": 0.956348178158572,
    "stat": -6.49215345915554,
    "pvalue": 8.46179975484216e-11,
    "comparisonDetails": {
      "group_A": "cells",
      "group_B": "extracellular-vesicles",
      "datasetId": "GSE38916",
      "covariates": "none",
      "experimentId": "GSE38916_cells_extracellular-vesicles_none_1",
      "folderName": "DE__GSE38916_a5c324806b9c5008d8103f0dcce00491_5b0fcaaa5e93f",
      "comparisonCondition": "tissue-part",
      "experimentHash": "GSE38916_a5c324806b9c5008d8103f0dcce00491",
      "genome": "hg38",
      "allConditions": [
        "tissue-part"
      ]
    },
    "xkey": "cells vs extracellular-vesicles -- GSE38916",
    "ykey": "hsa-let-7a-5p"
  },
  "cell216": {
    "experimentId": "GSE43793_ht29_hct15_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.80752274699788,
    "padj": 0.00186886092886466,
    "baseMean": 246217.748769918,
    "baseMean_GroupA": 434150.098592922,
    "baseMean_GroupB": 58285.3989469146,
    "lfcSE": 0.778829399603201,
    "stat": -3.60479810909585,
    "pvalue": 0.000312395680997553,
    "comparisonDetails": {
      "group_A": "ht29",
      "group_B": "hct15",
      "datasetId": "GSE43793",
      "covariates": "none",
      "experimentId": "GSE43793_ht29_hct15_none_1",
      "folderName": "DE__GSE43793_3d645baeb07bee768cf94b7b87e78acb_5b0eba670639e",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE43793_3d645baeb07bee768cf94b7b87e78acb",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "ht29 vs hct15 -- GSE43793",
    "ykey": "hsa-let-7a-5p"
  },
  "cell8": {
    "experimentId": "GSE50064_HCT-116-cell_HCT-116-p53-/-_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.493176017241564,
    "padj": 0.00174086283372092,
    "baseMean": 2083513.35983459,
    "baseMean_GroupA": 1727297.57406347,
    "baseMean_GroupB": 2439729.14560571,
    "lfcSE": 0.128242023598305,
    "stat": 3.84566621302193,
    "pvalue": 0.000120225333820506,
    "comparisonDetails": {
      "group_A": "HCT-116-cell",
      "group_B": "HCT-116-p53-/-",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell_HCT-116-p53-/-_none_1",
      "folderName": "DE__GSE50064_c3826dd254288cacad78b995061d179d_5b0fcab5ccd1e",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE50064_c3826dd254288cacad78b995061d179d",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HCT-116-cell vs HCT-116-p53-/- -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell213": {
    "experimentId": "GSE57012_epidermis:diseased-tissue_dermis:healthy-tissue_age_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.2053721358701899,
    "padj": 0.0122569357190513,
    "baseMean": 50419.0114815098,
    "baseMean_GroupA": 30317.7351105859,
    "baseMean_GroupB": 70520.2878524337,
    "lfcSE": 0.409540822064731,
    "stat": 2.94322829600531,
    "pvalue": 0.00324808796554861,
    "comparisonDetails": {
      "group_A": "epidermis:diseased-tissue",
      "group_B": "dermis:healthy-tissue",
      "datasetId": "GSE57012",
      "covariates": "age",
      "experimentId": "GSE57012_epidermis:diseased-tissue_dermis:healthy-tissue_age_1",
      "folderName": "DE__GSE57012_8a09f9709178ec41d7150a5c83511452_5b169077a81b6",
      "comparisonCondition": "tissue:tissue-phenotype",
      "experimentHash": "GSE57012_8a09f9709178ec41d7150a5c83511452",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "tissue-phenotype"
      ]
    },
    "xkey": "epidermis:diseased-tissue vs dermis:healthy-tissue -- GSE57012",
    "ykey": "hsa-let-7a-5p"
  },
  "cell9": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.83296950306346,
    "padj": 0.000022661840871562903,
    "baseMean": 8471.10015481039,
    "baseMean_GroupA": 6373.71451343179,
    "baseMean_GroupB": 22628.4532341159,
    "lfcSE": 0.422819018678752,
    "stat": 4.33511602385159,
    "pvalue": 0.000014568326274576101,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_d169f9c6d5e202ae3060fc657198f8e5_5b1588cf1f7a2",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_d169f9c6d5e202ae3060fc657198f8e5",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell214": {
    "experimentId": "GSE64142_Yersinia-pseudotuberculosis:4hN/A_Yersinia-pseudotuberculosis:18hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.453851471965633,
    "padj": 0.00707872274649148,
    "baseMean": 319304.80639461,
    "baseMean_GroupA": 369588.060120692,
    "baseMean_GroupB": 269021.552668527,
    "lfcSE": 0.140010399909081,
    "stat": -3.24155542917063,
    "pvalue": 0.00118879313299857,
    "comparisonDetails": {
      "group_A": "Yersinia-pseudotuberculosis:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:18hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Yersinia-pseudotuberculosis:4hN/A_Yersinia-pseudotuberculosis:18hN/A_none_1",
      "folderName": "DE__GSE64142_cf97b5650125f6eab3ce36c07e9794e0_5b15865a7689e",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_cf97b5650125f6eab3ce36c07e9794e0",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Yersinia-pseudotuberculosis:4hN/A vs Yersinia-pseudotuberculosis:18hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell6": {
    "experimentId": "GSE68189_fibroblast-of-lung_forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -9.68560246225763,
    "padj": 0,
    "baseMean": 98405.670846298,
    "baseMean_GroupA": 196572.922092672,
    "baseMean_GroupB": 238.419599924228,
    "lfcSE": 0.0901637251531687,
    "stat": -107.422385730003,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_forebrain-cell_none_1",
      "folderName": "DE__GSE68189_0b08194eb7ac9486483b3f18a59afaf8_5b15878ec6413",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_0b08194eb7ac9486483b3f18a59afaf8",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell211": {
    "experimentId": "GSE68189_forebrain-cell_mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 7.09622065530267,
    "padj": 0,
    "baseMean": 15368.4492322076,
    "baseMean_GroupA": 222.312445720153,
    "baseMean_GroupB": 30514.5860186951,
    "lfcSE": 0.139455523144089,
    "stat": 50.8851890216687,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "forebrain-cell",
      "group_B": "mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_forebrain-cell_mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_8d87e8e44688e106530d1820411c3c47_5b1587a754991",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_8d87e8e44688e106530d1820411c3c47",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "forebrain-cell vs mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell7": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -8.86389292010762,
    "padj": 0,
    "baseMean": 109359.365358036,
    "baseMean_GroupA": 218250.362523034,
    "baseMean_GroupB": 468.368193037902,
    "lfcSE": 0.146158662666501,
    "stat": -60.6456898167773,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:midbrain-cell_none_1",
      "folderName": "DE__GSE68189_ffee1ecefaa2710b163f3ee44d80d2bd_5b1587c79ba7b",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_ffee1ecefaa2710b163f3ee44d80d2bd",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell212": {
    "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.860716177333308,
    "padj": 0.0153444551499909,
    "baseMean": 114739.950781216,
    "baseMean_GroupA": 81359.4756448844,
    "baseMean_GroupB": 148120.425917548,
    "lfcSE": 0.334891153413199,
    "stat": 2.57013709845996,
    "pvalue": 0.010165827476258,
    "comparisonDetails": {
      "group_A": "DKS-8:wild-type-KRAS-allele:exosome",
      "group_B": "DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKS-8:wild-type-KRAS-allele:exosome_DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell_none_1",
      "folderName": "DE__GSE67004_8f19ee49398332fa9874d22c89e0d544_5b1586969049d",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_8f19ee49398332fa9874d22c89e0d544",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKS-8:wild-type-KRAS-allele:exosome vs DLD-1-cell:both-wild-type-and-mutant-(G13D)-KRAS-alleles:cell -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell219": {
    "experimentId": "GSE14362_no-TAP-treatment_size-range-50-200nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.74177802041602,
    "padj": 3.5022210216635494e-8,
    "baseMean": 13346.0993412511,
    "baseMean_GroupA": 20249.5383798909,
    "baseMean_GroupB": 2990.94078329145,
    "lfcSE": 0.470611903185739,
    "stat": -5.82598527970915,
    "pvalue": 5.67766206583681e-9,
    "comparisonDetails": {
      "group_A": "no-TAP-treatment",
      "group_B": "size-range-50-200nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_no-TAP-treatment_size-range-50-200nt_none_1",
      "folderName": "DE__GSE14362_ea2ec7538d99354f48e45705c5ec2134_5b0ebbb398c60",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_ea2ec7538d99354f48e45705c5ec2134",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "no-TAP-treatment vs size-range-50-200nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell220": {
    "experimentId": "GSE69837_Chlamydia-trachomatis-A2497P-_Chlamydia-trachomatis_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.34032002630604,
    "padj": 0.000017624989133526102,
    "baseMean": 14916.6161166345,
    "baseMean_GroupA": 20604.9763070705,
    "baseMean_GroupB": 8090.58388811124,
    "lfcSE": 0.288971525687638,
    "stat": -4.63824255042641,
    "pvalue": 0.0000035138427702916102,
    "comparisonDetails": {
      "group_A": "Chlamydia-trachomatis-A2497P-",
      "group_B": "Chlamydia-trachomatis",
      "datasetId": "GSE69837",
      "covariates": "none",
      "experimentId": "GSE69837_Chlamydia-trachomatis-A2497P-_Chlamydia-trachomatis_none_1",
      "folderName": "DE__GSE69837_0edb01faf2d02d89c517147540d14283_5b0eb8f61089c",
      "comparisonCondition": "virus",
      "experimentHash": "GSE69837_0edb01faf2d02d89c517147540d14283",
      "genome": "hg38",
      "allConditions": [
        "virus"
      ]
    },
    "xkey": "Chlamydia-trachomatis-A2497P- vs Chlamydia-trachomatis -- GSE69837",
    "ykey": "hsa-let-7a-5p"
  },
  "cell100": {
    "experimentId": "GSE50064_MCF-10A-cell_HCT-116-p53-/-_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.1684409670818101,
    "padj": 3.59883737085215e-7,
    "baseMean": 2740148.76478795,
    "baseMean_GroupA": 3801260.27823578,
    "baseMean_GroupB": 1679037.25134013,
    "lfcSE": 0.214945630775828,
    "stat": -5.43598380141261,
    "pvalue": 5.4494905831281495e-8,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell",
      "group_B": "HCT-116-p53-/-",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell_HCT-116-p53-/-_none_1",
      "folderName": "DE__GSE50064_907984d06fedc273ae6a54cb03f9fc1f_5b0fcab294a6b",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE50064_907984d06fedc273ae6a54cb03f9fc1f",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "MCF-10A-cell vs HCT-116-p53-/- -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell221": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-14_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -3.04744956712667,
    "padj": 0.000014555485734934702,
    "baseMean": 46.0492007476017,
    "baseMean_GroupA": 98.5339983697705,
    "baseMean_GroupB": 11.0593356661558,
    "lfcSE": 0.615345372833932,
    "stat": -4.95242135825585,
    "pvalue": 7.329568096432039e-7,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-14",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-14_none_1",
      "folderName": "DE__GSE62776_878c58cbfb81c7eb3710c2d819915187_5b158a0764258",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_878c58cbfb81c7eb3710c2d819915187",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-14 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell107": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_H9-cell:embryonic-stem-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.00250941706356,
    "padj": 8.93249038738169e-12,
    "baseMean": 99435.8197272562,
    "baseMean_GroupA": 193269.362025992,
    "baseMean_GroupB": 5602.2774285206,
    "lfcSE": 0.714142298918686,
    "stat": -7.00491964225908,
    "pvalue": 2.47127632046572e-12,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "H9-cell:embryonic-stem-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_H9-cell:embryonic-stem-cell_none_1",
      "folderName": "DE__GSE68189_2a5873fc97203d7e21eb72138adb5b0a_5b1587c1156b7",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_2a5873fc97203d7e21eb72138adb5b0a",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs H9-cell:embryonic-stem-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell228": {
    "experimentId": "GSE57012_epidermis_dermis_age_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.618460343674971,
    "padj": 0.0167397092409436,
    "baseMean": 65970.1964940757,
    "baseMean_GroupA": 51981.2478965388,
    "baseMean_GroupB": 79959.1450916127,
    "lfcSE": 0.227390270663276,
    "stat": 2.7198188465627,
    "pvalue": 0.0065317688998976,
    "comparisonDetails": {
      "group_A": "epidermis",
      "group_B": "dermis",
      "datasetId": "GSE57012",
      "covariates": "age",
      "experimentId": "GSE57012_epidermis_dermis_age_1",
      "folderName": "DE__GSE57012_c7b88f035c0aa3f484dc15014402b4eb_5b169053dc552",
      "comparisonCondition": "tissue",
      "experimentHash": "GSE57012_c7b88f035c0aa3f484dc15014402b4eb",
      "genome": "hg38",
      "allConditions": [
        "tissue"
      ]
    },
    "xkey": "epidermis vs dermis -- GSE57012",
    "ykey": "hsa-let-7a-5p"
  },
  "cell108": {
    "experimentId": "GSE32109_BC-1-cell:AGO2-immunoprecipitated_BC-3-cell:AGO2-immunoprecipitated_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.414429381978932,
    "padj": 0.0219905057093862,
    "baseMean": 465.976283419812,
    "baseMean_GroupA": 532.586782452694,
    "baseMean_GroupB": 399.36578438693,
    "lfcSE": 0.160160934771897,
    "stat": -2.58758093894224,
    "pvalue": 0.00966524856498193,
    "comparisonDetails": {
      "group_A": "BC-1-cell:AGO2-immunoprecipitated",
      "group_B": "BC-3-cell:AGO2-immunoprecipitated",
      "datasetId": "GSE32109",
      "covariates": "none",
      "experimentId": "GSE32109_BC-1-cell:AGO2-immunoprecipitated_BC-3-cell:AGO2-immunoprecipitated_none_1",
      "folderName": "DE__GSE32109_611d0cb360f33f9b1005feabbf8a11c5_5b1589bf81812",
      "comparisonCondition": "cell-line:experimental-process",
      "experimentHash": "GSE32109_611d0cb360f33f9b1005feabbf8a11c5",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "experimental-process"
      ]
    },
    "xkey": "BC-1-cell:AGO2-immunoprecipitated vs BC-3-cell:AGO2-immunoprecipitated -- GSE32109",
    "ykey": "hsa-let-7a-5p"
  },
  "cell229": {
    "experimentId": "GSE69825_heart_liver_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.65521135644073,
    "padj": 3.2138838492111e-7,
    "baseMean": 4293.75132625426,
    "baseMean_GroupA": 5253.95919629412,
    "baseMean_GroupB": 3333.5434562144,
    "lfcSE": 0.123781002526197,
    "stat": -5.29331111453924,
    "pvalue": 1.20121304900932e-7,
    "comparisonDetails": {
      "group_A": "heart",
      "group_B": "liver",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_heart_liver_none_1",
      "folderName": "DE__GSE69825_4b12ddf8de8e841660014147bc8117b0_5b15882d467ea",
      "comparisonCondition": "tissue",
      "experimentHash": "GSE69825_4b12ddf8de8e841660014147bc8117b0",
      "genome": "hg38",
      "allConditions": [
        "tissue"
      ]
    },
    "xkey": "heart vs liver -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell105": {
    "experimentId": "GSE69825_prefrontal-cortex_liver_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.01656690014082,
    "padj": 5.010009193639149e-22,
    "baseMean": 6774.90269479393,
    "baseMean_GroupA": 9069.93553131575,
    "baseMean_GroupB": 4479.86985827212,
    "lfcSE": 0.1036450360009,
    "stat": -9.80815810736939,
    "pvalue": 1.03846709551698e-22,
    "comparisonDetails": {
      "group_A": "prefrontal-cortex",
      "group_B": "liver",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_prefrontal-cortex_liver_none_1",
      "folderName": "DE__GSE69825_8d596e51c6990b193b0d0c6e8818bab9_5b1588298ec9d",
      "comparisonCondition": "tissue",
      "experimentHash": "GSE69825_8d596e51c6990b193b0d0c6e8818bab9",
      "genome": "hg38",
      "allConditions": [
        "tissue"
      ]
    },
    "xkey": "prefrontal-cortex vs liver -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell226": {
    "experimentId": "GSE14362_standard-protocol_no-TAP-treatment_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.47439435004634,
    "padj": 0.0030314889359211,
    "baseMean": 2106.45259068659,
    "baseMean_GroupA": 434.499048459211,
    "baseMean_GroupB": 5450.35967514135,
    "lfcSE": 1.01318463069967,
    "stat": 3.42918185370325,
    "pvalue": 0.000605403738222767,
    "comparisonDetails": {
      "group_A": "standard-protocol",
      "group_B": "no-TAP-treatment",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_standard-protocol_no-TAP-treatment_none_1",
      "folderName": "DE__GSE14362_2c5072f7131872c8194057154e2f2515_5b0ebb688356a",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_2c5072f7131872c8194057154e2f2515",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "standard-protocol vs no-TAP-treatment -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell106": {
    "experimentId": "GSE72193_Placebo:3day_Diphencyprone:3day_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.0505866698158,
    "padj": 0.0000649217294117978,
    "baseMean": 294956.794219755,
    "baseMean_GroupA": 399356.153364369,
    "baseMean_GroupB": 190557.435075142,
    "lfcSE": 0.220902879320168,
    "stat": -4.75587585390012,
    "pvalue": 0.0000019758787212286297,
    "comparisonDetails": {
      "group_A": "Placebo:3day",
      "group_B": "Diphencyprone:3day",
      "datasetId": "GSE72193",
      "covariates": "age-gender",
      "experimentId": "GSE72193_Placebo:3day_Diphencyprone:3day_age-gender_1",
      "folderName": "DE__GSE72193_a6e00bccf1ab257f158f893f5389b2c3_5b168f4cdcff0",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE72193_a6e00bccf1ab257f158f893f5389b2c3",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Placebo:3day vs Diphencyprone:3day -- GSE72193",
    "ykey": "hsa-let-7a-5p"
  },
  "cell227": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.8411926035525799,
    "padj": 0.00000236532312660201,
    "baseMean": 3048462.43235249,
    "baseMean_GroupA": 4783233.725914,
    "baseMean_GroupB": 1313691.13879098,
    "lfcSE": 0.352187707258641,
    "stat": -5.22787299387605,
    "pvalue": 1.71471229144719e-7,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-cell:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-cell:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_06c5ccb549edfa675e0bde06b41b7a80_5b0fcae3025eb",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_06c5ccb549edfa675e0bde06b41b7a80",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:24-hN/A vs HCT-116-cell:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell103": {
    "experimentId": "GSE45722_sample-a:bioo-scientific-kit_sample-b:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.878938091920623,
    "padj": 1.5344799193314301e-9,
    "baseMean": 4286.78331494113,
    "baseMean_GroupA": 3018.30451656301,
    "baseMean_GroupB": 5555.26211331924,
    "lfcSE": 0.141131893799827,
    "stat": 6.22777791933591,
    "pvalue": 4.7309702606733e-10,
    "comparisonDetails": {
      "group_A": "sample-a:bioo-scientific-kit",
      "group_B": "sample-b:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:bioo-scientific-kit_sample-b:NEB-kit_none_1",
      "folderName": "DE__GSE45722_f655cd2024502fece27471f2141d24ad_5b0eb5442bd5a",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_f655cd2024502fece27471f2141d24ad",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:bioo-scientific-kit vs sample-b:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell224": {
    "experimentId": "GSE68189_ESC-derived-cell-line:midbrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.72232198761911,
    "padj": 4.6232791633687797e-7,
    "baseMean": 848.518189468527,
    "baseMean_GroupA": 334.777477351942,
    "baseMean_GroupB": 1142.084310678,
    "lfcSE": 0.307411599243371,
    "stat": 5.60265777823038,
    "pvalue": 2.11089737987294e-8,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:midbrain-cell",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:midbrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_c3d33d1cfacebadb9a624b998c3969cc_5b1587ee7bb47",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_c3d33d1cfacebadb9a624b998c3969cc",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:midbrain-cell vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell104": {
    "experimentId": "GSE64142_non-infected:18hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.420007511329108,
    "padj": 0.0237879211580889,
    "baseMean": 239849.863149349,
    "baseMean_GroupA": 274753.676045472,
    "baseMean_GroupB": 204946.050253226,
    "lfcSE": 0.154530532884839,
    "stat": -2.71795808561737,
    "pvalue": 0.00656861585985556,
    "comparisonDetails": {
      "group_A": "non-infected:18hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:18hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_e3c18d68960a94ff929ae51f08430c43_5b0fcc97cda92",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_e3c18d68960a94ff929ae51f08430c43",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:18hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell225": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -7.25872993843716,
    "padj": 4.870552111575659e-126,
    "baseMean": 60259.887908102,
    "baseMean_GroupA": 163846.653399906,
    "baseMean_GroupB": 1067.45048421406,
    "lfcSE": 0.301838783672367,
    "stat": -24.0483673109291,
    "pvalue": 8.682648076662887e-128,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_68f343d185f80e0779d411c84cbe5ccb_5b1587ccc1165",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_68f343d185f80e0779d411c84cbe5ccb",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell101": {
    "experimentId": "GSE68189_mesenchymal-cell_hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.73652537241139,
    "padj": 3.790923809763798e-243,
    "baseMean": 15017.7756954843,
    "baseMean_GroupA": 29486.0855384507,
    "baseMean_GroupB": 549.465852517906,
    "lfcSE": 0.171617613830687,
    "stat": -33.4262040146467,
    "pvalue": 5.7074130257933675e-245,
    "comparisonDetails": {
      "group_A": "mesenchymal-cell",
      "group_B": "hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_mesenchymal-cell_hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_6ee751c59f9d6e9f9b93af5d39cf9db0_5b1587b3918f0",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_6ee751c59f9d6e9f9b93af5d39cf9db0",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "mesenchymal-cell vs hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell222": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A_salmonella-typhimurium-strain-keller:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.346115078024245,
    "padj": 0.0478130805592544,
    "baseMean": 271373.26309413,
    "baseMean_GroupA": 238748.949082185,
    "baseMean_GroupB": 303997.577106074,
    "lfcSE": 0.130238852094139,
    "stat": 2.65754091393608,
    "pvalue": 0.00787130355225023,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A_salmonella-typhimurium-strain-keller:4hN/A_none_1",
      "folderName": "DE__GSE64142_7047448709daf38a97654a0917e1d8ea_5b1585e0535b4",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_7047448709daf38a97654a0917e1d8ea",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A vs salmonella-typhimurium-strain-keller:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell102": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-10_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": -1.49520401309735,
    "padj": 0.0191696173624941,
    "baseMean": 60.4583531697031,
    "baseMean_GroupA": 99.9387281267913,
    "baseMean_GroupB": 34.1381031983109,
    "lfcSE": 0.517606449519464,
    "stat": -2.88868891507335,
    "pvalue": 0.00386851576047693,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-10",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-10_none_1",
      "folderName": "DE__GSE62776_cccaf4a843f602569bf6351e41bdae70_5b158a057e4fb",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_cccaf4a843f602569bf6351e41bdae70",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-10 -- GSE62776",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell223": {
    "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.73652537241139,
    "padj": 3.790923809763798e-243,
    "baseMean": 15017.7756954843,
    "baseMean_GroupA": 29486.0855384507,
    "baseMean_GroupB": 549.465852517906,
    "lfcSE": 0.171617613830687,
    "stat": -33.4262040146467,
    "pvalue": 5.7074130257933675e-245,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:mesenchymal-cell",
      "group_B": "ESC-derived-cell-line:hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_ae60861c3fe249e4a3e41a22718e9b57_5b1587e66802d",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_ae60861c3fe249e4a3e41a22718e9b57",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:mesenchymal-cell vs ESC-derived-cell-line:hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell109": {
    "experimentId": "GSE74759_Cell_Exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.91358584792463,
    "padj": 6.348781503950958e-128,
    "baseMean": 13036.9667366731,
    "baseMean_GroupA": 20605.9432903216,
    "baseMean_GroupB": 5467.99018302474,
    "lfcSE": 0.0792478167997031,
    "stat": -24.1468588688212,
    "pvalue": 8.056078974745678e-129,
    "comparisonDetails": {
      "group_A": "Cell",
      "group_B": "Exosome",
      "datasetId": "GSE74759",
      "covariates": "none",
      "experimentId": "GSE74759_Cell_Exosome_none_1",
      "folderName": "DE__GSE74759_c96e9af6114da1ffb10dd5781ad20c66_5b0eba8fd1d13",
      "comparisonCondition": "tissue-part",
      "experimentHash": "GSE74759_c96e9af6114da1ffb10dd5781ad20c66",
      "genome": "hg38",
      "allConditions": [
        "tissue-part"
      ]
    },
    "xkey": "Cell vs Exosome -- GSE74759",
    "ykey": "hsa-let-7a-5p"
  },
  "cell110": {
    "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.62952427230893,
    "padj": 2.3522669165485499e-32,
    "baseMean": 56.6640810152791,
    "baseMean_GroupA": 1.22105953412008,
    "baseMean_GroupB": 79.7653399657621,
    "lfcSE": 0.472847662634337,
    "stat": 11.905577032877,
    "pvalue": 1.10694913719932e-32,
    "comparisonDetails": {
      "group_A": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500",
      "group_B": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
      "folderName": "DE__GSE69825_5a1a8e3eaf324dd11bf1d5b7ce9cdd47_5b15889c12dc0",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_5a1a8e3eaf324dd11bf1d5b7ce9cdd47",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500 vs blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell231": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:illumina-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.45783362555084,
    "padj": 2.98132572047276e-26,
    "baseMean": 3472.21963669484,
    "baseMean_GroupA": 5092.7963999431,
    "baseMean_GroupB": 1851.64287344657,
    "lfcSE": 0.135306868033779,
    "stat": -10.7742766256839,
    "pvalue": 4.55347175956451e-27,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-a:illumina-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:illumina-kit_none_1",
      "folderName": "DE__GSE45722_58f613958b0bb31e3060023bcf89a102_5b0eb524ca4ed",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_58f613958b0bb31e3060023bcf89a102",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-a:illumina-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell111": {
    "experimentId": "GSE14362_no-TAP-treatment_size-range-<50nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.7608773645628,
    "padj": 2.95040840694337e-17,
    "baseMean": 10166.1031711461,
    "baseMean_GroupA": 21587.7216791756,
    "baseMean_GroupB": 6358.89700180298,
    "lfcSE": 0.204259448013258,
    "stat": -8.62078783473702,
    "pvalue": 6.6495014179858996e-18,
    "comparisonDetails": {
      "group_A": "no-TAP-treatment",
      "group_B": "size-range-<50nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_no-TAP-treatment_size-range-<50nt_none_1",
      "folderName": "DE__GSE14362_31598b7d1a74cad3b9be4c8484110528_5b0ebbad2d591",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_31598b7d1a74cad3b9be4c8484110528",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "no-TAP-treatment vs size-range-<50nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell232": {
    "experimentId": "GSE84779_pouchitis_ulcerative-colitis_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.395926891871906,
    "padj": 0.0211582509733252,
    "baseMean": 45499.8849613459,
    "baseMean_GroupA": 41269.5588909149,
    "baseMean_GroupB": 54729.6872968316,
    "lfcSE": 0.133030703712092,
    "stat": 2.97620685168125,
    "pvalue": 0.00291837944459658,
    "comparisonDetails": {
      "group_A": "pouchitis",
      "group_B": "ulcerative-colitis",
      "datasetId": "GSE84779",
      "covariates": "none",
      "experimentId": "GSE84779_pouchitis_ulcerative-colitis_none_1",
      "folderName": "DE__GSE84779_b4731ecdd639bc4c12ab4c09cadc9f17_5b0ebb44ead78",
      "comparisonCondition": "disease",
      "experimentHash": "GSE84779_b4731ecdd639bc4c12ab4c09cadc9f17",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "pouchitis vs ulcerative-colitis -- GSE84779",
    "ykey": "hsa-let-7a-5p"
  },
  "cell230": {
    "experimentId": "GSE68189_forebrain-cell_midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.849882358030682,
    "padj": 2.44664704534229e-7,
    "baseMean": 347.699194146126,
    "baseMean_GroupA": 246.954480286906,
    "baseMean_GroupB": 448.443908005346,
    "lfcSE": 0.153415522447403,
    "stat": 5.53974164069386,
    "pvalue": 3.0291820561380694e-8,
    "comparisonDetails": {
      "group_A": "forebrain-cell",
      "group_B": "midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_forebrain-cell_midbrain-cell_none_1",
      "folderName": "DE__GSE68189_21c4c318e61b2fb5bccacceffb45eb6c_5b1587a9ae004",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_21c4c318e61b2fb5bccacceffb45eb6c",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "forebrain-cell vs midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell118": {
    "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.993366551188882,
    "padj": 0.00665646064876513,
    "baseMean": 2640001.63209323,
    "baseMean_GroupA": 3521915.51963156,
    "baseMean_GroupB": 1758087.74455489,
    "lfcSE": 0.298468494672281,
    "stat": -3.32821242081044,
    "pvalue": 0.000874051932921683,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:control:0-hN/A",
      "group_B": "HCT-116-p53-/-:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_2188b21c060257eee6443dea4d72e917_5b0fcad0c15a9",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_2188b21c060257eee6443dea4d72e917",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:control:0-hN/A vs HCT-116-p53-/-:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell239": {
    "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.31278449702121,
    "padj": 0.000164685284103791,
    "baseMean": 2504900.07558809,
    "baseMean_GroupA": 3580822.66587371,
    "baseMean_GroupB": 1428977.48530248,
    "lfcSE": 0.30352034309156,
    "stat": -4.32519442897833,
    "pvalue": 0.000015239738670713501,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:control:0-hN/A",
      "group_B": "HCT-116-cell:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_29a8306dcc5c821108d413e49886b668_5b0fcacf54c51",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_29a8306dcc5c821108d413e49886b668",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:control:0-hN/A vs HCT-116-cell:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell119": {
    "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.39138317663852,
    "padj": 7.82081671349689e-9,
    "baseMean": 2689.93219476529,
    "baseMean_GroupA": 5157.78615729102,
    "baseMean_GroupB": 222.078232239556,
    "lfcSE": 0.734626137156682,
    "stat": -5.9777115930493,
    "pvalue": 2.26293901725983e-9,
    "comparisonDetails": {
      "group_A": "H9-cell:embryonic-stem-cell",
      "group_B": "ESC-derived-cell-line:forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:forebrain-cell_none_1",
      "folderName": "DE__GSE68189_2847d4a0ef86c50980614d4a4290de5a_5b1587ced4609",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_2847d4a0ef86c50980614d4a4290de5a",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "H9-cell:embryonic-stem-cell vs ESC-derived-cell-line:forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell116": {
    "experimentId": "GSE70432_Cells_exosomes_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.61048609587599,
    "padj": 1.44453037235498e-13,
    "baseMean": 1944.11722115904,
    "baseMean_GroupA": 3598.87302331443,
    "baseMean_GroupB": 289.361419003649,
    "lfcSE": 0.458931297514321,
    "stat": -7.86716032537161,
    "pvalue": 3.62781600363123e-15,
    "comparisonDetails": {
      "group_A": "Cells",
      "group_B": "exosomes",
      "datasetId": "GSE70432",
      "covariates": "none",
      "experimentId": "GSE70432_Cells_exosomes_none_1",
      "folderName": "DE__GSE70432_5f526047e36107d950595770dffad592_5b1588fa9c1e2",
      "comparisonCondition": "tissue-part",
      "experimentHash": "GSE70432_5f526047e36107d950595770dffad592",
      "genome": "hg38",
      "allConditions": [
        "tissue-part"
      ]
    },
    "xkey": "Cells vs exosomes -- GSE70432",
    "ykey": "hsa-let-7a-5p"
  },
  "cell237": {
    "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 4.04770603067502,
    "padj": 0.00198237043079016,
    "baseMean": 2157.15792186087,
    "baseMean_GroupA": 84.7855811903387,
    "baseMean_GroupB": 3637.42387948268,
    "lfcSE": 1.10503066781891,
    "stat": 3.66298071949831,
    "pvalue": 0.000249297309684015,
    "comparisonDetails": {
      "group_A": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500",
      "group_B": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_f69103205c428a462c375c92ea274e8e_5b158891550af",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_f69103205c428a462c375c92ea274e8e",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500 vs blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell117": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.3197366780181001,
    "padj": 1.41420736636381e-14,
    "baseMean": 5208.37594534127,
    "baseMean_GroupA": 7441.47921158501,
    "baseMean_GroupB": 2975.27267909754,
    "lfcSE": 0.167123010543388,
    "stat": -7.89679813526023,
    "pvalue": 2.86159261815552e-15,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-a:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-a:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_03c9b305952dd1252417efdb03a5691d_5b0eb5282bd8d",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_03c9b305952dd1252417efdb03a5691d",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-a:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell238": {
    "experimentId": "GSE72193_Diphencyprone:120day_Diphencyprone:3day_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.848311731866964,
    "padj": 0.0475510171754658,
    "baseMean": 198473.028826263,
    "baseMean_GroupA": 278785.056649652,
    "baseMean_GroupB": 152580.441498612,
    "lfcSE": 0.301954722254191,
    "stat": -2.80940044763678,
    "pvalue": 0.00496338669492281,
    "comparisonDetails": {
      "group_A": "Diphencyprone:120day",
      "group_B": "Diphencyprone:3day",
      "datasetId": "GSE72193",
      "covariates": "age-gender",
      "experimentId": "GSE72193_Diphencyprone:120day_Diphencyprone:3day_age-gender_1",
      "folderName": "DE__GSE72193_d8a76a2198b8e24e3b0c8d3abc0615ce_5b168f5a4ef7c",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE72193_d8a76a2198b8e24e3b0c8d3abc0615ce",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Diphencyprone:120day vs Diphencyprone:3day -- GSE72193",
    "ykey": "hsa-let-7a-5p"
  },
  "cell114": {
    "experimentId": "GSE45722_sample-a:NEB-kit_sample-b:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.12011997141347,
    "padj": 4.586066804469e-14,
    "baseMean": 6422.6997224847,
    "baseMean_GroupA": 10459.8067538605,
    "baseMean_GroupB": 2385.59269110887,
    "lfcSE": 0.271104800801931,
    "stat": -7.82029667177465,
    "pvalue": 5.26989892716e-15,
    "comparisonDetails": {
      "group_A": "sample-a:NEB-kit",
      "group_B": "sample-b:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:NEB-kit_sample-b:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_65cac22b199889636028326359330118_5b0eb540cdaa2",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_65cac22b199889636028326359330118",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:NEB-kit vs sample-b:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell235": {
    "experimentId": "GSE62776_ssea3-_tra-1-60+_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.80977995703453,
    "padj": 1.4764044616088e-14,
    "baseMean": 65432.4120235296,
    "baseMean_GroupA": 148533.674748997,
    "baseMean_GroupB": 10031.5702065515,
    "lfcSE": 0.465092653456147,
    "stat": -8.19144299253857,
    "pvalue": 2.58112668113426e-16,
    "comparisonDetails": {
      "group_A": "ssea3-",
      "group_B": "tra-1-60+",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_ssea3-_tra-1-60+_none_1",
      "folderName": "DE__GSE62776_5915161c69d32a1c139d4d30462ea221_5b158a2519235",
      "comparisonCondition": "marker",
      "experimentHash": "GSE62776_5915161c69d32a1c139d4d30462ea221",
      "genome": "hg38",
      "allConditions": [
        "marker"
      ]
    },
    "xkey": "ssea3- vs tra-1-60+ -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell115": {
    "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:exosome_DKS-8:wild-type-KRAS-allele:cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.3790292096252,
    "padj": 0.02286716024146,
    "baseMean": 145243.345141978,
    "baseMean_GroupA": 79889.6196375391,
    "baseMean_GroupB": 210597.070646416,
    "lfcSE": 0.549494474643163,
    "stat": 2.50963253182979,
    "pvalue": 0.0120856854508096,
    "comparisonDetails": {
      "group_A": "DKO-1:mutant-KRAS-allele:exosome",
      "group_B": "DKS-8:wild-type-KRAS-allele:cell",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_DKO-1:mutant-KRAS-allele:exosome_DKS-8:wild-type-KRAS-allele:cell_none_1",
      "folderName": "DE__GSE67004_601f68c3e7d73325a74a2011d6dd6605_5b15868906af1",
      "comparisonCondition": "cell-line:sample-details:tissue-part",
      "experimentHash": "GSE67004_601f68c3e7d73325a74a2011d6dd6605",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "DKO-1:mutant-KRAS-allele:exosome vs DKS-8:wild-type-KRAS-allele:cell -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell236": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.509505537965196,
    "padj": 0.00134426390306642,
    "baseMean": 263291.928071242,
    "baseMean_GroupA": 216824.311849622,
    "baseMean_GroupB": 309759.544292862,
    "lfcSE": 0.133255415953652,
    "stat": 3.82352592814995,
    "pvalue": 0.000131556750302221,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_98f76f97d0d1623e726785a4d8d1be1f_5b1585eeea3ef",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_98f76f97d0d1623e726785a4d8d1be1f",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:18hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell112": {
    "experimentId": "GSE72193_Placebo_Diphencyprone_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.668882965360401,
    "padj": 0.000207534761941196,
    "baseMean": 271737.186000715,
    "baseMean_GroupA": 344502.816147779,
    "baseMean_GroupB": 215141.695886332,
    "lfcSE": 0.148294775778993,
    "stat": -4.5104958138057,
    "pvalue": 0.00000646762777818575,
    "comparisonDetails": {
      "group_A": "Placebo",
      "group_B": "Diphencyprone",
      "datasetId": "GSE72193",
      "covariates": "age-gender",
      "experimentId": "GSE72193_Placebo_Diphencyprone_age-gender_1",
      "folderName": "DE__GSE72193_5213b19e8b23ba40d95fcdacd2be8468_5b16887190f51",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE72193_5213b19e8b23ba40d95fcdacd2be8468",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "Placebo vs Diphencyprone -- GSE72193",
    "ykey": "hsa-let-7a-5p"
  },
  "cell233": {
    "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_no-TAP-treatment_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.7512242367095805,
    "padj": 2.68813184039093e-35,
    "baseMean": 14169.877441334,
    "baseMean_GroupA": 496.12555844504,
    "baseMean_GroupB": 27843.629324223,
    "lfcSE": 0.453036755154382,
    "stat": 12.6948292192092,
    "pvalue": 6.31675889386902e-37,
    "comparisonDetails": {
      "group_A": "CIP-treatment-prior-to-TAP",
      "group_B": "no-TAP-treatment",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_no-TAP-treatment_none_1",
      "folderName": "DE__GSE14362_7f228121cc5fd598aa98db50192d8a44_5b0ebb949d8be",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_7f228121cc5fd598aa98db50192d8a44",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "CIP-treatment-prior-to-TAP vs no-TAP-treatment -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell113": {
    "experimentId": "GSE69837_HCjE_HEp-2_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.766299211377459,
    "padj": 1.0698739524724699e-7,
    "baseMean": 35575.2329660069,
    "baseMean_GroupA": 26337.1112263993,
    "baseMean_GroupB": 44813.3547056145,
    "lfcSE": 0.139620122138407,
    "stat": 5.48845825115251,
    "pvalue": 4.05457071891366e-8,
    "comparisonDetails": {
      "group_A": "HCjE",
      "group_B": "HEp-2",
      "datasetId": "GSE69837",
      "covariates": "none",
      "experimentId": "GSE69837_HCjE_HEp-2_none_1",
      "folderName": "DE__GSE69837_78cf9a64aa2eaf6553ec02b4e2588734_5b0eb8efae9de",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE69837_78cf9a64aa2eaf6553ec02b4e2588734",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HCjE vs HEp-2 -- GSE69837",
    "ykey": "hsa-let-7a-5p"
  },
  "cell234": {
    "experimentId": "GSE68189_midbrain-cell_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.72232198761911,
    "padj": 4.6232791633687797e-7,
    "baseMean": 848.518189468527,
    "baseMean_GroupA": 334.777477351942,
    "baseMean_GroupB": 1142.084310678,
    "lfcSE": 0.307411599243371,
    "stat": 5.60265777823038,
    "pvalue": 2.11089737987294e-8,
    "comparisonDetails": {
      "group_A": "midbrain-cell",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_midbrain-cell_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_5af45b78edc6142e1c08261888441f16_5b1587bc05370",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_5af45b78edc6142e1c08261888441f16",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "midbrain-cell vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell70": {
    "experimentId": "GSE50064_MCF-10A-cell_HCT-116-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.70948386017998,
    "padj": 9.07835531583208e-17,
    "baseMean": 2818156.93363542,
    "baseMean_GroupA": 4325838.9142366,
    "baseMean_GroupB": 1310474.95303423,
    "lfcSE": 0.197684479127784,
    "stat": -8.64753706372143,
    "pvalue": 5.26228135453582e-18,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell",
      "group_B": "HCT-116-cell",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell_HCT-116-cell_none_1",
      "folderName": "DE__GSE50064_1917a99d0f258f13f79a380bbc5ac2cb_5b0fcaaf6b812",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE50064_1917a99d0f258f13f79a380bbc5ac2cb",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "MCF-10A-cell vs HCT-116-cell -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell72": {
    "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.56570698800176,
    "padj": 0.00000312560992592099,
    "baseMean": 2976.30456783049,
    "baseMean_GroupA": 5513.93015753688,
    "baseMean_GroupB": 438.678978124106,
    "lfcSE": 0.732244701600559,
    "stat": -4.8695565569921495,
    "pvalue": 0.00000111848967781251,
    "comparisonDetails": {
      "group_A": "H9-cell:embryonic-stem-cell",
      "group_B": "ESC-derived-cell-line:midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:midbrain-cell_none_1",
      "folderName": "DE__GSE68189_cdb0575ef406f5d9a94504f79ee2b015_5b1587d32347b",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_cdb0575ef406f5d9a94504f79ee2b015",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "H9-cell:embryonic-stem-cell vs ESC-derived-cell-line:midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell71": {
    "experimentId": "GSE14362_alternative-primers_size-range-50-200nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.16422198150003,
    "padj": 0.0192654366810406,
    "baseMean": 1117.29472953494,
    "baseMean_GroupA": 204.560906770901,
    "baseMean_GroupB": 2030.02855229898,
    "lfcSE": 1.18829626766826,
    "stat": 2.66282245227367,
    "pvalue": 0.00774882877576911,
    "comparisonDetails": {
      "group_A": "alternative-primers",
      "group_B": "size-range-50-200nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_alternative-primers_size-range-50-200nt_none_1",
      "folderName": "DE__GSE14362_2e6f7bfe1672b72769d501f8d54e45d9_5b0ebb8e6f88d",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_2e6f7bfe1672b72769d501f8d54e45d9",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "alternative-primers vs size-range-50-200nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell74": {
    "experimentId": "GSE64142_Yersinia-pseudotuberculosis:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.679156046954546,
    "padj": 0.000011650612538344401,
    "baseMean": 280361.400004662,
    "baseMean_GroupA": 345482.285177846,
    "baseMean_GroupB": 215240.514831479,
    "lfcSE": 0.142498993652236,
    "stat": -4.76604100525792,
    "pvalue": 0.00000187881038374325,
    "comparisonDetails": {
      "group_A": "Yersinia-pseudotuberculosis:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Yersinia-pseudotuberculosis:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_54e78d137a0036a090f32e951fcad736_5b15865db0aca",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_54e78d137a0036a090f32e951fcad736",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Yersinia-pseudotuberculosis:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell73": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-b:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.56228719374391,
    "padj": 1.97137818606456e-7,
    "baseMean": 4357.83884412641,
    "baseMean_GroupA": 6521.69129470091,
    "baseMean_GroupB": 2193.98639355191,
    "lfcSE": 0.283874235330475,
    "stat": -5.50344835601286,
    "pvalue": 3.72433662992311e-8,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-b:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-b:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_3890b6e85f4836a786aaf61b3ec39315_5b0eb52e7584f",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_3890b6e85f4836a786aaf61b3ec39315",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-b:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell65": {
    "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.620611544277295,
    "padj": 4.55944344088872e-8,
    "baseMean": 5555.27157808105,
    "baseMean_GroupA": 6942.05329410106,
    "baseMean_GroupB": 4515.18529106605,
    "lfcSE": 0.1114454922513,
    "stat": -5.5687451483266,
    "pvalue": 2.56580444614718e-8,
    "comparisonDetails": {
      "group_A": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500",
      "group_B": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_48c467ca8e9f132fe384faf5f3cfa27c_5b15888ac60e7",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_48c467ca8e9f132fe384faf5f3cfa27c",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500 vs heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell64": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-b:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.311204148564163,
    "padj": 0.0150093640819166,
    "baseMean": 6526.99722643304,
    "baseMean_GroupA": 7233.06346223116,
    "baseMean_GroupB": 5820.93099063492,
    "lfcSE": 0.105272409276775,
    "stat": -2.9561795982645798,
    "pvalue": 0.00311475661309969,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-b:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-b:NEB-kit_none_1",
      "folderName": "DE__GSE45722_ebdf3d459ea525a52de8f7ace020b35e_5b0eb52cc295e",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_ebdf3d459ea525a52de8f7ace020b35e",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-b:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell67": {
    "experimentId": "GSE68189_neuronal-stem-cell_forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.13447223249666,
    "padj": 1.08607084486779e-12,
    "baseMean": 327.175706163893,
    "baseMean_GroupA": 476.902886627025,
    "baseMean_GroupB": 214.880320816544,
    "lfcSE": 0.151903832559154,
    "stat": -7.46835819336469,
    "pvalue": 8.12015584947881e-14,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_forebrain-cell_none_1",
      "folderName": "DE__GSE68189_c52455de5a0fba28631afbd19a948e7b_5b15878178d92",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_c52455de5a0fba28631afbd19a948e7b",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell66": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.4084182728616699,
    "padj": 0.00112037063124493,
    "baseMean": 6909.61628096745,
    "baseMean_GroupA": 5707.20896605056,
    "baseMean_GroupB": 15025.8656566565,
    "lfcSE": 0.421067881650362,
    "stat": 3.34487225038731,
    "pvalue": 0.000823204780123955,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_fab23d1f1d7ce2438ef0ca26ee7ccf47_5b1588e2caafb",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_fab23d1f1d7ce2438ef0ca26ee7ccf47",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell69": {
    "experimentId": "GSE45722_sample-a:NEB-kit_sample-c:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.40005594653397,
    "padj": 7.6128465455005495e-56,
    "baseMean": 4982.43166433283,
    "baseMean_GroupA": 8384.14341310033,
    "baseMean_GroupB": 1580.71991556533,
    "lfcSE": 0.150975216911366,
    "stat": -15.8970193627408,
    "pvalue": 6.645210613989759e-57,
    "comparisonDetails": {
      "group_A": "sample-a:NEB-kit",
      "group_B": "sample-c:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:NEB-kit_sample-c:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_7b9d1a82f15ad7bc64a12c06dfbcd901_5b0eb53d3a2fc",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_7b9d1a82f15ad7bc64a12c06dfbcd901",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:NEB-kit vs sample-c:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell68": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.97311250326011,
    "padj": 2.35801549891466e-9,
    "baseMean": 2690.71205639713,
    "baseMean_GroupA": 473.219803903268,
    "baseMean_GroupB": 7680.06962450833,
    "lfcSE": 0.655287400143404,
    "stat": 6.06316022922252,
    "pvalue": 1.3347257541026402e-9,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
      "folderName": "DE__GSE69825_8e3b654021e8075be6e5489a48be8b87_5b1588c58e4bf",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_8e3b654021e8075be6e5489a48be8b87",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell61": {
    "experimentId": "GSE68189_neuronal-stem-cell_embryonic-stem-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.22016264282566,
    "padj": 0.00000237137975266996,
    "baseMean": 2904.16283287239,
    "baseMean_GroupA": 477.528312867988,
    "baseMean_GroupB": 4724.13872287568,
    "lfcSE": 0.642759157155966,
    "stat": 5.00990550966866,
    "pvalue": 5.44567663072417e-7,
    "comparisonDetails": {
      "group_A": "neuronal-stem-cell",
      "group_B": "embryonic-stem-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_neuronal-stem-cell_embryonic-stem-cell_none_1",
      "folderName": "DE__GSE68189_7229959585af91c3ee9e5b2320ef4e95_5b15877f845c5",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_7229959585af91c3ee9e5b2320ef4e95",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "neuronal-stem-cell vs embryonic-stem-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell60": {
    "experimentId": "GSE68189_fibroblast-of-lung_hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -8.34484718438681,
    "padj": 0,
    "baseMean": 96432.389465299,
    "baseMean_GroupA": 192277.658775415,
    "baseMean_GroupB": 587.120155182783,
    "lfcSE": 0.130758154624011,
    "stat": -63.8189427526109,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_5179e043e1016fb7108d9053c513b1a6_5b15879574325",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_5179e043e1016fb7108d9053c513b1a6",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell63": {
    "experimentId": "GSE55362_ADAR1-RNA-binding-mutant-(eaa)_ADAR1-siRNA_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.986527964755692,
    "padj": 0.0467198966541742,
    "baseMean": 88021.9922733935,
    "baseMean_GroupA": 117506.293545483,
    "baseMean_GroupB": 58537.6910013044,
    "lfcSE": 0.381987711967805,
    "stat": -2.5826170158029598,
    "pvalue": 0.00980541040890075,
    "comparisonDetails": {
      "group_A": "ADAR1-RNA-binding-mutant-(eaa)",
      "group_B": "ADAR1-siRNA",
      "datasetId": "GSE55362",
      "covariates": "none",
      "experimentId": "GSE55362_ADAR1-RNA-binding-mutant-(eaa)_ADAR1-siRNA_none_1",
      "folderName": "DE__GSE55362_c0bc0e8e3ae9ac837430b4381fde94e4_5b1589dd48abd",
      "comparisonCondition": "transfection-unit",
      "experimentHash": "GSE55362_c0bc0e8e3ae9ac837430b4381fde94e4",
      "genome": "hg38",
      "allConditions": [
        "transfection-unit"
      ]
    },
    "xkey": "ADAR1-RNA-binding-mutant-(eaa) vs ADAR1-siRNA -- GSE55362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell62": {
    "experimentId": "GSE46579_healthy_Alzheimer's-disease_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.69644154961195,
    "padj": 0.00327197835846254,
    "baseMean": 7762.10585544632,
    "baseMean_GroupA": 10648.31112831,
    "baseMean_GroupB": 6439.26177205048,
    "lfcSE": 0.194632622207455,
    "stat": -3.57823648324291,
    "pvalue": 0.00034592039749125,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "Alzheimer's-disease",
      "datasetId": "GSE46579",
      "covariates": "age-gender",
      "experimentId": "GSE46579_healthy_Alzheimer's-disease_age-gender_1",
      "folderName": "DE__GSE46579_df134f794ccf7aafde8a23028bf7362c_5b16925629900",
      "comparisonCondition": "disease",
      "experimentHash": "GSE46579_df134f794ccf7aafde8a23028bf7362c",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs Alzheimer's-disease -- GSE46579",
    "ykey": "hsa-let-7a-5p"
  },
  "cell54": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.12650769006995,
    "padj": 0.0141248756600915,
    "baseMean": 3002507.72171256,
    "baseMean_GroupA": 4129193.5199467,
    "baseMean_GroupB": 1875821.92347841,
    "lfcSE": 0.360924531606699,
    "stat": -3.12117240979759,
    "pvalue": 0.00180132539200815,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-p53-/-:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_2bc6d1d51122d25fa67cdd7d0c9cc0d9_5b0fcae955e21",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_2bc6d1d51122d25fa67cdd7d0c9cc0d9",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:24-hN/A vs HCT-116-p53-/-:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell53": {
    "experimentId": "GSE68189_mesenchymal-cell_corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.51739964614711,
    "padj": 5.00724310813451e-43,
    "baseMean": 9637.88571729593,
    "baseMean_GroupA": 24627.2769266476,
    "baseMean_GroupB": 1072.51931195214,
    "lfcSE": 0.32307008005053,
    "stat": -13.9827236413863,
    "pvalue": 1.9873441153941997e-44,
    "comparisonDetails": {
      "group_A": "mesenchymal-cell",
      "group_B": "corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_mesenchymal-cell_corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_b36edffb92436312008fcf3577482849_5b1587b665417",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_b36edffb92436312008fcf3577482849",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "mesenchymal-cell vs corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell56": {
    "experimentId": "GSE34137_Merkel-cell-carcinoma_basal-cell-carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.28554639753157,
    "padj": 0.0101023808109155,
    "baseMean": 10830.2455746138,
    "baseMean_GroupA": 8485.48561735189,
    "baseMean_GroupB": 20990.8720560822,
    "lfcSE": 0.416052639161013,
    "stat": 3.08986478279268,
    "pvalue": 0.00200247637454673,
    "comparisonDetails": {
      "group_A": "Merkel-cell-carcinoma",
      "group_B": "basal-cell-carcinoma",
      "datasetId": "GSE34137",
      "covariates": "none",
      "experimentId": "GSE34137_Merkel-cell-carcinoma_basal-cell-carcinoma_none_1",
      "folderName": "DE__GSE34137_ff7f491ab51e17e9df0303c911c06952_5b0eb948a7453",
      "comparisonCondition": "disease",
      "experimentHash": "GSE34137_ff7f491ab51e17e9df0303c911c06952",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "Merkel-cell-carcinoma vs basal-cell-carcinoma -- GSE34137",
    "ykey": "hsa-let-7a-5p"
  },
  "cell55": {
    "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.37859494741144,
    "padj": 1.0242792015475001e-17,
    "baseMean": 393.016379904913,
    "baseMean_GroupA": 217.510948584693,
    "baseMean_GroupB": 568.521811225134,
    "lfcSE": 0.15475511965364,
    "stat": 8.90823483253346,
    "pvalue": 5.18514782526118e-19,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:forebrain-cell",
      "group_B": "ESC-derived-cell-line:hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_aff4bc6b06d1f52a5a8b1912cc211981_5b1587debbbfd",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_aff4bc6b06d1f52a5a8b1912cc211981",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:forebrain-cell vs ESC-derived-cell-line:hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell58": {
    "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.13020059785058,
    "padj": 0.000057687641552758406,
    "baseMean": 2776.21640192877,
    "baseMean_GroupA": 5004.94996363389,
    "baseMean_GroupB": 547.482840223654,
    "lfcSE": 0.740860656622861,
    "stat": -4.2250868228302,
    "pvalue": 0.000023884867362143602,
    "comparisonDetails": {
      "group_A": "H9-cell:embryonic-stem-cell",
      "group_B": "ESC-derived-cell-line:hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_9d62a4b349f7b5ddeaba68f2e2187058_5b1587d582f44",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_9d62a4b349f7b5ddeaba68f2e2187058",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "H9-cell:embryonic-stem-cell vs ESC-derived-cell-line:hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell57": {
    "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.148814802078148,
    "padj": 0.0364920419111934,
    "baseMean": 8769.95390452012,
    "baseMean_GroupA": 9289.96014824186,
    "baseMean_GroupB": 8379.94922172882,
    "lfcSE": 0.0700458878785076,
    "stat": -2.12453302521145,
    "pvalue": 0.0336255975314622,
    "comparisonDetails": {
      "group_A": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500",
      "group_B": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_221f2bec50a1d28da4bc3fdbfd8e2bb1_5b1588873c3f5",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_221f2bec50a1d28da4bc3fdbfd8e2bb1",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500 vs prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell59": {
    "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 7.09622065530267,
    "padj": 0,
    "baseMean": 15368.4492322076,
    "baseMean_GroupA": 222.312445720153,
    "baseMean_GroupB": 30514.5860186951,
    "lfcSE": 0.139455523144089,
    "stat": 50.8851890216687,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:forebrain-cell",
      "group_B": "ESC-derived-cell-line:mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_bb5312daacd973d40d52fa05ea94c7d3_5b1587da57c0c",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_bb5312daacd973d40d52fa05ea94c7d3",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:forebrain-cell vs ESC-derived-cell-line:mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell90": {
    "experimentId": "GSE14362_standard-protocol_size-range-<50nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.13682128409349,
    "padj": 0.00190662047813856,
    "baseMean": 1767.63734743562,
    "baseMean_GroupA": 570.947373979821,
    "baseMean_GroupB": 2565.43066307282,
    "lfcSE": 0.652895592383385,
    "stat": 3.27283766198062,
    "pvalue": 0.00106473611116829,
    "comparisonDetails": {
      "group_A": "standard-protocol",
      "group_B": "size-range-<50nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_standard-protocol_size-range-<50nt_none_1",
      "folderName": "DE__GSE14362_5df352943fb811fcc7a7106e8be2553a_5b0ebb6cc788f",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_5df352943fb811fcc7a7106e8be2553a",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "standard-protocol vs size-range-<50nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell92": {
    "experimentId": "GSE45722_sample-a:NEB-kit_sample-a:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.7528449714413301,
    "padj": 1.8659842726926097e-26,
    "baseMean": 7380.05026466272,
    "baseMean_GroupA": 11389.7169292878,
    "baseMean_GroupB": 3370.38360003763,
    "lfcSE": 0.161660798143475,
    "stat": -10.8427336223199,
    "pvalue": 2.1592103726871702e-27,
    "comparisonDetails": {
      "group_A": "sample-a:NEB-kit",
      "group_B": "sample-a:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:NEB-kit_sample-a:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_cc3b29ae21792d5ee668e053c54f853e_5b0eb53b8931d",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_cc3b29ae21792d5ee668e053c54f853e",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:NEB-kit vs sample-a:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell91": {
    "experimentId": "GSE14362_no-TAP-treatment_cap-captured-eluate-with-heat_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -7.58000516301627,
    "padj": 2.1541013427423398e-73,
    "baseMean": 8923.26846315157,
    "baseMean_GroupA": 14821.7327145863,
    "baseMean_GroupB": 75.5720859994533,
    "lfcSE": 0.410494182113284,
    "stat": -18.4655605202327,
    "pvalue": 3.9094398234888197e-76,
    "comparisonDetails": {
      "group_A": "no-TAP-treatment",
      "group_B": "cap-captured-eluate-with-heat",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_no-TAP-treatment_cap-captured-eluate-with-heat_none_1",
      "folderName": "DE__GSE14362_3b77fa15d2353775ec553271b29f6933_5b0ebbb845e9b",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_3b77fa15d2353775ec553271b29f6933",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "no-TAP-treatment vs cap-captured-eluate-with-heat -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell94": {
    "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_size-range-<50nt_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.2948577239165298,
    "padj": 1.2509509624935701e-18,
    "baseMean": 3918.16423093856,
    "baseMean_GroupA": 509.583936101537,
    "baseMean_GroupB": 5054.3576625509,
    "lfcSE": 0.366016320034605,
    "stat": 9.00194210904316,
    "pvalue": 2.21759488805679e-19,
    "comparisonDetails": {
      "group_A": "CIP-treatment-prior-to-TAP",
      "group_B": "size-range-<50nt",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_size-range-<50nt_none_1",
      "folderName": "DE__GSE14362_b5d628f77f74e993ba0bd18b786fb23e_5b0ebb98b61dd",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_b5d628f77f74e993ba0bd18b786fb23e",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "CIP-treatment-prior-to-TAP vs size-range-<50nt -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell93": {
    "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.06080434299686,
    "padj": 0.00287786023824139,
    "baseMean": 2564912.28172973,
    "baseMean_GroupA": 3474468.72171145,
    "baseMean_GroupB": 1655355.841748,
    "lfcSE": 0.294038441426282,
    "stat": -3.60770631843664,
    "pvalue": 0.000308915869283103,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:control:0-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_0d3c70a9e3dc4186a2ca1abb6bfe6164_5b0fcad27a74a",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_0d3c70a9e3dc4186a2ca1abb6bfe6164",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:control:0-hN/A vs HCT-116-p53-/-:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell96": {
    "experimentId": "GSE58127_HEK293_A549-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 6.80566266865795,
    "padj": 0.000275654109160269,
    "baseMean": 34.3371299049544,
    "baseMean_GroupA": 0.333134808818314,
    "baseMean_GroupB": 85.3431225491586,
    "lfcSE": 1.54303687555737,
    "stat": 4.41056385395822,
    "pvalue": 0.0000103101796672932,
    "comparisonDetails": {
      "group_A": "HEK293",
      "group_B": "A549-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_HEK293_A549-cell_none_1",
      "folderName": "DE__GSE58127_dc2f42c7539259047f087525468d6f2f_5b0fcb18150f4",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_dc2f42c7539259047f087525468d6f2f",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HEK293 vs A549-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell95": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.471382934026884,
    "padj": 0.000749458372416925,
    "baseMean": 317513.740556316,
    "baseMean_GroupA": 265919.368785695,
    "baseMean_GroupB": 369108.112326937,
    "lfcSE": 0.125889755215101,
    "stat": 3.74441060133494,
    "pvalue": 0.000180817743761155,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_ab88e4b3c3270a1af7401bb5df8efdcf_5b158607f2b32",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_ab88e4b3c3270a1af7401bb5df8efdcf",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:48hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell87": {
    "experimentId": "GSE72183_male-organism:second-void-urine:Cells_male-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.20900190383032,
    "padj": 0.0187920299262681,
    "baseMean": 1614.15418558287,
    "baseMean_GroupA": 542.16133891513,
    "baseMean_GroupB": 2686.14703225061,
    "lfcSE": 0.780903294843591,
    "stat": 2.82877779926997,
    "pvalue": 0.00467261284653153,
    "comparisonDetails": {
      "group_A": "male-organism:second-void-urine:Cells",
      "group_B": "male-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:second-void-urine:Cells_male-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_552b4dc1b6c4fc63c4ad60f309db1f97_5b15898cafde3",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_552b4dc1b6c4fc63c4ad60f309db1f97",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:second-void-urine:Cells vs male-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell86": {
    "experimentId": "GSE14362_alternative-primers_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.38326795240669,
    "padj": 0.0251528326685619,
    "baseMean": 77.8146679468074,
    "baseMean_GroupA": 143.756262340227,
    "baseMean_GroupB": 11.8730735533874,
    "lfcSE": 1.26700867050646,
    "stat": -2.67028003135471,
    "pvalue": 0.00757880110974136,
    "comparisonDetails": {
      "group_A": "alternative-primers",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_alternative-primers_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_f5b0b6d84f58c8d75a7f457d14c294ba_5b0ebb8fe4c87",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_f5b0b6d84f58c8d75a7f457d14c294ba",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "alternative-primers vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell89": {
    "experimentId": "GSE62776_no-dox_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.13349114055556,
    "padj": 0.0000231851723429289,
    "baseMean": 230318.457421225,
    "baseMean_GroupA": 535110.374455706,
    "baseMean_GroupB": 27123.8460649044,
    "lfcSE": 0.851824990290751,
    "stat": -4.8525121799311,
    "pvalue": 0.00000121907294382968,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_95da4ee91dc912aa8d74a59f097484c4_5b158a0072817",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_95da4ee91dc912aa8d74a59f097484c4",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell88": {
    "experimentId": "GSE68189_embryonic-stem-cell_mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.53465824888084,
    "padj": 0.000132776572703409,
    "baseMean": 17912.2252638765,
    "baseMean_GroupA": 4994.0075409469,
    "baseMean_GroupB": 30830.4429868061,
    "lfcSE": 0.622345670369331,
    "stat": 4.07274987126792,
    "pvalue": 0.000046461312565253005,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_embryonic-stem-cell_mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_a4f72b31c181ad6adec077780f76fc85_5b15879c7ea3f",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_a4f72b31c181ad6adec077780f76fc85",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell81": {
    "experimentId": "GSE62776_dox-days-0-5_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.47969788405454,
    "padj": 0.0281968228731675,
    "baseMean": 85687.6444643683,
    "baseMean_GroupA": 147033.968224834,
    "baseMean_GroupB": 24341.3207039029,
    "lfcSE": 0.822928783523734,
    "stat": -3.01325938975742,
    "pvalue": 0.00258457901543114,
    "comparisonDetails": {
      "group_A": "dox-days-0-5",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-5_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_1032aba8b86da5d75b7f5f141943cda8_5b158a13a16c7",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_1032aba8b86da5d75b7f5f141943cda8",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-5 vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell80": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.02456626824402,
    "padj": 0.00448881282098374,
    "baseMean": 2551530.24100868,
    "baseMean_GroupA": 3426880.71286984,
    "baseMean_GroupB": 1676179.76914753,
    "lfcSE": 0.296580762392782,
    "stat": -3.45459449216439,
    "pvalue": 0.000551121053509066,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:4-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-p53-/-:irradiation:4-hN/A_none_1",
      "folderName": "DE__GSE50064_bc8550cf90596029ea05a6c30d6bb84f_5b0fcadd98623",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_bc8550cf90596029ea05a6c30d6bb84f",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-p53-/-:irradiation:4-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell83": {
    "experimentId": "GSE67004_cell_exosome_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.977522946528087,
    "padj": 0.00296900699992091,
    "baseMean": 90829.184187373,
    "baseMean_GroupA": 120607.754447216,
    "baseMean_GroupB": 61050.6139275297,
    "lfcSE": 0.319415176766545,
    "stat": -3.06035222378472,
    "pvalue": 0.00221076828917188,
    "comparisonDetails": {
      "group_A": "cell",
      "group_B": "exosome",
      "datasetId": "GSE67004",
      "covariates": "none",
      "experimentId": "GSE67004_cell_exosome_none_1",
      "folderName": "DE__GSE67004_814aaa9f344860d809fd0b74ffe28abb_5b15867b0490c",
      "comparisonCondition": "tissue-part",
      "experimentHash": "GSE67004_814aaa9f344860d809fd0b74ffe28abb",
      "genome": "hg38",
      "allConditions": [
        "tissue-part"
      ]
    },
    "xkey": "cell vs exosome -- GSE67004",
    "ykey": "hsa-let-7a-5p"
  },
  "cell82": {
    "experimentId": "GSE64142_Mycobacterium-bovis-BCG:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.361031019226718,
    "padj": 0.0218795410734596,
    "baseMean": 286054.085682836,
    "baseMean_GroupA": 250232.240534057,
    "baseMean_GroupB": 321875.930831615,
    "lfcSE": 0.13165590743399,
    "stat": 2.7422318243314,
    "pvalue": 0.00610232566567806,
    "comparisonDetails": {
      "group_A": "Mycobacterium-bovis-BCG:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-bovis-BCG:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_2f6f22d6bc053b6fa610a570cfff9d9e_5b158531e073f",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_2f6f22d6bc053b6fa610a570cfff9d9e",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-bovis-BCG:48hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell85": {
    "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.53465824888084,
    "padj": 0.000132776572703409,
    "baseMean": 17912.2252638765,
    "baseMean_GroupA": 4994.0075409469,
    "baseMean_GroupB": 30830.4429868061,
    "lfcSE": 0.622345670369331,
    "stat": 4.07274987126792,
    "pvalue": 0.000046461312565253005,
    "comparisonDetails": {
      "group_A": "H9-cell:embryonic-stem-cell",
      "group_B": "ESC-derived-cell-line:mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_13e6b8e21a669d1ce5106c91a93dea33_5b1587d106ab4",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_13e6b8e21a669d1ce5106c91a93dea33",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "H9-cell:embryonic-stem-cell vs ESC-derived-cell-line:mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell84": {
    "experimentId": "GSE50064_HCT-116-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 0.942952550116248,
    "padj": 0.0470877754366495,
    "baseMean": 259.67205694395,
    "baseMean_GroupA": 174.785270671563,
    "baseMean_GroupB": 344.558843216337,
    "lfcSE": 0.312034271129362,
    "stat": 3.02195187311756,
    "pvalue": 0.00251150475112822,
    "comparisonDetails": {
      "group_A": "HCT-116-cell:control:0-hN/A",
      "group_B": "HCT-116-p53-/-:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell:control:0-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_b78a5bd2bb34d95bde7263dd245851fc_5b0fcaeea0252",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_b78a5bd2bb34d95bde7263dd245851fc",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "HCT-116-cell:control:0-hN/A vs HCT-116-p53-/-:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-2-3p"
  },
  "cell76": {
    "experimentId": "GSE37686_embryonic-stem-cell_ES-derived-Retinal-pigment-epithelium_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 4.16570309484908,
    "padj": 2.32265798547498e-8,
    "baseMean": 31253.2389265499,
    "baseMean_GroupA": 3152.93750869708,
    "baseMean_GroupB": 59353.5403444028,
    "lfcSE": 0.701048704653316,
    "stat": 5.94210226364959,
    "pvalue": 2.8138977647438104e-9,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "ES-derived-Retinal-pigment-epithelium",
      "datasetId": "GSE37686",
      "covariates": "none",
      "experimentId": "GSE37686_embryonic-stem-cell_ES-derived-Retinal-pigment-epithelium_none_1",
      "folderName": "DE__GSE37686_61ef8b3661853c09edd06b7ed0ec284e_5b0fcaa4416e3",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE37686_61ef8b3661853c09edd06b7ed0ec284e",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs ES-derived-Retinal-pigment-epithelium -- GSE37686",
    "ykey": "hsa-let-7a-5p"
  },
  "cell75": {
    "experimentId": "GSE68189_embryonic-stem-cell_midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.56570698800176,
    "padj": 0.00000312560992592099,
    "baseMean": 2976.30456783049,
    "baseMean_GroupA": 5513.93015753688,
    "baseMean_GroupB": 438.678978124106,
    "lfcSE": 0.732244701600559,
    "stat": -4.8695565569921495,
    "pvalue": 0.00000111848967781251,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_embryonic-stem-cell_midbrain-cell_none_1",
      "folderName": "DE__GSE68189_fd72fdd7105c709ae1c1bd3eb1208a58_5b15879eecb96",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_fd72fdd7105c709ae1c1bd3eb1208a58",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell78": {
    "experimentId": "GSE45722_sample-a:illumina-kit_sample-c:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.686735387774864,
    "padj": 0.00096489156475847,
    "baseMean": 1174.9363589724,
    "baseMean_GroupA": 1450.8365342326,
    "baseMean_GroupB": 899.036183712193,
    "lfcSE": 0.195086977929839,
    "stat": -3.52014980734307,
    "pvalue": 0.000431303119936207,
    "comparisonDetails": {
      "group_A": "sample-a:illumina-kit",
      "group_B": "sample-c:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:illumina-kit_sample-c:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_eeb0e510c0779454c2eb641ed9c49d55_5b0eb5350e603",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_eeb0e510c0779454c2eb641ed9c49d55",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:illumina-kit vs sample-c:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell77": {
    "experimentId": "GSE57012_healthy-tissue_diseased-tissue_age_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.522175539988217,
    "padj": 0.0164065947686359,
    "baseMean": 49823.4329219335,
    "baseMean_GroupA": 58807.2349361821,
    "baseMean_GroupB": 40839.630907685,
    "lfcSE": 0.154680151550963,
    "stat": -3.37584062824101,
    "pvalue": 0.000735905339512714,
    "comparisonDetails": {
      "group_A": "healthy-tissue",
      "group_B": "diseased-tissue",
      "datasetId": "GSE57012",
      "covariates": "age",
      "experimentId": "GSE57012_healthy-tissue_diseased-tissue_age_1",
      "folderName": "DE__GSE57012_7947825946c037f34fcdde1c83bf75a4_5b16905fbe43d",
      "comparisonCondition": "tissue-phenotype",
      "experimentHash": "GSE57012_7947825946c037f34fcdde1c83bf75a4",
      "genome": "hg38",
      "allConditions": [
        "tissue-phenotype"
      ]
    },
    "xkey": "healthy-tissue vs diseased-tissue -- GSE57012",
    "ykey": "hsa-let-7a-5p"
  },
  "cell79": {
    "experimentId": "GSE69837_Chlamydia-trachomatis-A2497_Chlamydia-trachomatis_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.2139070385602,
    "padj": 0.000013912816937386301,
    "baseMean": 13985.070417178,
    "baseMean_GroupA": 18883.0304964902,
    "baseMean_GroupB": 8107.51832200337,
    "lfcSE": 0.259581807941328,
    "stat": -4.67639488370686,
    "pvalue": 0.00000291962148517194,
    "comparisonDetails": {
      "group_A": "Chlamydia-trachomatis-A2497",
      "group_B": "Chlamydia-trachomatis",
      "datasetId": "GSE69837",
      "covariates": "none",
      "experimentId": "GSE69837_Chlamydia-trachomatis-A2497_Chlamydia-trachomatis_none_1",
      "folderName": "DE__GSE69837_ce582a87ee1af3bad433dac03876a12f_5b0eb8fb235df",
      "comparisonCondition": "virus",
      "experimentHash": "GSE69837_ce582a87ee1af3bad433dac03876a12f",
      "genome": "hg38",
      "allConditions": [
        "virus"
      ]
    },
    "xkey": "Chlamydia-trachomatis-A2497 vs Chlamydia-trachomatis -- GSE69837",
    "ykey": "hsa-let-7a-5p"
  },
  "cell165": {
    "experimentId": "GSE34137_healthy_Merkel-cell-carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.0001976768,
    "padj": 0.0291709770829392,
    "baseMean": 7721.2785952844,
    "baseMean_GroupA": 13102.4945711491,
    "baseMean_GroupB": 6479.45952393102,
    "lfcSE": 0.388733715204419,
    "stat": -2.57296354208443,
    "pvalue": 0.0100831828733631,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "Merkel-cell-carcinoma",
      "datasetId": "GSE34137",
      "covariates": "none",
      "experimentId": "GSE34137_healthy_Merkel-cell-carcinoma_none_1",
      "folderName": "DE__GSE34137_c059795661ac36edea86c9e3b1d3f102_5b0eb92f8a696",
      "comparisonCondition": "disease",
      "experimentHash": "GSE34137_c059795661ac36edea86c9e3b1d3f102",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs Merkel-cell-carcinoma -- GSE34137",
    "ykey": "hsa-let-7a-5p"
  },
  "cell166": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -8.34484718438681,
    "padj": 0,
    "baseMean": 96432.389465299,
    "baseMean_GroupA": 192277.658775415,
    "baseMean_GroupB": 587.120155182783,
    "lfcSE": 0.130758154624011,
    "stat": -63.8189427526109,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_b63c4aabbc983a6385f95cd15e7ff347_5b1587c9c4b30",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_b63c4aabbc983a6385f95cd15e7ff347",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell163": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-H37Rv:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.484209015780033,
    "padj": 0.0375192114007845,
    "baseMean": 316498.929744013,
    "baseMean_GroupA": 272609.559095649,
    "baseMean_GroupB": 382332.985716559,
    "lfcSE": 0.189446942082858,
    "stat": 2.55590832164637,
    "pvalue": 0.0105911018937645,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-H37Rv:48hN/A",
      "group_B": "staphloccocus-epidermidis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-H37Rv:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
      "folderName": "DE__GSE64142_113366c56520a570316798c7cb350df0_5b1585ad113b7",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_113366c56520a570316798c7cb350df0",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-H37Rv:48hN/A vs staphloccocus-epidermidis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell164": {
    "experimentId": "GSE65752_Mutu_Sav_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.01657906245373,
    "padj": 0.0378108323544154,
    "baseMean": 324468.57259085,
    "baseMean_GroupA": 124650.800264428,
    "baseMean_GroupB": 524286.344917273,
    "lfcSE": 0.788952015747928,
    "stat": 2.5560224477556,
    "pvalue": 0.0105876288437147,
    "comparisonDetails": {
      "group_A": "Mutu",
      "group_B": "Sav",
      "datasetId": "GSE65752",
      "covariates": "none",
      "experimentId": "GSE65752_Mutu_Sav_none_1",
      "folderName": "DE__GSE65752_22cecd7e66d206f03f4e9bd24676b382_5b0eb8182cefd",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE65752_22cecd7e66d206f03f4e9bd24676b382",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "Mutu vs Sav -- GSE65752",
    "ykey": "hsa-let-7a-5p"
  },
  "cell161": {
    "experimentId": "GSE69837_trachoma_carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.954567571378019,
    "padj": 0.00106207065386265,
    "baseMean": 24471.0191233151,
    "baseMean_GroupA": 18384.5829825875,
    "baseMean_GroupB": 35774.4005275235,
    "lfcSE": 0.257625570598096,
    "stat": 3.7052516532498,
    "pvalue": 0.000211180957299249,
    "comparisonDetails": {
      "group_A": "trachoma",
      "group_B": "carcinoma",
      "datasetId": "GSE69837",
      "covariates": "none",
      "experimentId": "GSE69837_trachoma_carcinoma_none_1",
      "folderName": "DE__GSE69837_e6d77e6335c685de36fde2f632e18486_5b0eb8e76a988",
      "comparisonCondition": "disease",
      "experimentHash": "GSE69837_e6d77e6335c685de36fde2f632e18486",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "trachoma vs carcinoma -- GSE69837",
    "ykey": "hsa-let-7a-5p"
  },
  "cell162": {
    "experimentId": "GSE57012_skin:healthy-tissue_epidermis:diseased-tissue_age_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.10915925080064,
    "padj": 0.0078592024824623,
    "baseMean": 39299.4273131844,
    "baseMean_GroupA": 53817.8050989493,
    "baseMean_GroupB": 24781.0495274194,
    "lfcSE": 0.367063075328306,
    "stat": -3.02171295712212,
    "pvalue": 0.00251348770006905,
    "comparisonDetails": {
      "group_A": "skin:healthy-tissue",
      "group_B": "epidermis:diseased-tissue",
      "datasetId": "GSE57012",
      "covariates": "age",
      "experimentId": "GSE57012_skin:healthy-tissue_epidermis:diseased-tissue_age_1",
      "folderName": "DE__GSE57012_f49ea5275587d5687e036cc723f5f537_5b1690632bb06",
      "comparisonCondition": "tissue:tissue-phenotype",
      "experimentHash": "GSE57012_f49ea5275587d5687e036cc723f5f537",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "tissue-phenotype"
      ]
    },
    "xkey": "skin:healthy-tissue vs epidermis:diseased-tissue -- GSE57012",
    "ykey": "hsa-let-7a-5p"
  },
  "cell30": {
    "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.91071480123671,
    "padj": 0.0237162807476027,
    "baseMean": 687.723324589579,
    "baseMean_GroupA": 288.006043147251,
    "baseMean_GroupB": 1387.22856711365,
    "lfcSE": 1.26629336313611,
    "stat": 2.29861016883798,
    "pvalue": 0.0215270856016701,
    "comparisonDetails": {
      "group_A": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_a3a8cb6c5f0f5b000053546dc2a0ec11_5b1588bdb5035",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_a3a8cb6c5f0f5b000053546dc2a0ec11",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell160": {
    "experimentId": "GSE64142_non-infected:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.526662834965797,
    "padj": 0.0072788590307824,
    "baseMean": 228975.660294489,
    "baseMean_GroupA": 270613.154523436,
    "baseMean_GroupB": 187338.166065543,
    "lfcSE": 0.162387547317907,
    "stat": -3.24324644139581,
    "pvalue": 0.00118175945013595,
    "comparisonDetails": {
      "group_A": "non-infected:4hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_77ade4780c5d3221527f971e4889512a_5b158487d282e",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_77ade4780c5d3221527f971e4889512a",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:4hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell21": {
    "experimentId": "GSE68189_fibroblast-of-lung_mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.5601759434962,
    "padj": 9.445994191831948e-85,
    "baseMean": 112639.826848302,
    "baseMean_GroupA": 192642.071066314,
    "baseMean_GroupB": 32637.58263029,
    "lfcSE": 0.130235467849459,
    "stat": -19.6580546434212,
    "pvalue": 4.9328262732092e-86,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_742e4dbf6fe1e11da3ee964d91c894d6_5b158790f1a71",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_742e4dbf6fe1e11da3ee964d91c894d6",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell20": {
    "experimentId": "GSE37686_partially-differentiated-ESC_ES-derived-Retinal-pigment-epithelium_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.74656030442026,
    "padj": 2.7913505931260197e-11,
    "baseMean": 40972.3653241473,
    "baseMean_GroupA": 1108.45348070465,
    "baseMean_GroupB": 67548.306553109,
    "lfcSE": 0.800585643235864,
    "stat": 7.17794573631548,
    "pvalue": 7.07666347553075e-13,
    "comparisonDetails": {
      "group_A": "partially-differentiated-ESC",
      "group_B": "ES-derived-Retinal-pigment-epithelium",
      "datasetId": "GSE37686",
      "covariates": "none",
      "experimentId": "GSE37686_partially-differentiated-ESC_ES-derived-Retinal-pigment-epithelium_none_1",
      "folderName": "DE__GSE37686_78643c962b08fc50b9b4ab4bfee890b8_5b0fcaa641868",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE37686_78643c962b08fc50b9b4ab4bfee890b8",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "partially-differentiated-ESC vs ES-derived-Retinal-pigment-epithelium -- GSE37686",
    "ykey": "hsa-let-7a-5p"
  },
  "cell23": {
    "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.08134393594588,
    "padj": 0.000680309676822971,
    "baseMean": 2252.96373749635,
    "baseMean_GroupA": 4406.13718738752,
    "baseMean_GroupB": 1022.5789089871,
    "lfcSE": 0.577449637124656,
    "stat": -3.60437309530524,
    "pvalue": 0.000312907293215733,
    "comparisonDetails": {
      "group_A": "H9-cell:embryonic-stem-cell",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_H9-cell:embryonic-stem-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_80a4f8c4892621f6eba371adb51e8b0d_5b1587d830c50",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_80a4f8c4892621f6eba371adb51e8b0d",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "H9-cell:embryonic-stem-cell vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell22": {
    "experimentId": "GSE79099_p53-+/+:Inauhzin_p53--/-:dimethyl-sulfoxide_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.493216278205691,
    "padj": 0.0221944779217276,
    "baseMean": 75841.9276766893,
    "baseMean_GroupA": 62793.5024289813,
    "baseMean_GroupB": 88890.3529243973,
    "lfcSE": 0.158405207729844,
    "stat": 3.11363676279418,
    "pvalue": 0.00184796842508862,
    "comparisonDetails": {
      "group_A": "p53-+/+:Inauhzin",
      "group_B": "p53--/-:dimethyl-sulfoxide",
      "datasetId": "GSE79099",
      "covariates": "none",
      "experimentId": "GSE79099_p53-+/+:Inauhzin_p53--/-:dimethyl-sulfoxide_none_1",
      "folderName": "DE__GSE79099_ec7dd3da2b9bb48351f10c6a58cc9c36_5b0ebae35688a",
      "comparisonCondition": "genotype:treatment",
      "experimentHash": "GSE79099_ec7dd3da2b9bb48351f10c6a58cc9c36",
      "genome": "hg38",
      "allConditions": [
        "genotype",
        "treatment"
      ]
    },
    "xkey": "p53-+/+:Inauhzin vs p53--/-:dimethyl-sulfoxide -- GSE79099",
    "ykey": "hsa-let-7a-5p"
  },
  "cell25": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -9.68560246225763,
    "padj": 0,
    "baseMean": 98405.670846298,
    "baseMean_GroupA": 196572.922092672,
    "baseMean_GroupB": 238.419599924228,
    "lfcSE": 0.0901637251531687,
    "stat": -107.422385730003,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:forebrain-cell_none_1",
      "folderName": "DE__GSE68189_9e625d8892a819450d2db06053fc5dc9_5b1587c34e0a4",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_9e625d8892a819450d2db06053fc5dc9",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell169": {
    "experimentId": "GSE45722_sample-c:NEB-kit_sample-c:bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.8751811764164201,
    "padj": 5.51419677119215e-28,
    "baseMean": 3387.34246660657,
    "baseMean_GroupA": 5328.60015116816,
    "baseMean_GroupB": 1446.08478204499,
    "lfcSE": 0.168303713640838,
    "stat": -11.1416506258328,
    "pvalue": 7.86479988198881e-29,
    "comparisonDetails": {
      "group_A": "sample-c:NEB-kit",
      "group_B": "sample-c:bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-c:NEB-kit_sample-c:bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_ca8dbd6388e307bf6170ac7ec3cf3567_5b0eb52ab44d6",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_ca8dbd6388e307bf6170ac7ec3cf3567",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-c:NEB-kit vs sample-c:bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell24": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.28233813257212,
    "padj": 0.000010983554714712601,
    "baseMean": 2460856.91709551,
    "baseMean_GroupA": 3492970.63711361,
    "baseMean_GroupB": 1428743.1970774,
    "lfcSE": 0.262645474350913,
    "stat": -4.8823918848828995,
    "pvalue": 0.0000010480670523666699,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_f396670c21dc63b11467a204ad8c03d7_5b0fcada1bd17",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_f396670c21dc63b11467a204ad8c03d7",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell27": {
    "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 4.44277816487478,
    "padj": 0.000497713011920531,
    "baseMean": 1746.13412748944,
    "baseMean_GroupA": 292.05384423298,
    "baseMean_GroupB": 4290.77462318824,
    "lfcSE": 1.26127846007796,
    "stat": 3.52244036943291,
    "pvalue": 0.000427593222948302,
    "comparisonDetails": {
      "group_A": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_8115886f46c8bea37c71cdaf6f0074cd_5b1588b80a76e",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_8115886f46c8bea37c71cdaf6f0074cd",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell167": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.80413416907688,
    "padj": 5.34234949801343e-39,
    "baseMean": 34.1711703806644,
    "baseMean_GroupA": 10.8062520915275,
    "baseMean_GroupB": 104.265925248075,
    "lfcSE": 0.288737257861691,
    "stat": 13.1750720265519,
    "pvalue": 1.2211084566887799e-39,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "group_B": "prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_12904ee46baa84bfe7c99753740c62f1_5b1588e8973f6",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_12904ee46baa84bfe7c99753740c62f1",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq vs prefrontal-cortex:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell26": {
    "experimentId": "GSE21279_hemangioma-of-liver:healthy-tissue_hepatitis-B:distal-diseased-tissue_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.5720631062182,
    "padj": 0.0108765571219584,
    "baseMean": 216635.075691115,
    "baseMean_GroupA": 328511.720633753,
    "baseMean_GroupB": 48820.1082771587,
    "lfcSE": 0.7001737317218,
    "stat": -3.67346415566495,
    "pvalue": 0.000239284256683084,
    "comparisonDetails": {
      "group_A": "hemangioma-of-liver:healthy-tissue",
      "group_B": "hepatitis-B:distal-diseased-tissue",
      "datasetId": "GSE21279",
      "covariates": "none",
      "experimentId": "GSE21279_hemangioma-of-liver:healthy-tissue_hepatitis-B:distal-diseased-tissue_none_1",
      "folderName": "DE__GSE21279_3cba857c59882eb33e3b935bd85911fd_5b0ebbf839692",
      "comparisonCondition": "disease:tissue-phenotype",
      "experimentHash": "GSE21279_3cba857c59882eb33e3b935bd85911fd",
      "genome": "hg38",
      "allConditions": [
        "disease",
        "tissue-phenotype"
      ]
    },
    "xkey": "hemangioma-of-liver:healthy-tissue vs hepatitis-B:distal-diseased-tissue -- GSE21279",
    "ykey": "hsa-let-7a-5p"
  },
  "cell168": {
    "experimentId": "GSE62776_dox-days-0-10_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.08219970589,
    "padj": 0.0103401692508419,
    "baseMean": 94204.2826157834,
    "baseMean_GroupA": 154664.090102272,
    "baseMean_GroupB": 33744.4751292946,
    "lfcSE": 0.610104902497738,
    "stat": -3.41285522762656,
    "pvalue": 0.000642860833385673,
    "comparisonDetails": {
      "group_A": "dox-days-0-10",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-10_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_78085df089de6c2fee21a88b9109fe36_5b158a1877ed5",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_78085df089de6c2fee21a88b9109fe36",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-10 vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell29": {
    "experimentId": "GSE64142_non-infected:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.318430039796715,
    "padj": 0.0311330813398758,
    "baseMean": 294272.141589809,
    "baseMean_GroupA": 261699.362659725,
    "baseMean_GroupB": 326844.920519893,
    "lfcSE": 0.117711150879619,
    "stat": 2.70518160273846,
    "pvalue": 0.00682670572262529,
    "comparisonDetails": {
      "group_A": "non-infected:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:48hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_2464e8f9e657f15be20c68a60230fd01_5b158499edc00",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_2464e8f9e657f15be20c68a60230fd01",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:48hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell28": {
    "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_cap-captured-eluate-with-heat_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.08100753276187,
    "padj": 0.00315864078295997,
    "baseMean": 192.200899390613,
    "baseMean_GroupA": 277.445122183546,
    "baseMean_GroupB": 64.3345652012138,
    "lfcSE": 0.636065583131398,
    "stat": -3.27168705232708,
    "pvalue": 0.00106907841884799,
    "comparisonDetails": {
      "group_A": "CIP-treatment-prior-to-TAP",
      "group_B": "cap-captured-eluate-with-heat",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_CIP-treatment-prior-to-TAP_cap-captured-eluate-with-heat_none_1",
      "folderName": "DE__GSE14362_909b82fb72adad49d6906bf11beb8c32_5b0ebba3de1cd",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_909b82fb72adad49d6906bf11beb8c32",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "CIP-treatment-prior-to-TAP vs cap-captured-eluate-with-heat -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell176": {
    "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.849882358030682,
    "padj": 2.44664704534229e-7,
    "baseMean": 347.699194146126,
    "baseMean_GroupA": 246.954480286906,
    "baseMean_GroupB": 448.443908005346,
    "lfcSE": 0.153415522447403,
    "stat": 5.53974164069386,
    "pvalue": 3.0291820561380694e-8,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:forebrain-cell",
      "group_B": "ESC-derived-cell-line:midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:midbrain-cell_none_1",
      "folderName": "DE__GSE68189_ee8ff40113a4d0ca8b97807efb387afc_5b1587dc8e978",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_ee8ff40113a4d0ca8b97807efb387afc",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:forebrain-cell vs ESC-derived-cell-line:midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell177": {
    "experimentId": "GSE72193_Placebo:14day_Diphencyprone:3day_age-gender_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.11320473036628,
    "padj": 0.000362063613056108,
    "baseMean": 232484.735807751,
    "baseMean_GroupA": 319642.502230887,
    "baseMean_GroupB": 145326.969384615,
    "lfcSE": 0.251977438931302,
    "stat": -4.417874612456,
    "pvalue": 0.000009967621429118431,
    "comparisonDetails": {
      "group_A": "Placebo:14day",
      "group_B": "Diphencyprone:3day",
      "datasetId": "GSE72193",
      "covariates": "age-gender",
      "experimentId": "GSE72193_Placebo:14day_Diphencyprone:3day_age-gender_1",
      "folderName": "DE__GSE72193_5a977044ba8c123b1143f7429fe31bb3_5b168f6d6af7f",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE72193_5a977044ba8c123b1143f7429fe31bb3",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Placebo:14day vs Diphencyprone:3day -- GSE72193",
    "ykey": "hsa-let-7a-5p"
  },
  "cell174": {
    "experimentId": "GSE68189_fibroblast-of-lung_midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -8.86389292010762,
    "padj": 0,
    "baseMean": 109359.365358036,
    "baseMean_GroupA": 218250.362523034,
    "baseMean_GroupB": 468.368193037902,
    "lfcSE": 0.146158662666501,
    "stat": -60.6456898167773,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "fibroblast-of-lung",
      "group_B": "midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_fibroblast-of-lung_midbrain-cell_none_1",
      "folderName": "DE__GSE68189_de7a43dad8718dbf59fa00ae423fa9e9_5b1587934ddbb",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_de7a43dad8718dbf59fa00ae423fa9e9",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "fibroblast-of-lung vs midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell175": {
    "experimentId": "GSE69837_healthy_carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.08481212544858,
    "padj": 0.00000899065852069281,
    "baseMean": 20307.5946995342,
    "baseMean_GroupA": 13310.0923247939,
    "baseMean_GroupB": 28304.740270666,
    "lfcSE": 0.229725781938234,
    "stat": 4.72220451834288,
    "pvalue": 0.0000023330189832177598,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "carcinoma",
      "datasetId": "GSE69837",
      "covariates": "none",
      "experimentId": "GSE69837_healthy_carcinoma_none_1",
      "folderName": "DE__GSE69837_e58ddf107702e08f4049a49980e44890_5b0eb8eb59b46",
      "comparisonCondition": "disease",
      "experimentHash": "GSE69837_e58ddf107702e08f4049a49980e44890",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs carcinoma -- GSE69837",
    "ykey": "hsa-let-7a-5p"
  },
  "cell172": {
    "experimentId": "GSE64142_Mycobacterium-bovis-BCG:48hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.367086995282798,
    "padj": 0.0169981899891644,
    "baseMean": 234264.532840437,
    "baseMean_GroupA": 264263.394210978,
    "baseMean_GroupB": 204265.671469896,
    "lfcSE": 0.112333322979161,
    "stat": -3.26783705446778,
    "pvalue": 0.00108372739668908,
    "comparisonDetails": {
      "group_A": "Mycobacterium-bovis-BCG:48hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-bovis-BCG:48hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_be422441979c16f0b89103509253cd40_5b158537de895",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_be422441979c16f0b89103509253cd40",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-bovis-BCG:48hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell173": {
    "experimentId": "GSE64142_Mycobacterium-bovis-BCG:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.425724855654307,
    "padj": 0.00711188233827905,
    "baseMean": 294292.36219325,
    "baseMean_GroupA": 337605.630916538,
    "baseMean_GroupB": 250979.093469961,
    "lfcSE": 0.135618127179446,
    "stat": -3.13914418749495,
    "pvalue": 0.00169442051326855,
    "comparisonDetails": {
      "group_A": "Mycobacterium-bovis-BCG:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-bovis-BCG:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_164f40e3c4ab1afc10084807424407ad_5b1584d38b703",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_164f40e3c4ab1afc10084807424407ad",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-bovis-BCG:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell170": {
    "experimentId": "GSE68085_healthy_invasive-ductal-carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.96485863334166,
    "padj": 1.2466802719729699e-25,
    "baseMean": 8458.89809646347,
    "baseMean_GroupA": 25855.6936408114,
    "baseMean_GroupB": 6600.98789269817,
    "lfcSE": 0.184293157782029,
    "stat": -10.6615929586793,
    "pvalue": 1.53937911843619e-26,
    "comparisonDetails": {
      "group_A": "healthy",
      "group_B": "invasive-ductal-carcinoma",
      "datasetId": "GSE68085",
      "covariates": "none",
      "experimentId": "GSE68085_healthy_invasive-ductal-carcinoma_none_1",
      "folderName": "DE__GSE68085_64bbf35fc4061ef6e822b9884ca50fa1_5b1586b24c409",
      "comparisonCondition": "disease",
      "experimentHash": "GSE68085_64bbf35fc4061ef6e822b9884ca50fa1",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "healthy vs invasive-ductal-carcinoma -- GSE68085",
    "ykey": "hsa-let-7a-5p"
  },
  "cell171": {
    "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.5174029478559001,
    "padj": 0.000876213277951305,
    "baseMean": 7413.28449571646,
    "baseMean_GroupA": 5992.77449923969,
    "baseMean_GroupB": 17001.7269719346,
    "lfcSE": 0.441250127315692,
    "stat": 3.43887254398519,
    "pvalue": 0.00058414218530087,
    "comparisonDetails": {
      "group_A": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_1006f49e333aaee158ba75583c8caf8e_5b1588d934912",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_1006f49e333aaee158ba75583c8caf8e",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell10": {
    "experimentId": "GSE64142_Mycobacterium-bovis-BCG:18hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.487397581858668,
    "padj": 0.00263291710293924,
    "baseMean": 305872.81773255,
    "baseMean_GroupA": 254122.612105414,
    "baseMean_GroupB": 357623.023359685,
    "lfcSE": 0.13124430985973,
    "stat": 3.71366638583862,
    "pvalue": 0.000204278051090113,
    "comparisonDetails": {
      "group_A": "Mycobacterium-bovis-BCG:18hN/A",
      "group_B": "Yersinia-pseudotuberculosis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-bovis-BCG:18hN/A_Yersinia-pseudotuberculosis:4hN/A_none_1",
      "folderName": "DE__GSE64142_46af595d0fde7932846f3c78fdda9c07_5b1585016af0e",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_46af595d0fde7932846f3c78fdda9c07",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-bovis-BCG:18hN/A vs Yersinia-pseudotuberculosis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell12": {
    "experimentId": "GSE45722_NEB-kit_bioo-scientific-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.6363521548009499,
    "padj": 5.8173858884587e-9,
    "baseMean": 4611.20633421036,
    "baseMean_GroupA": 6991.35021028241,
    "baseMean_GroupB": 2231.06245813831,
    "lfcSE": 0.269580649734378,
    "stat": -6.06999113776629,
    "pvalue": 1.27917308751823e-9,
    "comparisonDetails": {
      "group_A": "NEB-kit",
      "group_B": "bioo-scientific-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_NEB-kit_bioo-scientific-kit_none_1",
      "folderName": "DE__GSE45722_25fc5786dc092da258061fe8ab49596b_5b0eb51f1f0c1",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE45722_25fc5786dc092da258061fe8ab49596b",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "NEB-kit vs bioo-scientific-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell11": {
    "experimentId": "GSE72183_male-organism:first-void-urine:Cells_male-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.98541205753177,
    "padj": 0.0444339000156288,
    "baseMean": 1409.15888550292,
    "baseMean_GroupA": 532.03749399101,
    "baseMean_GroupB": 2286.28027701482,
    "lfcSE": 0.818339872431056,
    "stat": 2.42614606035714,
    "pvalue": 0.0152601272780947,
    "comparisonDetails": {
      "group_A": "male-organism:first-void-urine:Cells",
      "group_B": "male-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:first-void-urine:Cells_male-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_c91ac31768e54872f7bc0116804897b6_5b1589508e6e4",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_c91ac31768e54872f7bc0116804897b6",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:first-void-urine:Cells vs male-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell14": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.81794507260721,
    "padj": 0.00643137903134581,
    "baseMean": 121214.190357819,
    "baseMean_GroupA": 214098.569431021,
    "baseMean_GroupB": 28329.8112846163,
    "lfcSE": 0.836453542523508,
    "stat": -3.36892000493622,
    "pvalue": 0.000754633269005692,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_21859766c108845e22c092297a8c24ba_5b158a0b45902",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_21859766c108845e22c092297a8c24ba",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell13": {
    "experimentId": "GSE72183_male-organism:second-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.26505453645092,
    "padj": 0.0188429305165544,
    "baseMean": 2349.53485167374,
    "baseMean_GroupA": 772.753825843256,
    "baseMean_GroupB": 3926.31587750422,
    "lfcSE": 0.81977235098772,
    "stat": 2.76302870391008,
    "pvalue": 0.00572677300012928,
    "comparisonDetails": {
      "group_A": "male-organism:second-void-urine:Cells",
      "group_B": "female-organism:first-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:second-void-urine:Cells_female-organism:first-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_fa2edc49303bdeecd49a511193cb6629_5b15898488452",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_fa2edc49303bdeecd49a511193cb6629",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:second-void-urine:Cells vs female-organism:first-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell16": {
    "experimentId": "GSE31037_healthy:healthy-tissue_psoriasis:diseased-tissue_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.496349365941772,
    "padj": 0.0419936832282318,
    "baseMean": 562307.631221616,
    "baseMean_GroupA": 672670.809891618,
    "baseMean_GroupB": 470338.315663281,
    "lfcSE": 0.20167014243023,
    "stat": -2.4611941061800398,
    "pvalue": 0.0138475425182281,
    "comparisonDetails": {
      "group_A": "healthy:healthy-tissue",
      "group_B": "psoriasis:diseased-tissue",
      "datasetId": "GSE31037",
      "covariates": "none",
      "experimentId": "GSE31037_healthy:healthy-tissue_psoriasis:diseased-tissue_none_1",
      "folderName": "DE__GSE31037_d65460632e060ff7902d08a95547b1f4_5b0eb9fcbac0d",
      "comparisonCondition": "disease:tissue-phenotype",
      "experimentHash": "GSE31037_d65460632e060ff7902d08a95547b1f4",
      "genome": "hg38",
      "allConditions": [
        "disease",
        "tissue-phenotype"
      ]
    },
    "xkey": "healthy:healthy-tissue vs psoriasis:diseased-tissue -- GSE31037",
    "ykey": "hsa-let-7a-5p"
  },
  "cell178": {
    "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.503846563380743,
    "padj": 0.00436084877008755,
    "baseMean": 270203.068667958,
    "baseMean_GroupA": 317220.125325703,
    "baseMean_GroupB": 223186.012010213,
    "lfcSE": 0.15278752059203,
    "stat": -3.29769448072991,
    "pvalue": 0.000974821401611441,
    "comparisonDetails": {
      "group_A": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_cb3baec5a75ba457a0d7d0cbfca735fc_5b1585d9968f4",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_cb3baec5a75ba457a0d7d0cbfca735fc",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "Mycobacterium-tuberculosis-str.-Beijing/GC1237:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell15": {
    "experimentId": "GSE62776_dox-days-0-2_dox-days-0-20_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.95901280771835,
    "padj": 0.00281267362174365,
    "baseMean": 129270.88420014,
    "baseMean_GroupA": 274584.863410598,
    "baseMean_GroupB": 32394.8980598347,
    "lfcSE": 0.805921848377326,
    "stat": -3.67158777699865,
    "pvalue": 0.000241048260196781,
    "comparisonDetails": {
      "group_A": "dox-days-0-2",
      "group_B": "dox-days-0-20",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-2_dox-days-0-20_none_1",
      "folderName": "DE__GSE62776_8f4fa4992180d5585c71a24b730dddd6_5b158a098315e",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_8f4fa4992180d5585c71a24b730dddd6",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-2 vs dox-days-0-20 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell179": {
    "experimentId": "GSE62776_dox-days-0-10_dox-days-0-24_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.92853643847185,
    "padj": 0.0126759855651047,
    "baseMean": 89431.0216074961,
    "baseMean_GroupA": 127764.443353532,
    "baseMean_GroupB": 31930.8889884424,
    "lfcSE": 0.604258538150501,
    "stat": -3.19157499102066,
    "pvalue": 0.00141499373750006,
    "comparisonDetails": {
      "group_A": "dox-days-0-10",
      "group_B": "dox-days-0-24",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_dox-days-0-10_dox-days-0-24_none_1",
      "folderName": "DE__GSE62776_1b6433c32038f7fc1c2dda7f8755f91a_5b158a1a306c2",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_1b6433c32038f7fc1c2dda7f8755f91a",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "dox-days-0-10 vs dox-days-0-24 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell18": {
    "experimentId": "GSE64142_non-infected:48hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.387435573826883,
    "padj": 0.00526244356711173,
    "baseMean": 234272.196751108,
    "baseMean_GroupA": 265728.119250806,
    "baseMean_GroupB": 202816.274251409,
    "lfcSE": 0.112106921217558,
    "stat": -3.4559469622309598,
    "pvalue": 0.000548363208832133,
    "comparisonDetails": {
      "group_A": "non-infected:48hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:48hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_4cf44a40240e55f85f4ba9c6ed8586f5_5b158491808f2",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_4cf44a40240e55f85f4ba9c6ed8586f5",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:48hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell17": {
    "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.51727406168862,
    "padj": 7.63338822149939e-16,
    "baseMean": 737.037939330322,
    "baseMean_GroupA": 178.081792121721,
    "baseMean_GroupB": 1056.44145202095,
    "lfcSE": 0.296510562462137,
    "stat": 8.48966067443236,
    "pvalue": 2.07241309180979e-17,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:forebrain-cell",
      "group_B": "ESC-derived-cell-line:corin-positive-ventral-midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:forebrain-cell_ESC-derived-cell-line:corin-positive-ventral-midbrain-cell_none_1",
      "folderName": "DE__GSE68189_7c26c6bfd10f512179b5b4e8bb8fdc3e_5b1587e203a05",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_7c26c6bfd10f512179b5b4e8bb8fdc3e",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:forebrain-cell vs ESC-derived-cell-line:corin-positive-ventral-midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell19": {
    "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.52355215202116,
    "padj": 0.00172855398841445,
    "baseMean": 287818.074701991,
    "baseMean_GroupA": 339748.771767487,
    "baseMean_GroupB": 235887.377636496,
    "lfcSE": 0.146607245949145,
    "stat": -3.57112057205392,
    "pvalue": 0.000355457204914415,
    "comparisonDetails": {
      "group_A": "salmonella-typhimurium-strain-keller:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_96c2308adc27412c1b682b28322aeef4_5b158622b96e5",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_96c2308adc27412c1b682b28322aeef4",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "salmonella-typhimurium-strain-keller:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell180": {
    "experimentId": "GSE45722_sample-a:NEB-kit_sample-b:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.864258358035646,
    "padj": 2.2627716803896897e-33,
    "baseMean": 8943.80622709751,
    "baseMean_GroupA": 11550.660486408,
    "baseMean_GroupB": 6336.95196778699,
    "lfcSE": 0.0704178838962016,
    "stat": -12.27327931793,
    "pvalue": 1.26042984849293e-34,
    "comparisonDetails": {
      "group_A": "sample-a:NEB-kit",
      "group_B": "sample-b:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:NEB-kit_sample-b:NEB-kit_none_1",
      "folderName": "DE__GSE45722_a6995e447fdca023d4fd28b6cdcdbd28_5b0eb53ec3137",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_a6995e447fdca023d4fd28b6cdcdbd28",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:NEB-kit vs sample-b:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell187": {
    "experimentId": "GSE72183_female-organism:first-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.60866485472839,
    "padj": 0.0347447774538241,
    "baseMean": 1505.19175211634,
    "baseMean_GroupA": 722.794564228946,
    "baseMean_GroupB": 2287.58894000374,
    "lfcSE": 0.683586747416855,
    "stat": 2.35327097958998,
    "pvalue": 0.018609067246455,
    "comparisonDetails": {
      "group_A": "female-organism:first-void-urine:Cells",
      "group_B": "male-organism:second-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_female-organism:first-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_78f5412054f8574baad5e6a3732efe43_5b158938521a9",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_78f5412054f8574baad5e6a3732efe43",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "female-organism:first-void-urine:Cells vs male-organism:second-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell188": {
    "experimentId": "GSE50064_HCT-116-cell:irradiation:4-hN/A_HCT-116-p53-/-:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.692300146983381,
    "padj": 0.0437943820798787,
    "baseMean": 2209178.34965909,
    "baseMean_GroupA": 1673362.40763753,
    "baseMean_GroupB": 2744994.29168065,
    "lfcSE": 0.227308899387546,
    "stat": 3.04563591152257,
    "pvalue": 0.00232188812005882,
    "comparisonDetails": {
      "group_A": "HCT-116-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-p53-/-:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_HCT-116-cell:irradiation:4-hN/A_HCT-116-p53-/-:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_a588dbf1c90a5839d7d4d1ccd518df8e_5b0fcaf8b167b",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_a588dbf1c90a5839d7d4d1ccd518df8e",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "HCT-116-cell:irradiation:4-hN/A vs HCT-116-p53-/-:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell185": {
    "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.20888261904982,
    "padj": 2.73783159761778e-14,
    "baseMean": 5142.9065947813,
    "baseMean_GroupA": 6878.40736232221,
    "baseMean_GroupB": 2973.53063535517,
    "lfcSE": 0.156597265028816,
    "stat": -7.71969177640087,
    "pvalue": 1.16611345824461e-14,
    "comparisonDetails": {
      "group_A": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_bafb8a727928c5ee25e6d37081e51c17_5b1588a5b76bb",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_bafb8a727928c5ee25e6d37081e51c17",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:pippin-prep-automated-gel-system-(sage-3%):Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell186": {
    "experimentId": "GSE14362_size-range-50-200nt_cap-captured-eluate-with-heat_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.26479276715471,
    "padj": 0.00000912819935426336,
    "baseMean": 379.797059843148,
    "baseMean_GroupA": 689.765888907081,
    "baseMean_GroupB": 69.8282307792152,
    "lfcSE": 0.698941854850105,
    "stat": -4.67105059526716,
    "pvalue": 0.0000029966311011470596,
    "comparisonDetails": {
      "group_A": "size-range-50-200nt",
      "group_B": "cap-captured-eluate-with-heat",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_size-range-50-200nt_cap-captured-eluate-with-heat_none_1",
      "folderName": "DE__GSE14362_f47330dfa0a55fbcc6ca729c5a05e226_5b0ebbd590ab5",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_f47330dfa0a55fbcc6ca729c5a05e226",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "size-range-50-200nt vs cap-captured-eluate-with-heat -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell50": {
    "experimentId": "GSE62776_no-dox_dox-days-0-10_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.0277329680484,
    "padj": 0.0000289100881176733,
    "baseMean": 296272.889098562,
    "baseMean_GroupA": 544418.200161175,
    "baseMean_GroupB": 130842.681723487,
    "lfcSE": 0.439493243134567,
    "stat": -4.61379782220573,
    "pvalue": 0.00000395377040581033,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-10",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-10_none_1",
      "folderName": "DE__GSE62776_93860658856b17818835209e2c747b48_5b1589fbd2160",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_93860658856b17818835209e2c747b48",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-10 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell183": {
    "experimentId": "GSE68085_invasive-ductal-carcinoma:Luminal-A-Breast-Carcinoma:diseased-tissue_invasive-ductal-carcinoma:TNBC:diseased-tissue_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.42634299860263,
    "padj": 0.00519236733009048,
    "baseMean": 7361.58825608353,
    "baseMean_GroupA": 8023.61476216998,
    "baseMean_GroupB": 5946.22124307113,
    "lfcSE": 0.127498259305705,
    "stat": -3.34391230848398,
    "pvalue": 0.000826058438878031,
    "comparisonDetails": {
      "group_A": "invasive-ductal-carcinoma:Luminal-A-Breast-Carcinoma:diseased-tissue",
      "group_B": "invasive-ductal-carcinoma:TNBC:diseased-tissue",
      "datasetId": "GSE68085",
      "covariates": "none",
      "experimentId": "GSE68085_invasive-ductal-carcinoma:Luminal-A-Breast-Carcinoma:diseased-tissue_invasive-ductal-carcinoma:TNBC:diseased-tissue_none_1",
      "folderName": "DE__GSE68085_736b8129a0f9fdf633aa9131af376e76_5b158733b5cd4",
      "comparisonCondition": "disease:disease-details:tissue-phenotype",
      "experimentHash": "GSE68085_736b8129a0f9fdf633aa9131af376e76",
      "genome": "hg38",
      "allConditions": [
        "disease",
        "disease-details",
        "tissue-phenotype"
      ]
    },
    "xkey": "invasive-ductal-carcinoma:Luminal-A-Breast-Carcinoma:diseased-tissue vs invasive-ductal-carcinoma:TNBC:diseased-tissue -- GSE68085",
    "ykey": "hsa-let-7a-5p"
  },
  "cell184": {
    "experimentId": "GSE69825_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.65521135644073,
    "padj": 3.2138838492111e-7,
    "baseMean": 4293.75132625426,
    "baseMean_GroupA": 5253.95919629412,
    "baseMean_GroupB": 3333.5434562144,
    "lfcSE": 0.123781002526197,
    "stat": -5.29331111453924,
    "pvalue": 1.20121304900932e-7,
    "comparisonDetails": {
      "group_A": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_fff3c86ec50cdd07ae89a1dcb92edf32_5b1588f931a72",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_fff3c86ec50cdd07ae89a1dcb92edf32",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "heart:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell52": {
    "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.45315039874865,
    "padj": 0.000541190249583797,
    "baseMean": 2733194.6562708,
    "baseMean_GroupA": 4019217.81314659,
    "baseMean_GroupB": 1447171.499395,
    "lfcSE": 0.356654033397512,
    "stat": -4.07439777115608,
    "pvalue": 0.0000461335724989246,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:control:0-hN/A",
      "group_B": "HCT-116-cell:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:control:0-hN/A_HCT-116-cell:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_245bc1118093b9fdd96d4cc0b06a21ff_5b0fcacaedb35",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_245bc1118093b9fdd96d4cc0b06a21ff",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:control:0-hN/A vs HCT-116-cell:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell181": {
    "experimentId": "GSE14362_standard-protocol_cap-captured-eluate-with-TAP_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.75687883298687,
    "padj": 0.0304990982644481,
    "baseMean": 125.266328594775,
    "baseMean_GroupA": 164.160839548349,
    "baseMean_GroupB": 8.58279573405274,
    "lfcSE": 1.23084920245392,
    "stat": -3.05226572475073,
    "pvalue": 0.00227120944522486,
    "comparisonDetails": {
      "group_A": "standard-protocol",
      "group_B": "cap-captured-eluate-with-TAP",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_standard-protocol_cap-captured-eluate-with-TAP_none_1",
      "folderName": "DE__GSE14362_1f3a60e95e9f62d815fa529369f01093_5b0ebb7523d5b",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_1f3a60e95e9f62d815fa529369f01093",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "standard-protocol vs cap-captured-eluate-with-TAP -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell51": {
    "experimentId": "GSE72183_male-organism:second-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.11429793262684,
    "padj": 0.000979840748714154,
    "baseMean": 993.884748594445,
    "baseMean_GroupA": 363.796509377771,
    "baseMean_GroupB": 1623.97298781112,
    "lfcSE": 0.587393676728726,
    "stat": 3.59945640613916,
    "pvalue": 0.000318883083901056,
    "comparisonDetails": {
      "group_A": "male-organism:second-void-urine:Cells",
      "group_B": "male-organism:second-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:second-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_43b2f5c056705ded13503468ad4355f7_5b1589990e7ae",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_43b2f5c056705ded13503468ad4355f7",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:second-void-urine:Cells vs male-organism:second-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell182": {
    "experimentId": "GSE64142_4hN/A_18hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.243250733382393,
    "padj": 0.0000053939342287636606,
    "baseMean": 284362.034230368,
    "baseMean_GroupA": 308328.841338841,
    "baseMean_GroupB": 260395.227121895,
    "lfcSE": 0.0498237360936241,
    "stat": -4.88222587172707,
    "pvalue": 0.00000104895004448731,
    "comparisonDetails": {
      "group_A": "4hN/A",
      "group_B": "18hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_4hN/A_18hN/A_none_1",
      "folderName": "DE__GSE64142_f860792e91d759fa8a1da40f2332cbe7_5b0fcbea2b4be",
      "comparisonCondition": "time",
      "experimentHash": "GSE64142_f860792e91d759fa8a1da40f2332cbe7",
      "genome": "hg38",
      "allConditions": [
        "time"
      ]
    },
    "xkey": "4hN/A vs 18hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell43": {
    "experimentId": "GSE64142_non-infected:18hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.363430438833534,
    "padj": 0.0463361144365564,
    "baseMean": 229824.88346723,
    "baseMean_GroupA": 258793.335646405,
    "baseMean_GroupB": 200856.431288055,
    "lfcSE": 0.141925825046822,
    "stat": -2.56070689540566,
    "pvalue": 0.0104459444348672,
    "comparisonDetails": {
      "group_A": "non-infected:18hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:18hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_fe636cbbfa13037e6ee0e67ab5d75abc_5b0fcc862640c",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_fe636cbbfa13037e6ee0e67ab5d75abc",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:18hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell42": {
    "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:mesenchymal-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.5601759434962,
    "padj": 9.445994191831948e-85,
    "baseMean": 112639.826848302,
    "baseMean_GroupA": 192642.071066314,
    "baseMean_GroupB": 32637.58263029,
    "lfcSE": 0.130235467849459,
    "stat": -19.6580546434212,
    "pvalue": 4.9328262732092e-86,
    "comparisonDetails": {
      "group_A": "hFL1s:fibroblast-of-lung",
      "group_B": "ESC-derived-cell-line:mesenchymal-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s:fibroblast-of-lung_ESC-derived-cell-line:mesenchymal-cell_none_1",
      "folderName": "DE__GSE68189_99e0059a42c40e4792ca2be63b414828_5b1587c5739c5",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_99e0059a42c40e4792ca2be63b414828",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "hFL1s:fibroblast-of-lung vs ESC-derived-cell-line:mesenchymal-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell45": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.45015335883146,
    "padj": 0.0000254391338817522,
    "baseMean": 2722480.91881496,
    "baseMean_GroupA": 3994804.33410823,
    "baseMean_GroupB": 1450157.50352169,
    "lfcSE": 0.305499820566367,
    "stat": -4.74682229319488,
    "pvalue": 0.00000206637348670276,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:4-hN/A",
      "group_B": "HCT-116-cell:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:4-hN/A_HCT-116-cell:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_b50454cdda6c430ee297f656dc3165a6_5b0fcad724fee",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_b50454cdda6c430ee297f656dc3165a6",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:4-hN/A vs HCT-116-cell:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell44": {
    "experimentId": "GSE68189_embryonic-stem-cell_hindbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.13020059785058,
    "padj": 0.000057687641552758406,
    "baseMean": 2776.21640192877,
    "baseMean_GroupA": 5004.94996363389,
    "baseMean_GroupB": 547.482840223654,
    "lfcSE": 0.740860656622861,
    "stat": -4.2250868228302,
    "pvalue": 0.000023884867362143602,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "hindbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_embryonic-stem-cell_hindbrain-cell_none_1",
      "folderName": "DE__GSE68189_b2b6f1104dfb9911b42ebfb6288d893b_5b1587a1668bf",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_b2b6f1104dfb9911b42ebfb6288d893b",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs hindbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell47": {
    "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 0.699146827639689,
    "padj": 0.0226582288017147,
    "baseMean": 260847.825229194,
    "baseMean_GroupA": 207982.357342992,
    "baseMean_GroupB": 340146.027058498,
    "lfcSE": 0.235004103653732,
    "stat": 2.97504093234836,
    "pvalue": 0.00292949455522685,
    "comparisonDetails": {
      "group_A": "salmonella-typhimurium-strain-keller:48hN/A",
      "group_B": "staphloccocus-epidermidis:4hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:48hN/A_staphloccocus-epidermidis:4hN/A_none_1",
      "folderName": "DE__GSE64142_3b507140653b0711130eff079314848d_5b15863763202",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_3b507140653b0711130eff079314848d",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "salmonella-typhimurium-strain-keller:48hN/A vs staphloccocus-epidermidis:4hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell46": {
    "experimentId": "GSE68085_Luminal-A-Breast-Carcinoma_TNBC_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.42634299860263,
    "padj": 0.00519236733009048,
    "baseMean": 7361.58825608353,
    "baseMean_GroupA": 8023.61476216998,
    "baseMean_GroupB": 5946.22124307113,
    "lfcSE": 0.127498259305705,
    "stat": -3.34391230848398,
    "pvalue": 0.000826058438878031,
    "comparisonDetails": {
      "group_A": "Luminal-A-Breast-Carcinoma",
      "group_B": "TNBC",
      "datasetId": "GSE68085",
      "covariates": "none",
      "experimentId": "GSE68085_Luminal-A-Breast-Carcinoma_TNBC_none_1",
      "folderName": "DE__GSE68085_8609f7427d50c3b527975171aea08edf_5b1586c9e16ad",
      "comparisonCondition": "disease-details",
      "experimentHash": "GSE68085_8609f7427d50c3b527975171aea08edf",
      "genome": "hg38",
      "allConditions": [
        "disease-details"
      ]
    },
    "xkey": "Luminal-A-Breast-Carcinoma vs TNBC -- GSE68085",
    "ykey": "hsa-let-7a-5p"
  },
  "cell49": {
    "experimentId": "GSE62776_no-dox_dox-days-0-14_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.33628944493496,
    "padj": 0.0000219722116207204,
    "baseMean": 281284.561511116,
    "baseMean_GroupA": 545744.941611817,
    "baseMean_GroupB": 104977.641443982,
    "lfcSE": 0.48477494349868,
    "stat": -4.81932797118942,
    "pvalue": 0.0000014404259439182,
    "comparisonDetails": {
      "group_A": "no-dox",
      "group_B": "dox-days-0-14",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_no-dox_dox-days-0-14_none_1",
      "folderName": "DE__GSE62776_d1de9fbbb00fd9a44e67428470f246fe_5b1589fea0a90",
      "comparisonCondition": "treatment",
      "experimentHash": "GSE62776_d1de9fbbb00fd9a44e67428470f246fe",
      "genome": "hg38",
      "allConditions": [
        "treatment"
      ]
    },
    "xkey": "no-dox vs dox-days-0-14 -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell189": {
    "experimentId": "GSE64142_4hN/A_48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.296733582940822,
    "padj": 2.4141628751976e-8,
    "baseMean": 279623.907936685,
    "baseMean_GroupA": 306656.524318865,
    "baseMean_GroupB": 249587.66751204,
    "lfcSE": 0.0510956192001374,
    "stat": -5.80741730085588,
    "pvalue": 6.34438707816434e-9,
    "comparisonDetails": {
      "group_A": "4hN/A",
      "group_B": "48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_4hN/A_48hN/A_none_1",
      "folderName": "DE__GSE64142_a04bc04d8421deab5fd6233b247d0e56_5b0fcbf8d3c81",
      "comparisonCondition": "time",
      "experimentHash": "GSE64142_a04bc04d8421deab5fd6233b247d0e56",
      "genome": "hg38",
      "allConditions": [
        "time"
      ]
    },
    "xkey": "4hN/A vs 48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell48": {
    "experimentId": "GSE14362_size-range-<50nt_cap-captured-eluate-with-heat_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.06999077996316,
    "padj": 1.12739381619132e-35,
    "baseMean": 3016.00889576394,
    "baseMean_GroupA": 3638.69220895486,
    "baseMean_GroupB": 213.933986404796,
    "lfcSE": 0.321241941792561,
    "stat": -12.6695498017856,
    "pvalue": 8.721348389404509e-37,
    "comparisonDetails": {
      "group_A": "size-range-<50nt",
      "group_B": "cap-captured-eluate-with-heat",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_size-range-<50nt_cap-captured-eluate-with-heat_none_1",
      "folderName": "DE__GSE14362_025e89f02fcb191f011fedfdb341607d_5b0ebbcaa0b33",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE14362_025e89f02fcb191f011fedfdb341607d",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "size-range-<50nt vs cap-captured-eluate-with-heat -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell190": {
    "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -6.21264166554912,
    "padj": 2.2484375559862186e-283,
    "baseMean": 16676.6517410833,
    "baseMean_GroupA": 32910.8852738272,
    "baseMean_GroupB": 442.41820833934503,
    "lfcSE": 0.172155092254259,
    "stat": -36.0874696426263,
    "pvalue": 3.566252921126168e-285,
    "comparisonDetails": {
      "group_A": "ESC-derived-cell-line:mesenchymal-cell",
      "group_B": "ESC-derived-cell-line:midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_ESC-derived-cell-line:mesenchymal-cell_ESC-derived-cell-line:midbrain-cell_none_1",
      "folderName": "DE__GSE68189_5e1f0efaeff75fdf0908002ecd1905e2_5b1587e4315dd",
      "comparisonCondition": "cell-line:cell-type",
      "experimentHash": "GSE68189_5e1f0efaeff75fdf0908002ecd1905e2",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "cell-type"
      ]
    },
    "xkey": "ESC-derived-cell-line:mesenchymal-cell vs ESC-derived-cell-line:midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell191": {
    "experimentId": "GSE21279_healthy-tissue_distal-diseased-tissue_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.33275035181031,
    "padj": 0.00482701021862025,
    "baseMean": 206772.438225048,
    "baseMean_GroupA": 349984.666780636,
    "baseMean_GroupB": 63560.2096694597,
    "lfcSE": 0.618234038687879,
    "stat": -3.77324800291047,
    "pvalue": 0.0001611359678881,
    "comparisonDetails": {
      "group_A": "healthy-tissue",
      "group_B": "distal-diseased-tissue",
      "datasetId": "GSE21279",
      "covariates": "none",
      "experimentId": "GSE21279_healthy-tissue_distal-diseased-tissue_none_1",
      "folderName": "DE__GSE21279_b650540c1a30f8ebfcdc371072262388_5b0ebbea023f0",
      "comparisonCondition": "tissue-phenotype",
      "experimentHash": "GSE21279_b650540c1a30f8ebfcdc371072262388",
      "genome": "hg38",
      "allConditions": [
        "tissue-phenotype"
      ]
    },
    "xkey": "healthy-tissue vs distal-diseased-tissue -- GSE21279",
    "ykey": "hsa-let-7a-5p"
  },
  "cell198": {
    "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.519932523855758,
    "padj": 0.00254503232604307,
    "baseMean": 276612.634855897,
    "baseMean_GroupA": 326187.301171587,
    "baseMean_GroupB": 227037.968540207,
    "lfcSE": 0.143526431079066,
    "stat": -3.62255592887513,
    "pvalue": 0.000291706362951781,
    "comparisonDetails": {
      "group_A": "salmonella-typhimurium-strain-keller:4hN/A",
      "group_B": "salmonella-typhimurium-strain-keller:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_salmonella-typhimurium-strain-keller:4hN/A_salmonella-typhimurium-strain-keller:48hN/A_none_1",
      "folderName": "DE__GSE64142_be1bdf18ac4d9360dd1cb6fde7d0f3d8_5b1586140f0b1",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_be1bdf18ac4d9360dd1cb6fde7d0f3d8",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "salmonella-typhimurium-strain-keller:4hN/A vs salmonella-typhimurium-strain-keller:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell199": {
    "experimentId": "GSE68189_embryonic-stem-cell_forebrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -4.39138317663852,
    "padj": 7.82081671349689e-9,
    "baseMean": 2689.93219476529,
    "baseMean_GroupA": 5157.78615729102,
    "baseMean_GroupB": 222.078232239556,
    "lfcSE": 0.734626137156682,
    "stat": -5.9777115930493,
    "pvalue": 2.26293901725983e-9,
    "comparisonDetails": {
      "group_A": "embryonic-stem-cell",
      "group_B": "forebrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_embryonic-stem-cell_forebrain-cell_none_1",
      "folderName": "DE__GSE68189_89fae1de4bc237b82b60f9fb0aeaea0a_5b15879a411e2",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_89fae1de4bc237b82b60f9fb0aeaea0a",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "embryonic-stem-cell vs forebrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell196": {
    "experimentId": "GSE34137_basal-cell-carcinoma_squamous-cell-carcinoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.10406516717684,
    "padj": 0.000267589503757297,
    "baseMean": 9824.14556181973,
    "baseMean_GroupA": 12653.4155534956,
    "baseMean_GroupB": 1336.33558679218,
    "lfcSE": 0.721102955450623,
    "stat": -4.3046074679268,
    "pvalue": 0.000016728207332001102,
    "comparisonDetails": {
      "group_A": "basal-cell-carcinoma",
      "group_B": "squamous-cell-carcinoma",
      "datasetId": "GSE34137",
      "covariates": "none",
      "experimentId": "GSE34137_basal-cell-carcinoma_squamous-cell-carcinoma_none_1",
      "folderName": "DE__GSE34137_f855ad8a65e56f9713c8ce79fde31a81_5b0eb9641d030",
      "comparisonCondition": "disease",
      "experimentHash": "GSE34137_f855ad8a65e56f9713c8ce79fde31a81",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "basal-cell-carcinoma vs squamous-cell-carcinoma -- GSE34137",
    "ykey": "hsa-let-7a-5p"
  },
  "cell197": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.7240996138737001,
    "padj": 0.0000142959544162218,
    "baseMean": 2888569.70505997,
    "baseMean_GroupA": 4451834.73499198,
    "baseMean_GroupB": 1325304.67512796,
    "lfcSE": 0.353385800824144,
    "stat": -4.87880274151611,
    "pvalue": 0.00000106731741588686,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-cell:irradiation:24-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-cell:irradiation:24-hN/A_none_1",
      "folderName": "DE__GSE50064_9233daec84151e08d1e0249658a43a75_5b0fcae4b98fd",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_9233daec84151e08d1e0249658a43a75",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:24-hN/A vs HCT-116-cell:irradiation:24-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell194": {
    "experimentId": "GSE21279_hemangioma-of-liver_hepatitis-C_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.30267322435375,
    "padj": 0.00215434107250644,
    "baseMean": 325577.284383268,
    "baseMean_GroupA": 481371.867325315,
    "baseMean_GroupB": 91885.4099701978,
    "lfcSE": 0.654339660005643,
    "stat": -3.51907940951324,
    "pvalue": 0.000433047067671964,
    "comparisonDetails": {
      "group_A": "hemangioma-of-liver",
      "group_B": "hepatitis-C",
      "datasetId": "GSE21279",
      "covariates": "none",
      "experimentId": "GSE21279_hemangioma-of-liver_hepatitis-C_none_1",
      "folderName": "DE__GSE21279_9de4715d8040baf5c7a4449979f1202c_5b0ebbdf4880f",
      "comparisonCondition": "disease",
      "experimentHash": "GSE21279_9de4715d8040baf5c7a4449979f1202c",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "hemangioma-of-liver vs hepatitis-C -- GSE21279",
    "ykey": "hsa-let-7a-5p"
  },
  "cell195": {
    "experimentId": "GSE72183_male-organism:first-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.10666689777859,
    "padj": 0.0258331219894887,
    "baseMean": 878.708222361094,
    "baseMean_GroupA": 315.942430747763,
    "baseMean_GroupB": 1441.47401397442,
    "lfcSE": 0.795070254451331,
    "stat": 2.6496613173289703,
    "pvalue": 0.00805724964838563,
    "comparisonDetails": {
      "group_A": "male-organism:first-void-urine:Cells",
      "group_B": "male-organism:second-void-urine:Extracellular-Vesicles",
      "datasetId": "GSE72183",
      "covariates": "none",
      "experimentId": "GSE72183_male-organism:first-void-urine:Cells_male-organism:second-void-urine:Extracellular-Vesicles_none_1",
      "folderName": "DE__GSE72183_246228759b26f2d1bef24fce4b73958e_5b15895d255a3",
      "comparisonCondition": "gender:sample-details:tissue-part",
      "experimentHash": "GSE72183_246228759b26f2d1bef24fce4b73958e",
      "genome": "hg38",
      "allConditions": [
        "gender",
        "sample-details",
        "tissue-part"
      ]
    },
    "xkey": "male-organism:first-void-urine:Cells vs male-organism:second-void-urine:Extracellular-Vesicles -- GSE72183",
    "ykey": "hsa-let-7a-5p"
  },
  "cell41": {
    "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.39834644151846,
    "padj": 0.00219293947715408,
    "baseMean": 3009736.02169088,
    "baseMean_GroupA": 4379177.52676342,
    "baseMean_GroupB": 1640294.51661834,
    "lfcSE": 0.380737424513861,
    "stat": -3.67273178701547,
    "pvalue": 0.000239971318217986,
    "comparisonDetails": {
      "group_A": "MCF-10A-cell:irradiation:24-hN/A",
      "group_B": "HCT-116-p53-/-:control:0-hN/A",
      "datasetId": "GSE50064",
      "covariates": "none",
      "experimentId": "GSE50064_MCF-10A-cell:irradiation:24-hN/A_HCT-116-p53-/-:control:0-hN/A_none_1",
      "folderName": "DE__GSE50064_57d83d6a7859bf74562a9ae03612a8fd_5b0fcae640523",
      "comparisonCondition": "cell-line:treatment:time",
      "experimentHash": "GSE50064_57d83d6a7859bf74562a9ae03612a8fd",
      "genome": "hg38",
      "allConditions": [
        "cell-line",
        "treatment",
        "time"
      ]
    },
    "xkey": "MCF-10A-cell:irradiation:24-hN/A vs HCT-116-p53-/-:control:0-hN/A -- GSE50064",
    "ykey": "hsa-let-7a-5p"
  },
  "cell192": {
    "experimentId": "GSE36147_leiomyosarcoma_liposarcoma_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 2.07549023459137,
    "padj": 0,
    "baseMean": 45192.1354713693,
    "baseMean_GroupA": 17325.7990918712,
    "baseMean_GroupB": 73058.4718508674,
    "lfcSE": 0.0467036959517554,
    "stat": 44.4395286560476,
    "pvalue": 0,
    "comparisonDetails": {
      "group_A": "leiomyosarcoma",
      "group_B": "liposarcoma",
      "datasetId": "GSE36147",
      "covariates": "none",
      "experimentId": "GSE36147_leiomyosarcoma_liposarcoma_none_1",
      "folderName": "DE__GSE36147_d3bf6479f2709ec61d813494b6146ecf_5b0fca9e3e9ca",
      "comparisonCondition": "disease",
      "experimentHash": "GSE36147_d3bf6479f2709ec61d813494b6146ecf",
      "genome": "hg38",
      "allConditions": [
        "disease"
      ]
    },
    "xkey": "leiomyosarcoma vs liposarcoma -- GSE36147",
    "ykey": "hsa-let-7a-5p"
  },
  "cell40": {
    "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.42816649270135,
    "padj": 4.582949647220149e-57,
    "baseMean": 5177.05588039095,
    "baseMean_GroupA": 8079.27112447037,
    "baseMean_GroupB": 3000.39444733138,
    "lfcSE": 0.0890198697005672,
    "stat": -16.0432327917938,
    "pvalue": 6.374779584479149e-58,
    "comparisonDetails": {
      "group_A": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500",
      "group_B": "liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_96f80da265440bf2776c7ec6e6342646_5b15888da21cc",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_96f80da265440bf2776c7ec6e6342646",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500 vs liver:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell193": {
    "experimentId": "GSE64142_non-infected:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -0.530785586115083,
    "padj": 0.00312313757198388,
    "baseMean": 238026.27675994,
    "baseMean_GroupA": 281601.891573194,
    "baseMean_GroupB": 194450.661946687,
    "lfcSE": 0.156866155855876,
    "stat": -3.38368453806411,
    "pvalue": 0.000715201265379155,
    "comparisonDetails": {
      "group_A": "non-infected:4hN/A",
      "group_B": "Yersinia-pseudotuberculosis:48hN/A",
      "datasetId": "GSE64142",
      "covariates": "none",
      "experimentId": "GSE64142_non-infected:4hN/A_Yersinia-pseudotuberculosis:48hN/A_none_1",
      "folderName": "DE__GSE64142_558474389886461cf889db6e3bbce90d_5b0fcc5756b53",
      "comparisonCondition": "treatment:time",
      "experimentHash": "GSE64142_558474389886461cf889db6e3bbce90d",
      "genome": "hg38",
      "allConditions": [
        "treatment",
        "time"
      ]
    },
    "xkey": "non-infected:4hN/A vs Yersinia-pseudotuberculosis:48hN/A -- GSE64142",
    "ykey": "hsa-let-7a-5p"
  },
  "cell32": {
    "experimentId": "GSE45722_sample-a:illumina-kit_sample-a:NEB-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 1.98274828464028,
    "padj": 8.029721253051499e-79,
    "baseMean": 5125.07551866159,
    "baseMean_GroupA": 2067.36583173021,
    "baseMean_GroupB": 8182.78520559296,
    "lfcSE": 0.104770428497577,
    "stat": 18.9246938575433,
    "pvalue": 7.140313750763429e-80,
    "comparisonDetails": {
      "group_A": "sample-a:illumina-kit",
      "group_B": "sample-a:NEB-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_sample-a:illumina-kit_sample-a:NEB-kit_none_1",
      "folderName": "DE__GSE45722_4ed36d89642d9b2b84006a84c7ece8e4_5b0eb530983e6",
      "comparisonCondition": "sample-details:experimental-process",
      "experimentHash": "GSE45722_4ed36d89642d9b2b84006a84c7ece8e4",
      "genome": "hg38",
      "allConditions": [
        "sample-details",
        "experimental-process"
      ]
    },
    "xkey": "sample-a:illumina-kit vs sample-a:NEB-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell31": {
    "experimentId": "GSE14362_HeLa_HepG2_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 3.15570705952082,
    "padj": 0.000005916839856029281,
    "baseMean": 1647.83399120785,
    "baseMean_GroupA": 221.689430573356,
    "baseMean_GroupB": 2123.21551141935,
    "lfcSE": 0.652059165944535,
    "stat": 4.83960233110081,
    "pvalue": 0.00000130099189605463,
    "comparisonDetails": {
      "group_A": "HeLa",
      "group_B": "HepG2",
      "datasetId": "GSE14362",
      "covariates": "none",
      "experimentId": "GSE14362_HeLa_HepG2_none_1",
      "folderName": "DE__GSE14362_59a8904253b381e78cecf73bbd002d80_5b0ebb58831fc",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE14362_59a8904253b381e78cecf73bbd002d80",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "HeLa vs HepG2 -- GSE14362",
    "ykey": "hsa-let-7a-5p"
  },
  "cell34": {
    "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": 5.02326974385107,
    "padj": 2.48715247770303e-13,
    "baseMean": 18.6520445493255,
    "baseMean_GroupA": 0.555026321529954,
    "baseMean_GroupB": 23.1762991062744,
    "lfcSE": 0.680437450526086,
    "stat": 7.38241221139031,
    "pvalue": 1.5544702985644e-13,
    "comparisonDetails": {
      "group_A": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500",
      "group_B": "blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq_none_1",
      "folderName": "DE__GSE69825_bdb0b8d102341dcee58bb2bfe5eae234_5b158884ae78f",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_bdb0b8d102341dcee58bb2bfe5eae234",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:Novex-TBE-page-gel:Illumina-HiSeq-2500 vs blood:0.05-ug:ampure-xp-beads:Illumina-MiSeq -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell33": {
    "experimentId": "GSE62776_ssea3+_tra-1-60+_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -3.18963395531251,
    "padj": 5.5943358592302e-11,
    "baseMean": 51075.7802681931,
    "baseMean_GroupA": 110593.828831644,
    "baseMean_GroupB": 11397.0812258923,
    "lfcSE": 0.445577854964939,
    "stat": -7.1584211822275,
    "pvalue": 8.161148782877001e-13,
    "comparisonDetails": {
      "group_A": "ssea3+",
      "group_B": "tra-1-60+",
      "datasetId": "GSE62776",
      "covariates": "none",
      "experimentId": "GSE62776_ssea3+_tra-1-60+_none_1",
      "folderName": "DE__GSE62776_e7dc2c80e718f903ba75161af1be752c_5b158a28bfcfc",
      "comparisonCondition": "marker",
      "experimentHash": "GSE62776_e7dc2c80e718f903ba75161af1be752c",
      "genome": "hg38",
      "allConditions": [
        "marker"
      ]
    },
    "xkey": "ssea3+ vs tra-1-60+ -- GSE62776",
    "ykey": "hsa-let-7a-5p"
  },
  "cell36": {
    "experimentId": "GSE45722_NEB-kit_illumina-kit_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -1.39242425972383,
    "padj": 0.00000731206752359394,
    "baseMean": 5620.01341467034,
    "baseMean_GroupA": 6653.17946268363,
    "baseMean_GroupB": 2520.51527063049,
    "lfcSE": 0.291656353953154,
    "stat": -4.7741948387227,
    "pvalue": 0.00000180427640192578,
    "comparisonDetails": {
      "group_A": "NEB-kit",
      "group_B": "illumina-kit",
      "datasetId": "GSE45722",
      "covariates": "none",
      "experimentId": "GSE45722_NEB-kit_illumina-kit_none_1",
      "folderName": "DE__GSE45722_2801ab3b7bebb3cf5d68875f0e0b9ed4_5b0eb51bae69a",
      "comparisonCondition": "experimental-process",
      "experimentHash": "GSE45722_2801ab3b7bebb3cf5d68875f0e0b9ed4",
      "genome": "hg38",
      "allConditions": [
        "experimental-process"
      ]
    },
    "xkey": "NEB-kit vs illumina-kit -- GSE45722",
    "ykey": "hsa-let-7a-5p"
  },
  "cell35": {
    "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -2.34194211943476,
    "padj": 0.00878317372584625,
    "baseMean": 12364.8460610867,
    "baseMean_GroupA": 44099.086035449,
    "baseMean_GroupB": 4137.45051217795,
    "lfcSE": 0.870727733814192,
    "stat": -2.68963767718293,
    "pvalue": 0.00715296345097327,
    "comparisonDetails": {
      "group_A": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "group_B": "blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500",
      "datasetId": "GSE69825",
      "covariates": "none",
      "experimentId": "GSE69825_blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500_blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500_none_1",
      "folderName": "DE__GSE69825_f17aa7f220afe79df08e5a20201f83a4_5b1588ac93a71",
      "comparisonCondition": "tissue:input:purification:experimental-process",
      "experimentHash": "GSE69825_f17aa7f220afe79df08e5a20201f83a4",
      "genome": "hg38",
      "allConditions": [
        "tissue",
        "input",
        "purification",
        "experimental-process"
      ]
    },
    "xkey": "blood:1-ug:ampure-xp-beads:Illumina-HiSeq-2500 vs blood:0.05-ug:ampure-xp-beads:Illumina-HiSeq-2500 -- GSE69825",
    "ykey": "hsa-let-7a-5p"
  },
  "cell38": {
    "experimentId": "GSE68189_hFL1s_H9-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -5.00250941706356,
    "padj": 8.93249038738169e-12,
    "baseMean": 99435.8197272562,
    "baseMean_GroupA": 193269.362025992,
    "baseMean_GroupB": 5602.2774285206,
    "lfcSE": 0.714142298918686,
    "stat": -7.00491964225908,
    "pvalue": 2.47127632046572e-12,
    "comparisonDetails": {
      "group_A": "hFL1s",
      "group_B": "H9-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_hFL1s_H9-cell_none_1",
      "folderName": "DE__GSE68189_d1abc33fe06c71a470aceb143aaff764_5b15876e21b65",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE68189_d1abc33fe06c71a470aceb143aaff764",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "hFL1s vs H9-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell37": {
    "experimentId": "GSE68189_mesenchymal-cell_midbrain-cell_none_1",
    "srnaId": "hsa-let-7a-5p",
    "log2FoldChange": -6.21264166554912,
    "padj": 2.2484375559862186e-283,
    "baseMean": 16676.6517410833,
    "baseMean_GroupA": 32910.8852738272,
    "baseMean_GroupB": 442.41820833934503,
    "lfcSE": 0.172155092254259,
    "stat": -36.0874696426263,
    "pvalue": 3.566252921126168e-285,
    "comparisonDetails": {
      "group_A": "mesenchymal-cell",
      "group_B": "midbrain-cell",
      "datasetId": "GSE68189",
      "covariates": "none",
      "experimentId": "GSE68189_mesenchymal-cell_midbrain-cell_none_1",
      "folderName": "DE__GSE68189_3e4602963aa5744cf05ed6870ee26034_5b1587b15cb8a",
      "comparisonCondition": "cell-type",
      "experimentHash": "GSE68189_3e4602963aa5744cf05ed6870ee26034",
      "genome": "hg38",
      "allConditions": [
        "cell-type"
      ]
    },
    "xkey": "mesenchymal-cell vs midbrain-cell -- GSE68189",
    "ykey": "hsa-let-7a-5p"
  },
  "cell39": {
    "experimentId": "GSE58127_C8166-cell_A549-cell_none_1",
    "srnaId": "hsa-let-7a-2-3p",
    "log2FoldChange": 6.20399964234678,
    "padj": 0.0180559554586613,
    "baseMean": 24.187521458042,
    "baseMean_GroupA": 0,
    "baseMean_GroupB": 48.3750429160839,
    "lfcSE": 1.92639938964466,
    "stat": 3.22051578488673,
    "pvalue": 0.00127960145726441,
    "comparisonDetails": {
      "group_A": "C8166-cell",
      "group_B": "A549-cell",
      "datasetId": "GSE58127",
      "covariates": "none",
      "experimentId": "GSE58127_C8166-cell_A549-cell_none_1",
      "folderName": "DE__GSE58127_b06b66f92a1810c253677fb9ebfe1084_5b0fcb1e2170d",
      "comparisonCondition": "cell-line",
      "experimentHash": "GSE58127_b06b66f92a1810c253677fb9ebfe1084",
      "genome": "hg38",
      "allConditions": [
        "cell-line"
      ]
    },
    "xkey": "C8166-cell vs A549-cell -- GSE58127",
    "ykey": "hsa-let-7a-2-3p"
  }
}
