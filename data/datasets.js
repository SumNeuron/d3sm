dsets = {
  "GSE31069": {
    "Dataset": "GSE31069",
    "noSamples": 4,
    "OasisOutputFolder": "GSE31069_5adc6d4043511",
    "disease": [
      "adenocarcinoma"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE33665": {
    "Dataset": "GSE33665",
    "noSamples": 30,
    "OasisOutputFolder": "GSE33665_5ae0427445fb9",
    "disease": [
      "infiltrative basal cell carcinoma",
      "nodular basal cell carcinoma"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [
      "skin"
    ],
    "cell_type": []
  },
  "GSE52575": {
    "Dataset": "GSE52575",
    "noSamples": 2,
    "OasisOutputFolder": "GSE52575_5adca5b334180",
    "disease": [
      "adenocarcinoma"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE51774": {
    "Dataset": "GSE51774",
    "noSamples": 2,
    "OasisOutputFolder": "GSE51774_5adc9eeb9ac80",
    "disease": [
      "chronic myeloid leukemia"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE72721": {
    "Dataset": "GSE72721",
    "noSamples": 4,
    "OasisOutputFolder": "GSE72721_5adcc990e5986",
    "disease": [
      "neuroblastoma"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE30672": {
    "Dataset": "GSE30672",
    "noSamples": 1,
    "OasisOutputFolder": "GSE30672_5adc668656051",
    "disease": [
      "essential thrombocythemia"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE22918": {
    "Dataset": "GSE22918",
    "noSamples": 2,
    "OasisOutputFolder": "GSE22918_5ae755f421d39",
    "disease": [
      "nasopharynx carcinoma"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE48059": {
    "Dataset": "GSE48059",
    "noSamples": 3,
    "OasisOutputFolder": "GSE48059_5adc9840a209c",
    "disease": [
      "acute monocytic leukemia",
      "acute promyelocytic leukemia",
      "chronic myeloid leukemia"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [],
    "cell_type": []
  },
  "GSE58410": {
    "Dataset": "GSE58410",
    "noSamples": 23,
    "OasisOutputFolder": "GSE58410_5add2d68e3292",
    "disease": [
      "prostate cancer"
    ],
    "organism": [
      "homo sapiens"
    ],
    "cell_line": [],
    "tissue": [
      "blood plasma"
    ],
    "cell_type": []
  }
}
