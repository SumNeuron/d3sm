# About
See the [GitLab Pages][gitlab]


d3sm is an extension of d3's core functionality with some aesthetically appealing
function for bar, box-and-whisker, violin, and scatter plots.

Following the precedence set by d3, these closures are not "top-level". To clarify,
one must still write their own function - e.g. ScatterPlot - which combines the
desired functionality from the functions scatter, axis, zoom, etc.


# Closure structure
For functions that render elements in the DOM (e.g. bar, boxwhisker, etc)
they have the following structure:

<g class="<passed-selection>">  <!-- provided by user -->

  <g class="<namespace>-<function>" clip-path="url(#<namespace>-clip-path)">
    <!-- color of background -->
    <rect class="bg" x=0 y=0 width=<spaceX> height=<spaceY>>
    <!-- needed for clip path -->
    <defs class="<namespace>-definitions">
      <!-- needed to hide overflow -->
      <clipPath id="<namespace>-clip-path">
        <rect x=0 y=0 width=<spaceX> height=<spaceY>></rect>
      </clipPath>
    </defs>

    <!-- container where objects of the function are placed -->
    <g class="<namespace>-object-container">
      ...
    </g>

  </g>
</g>



[gitlab]: https://sumneuron.gitlab.io/d3sm/
