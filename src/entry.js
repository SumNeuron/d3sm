import utils from './modules/utils/index.js'
import axis from './modules/axis'
import groupingSpacer from './modules/grouping-spacer'
import tooltip from './modules/tooltip'
import colorFunction from './modules/color-function'
import charts from './modules/charts/index.js'
import legends from './modules/legends/index.js'
import aux from './modules/aux/index.js'

import { event, mouse} from 'd3-selection';


let d3sm = {
  aux,
  axis,
  charts,
  colorFunction,
  debugQ: false,
  groupingSpacer,
  legends,
  tooltip,
  utils,
}

if (typeof window !== 'undefined') {
  window.d3sm = d3sm;
}

export default d3sm
