import * as d3 from 'd3'
import {color} from './utils/index.js'

/**
 * Creates a colorFunction
 * @constructor colorFunction
 * @namespace colorFunction
 * @returns {function} colorFunction
 */
export default function colorFunction() {
  var
  data,

  /**
  * Default colors to use
  * @param {number[]} [colors=["#2c7bb6", "#00a6ca", "#00ccbc", "#90eb9d", "#ffff8c", "#f9d057", "#f29e2e", "#e76818", "#d7191c"]]
  * @memberof colorFunction#
  * @property
  */
  colors = ["#2c7bb6", "#00a6ca", "#00ccbc", "#90eb9d", "#ffff8c", "#f9d057", "#f29e2e", "#e76818", "#d7191c"],
  /**
  * Interpolator for colors
  * @param {d3.interpolation} [interpolation=d3.interpolateRgb]
  * @memberof colorFunction#
  * @property
  */
  interpolation = d3.interpolateRgb,
  /**
  * Function for modifying color luminance
  * @param {function} [modifyOpacity=modifyHexidecimalColorLuminance]
  * @memberof colorFunction#
  * @property
  */
  modifyOpacity = color.modifyHexidecimalColorLuminance,
  /**
  * How to modify color for stroke
  * @param {number} [strokeOpacity=0]
  * @memberof colorFunction#
  * @property
  */
  strokeOpacity = 0,
  /**
  * How to modify color for fill
  * @param {number} [fillOpacity=0.4]
  * @memberof colorFunction#
  * @property
  */
  fillOpacity = 0.4,
  /**
  * How to determine the color to use
  * @param {string} [colorBy='index']
  * @memberof colorFunction#
  * @property
  */
  colorBy = 'index',
  /**
  * Sets the scale for interpolating the colors
  * @param {number[]} [dataExtent=[0, colors.length - 1]]
  * @memberof colorFunction#
  * @property
  */
  dataExtent = [0, colors.length - 1],
  /**
  * Extracts the value to color by
  * @param {function} [valueExtractor=function(k, v, i) {return v}]
  * @memberof colorFunction#
  * @property
  */
  valueExtractor = function(k, v, i) {return v},

  /**
  * Extracts the category to color by
  * @param {function} [categoryExtractor=function(k, v, i) {return v.category}]
  * @memberof colorFunction#
  * @property
  */
  categoryExtractor = function(k, v, i) {return v.category},

  /**
  * The different type of categories of which to color by
  * @param {string[]} [categories=undefined]
  * @memberof colorFunction#
  * @property
  */
  categories,

  /**
  * Scale for interpolating the colors
  * @param {d3.scale} [scale=d3.scaleLinear()]
  * @memberof colorFunction#
  * @property
  */
  scale = d3.scaleLinear()
  .interpolate(interpolation).domain(dataExtent).range(colors),
  helperScale = d3.scaleLinear()



  // var h = x => '#' + x.match(/\d+/g).map(y = z => ((+z < 16)?'0':'') + (+z).toString(16)).join('');
  var h = function(x) {
    return "#" + x.match(/\d+/g).map(
      function(y, i) {
        return  ((+y < 16)?'0':'') + (+y).toString(16)
      }).join('');
  }

  /**
   * Gets or sets the default colors
   * (see {@link colorFunction#colors})
   * @param {number[]} [_=none]
   * @returns {colorFunction | number[]}
   * @memberof colorFunction
   * @property
   */
  colorFunction.colors = function(_) {
    return arguments.length
    ?
      (
        colors = _,
        scale.range(colors),
        colorFunction
      )
    : colors;
  };
  /**
   * Gets or sets the function for interpolating the colors
   * (see {@link colorFunction#interpolation})
   * @param {d3.interpolation} [_=none]
   * @returns {colorFunction | d3.interpolation}
   * @memberof colorFunction
   * @property
   */
  colorFunction.interpolation = function(_) {
    return arguments.length
    ?
    (
      interpolation = _,
      scale.interpolate(interpolation).range(colors),
      colorFunction
    )
    : interpolation;
  };
  /**
   * Gets or sets the values for the scale which transforms the value to a color
   * (see {@link colorFunction#dataExtent})
   * @param {number[]} [_=none]
   * @returns {colorFunction | number[]}
   * @memberof colorFunction
   * @property
   */
  colorFunction.dataExtent = function(_) {
    return arguments.length
    ? (
        dataExtent = _,
        scale.domain(dataExtent).interpolate(scale.interpolate()),
        colorFunction
      )
    : dataExtent;
  };
  /**
   * Gets or sets the vthe scale which transforms the value to a color
   * (see {@link colorFunction#scale})
   * @param {d3.scale} [_=none]
   * @returns {colorFunction | d3.scale}
   * @memberof colorFunction
   * @property
   */
  colorFunction.scale = function(_) {
    return arguments.length
    ? (
        _ = _.domain(scale.domain()).interpolate(scale.interpolate()).range(scale.range()),
        scale = _,
        colorFunction
      )
    : scale;
  };
  /**
   * Gets or sets the function for modify opacity
   * (see {@link colorFunction#modifyOpacity})
   * @param {function} [_=none]
   * @returns {colorFunction | function}
   * @memberof colorFunction
   * @property
   */
  colorFunction.modifyOpacity = function(_) { return arguments.length ? (modifyOpacity = _, colorFunction) : modifyOpacity; };
  /**
   * Gets or sets the value to modify the color for the stroke via {@link colorFunction#modifyOpacity}
   * (see {@link colorFunction#strokeOpacity})
   * @param {number} [_=none]
   * @returns {colorFunction | number}
   * @memberof colorFunction
   * @property
   */
  colorFunction.strokeOpacity = function(_) { return arguments.length ? (strokeOpacity = _, colorFunction) : strokeOpacity; };
  /**
   * Gets or sets the value to modify the color for the stroke via {@link colorFunction#fillOpacity}
   * (see {@link colorFunction#fillOpacity})
   * @param {number} [_=none]
   * @returns {colorFunction | number}
   * @memberof colorFunction
   * @property
   */
  colorFunction.fillOpacity = function(_) { return arguments.length ? (fillOpacity = _, colorFunction) : fillOpacity; };
  /**
   * Gets or sets the value to colorBy
   * (see {@link colorFunction#colorBy})
   * @param {string} [_=none]
   * @returns {colorFunction | string}
   * @memberof colorFunction
   * @property
   */
  colorFunction.colorBy = function(_) { return arguments.length ? (colorBy = _, colorFunction) : colorBy; };
  /**
   * Gets or sets the value of valueExtractor
   * (see {@link colorFunction#valueExtractor})
   * @param {function} [_=none]
   * @returns {colorFunction | function}
   * @memberof colorFunction
   * @property
   */
  colorFunction.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, colorFunction) : valueExtractor; };

  /**
   * Gets or sets the value of categoryExtractor
   * (see {@link colorFunction#categoryExtractor})
   * @param {function} [_=none]
   * @returns {colorFunction | function}
   * @memberof colorFunction
   * @property
   */
  colorFunction.categoryExtractor = function(_) { return arguments.length ? (categoryExtractor = _, colorFunction) : categoryExtractor; };
  /**
   * Gets or sets the value of categoryExtractor
   * (see {@link colorFunction#categories})
   * @param {string[]} [_=none]
   * @returns {colorFunction | string[]}
   * @memberof colorFunction
   * @property
   */
  colorFunction.categories = function(_) { return arguments.length ? (categories = _, colorFunction) : categories; };


  function colorFunction(key, value, index, type, hoverQ) {
    var c,
    opac = type == "fill" ? fillOpacity : strokeOpacity;


    updateScale()

    if (colorBy == "index") {
      c = (type != undefined) ? modifyOpacity(h(scale(index)), opac) : h(scale(index))
    }

    else if (colorBy == 'value') {
      var v = valueExtractor(key, value, index);
      // if (v < dataExtent[0]) {dataExtent[0] = v; updateScale()}
      // if (v > dataExtent[1]) {dataExtent[1] = v; updateScale()}

      c = (type != undefined) ? modifyOpacity(h(scale(v)), opac) : h(scale(v))
    }

    else if (colorBy == 'category' ){
      var cat = categoryExtractor(key, value, index);
      var v = categories.indexOf(cat)
      c = (type != undefined) ? modifyOpacity(h(scale(v)), opac) : h(scale(v))

    }

    else {
      c = (type != undefined) ? modifyOpacity(h(scale(index)), opac) : h(scale(index))
    }

    return c
  }

  function updateScale(){


    helperScale.domain([0, colors.length])
    if (colorBy == 'category' && categories != undefined) { helperScale.range([0, categories.length]) }
    else { helperScale.range(dataExtent) }


    var a = Array(colors.length).fill(0).map(function(d, i){ return helperScale(i) })
    scale.domain(a)
  }

  return colorFunction
}
