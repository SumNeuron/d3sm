import utils from './utils/index.js'
import * as d3 from "d3";
/*******************************************************************************
**                                                                            **
**                                                                            **
**                             D3 EXTENSIONS                                  **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
* Recursively ascends parents of selection until it finds an svg tag
* @function d3.selection.thisSVG
* @augments d3.selection
* @returns {Element} which is the svg tag, not the d3 selection of that tag
*/
d3.selection.prototype.thisSVG = function() { return utils.sel.getContainingSVG(this.node()); }


/**
* Helper for getting absolute position of the mouse
* @function d3.mouse.absolute
* @augments d3.mouse
* @returns {number[]} [x, y] as they relate to `html` not to local scope.
*/
d3.mouse.absolute = function() {
  var html = d3.select('html').node()
  var [x, y] = this(html)
  return [x, y]
}


/**
* Gets position of the selection in relation to the containing svg
* @see{@link getContainingSVG}
* @function d3.selection.absolutePosition
* @augments d3.selection
* @returns {Object} with structure similar to getBoundingClientRect, e.g.
* top, left, bottom, right, height, width
*/
d3.selection.prototype.absolutePosition = function() {
    var element = this.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = utils.sel.getContainingSVG(element)
    var svgPosition = containerSVG.getBoundingClientRect();

    return {
        top:    elementPosition.top    - svgPosition.top,
        left:   elementPosition.left   - svgPosition.left,
        bottom: elementPosition.bottom - svgPosition.top,
        right:  elementPosition.right  - svgPosition.left,
        height: elementPosition.height,
        width:  elementPosition.width
    };

}


d3.selection.prototype.relativePositionTo = function(container) {
    var element = this.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = container
    var svgPosition = containerSVG.getBoundingClientRect();

    return {
        top:    elementPosition.top    - svgPosition.top,
        left:   elementPosition.left   - svgPosition.left,
        bottom: elementPosition.bottom - svgPosition.top,
        right:  elementPosition.right  - svgPosition.left,
        height: elementPosition.height,
        width:  elementPosition.width
    };

}
