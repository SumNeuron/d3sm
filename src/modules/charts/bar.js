import * as d3 from 'd3'
import utils from '../utils/index.js'
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';
/*******************************************************************************
**                                                                            **
**                                                                            **
**                                   BAR                                      **
**                                                                            **
**                                                                            **
*******************************************************************************/

/**
 * Creates a bar
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/bar-chart-same-data-complex-grouping/index.html Demo}
 * @constructor bar
 * @param {d3.selection} selection
 * @namespace bar
 * @returns {function} bar
 */
export default function bar ( selection ) {
  /*
  Assumes that data is list an object.

  The keys of data will be bound to the bars.
  The valueExtractor function will extract the value from data[key].

  Grouping can be used if desired. It should be an arbitrary complex list where
  the values are strings matching keys in data.
  */
  var
  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a bar
  * (see {@link bar#data})
  * @param {Object} [data=undefined]
  * @memberof bar#
  * @property
  */
  data,
  /**
  * Which direction to render the bars in
  * (see {@link bar#orient})
  * @param {number} [orient='horizontal']
  * @memberof bar#
  * @property
  */
  orient='horizontal',
  /**
  * Amount of horizontal space (in pixels) avaible to render the bar in
  * (see {@link bar#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof bar#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the bar in
  * (see {@link bar.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof bar#
  * @property
  */
  spaceY,

  /**
  * Whether or not to allow bar to render elements pass the main spatial dimension
  * given the orientation (see {@link bar#orient}), where {@link bar#orient}="horizontal"
  * the main dimension is {@link bar#spaceX} and where {@link bar#orient}="vertical"
  * the main dimension is {@link bar#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof bar#
  * @property
  */
  overflowQ = false,

  /**
  * An array - putatively of other arrays - depicting how bars should be arranged
  * @param {Array[]} [grouping=undefined]
  * @memberof bar#
  * @property
  */
  grouping,

  /**
  * How to get the value of the bar
  * @param {function} [valueExtractor=function(key, index) { return data[key] }]
  * @memberof bar#
  * @property
  */
  valueExtractor = function(key, index) { return data[key] },
  /**
  * How to sort the bars - if {@link bar#grouping} is not provided.
  * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
  * @memberof bar#
  * @property
  */
  sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},

  /**
  * The scale for which bar values should be transformed by
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof bar#
  * @property
  */
  scale = d3.scaleLinear(),
  /**
  * The padding for the domain of the scale (see {@link bar#scale})
  * @param {number} [domainPadding=0.5]
  * @memberof bar#
  * @property
  */
  domainPadding = 0.5,

  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link bar#orient}), where {@link bar#orient}="horizontal"
  * the main dimension is {@link bar#spaceX} and where {@link bar#orient}="vertical"
  * the main dimension is {@link bar#spaceY} between bars
  * @param {number} [objectSpacer=0.05]
  * @memberof bar#
  * @property
  */
  objectSpacer = 0.05,
  /**
  * The minimum size that an object can be
  * @param {number} [minObjectSize=50]
  * @memberof bar#
  * @property
  */
  minObjectSize = 50,
  /**
  * The maximum size that an object can be
  * @param {number} [maxObjectSize=100]
  * @memberof bar#
  * @property
  */
  maxObjectSize = 100,

  /**
  * The stroke width of the bars
  * @param {number} [barStrokeWidth=2]
  * @memberof bar#
  * @property
  */
  barStrokeWidth = 2,
  /**
  * Instance of ColorFunction
  * @param {function} [colorFunction = colorFunction()]
  * @memberof bar#
  * @property
  */
  colorFunction = CF(),


  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof bar#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of bar
  * @param {string} [namespace="d3sm-bar"]
  * @memberof bar#
  * @property
  */
  namespace = 'd3sm-bar',
  /**
  * Class name for bar container (<g> element)
  * @param {string} [objectClass="bar"]
  * @memberof bar#
  * @property
  */
  objectClass = 'bar',

  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof bar#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof bar#
  * @property
  */
  easeFunc = d3.easeExp,

  // useful values to extract to prevent re-calculation
  /**
  * The keys of the bars
  * @param {string[]} [barKeys=undefined]
  * @memberof bar#
  * @property
  */
  barKeys,
  /**
  * The values of the bars
  * @param {number[]} [barValues=undefined]
  * @memberof bar#
  * @property
  */
  barValues,
  /**
  * The objectSize (actual width) used by the bars
  * @param {number} [objectSize=undefined]
  * @memberof bar#
  * @property
  */
  objectSize,
  /**
  * The spacerSize (actual width) used by the spacers between the bars
  * @param {number} [spacerSize=undefined]
  * @memberof bar#
  * @property
  */
  spacerSize,
  /**
  * Instance of Tooltip
  * @param {function} [tooltip=tooltip()]
  * @memberof bar#
  * @property
  */
  tooltip = TTip(),
  barPercent = 1

  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {bar | d3.selection}
   * @memberof bar
   * @property
   * by default selection = selection
   */
  bar.selection = function(_) { return arguments.length ? (selection = _, bar) : selection; };
  /**
   * Gets or sets the data
   * (see {@link bar#data})
   * @param {number} [_=none]
   * @returns {bar | object}
   * @memberof bar
   * @property
   */
  bar.data = function(_) { return arguments.length ? (data = _, bar) : data; };
  /**
   * Gets or sets the orient of the bars
   * (see {@link bar#orient})
   * @param {number} [_=none]
   * @returns {bar | object}
   * @memberof bar
   * @property
   */
  bar.orient = function(_) { return arguments.length ? (orient = _, bar) : orient; };
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link bar#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default spaceX = undefined
   */
  bar.spaceX = function(_) { return arguments.length ? (spaceX = _, bar) : spaceX; };
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link bar#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default spaceY = undefined
   */
  bar.spaceY = function(_) { return arguments.length ? (spaceY = _, bar) : spaceY; };

  /**
   * Gets / sets whether or not bar is allowed to go beyond specified dimensions
   * (see {@link bar#spaceX})
   * @param {boolean} [_=none]
   * @returns {bar | boolean}
   * @memberof bar
   * @property
   * by default overflowQ = false
   */
  bar.overflowQ = function(_) { return arguments.length ? (overflowQ = _, bar) : overflowQ; };
  /**
   * Gets / sets the grouping of the bars
   * (see {@link bar#grouping})
   * @param {Array[]} [_=none]
   * @returns {bar | Array[]}
   * @memberof bar
   * @property
   * by default grouping = undefined
   */
  bar.grouping = function(_) { return arguments.length ? (grouping = _, bar) : grouping; };
  /**
   * Gets / sets the valueExtractor
   * (see {@link bar#valueExtractor})
   * @param {function} [_=none]
   * @returns {bar | function}
   * @memberof bar
   * @property
   * by default valueExtractor = function(key, index) { return data[key] },
   */
  bar.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, bar) : valueExtractor; };
  /**
   * Gets / sets the sortingFunction
   * (see {@link bar#sortingFunction})
   * @param {function} [_=none]
   * @returns {bar | function}
   * @memberof bar
   * @property
   * by default sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},
   */
  bar.sortingFunction = function(_) { return arguments.length ? (sortingFunction = _, bar) : sortingFunction; };
  /**
   * Gets / sets the scale for which the bar values should be transformed by
   * (see {@link bar#scale})
   * @param {d3.scale} [_=none]
   * @returns {bar | d3.scale}
   * @memberof bar
   * @property
   * by default scale = d3.scaleLinear()
   */
  bar.scale = function(_) { return arguments.length ? (scale = _, bar) : scale; };
  /**
   * Gets / sets the padding for the domain of the scale
   * (see {@link bar#domainPadding})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default domainPadding = 0.5
   */
  bar.domainPadding = function(_) { return arguments.length ? (domainPadding = _, bar) : domainPadding; };
  /**
   * Gets / sets objectSpacer
   * (see {@link bar#objectSpacer})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default objectSpacer = 0.05
   */
  bar.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, bar) : objectSpacer; };
  /**
   * Gets / sets the minObjectSize
   * (see {@link bar#minObjectSize})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default minObjectSize = 50
   */
  bar.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, bar) : minObjectSize; };
  /**
   * Gets / sets the maxObjectSize
   * (see {@link bar#maxObjectSize})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default maxObjectSize = 100
   */
  bar.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, bar) : maxObjectSize; };

  /**
   * Gets / sets the barStrokeWidth
   * (see {@link bar#barStrokeWidth})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default barStrokeWidth = 2
   */
  bar.barStrokeWidth = function(_) { return arguments.length ? (barStrokeWidth = _, bar) : barStrokeWidth; };
  /**
   * Gets / sets the colorFunction
   * (see {@link bar#colorFunction})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default colorFunction = colorFunction()
   */
  bar.colorFunction = function(_) { return arguments.length ? (colorFunction = _, bar) : colorFunction; };

  /**
   * Gets / sets the backgroundFill
   * (see {@link bar#backgroundFill})
   * @param {string} [_=none]
   * @returns {bar | string}
   * @memberof bar
   * @property
   * by default backgroundFill = 'transparent'
   */
  bar.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, bar) : backgroundFill; };
  /**
   * Gets / sets the namespace
   * (see {@link bar#namespace})
   * @param {string} [_=none]
   * @returns {bar | string}
   * @memberof bar
   * @property
   * by default namespace = 'd3sm-bar'
   */
  bar.namespace = function(_) { return arguments.length ? (namespace = _, bar) : namespace; };
  /**
   * Gets / sets the objectClass
   * (see {@link bar#objectClass})
   * @param {string} [_=none]
   * @returns {bar | string}
   * @memberof bar
   * @property
   * by default objectClass = 'tick-group'
   */
  bar.objectClass = function(_) { return arguments.length ? (objectClass = _, bar) : objectClass; };
  /**
   * Gets / sets the transitionDuration
   * (see {@link bar#transitionDuration})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default transitionDuration = 1000
   */
  bar.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, bar) : transitionDuration; };
  /**
   * Gets / sets the easeFunc
   * (see {@link bar#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {bar | d3.ease}
   * @memberof bar
   * @property
   * by default easeFunc = d3.easeExp
   */
  bar.easeFunc = function(_) { return arguments.length ? (easeFunc = _, bar) : easeFunc; };


  /**
   * Gets / sets the barKeys
   * (see {@link bar#barKeys})
   * @param {string[]} [_=none]
   * @returns {bar | string[]}
   * @memberof bar
   * @property
   * by default barKeys = undefined
   */
  bar.barKeys = function(_) { return arguments.length ? (barKeys = _, bar) : barKeys; };
  /**
   * Gets / sets the barValues
   * (see {@link bar#barValues})
   * @param {number[]} [_=none]
   * @returns {bar | number[]}
   * @memberof bar
   * @property
   * by default barValues = undefined
   */
  bar.barValues = function(_) { return arguments.length ? (barValues = _, bar) : barValues; };
  /**
   * Gets / sets the objectSize
   * (see {@link bar#objectSize})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default objectSize = undefined
   */
  bar.objectSize = function(_) { return arguments.length ? (objectSize = _, bar) : objectSize; };
  /**
   * Gets / sets the spacerSize
   * (see {@link bar#spacerSize})
   * @param {number} [_=none]
   * @returns {bar | number}
   * @memberof bar
   * @property
   * by default spacerSize = undefined
   */
  bar.spacerSize = function(_) { return arguments.length ? (spacerSize = _, bar) : spacerSize; };

  /**
   * Gets / sets the tooltip
   * (see {@link bar#tooltip})
   * @param {tooltip} [_=none]
   * @returns {bar | tooltip}
   * @memberof bar
   * @property
   * by default tooltip = tooltip()
   */
  bar.tooltip = function(_) { return arguments.length ? (tooltip = _, bar) : tooltip; };

  bar.barPercent = function(_) { return arguments.length ? (barPercent = _, bar) : barPercent; };

  function bar() {
    // for convenience in handling orientation specific values
    var horizontalQ = (orient == 'horizontal' || orient == 'bottom' || orient == 'top') ? true : false
    var verticalQ = !horizontalQ

    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    // to prevent re-calculation and getters to be passed to axes
    barKeys = d3.keys(data)
    barValues = barKeys.map(valueExtractor)

    // if grouping is undefined sort barKeys by sortingFunction
    var ordered = (grouping == undefined) ? barKeys.sort(sortingFunction) : grouping
    // ordered might be nested depending on grouping
    barKeys = utils.arr.flatten(ordered)

    var numberOfObjects = barKeys.length
    var extent = [Math.min(...barValues) - domainPadding,Math.max(...barValues) + domainPadding];



    // set the scale

    scale.domain(extent).range(horizontalQ
      ? [0,spaceY]
      : orient == 'right'
        ? [0, spaceX]
        : [spaceX, 0]
    )
    var space = horizontalQ ? spaceX : spaceY
    // calculate object size
    objectSize =  (objectSize == undefined)
    ? utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    : objectSize

    // calculate spacer size if needed
    spacerSize = (spacerSize == undefined)
    ? utils.math.calculateWidthOfSpacer(barKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ)
    : spacerSize
    // make the nested groups
    var spacerFunction = groupingSpacer()
    .horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects)
    .objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace(namespace)
    // safe default function
    var defaultExit = spacerFunction.exitFunction()

    spacerFunction.exitFunction(function(sel){
      // use default to move objects off screen
      // console.log("EXIT", sel.nodes(), objectSize, scale(extent[1]))
      if (objectSize == undefined) {console.log(sel.nodes(), objectSize)}
      defaultExit(sel)
      sel.selectAll('g').classed("to-remove", true)
      // shrink rectangles in addition
      sel.selectAll('* > rect')
      .transition().duration(transitionDuration)
      .attr('transform', function(d, i) {
        var
        x = horizontalQ
          ? 0
          : 0
        ,
        y = verticalQ
          ? 0
          : scale(extent[1])
        ,
        t = 'translate('+x+','+y+')'
        return t
      })
      .attr('width', horizontalQ ? objectSize : 0)
      .attr('height', verticalQ ? objectSize : 0)
      .remove()
    })


    // move stuff
    spacerFunction(container, ordered, 0)





    var parentIndexArray = []
    container.selectAll('g:not(.to-remove).'+objectClass)
    .each(function(d, i){parentIndexArray.push(Number(d3.select(this).attr('parent-index')))})


    colorFunction = colorFunction.colorBy() == 'index'
    ? colorFunction.dataExtent([0, Math.max(...parentIndexArray)])
    : colorFunction.dataExtent(extent)



    container.selectAll('g.'+objectClass+':not(.to-remove)').each(function(key, i) {
      // console.log(key, scale(extent[1]) - scale(valueExtractor(key, i)))
      var t = d3.select(this),
      currentData = data[key],
      value = valueExtractor(key, i),
      i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
      fillColor = colorFunction(key, value, i, 'fill'), // prevent duplicate computation
      strokeColor = colorFunction(key, value, i,  'stroke')


      var bar = utils.sel.safeSelect(t, 'rect', 'bar-rect')

      if (bar.attr('transform') == undefined) {
        bar.attr('transform', function(d, i) {
          var
          x = horizontalQ
            ? 0
            : 0
          ,
          y = verticalQ
            ? 0
            : scale(extent[1])
          ,
          t = 'translate('+x+','+y+')'
          return t
        })
        .attr('width', horizontalQ ? objectSize : 0)
        .attr('height', verticalQ ? objectSize : 0)

      }


      bar.transition().duration(transitionDuration).ease(easeFunc)
      .attr('transform', function(d, i) {
        var
        x = horizontalQ
          ? objectSize - objectSize * barPercent
          : orient == 'right'
            ? scale(extent[1]) - scale(value)
            : objectSize - objectSize * barPercent
          ,
        y = verticalQ
          ? objectSize - objectSize * barPercent
          : scale(extent[1]) - scale(value)
        ,
        t = 'translate('+x+','+y+')'
        return t
      })
      .attr('width', horizontalQ ? objectSize * barPercent : scale(value))
      .attr('height', verticalQ ? objectSize * barPercent: scale(value))
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', barStrokeWidth)



      t.on('mouseover', function(d, i){
        container.selectAll('g.'+objectClass).style('opacity', 0.2)
        t.style('opacity', 1)
        bar.attr('stroke-width',barStrokeWidth*2)

      })
      t.on('mouseout', function(){
        container.selectAll('g.'+objectClass).style('opacity', 1)
        bar.attr('stroke-width', barStrokeWidth)
      })
    })

    tooltip.selection(container.selectAll('.bar-rect'))
    .data(data)

    tooltip()

  }
  return bar
}
