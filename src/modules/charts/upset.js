import * as d3 from 'd3'
import utils from '../utils/index.js'
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';
export default function upset ( selection ) {
  var
  data,
  orient='horizontal',
  spaceX,
  spaceY,
  overflowQ=false,
  minObjectSize=20,
  maxObjectSize=50,
  circleStrokeWidth=2,
  // colorFunction=
  backgroundFill = 'transparent',
  namespace='d3sm-upset',
  objectClass = 'upset',

  transitionDuration = 1000,
  easeFunc = d3.easeExp,

  setKey = "set",
  intersectionKey = "intersection",
  elementsKey = "elements",

  setExtractor = function(key, i) {return data[key][setKey]},
  intersectionExtractor = function(key, i) {return data[key][intersectionKey]},
  elementExtractor = function(key, i) {return data[key][elementsKey]},

  cellKeys,
  setValues,
  intersectionValues,
  elementValues,

  xObjectSpacer = 0.05,
  yObjectSpacer = 0.05,
  radius,

  // listDelim = ';'

  yObjectSize,
  ySpacerSize,
  xObjectSize,
  xSpacerSize,

  setKeySortingFunction = function(a, b) { return setValues.indexOf(setExtractor(a)) - setValues.indexOf(setExtractor(b)) },
  intersectionKeySortingFunction = function(a, b) { return intersectionValues.indexOf(intersectionExtractor(a)) - intersectionValues.indexOf(intersectionExtractor(b)) }


  upset.selection = function(_) { return arguments.length ? (selection = _, upset) : selection; };
  upset.data = function(_) { return arguments.length ? (data = _, upset) : data; };
  upset.orient = function(_) { return arguments.length ? (orient = _, upset) : orient; };
  upset.spaceX = function(_) { return arguments.length ? (spaceX = _, upset) : spaceX; };
  upset.spaceY = function(_) { return arguments.length ? (spaceY = _, upset) : spaceY; };
  upset.overflowQ = function(_) { return arguments.length ? (overflowQ = _, upset) : overflowQ; };
  upset.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, upset) : minObjectSize; };
  upset.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, upset) : maxObjectSize; };
  upset.circleStrokeWidth = function(_) { return arguments.length ? (circleStrokeWidth = _, upset) : circleStrokeWidth; };
  upset.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, upset) : backgroundFill; };
  upset.namespace = function(_) { return arguments.length ? (namespace = _, upset) : namespace; };
  upset.objectClass = function(_) { return arguments.length ? (objectClass = _, upset) : objectClass; };
  upset.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, upset) : transitionDuration; };
  upset.easeFunc = function(_) { return arguments.length ? (easeFunc = _, upset) : easeFunc; };
  upset.cellKeys = function(_) { return arguments.length ? (cellKeys = _, upset) : cellKeys; };
  upset.setValues = function(_) { return arguments.length ? (setValues = _, upset) : setValues; };
  upset.intersectionValues = function(_) { return arguments.length ? (intersectionValues = _, upset) : intersectionValues; };
  upset.xObjectSpacer = function(_) { return arguments.length ? (xObjectSpacer = _, upset) : xObjectSpacer; };
  upset.yObjectSpacer = function(_) { return arguments.length ? (yObjectSpacer = _, upset) : yObjectSpacer; };
  upset.radius = function(_) { return arguments.length ? (radius = _, upset) : radius; };
  upset.setExtractor = function(_) { return arguments.length ? (setExtractor = _, upset) : setExtractor; };
  upset.intersectionExtractor = function(_) { return arguments.length ? (intersectionExtractor = _, upset) : intersectionExtractor; };
  upset.elementExtractor = function(_) { return arguments.length ? (elementExtractor = _, upset) : elementExtractor; };
  upset.setKeySortingFunction = function(_) { return arguments.length ? (setKeySortingFunction = _, upset) : setKeySortingFunction; };
  upset.intersectionKeySortingFunction = function(_) { return arguments.length ? (intersectionKeySortingFunction = _, upset) : intersectionKeySortingFunction; };

  upset.yObjectSize = function(_) { return arguments.length ? (yObjectSize = _, upset) : yObjectSize; };
  upset.ySpacerSize = function(_) { return arguments.length ? (ySpacerSize = _, upset) : ySpacerSize; };
  upset.xObjectSize = function(_) { return arguments.length ? (xObjectSize = _, upset) : xObjectSize; };
  upset.xSpacerSize = function(_) { return arguments.length ? (xSpacerSize = _, upset) : xSpacerSize; };

  function upset() {
    // for convenience in handling orientation specific values
    var horizontalQ = (orient == 'horizontal') ? true : false
    var verticalQ = !horizontalQ

    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );


    cellKeys = d3.keys(data)
    setValues = utils.arr.unique(cellKeys.map(setExtractor)).sort()
    intersectionValues = utils.arr.unique(cellKeys.map(intersectionExtractor)).sort().sort(function(a, b){
      return a.split(';').length - b.split(';').length
    })

    if (!horizontalQ) {
      cellKeys.sort(function(a, b){ return setKeySortingFunction(a, b) || intersectionKeySortingFunction(a, b) })
    } else {
      cellKeys.sort(function(a, b){ return intersectionKeySortingFunction(a, b) || setKeySortingFunction(a, b) })
    }




    var
    xValues = horizontalQ ? intersectionValues : setValues,
    yValues = horizontalQ ? setValues : intersectionValues,
    xDim = horizontalQ ? xValues.length : yValues.length,
    yDim = horizontalQ ? yValues.length : xValues.length

    // console.utils.con.log(xValues, yValues)


    xObjectSize = utils.math.calculateWidthOfObject(spaceX, xDim, minObjectSize, maxObjectSize, xObjectSpacer, overflowQ)
    yObjectSize = utils.math.calculateWidthOfObject(spaceY, yDim, minObjectSize, maxObjectSize, yObjectSpacer, overflowQ)
    xSpacerSize = utils.math.calculateWidthOfSpacer(xValues, spaceX, xObjectSize, xDim, xObjectSpacer, overflowQ)
    ySpacerSize = utils.math.calculateWidthOfSpacer(yValues, spaceY, yObjectSize, yDim, yObjectSpacer, overflowQ)

    var ySpacer = groupingSpacer()
    .horizontalQ(false)
    .moveby('category').numberOfObjects(yDim)
    .objectSize(yObjectSize).spacerSize(ySpacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)

    var xSpacer = groupingSpacer()
    .horizontalQ(true)
    .moveby('category').numberOfObjects(xDim)
    .objectClass(objectClass)
    .objectSize(xObjectSize).spacerSize(xSpacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)



    if (verticalQ) {
      xSpacer.objectClass(objectClass)
      ySpacer.namespace('across').objectClass(utils.str.hypenate(objectClass, 'across'))

      ySpacer(container, yValues, 0)
      container.selectAll('g.'+utils.str.hypenate(objectClass, 'across'))
      .each(function(d, i){ xSpacer(d3.select(this), xValues, 0) })
    } else {
      xSpacer.namespace('across').objectClass(utils.str.hypenate(objectClass, 'across'))
      ySpacer.objectClass(objectClass)

      xSpacer(container, xValues, 0)
      container.selectAll('g.'+utils.str.hypenate(objectClass, 'across'))
      .each(function(d, i){ ySpacer(d3.select(this), yValues, 0) })
    }


    var cells = container.selectAll('g:not(.to-remove).'+objectClass)
    var lookup = {}
    cellKeys.map(function(k, i){
      lookup[setExtractor(k)+'::'+intersectionExtractor(k)] = k
    })

    // var positionedCellKeys = []
    // for (var i = 0; i < setValues.length; i++) {
    //   for (var j = 0; j < intersectionValues.length; j++) {
    //     var lookupValue = lookup[setValues[j]+"::"+intersectionValues[i]]
    //     if (lookupValue == undefined) {
    //       positionedCellKeys.push(undefined)
    //     } else {
    //       positionedCellKeys.push(lookupValue)
    //     }
    //     console.utils.con.log(i, j, lookupValue)
    //   }
    // }
    //
    // // console.utils.con.log(positionedCellKeys)
    //
    // cells.data(positionedCellKeys);


    cells.data(cellKeys);

    cells.each(function(key, i) {
      var t = d3.select(this)
      if (key == undefined) {return }
      var
      currentData = data[key]
      // console.utils.con.log(key, currentData)
      var
      set = setExtractor(key, i),
      intersection = intersectionExtractor(key, i)

      // console.utils.con.log(set, intersection)

      t.classed(intersection, true)
      t.classed(set, true)

      var c = utils.sel.safeSelect(t, 'circle', utils.str.hypenate(objectClass,'circle'))
      c.attr('cx', xObjectSize / 2)
      .attr('cy', yObjectSize / 2 )
      .attr('r', radius == undefined ? Math.min(xObjectSize, yObjectSize) / 2 : radius)
      .attr('fill', intersection.includes(set) ? "black": 'rgb(233,233,233)')
      .attr('stroke', "black")
      .attr("in-intersection", intersection.includes(set))
    })



  }

  function intersectionTotals() {
    var totals = {}
    // intersectionValues.sort(function(a, b){ return intersectionKeySortingFunction(a, b) })

    intersectionValues.map(function(k, i){ totals[k] = {'total': 0} })
    cellKeys.map(function(k, i){
      var e = elementExtractor(k, i);
      if (totals[intersectionExtractor(k, i)]['total'] == 0) {
        if (Array.isArray(e)) {
          totals[intersectionExtractor(k, i)]['total']+= e.length
          totals[intersectionExtractor(k, i)]['values'] = e
        } else {
          totals[intersectionExtractor(k, i)]['total']+= e
        }

      }
    })
    return totals
  }

  function setTotals(){
    var totals = {}
    // intersectionValues.sort(function(a, b){ return intersectionKeySortingFunction(a, b) })

    setValues.map(function(k, i){ totals[k] = {'total': 0} })

    cellKeys.map(function(k, i){
      var e = elementExtractor(k, i);
      if (Array.isArray(e)) {
        totals[setExtractor(k, i)]['total']+= e.length
      } else {
        totals[setExtractor(k, i)]['total']+= e
      }
    })
    return totals
  }

  upset.intersectionTotals = intersectionTotals
  upset.setTotals = setTotals

  return upset
}
