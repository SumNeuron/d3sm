import * as d3 from 'd3'
import utils from '../utils/index.js';
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';
/*******************************************************************************
**                                                                            **
**                                                                            **
**                             BOX AND WHISKER                                **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Creates a boxwhisker
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/box-whiskers/index.html Demo}
 * @constructor boxwhisker
 * @param {d3.selection} selection
 * @namespace boxwhisker
 * @returns {function} boxwhisker
 */
export default function boxwhisker( selection ) {
  var
  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a box
  * (see {@link boxwhisker#data})
  * @param {Object} [data=undefined]
  * @memberof boxwhisker#
  * @property
  */
  data,
  /**
  * Which direction to render the boxes in
  * (see {@link boxwhisker#orient})
  * @param {number} [orient='horizontal']
  * @memberof boxwhisker#
  * @property
  */
  orient = 'horizontal',
  /**
  * Amount of horizontal space (in pixels) avaible to render the boxwhisker in
  * (see {@link boxwhisker#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof boxwhisker#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the boxwhisker in
  * (see {@link boxwhisker.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof boxwhisker#
  * @property
  */
  spaceY,
  /**
  * Whether or not to allow boxwhisker to render elements pass the main spatial dimension
  * given the orientation (see {@link boxwhisker#orient}), where {@link boxwhisker#orient}="horizontal"
  * the main dimension is {@link boxwhisker#spaceX} and where {@link boxwhisker#orient}="vertical"
  * the main dimension is {@link boxwhisker#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof boxwhisker#
  * @property
  */
  overflowQ = true,

  /**
  * An array - putatively of other arrays - depicting how boxes should be arranged
  * @param {Array[]} [grouping=undefined]
  * @memberof boxwhisker#
  * @property
  */
  grouping,
  quartilesKey = 'quartiles', // key in object where quartiles are stored
  quartilesKeys = ["0.00", "0.25", "0.50", "0.75", "1.00"], // keys in quartiles object mapping values to min, q1, q2, q3 and max


  /**
  * How to get the value of the boxwhisker
  * @param {function} [valueExtractor=function(key, index) { return data[key][quartilesKey] }]
  * @memberof boxwhisker#
  * @property
  */
  valueExtractor = function(key, index) { return data[key][quartilesKey] },
  /**
  * How to sort the boxes - if {@link boxwhisker#grouping} is not provided.
  * @param {function} [sortingFunction=descending]
  * @memberof boxwhisker#
  * @property
  * default
  * function(keyA, keyB) {return d3.descending(
  *   valueExtractor(keyA)[quartilesKeys[4]],
  *   valueExtractor(keyB)[quartilesKeys[4]]
  * )}
  */
  sortingFunction = function(keyA, keyB) {return d3.descending(
    valueExtractor(keyA)[quartilesKeys[4]],
    valueExtractor(keyB)[quartilesKeys[4]]
  )},
  /**
  * The scale for which boxwhisker values should be transformed by
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof boxwhisker#
  * @property
  */
  scale = d3.scaleLinear(),
  /**
  * The padding for the domain of the scale (see {@link boxwhisker#scale})
  * @param {number} [domainPadding=0.5]
  * @memberof boxwhisker#
  * @property
  */
  domainPadding = 0.5,
  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link boxwhisker#orient}), where {@link boxwhisker#orient}="horizontal"
  * the main dimension is {@link boxwhisker#spaceX} and where {@link boxwhisker#orient}="vertical"
  * the main dimension is {@link boxwhisker#spaceY} between boxes
  * @param {number} [objectSpacer=0.05]
  * @memberof boxwhisker#
  * @property
  */
  objectSpacer = 0.05,
  /**
  * The minimum size that an object can be
  * @param {number} [minObjectSize=15]
  * @memberof boxwhisker#
  * @property
  */
  minObjectSize = 15,
  /**
  * The maximum size that an object can be
  * @param {number} [maxObjectSize=50]
  * @memberof boxwhisker#
  * @property
  */
  maxObjectSize = 50,
  /**
  * Percent of box and whisker size that whiskers will be rendered
  * see {@link boxwhisker#objectSize}
  * @param {number} [whiskerWidthPercent=0.6]
  * @memberof boxwhisker#
  * @property
  */
  whiskerWidthPercent = .6,
  /**
  * Instance of ColorFunction
  * @param {function} [colorFunction = colorFunction()]
  * @memberof boxwhisker#
  * @property
  */
  colorFunction = CF(),
  /**
  * The stroke width of the boxes
  * @param {number} [boxStrokeWidth=2]
  * @memberof boxwhisker#
  * @property
  */
  boxStrokeWidth = 2,
  /**
  * The stroke width of the whiskers
  * @param {number} [whiskerStrokeWidth=2]
  * @memberof boxwhisker#
  * @property
  */
  whiskerStrokeWidth = 2,

  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof boxwhisker#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of boxwhisker
  * @param {string} [namespace="d3sm-box-whisker"]
  * @memberof boxwhisker#
  * @property
  */
  namespace = 'd3sm-box-whisker',
  /**
  * Class name for boxwhisker container (<g> element)
  * @param {string} [objectClass="box-whisk"]
  * @memberof boxwhisker#
  * @property
  */
  objectClass = 'box-whisk',

  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof boxwhisker#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof boxwhisker#
  * @property
  */
  easeFunc = d3.easeExp,

  /**
  * The keys of the boxes
  * @param {string[]} [boxKeys=undefined]
  * @memberof boxwhisker#
  * @property
  */
  boxKeys,
  /**
  * The values of the boxes
  * @param {string[]} [boxValues=undefined]
  * @memberof boxwhisker#
  * @property
  */
  boxValues,
  /**
  * The objectSize (actual width) used by the boxes
  * @param {number} [objectSize=undefined]
  * @memberof boxwhisker#
  * @property
  */
  objectSize,
  /**
  * The spacerSize (actual width) used by the spacers between the boxes
  * @param {number} [spacerSize=undefined]
  * @memberof boxwhisker#
  * @property
  */
  spacerSize,
  /**
  * Instance of Tooltip
  * @param {function} [tooltip=tooltip()]
  * @memberof boxwhisker#
  * @property
  */
  tooltip = TTip()
  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {boxwhisker | d3.selection}
   * @memberof boxwhisker
   * @property
   * by default selection = selection
   */
  boxwhisker.selection = function(_) { return arguments.length ? (selection = _, boxwhisker) : selection; };
  /**
   * Gets or sets the data
   * (see {@link boxwhisker#data})
   * @param {number} [_=none]
   * @returns {boxwhisker | object}
   * @memberof boxwhisker
   * @property
   */
  boxwhisker.data = function(_) { return arguments.length ? (data = _, boxwhisker) : data; };
  /**
   * Gets or sets the orient of the boxes
   * (see {@link boxwhisker#orient})
   * @param {number} [_=none]
   * @returns {boxwhisker | object}
   * @memberof boxwhisker
   * @property
   */
  boxwhisker.orient = function(_) { return arguments.length ? (orient = _, boxwhisker) : orient; };
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link boxwhisker#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default spaceX = undefined
   */
  boxwhisker.spaceX = function(_) { return arguments.length ? (spaceX = _, boxwhisker) : spaceX; };
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link boxwhisker#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default spaceY = undefined
   */
  boxwhisker.spaceY = function(_) { return arguments.length ? (spaceY = _, boxwhisker) : spaceY; };
  /**
   * Gets / sets whether or not boxwhisker is allowed to go beyond specified dimensions
   * (see {@link boxwhisker#overflowQ})
   * @param {boolean} [_=none]
   * @returns {boxwhisker | boolean}
   * @memberof boxwhisker
   * @property
   * by default overflowQ = false
   */
  boxwhisker.overflowQ = function(_) { return arguments.length ? (overflowQ = _, boxwhisker) : overflowQ; };
  /**
   * Gets / sets the grouping of the boxes
   * (see {@link boxwhisker#grouping})
   * @param {Array[]} [_=none]
   * @returns {boxwhisker | Array[]}
   * @memberof boxwhisker
   * @property
   * by default grouping = undefined
   */
  boxwhisker.grouping = function(_) { return arguments.length ? (grouping = _, boxwhisker) : grouping; };
  /**
   * Gets / sets the quartilesKey
   * (see {@link boxwhisker#quartilesKey})
   * @param {string} [_=none]
   * @returns {boxwhisker | string}
   * @memberof boxwhisker
   * @property
   * by default quartilesKey = quartiles
   */
  boxwhisker.quartilesKey = function(_) { return arguments.length ? (quartilesKey = _, boxwhisker) : quartilesKey; };
  /**
   * Gets / sets the quartilesKeys
   * (see {@link boxwhisker#quartilesKeys})
   * @param {string[]} [_=none]
   * @returns {boxwhisker | string[]}
   * @memberof boxwhisker
   * @property
   * by default quartilesKeys = [Q0, Q1, Q2, Q3, Q4]
   */
  boxwhisker.quartilesKeys = function(_) { return arguments.length ? (quartilesKeys = _, boxwhisker) : quartilesKeys; };
  /**
   * Gets / sets the valueExtractor
   * (see {@link boxwhisker#valueExtractor})
   * @param {function} [_=none]
   * @returns {boxwhisker | function}
   * @memberof boxwhisker
   * @property
   * by default valueExtractor = function(key, index) { return data[key][quartilesKey] },
   */

  boxwhisker.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, boxwhisker) : valueExtractor; };
  /**
   * Gets / sets the sortingFunction
   * (see {@link boxwhisker#sortingFunction})
   * @param {function} [_=none]
   * @returns {boxwhisker | function}
   * @memberof boxwhisker
   * @property
   * by default sortingFunction = function(keyA, keyB) {return d3.descending(
   *   valueExtractor(keyA)[quartilesKeys[4]],
   *   valueExtractor(keyB)[quartilesKeys[4]]
   * )},
   */
  boxwhisker.sortingFunction = function(_) { return arguments.length ? (sortingFunction = _, boxwhisker) : sortingFunction; };
  /**
   * Gets / sets the scale for which the boxwhisker values should be transformed by
   * (see {@link boxwhisker#scale})
   * @param {d3.scale} [_=none]
   * @returns {boxwhisker | d3.scale}
   * @memberof boxwhisker
   * @property
   * by default scale = d3.scaleLinear()
   */
  boxwhisker.scale = function(_) { return arguments.length ? (scale = _, boxwhisker) : scale; };
  /**
   * Gets / sets the padding for the domain of the scale
   * (see {@link boxwhisker#domainPadding})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default domainPadding = 0.5
   */
  boxwhisker.domainPadding = function(_) { return arguments.length ? (domainPadding = _, boxwhisker) : domainPadding; };
  /**
   * Gets / sets objectSpacer
   * (see {@link boxwhisker#objectSpacer})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default objectSpacer = 0.05
   */
  boxwhisker.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, boxwhisker) : objectSpacer; };
  /**
   * Gets / sets the minObjectSize
   * (see {@link boxwhisker#minObjectSize})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default minObjectSize = 15
   */
  boxwhisker.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, boxwhisker) : minObjectSize; };
  /**
   * Gets / sets the maxObjectSize
   * (see {@link boxwhisker#maxObjectSize})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default maxObjectSize = 50
   */
  boxwhisker.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, boxwhisker) : maxObjectSize; };
  /**
   * Gets / sets the whiskerWidthPercent
   * (see {@link boxwhisker#whiskerWidthPercent})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default maxObjectSize = 0.6
   */
  boxwhisker.whiskerWidthPercent = function(_) { return arguments.length ? (whiskerWidthPercent = _, boxwhisker) : whiskerWidthPercent; };
  /**
   * Gets / sets the colorFunction
   * (see {@link boxwhisker#colorFunction})
   * @param {colorFunction} [_=none]
   * @returns {boxwhisker | colorFunction}
   * @memberof boxwhisker
   * @property
   * by default colorFunction = colorFunction()
   */
  boxwhisker.colorFunction = function(_) { return arguments.length ? (colorFunction = _, boxwhisker) : colorFunction; };
  /**
   * Gets / sets the boxStrokeWidth
   * (see {@link boxwhisker#boxStrokeWidth})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default boxStrokeWidth = 2
   */
  boxwhisker.boxStrokeWidth = function(_) { return arguments.length ? (boxStrokeWidth = _, boxwhisker) : boxStrokeWidth; };
  /**
   * Gets / sets the whiskerStrokeWidth
   * (see {@link boxwhisker#whiskerStrokeWidth})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default whiskerStrokeWidth = 2
   */
  boxwhisker.whiskerStrokeWidth = function(_) { return arguments.length ? (whiskerStrokeWidth = _, boxwhisker) : whiskerStrokeWidth; };

  /**
   * Gets / sets the backgroundFill
   * (see {@link boxwhisker#backgroundFill})
   * @param {string} [_=none]
   * @returns {boxwhisker | string}
   * @memberof boxwhisker
   * @property
   * by default backgroundFill = 'transparent'
   */
  boxwhisker.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, boxwhisker) : backgroundFill; };
  /**
   * Gets / sets the namespace
   * (see {@link boxwhisker#namespace})
   * @param {string} [_=none]
   * @returns {boxwhisker | string}
   * @memberof boxwhisker
   * @property
   * by default namespace = 'd3sm-boxwhisker'
   */
  boxwhisker.namespace = function(_) { return arguments.length ? (namespace = _, boxwhisker) : namespace; };
  /**
   * Gets / sets the objectClass
   * (see {@link boxwhisker#objectClass})
   * @param {string} [_=none]
   * @returns {boxwhisker | string}
   * @memberof boxwhisker
   * @property
   * by default objectClass = 'tick-group'
   */
  boxwhisker.objectClass = function(_) { return arguments.length ? (objectClass = _, boxwhisker) : objectClass; };
  /**
   * Gets / sets the transitionDuration
   * (see {@link boxwhisker#transitionDuration})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default transitionDuration = 1000
   */
  boxwhisker.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, boxwhisker) : transitionDuration; };
  /**
   * Gets / sets the easeFunc
   * (see {@link boxwhisker#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {boxwhisker | d3.ease}
   * @memberof boxwhisker
   * @property
   * by default easeFunc = d3.easeExp
   */
  boxwhisker.easeFunc = function(_) { return arguments.length ? (easeFunc = _, boxwhisker) : easeFunc; };

  /**
   * Gets / sets the barKeys
   * (see {@link boxwhisker#boxKeys})
   * @param {string[]} [_=none]
   * @returns {boxwhisker | string[]}
   * @memberof boxwhisker
   * @property
   * by default boxKeys = undefined
   */
  boxwhisker.boxKeys = function(_) { return arguments.length ? (boxKeys = _, boxwhisker) : boxKeys; };
  /**
   * Gets / sets the boxValues
   * (see {@link boxwhisker#boxValues})
   * @param {number[]} [_=none]
   * @returns {boxwhisker | number[]}
   * @memberof boxwhisker
   * @property
   * by default boxValues = undefined
   */
  boxwhisker.boxValues = function(_) { return arguments.length ? (boxValues = _, boxwhisker) : boxValues; };
  /**
   * Gets / sets the objectSize
   * (see {@link boxwhisker#objectSize})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default objectSize = undefined
   */
  boxwhisker.objectSize = function(_) { return arguments.length ? (objectSize = _, boxwhisker) : objectSize; };
  /**
   * Gets / sets the spacerSize
   * (see {@link boxwhisker#spacerSize})
   * @param {number} [_=none]
   * @returns {boxwhisker | number}
   * @memberof boxwhisker
   * @property
   * by default spacerSize = undefined
   */
  boxwhisker.spacerSize = function(_) { return arguments.length ? (spacerSize = _, boxwhisker) : spacerSize; };
  /**
   * Gets / sets the tooltip
   * (see {@link boxwhisker#tooltip})
   * @param {tooltip} [_=none]
   * @returns {boxwhisker | tooltip}
   * @memberof boxwhisker
   * @property
   * by default tooltip = tooltip()
   */
  boxwhisker.tooltip = function(_) { return arguments.length ? (tooltip = _, boxwhisker) : tooltip; };


  function boxwhisker() {
    // for convenience in handling orientation specific values
    var horizontalQ = (orient == 'horizontal') ? true : false
    var verticalQ = !horizontalQ

    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    // if grouping is undefined sort keys by sorting funct
    var ordered = (grouping == undefined) ? d3.keys(data).sort(sortingFunction) : grouping
    // to prevent re-calculation and getters to be passed to axes
    boxKeys = utils.arr.flatten(ordered)
    boxValues = boxKeys.map(valueExtractor)


    var numberOfObjects = boxKeys.length
    var extent = [
      Math.min(...boxValues.map(function(d,i){return d[quartilesKeys[0]]})) - domainPadding,
      Math.max(...boxValues.map(function(d,i){return d[quartilesKeys[4]]})) + domainPadding
    ];

    // set the scale
    scale.domain(extent).range(horizontalQ ? [0,spaceY] : [spaceX, 0])
    var space = horizontalQ ? spaceX : spaceY
    // calculate object size
    objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    // calculate spacer size if needed
    spacerSize = utils.math.calculateWidthOfSpacer(boxKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ)
    // make the nested groups
    var spacerFunction = groupingSpacer()
    .horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects)
    .objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace(namespace)

    // move stuff
    spacerFunction(container, ordered, 0)

    var parentIndexArray = []
    container.selectAll('g:not(.to-remove).'+objectClass)
    .each(function(d, i){if (utils.arr.hasQ(boxKeys, d)){ parentIndexArray.push(Number(d3.select(this).attr('parent-index')))}})



    colorFunction = colorFunction.colorBy() == 'index'
    ? colorFunction.dataExtent([0, Math.max(...parentIndexArray)])
    : colorFunction.dataExtent(extent)


    // set attributes for box and whiskers
    container.selectAll('g:not(.to-remove).'+objectClass).each(function(key, i) {
      var t = d3.select(this),
      currentData = data[key],

      quartiles = valueExtractor(key, i),
      q0 = quartiles[quartilesKeys[0]],
      q1 = quartiles[quartilesKeys[1]],
      q2 = quartiles[quartilesKeys[2]],
      q3 = quartiles[quartilesKeys[3]],
      q4 = quartiles[quartilesKeys[4]]

      var i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
      fillColor = colorFunction(key, q2, i, 'fill'), // prevent duplicate computation
      strokeColor = colorFunction(key, q2, i,  'stroke')


      var
      whisk = utils.sel.safeSelect(t, 'g', 'whisker'),
      uWhisk = utils.sel.safeSelect(whisk, 'path', 'upper'),
      lWhisk = utils.sel.safeSelect(whisk, 'path', 'lower'),
      quart = utils.sel.safeSelect(t, 'g', 'quartile'),
      uQuart = utils.sel.safeSelect(quart, 'rect', 'upper'),
      lQuart = utils.sel.safeSelect(quart, 'rect', 'lower'),
      mQuart = utils.sel.safeSelect(quart, 'circle', 'median')


      // set upper quartile (q3)
      uQuart.transition().duration(transitionDuration).ease(easeFunc)
      .attr('width', horizontalQ ? objectSize : scale(q3) - scale(q2))
      .attr('height', verticalQ ? objectSize : scale(q3) - scale(q2))
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', boxStrokeWidth)
      .attr('transform', function(d, i){
        var
        x = horizontalQ ? 0 : scale(q2),
        y = verticalQ ? 0 : scale(extent[1]) - scale(q3),
        t = 'translate('+x+','+y+')'
        return t
      })

      // set lower quartile (q1)
      lQuart.transition().duration(transitionDuration).ease(easeFunc)
      .attr('width', horizontalQ ? objectSize : scale(q2) - scale(q1))
      .attr('height', verticalQ ? objectSize : scale(q2) - scale(q1))
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', boxStrokeWidth)
      .attr('transform', function(d, i){
        var
        x = horizontalQ ? 0 : scale(q1),
        y = verticalQ ? 0 : scale(extent[1]) - scale(q2),
        t = 'translate('+x+','+y+')'
        return t
      })


      // set median (q2)
      mQuart.transition().duration(transitionDuration).ease(easeFunc)
      .attr('r', function(d, i){
        var r = objectSize / 2
        var dif = (scale(q3) - scale(q1)) / 2
        return (r > dif) ? dif : r
      })
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', boxStrokeWidth)
      .attr('transform', function(d, i){
        var
        x = horizontalQ ? objectSize / 2 : scale(q2),
        y = verticalQ ? objectSize / 2 : scale(extent[1]) - scale(q2),
        t = 'translate('+x+','+y+')'
        return t
      })

      // set lower whisker (min)
      lWhisk.transition().duration(transitionDuration).ease(easeFunc)
      .attr('d', function(dd, ii){
        var
        dir = false,
        x = 0,
        y = 0,
        h = horizontalQ ? scale(q1) - scale(q0) : objectSize,
        w = verticalQ ? scale(q1) - scale(q0) : objectSize
        return utils.paths.whiskerPath(dir, x, y, w, h, whiskerWidthPercent, orient)
      })
      .attr('transform', function(d, i){
        var
        x = horizontalQ ? 0 : scale(q1),
        y = verticalQ ? 0 : scale(extent[1]) - scale(q1),
        t = 'translate('+x+','+y+')'
        return t
      })
      .attr('stroke', 'black').attr('stroke-width', whiskerStrokeWidth)
      .attr('fill', 'none')

      // set upper whisker (max)
      uWhisk.transition().duration(transitionDuration).ease(easeFunc)
      .attr('d', function(dd, ii){
        var
        dir = true,
        x = 0,
        y = 0,
        h = horizontalQ ? scale(q4) - scale(q3) : objectSize,
        w = verticalQ ? scale(q4) - scale(q3) : objectSize
        return utils.paths.whiskerPath(dir, x, y, w, h, whiskerWidthPercent, orient)
      })
      .attr('transform', function(d, i){
        var
        x = horizontalQ ? 0 : scale(q3),
        y = verticalQ ? 0 :  scale(extent[1]) - scale(q4),
        t = 'translate('+x+','+y+')'
        return t
      })
      .attr('stroke', 'black')
      .attr('stroke-width', whiskerStrokeWidth)
      .attr('fill', 'none')

    })

    tooltip.selection(container.selectAll('g:not(.to-remove).'+objectClass))
    .data(data)
    tooltip()


  }

  return boxwhisker
}
