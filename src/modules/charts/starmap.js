import * as d3 from 'd3'
import utils from '../utils/index.js'
import CF from '../color-function';
import TTip from '../tooltip';
export default function starmap ( selection ) {
  var
  /*
  {
    dataset: {
      element: {
        tooltip: elementName,
        value: ###
      }
    }
  }
  */
  data,

  spaceX,
  spaceY,

  // [datasetNames,...]
  dataKeys,

  // [[elementName,...],...]
  dataValues,
  elementValues,

  elementExtractor = (dataset, index)=>Object.keys(dataset),
  valueExtractor = (element, index)=>element,

  namespace = 'd3sm-starmap',
  backgroundFill = 'transparent',
  domainPadding = 0.5,
  objectClass = 'star-polygon',
  radiusPercent = 0.8,
  radius,
  scale = d3.scaleLinear(),
  transitionDuration = 1000,
  numberOfCircles = 5,
  colorFunction = CF(),
  tooltip = TTip(),
  easeFunc = d3.easeSinIn,
  shapeStrokeWidth = 1,
  shapeOpacity = 0.8,
  labelPadding = 10

  starmap.data = function(_) { return arguments.length ? (data = _, starmap) : data; };
  starmap.spaceX = function(_) { return arguments.length ? (spaceX = _, starmap) :  spaceX; };
  starmap.spaceY = function(_) { return arguments.length ? (spaceY = _, starmap) :  spaceY; };
  starmap.dataKeys = function(_) { return arguments.length ? (dataKeys = _, starmap) :  dataKeys; };
  starmap.dataValues = function(_) { return arguments.length ? (dataValues = _, starmap) :  dataValues; };
  starmap.elementValues = function(_) { return arguments.length ? (elementValues = _, starmap) :  elementValues; };
  starmap.elementExtractor = function(_) { return arguments.length ? (elementExtractor = _, starmap) :  elementExtractor; };
  starmap.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, starmap) :  valueExtractor; };
  starmap.namespace = function(_) { return arguments.length ? (namespace = _, starmap) : namespace; };
  starmap.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, starmap) :  backgroundFill; };
  starmap.domainPadding = function(_) { return arguments.length ? (domainPadding = _, starmap) :  domainPadding; };
  starmap.objectClass = function(_) { return arguments.length ? (objectClass = _, starmap) :  objectClass; };
  starmap.radiusPercent = function(_) { return arguments.length ? (radiusPercent = _, starmap) :  radiusPercent; };
  starmap.radius = function(_) { return arguments.length ? (radius = _, starmap) :  radius; };
  starmap.scale = function(_) { return arguments.length ? (scale = _, starmap) :  scale; };
  starmap.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, starmap) :  transitionDuration; };
  starmap.colorFunction = function(_) { return arguments.length ? (colorFunction = _, starmap) :  colorFunction; };
  starmap.tooltip = function(_) { return arguments.length ? (tooltip = _, starmap) :  tooltip; };
  starmap.easeFunc = function(_) { return arguments.length ? (easeFunc = _, starmap) :  easeFunc; };

  function starmap() {

    // set up container
    let bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    let container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    // circles fit in a square
    let minDim = Math.min(...[spaceX, spaceY])
    let dimDif = {x:spaceX-minDim, y:spaceY-minDim}
    radius = (radiusPercent * minDim) / 2
    let center = {x: minDim / 2, y: minDim / 2}

    // shift container to compensate for uneven spacing
    container.attr('transform', `translate(${dimDif.x/2},${dimDif.y/2})`)

    // add background container for circles, spokes and text
    let radialBackgroundContainer = container.select('g.radial-background-container')
    if (radialBackgroundContainer.empty()) {
      radialBackgroundContainer = container.append('g').attr('class', 'radial-background-container')
    }

    // these are the background circles
    radialBackgroundContainer.selectAll('circle').data(Array(numberOfCircles))
    .enter().append('circle')
    .attr('r', 0)
    .merge(radialBackgroundContainer.selectAll('circle'))
    .attr('fill', 'transparent')
    .attr('stroke', 'black')
    .attr('opacity', '1')
    .attr("cx", center.x)
    .attr("cy", center.y)
    .transition()
    .duration(transitionDuration)
    .ease(easeFunc)
    .attr('r', (d, i)=>radius * (i+1)/numberOfCircles)
    radialBackgroundContainer.selectAll('circle').exit().remove()




    // keys of datasets
    dataKeys = d3.keys(data)
    // names of all the elements that appear in the datasets
    elementValues = []
    // the values for each
    dataValues = dataKeys.map((ds,i)=>{
      let elementKeys = elementExtractor(data[ds], i)
      return elementKeys.map((e, j)=>{
        if (elementValues.indexOf(e)<0) elementValues.push(e)
        return valueExtractor(data[ds][e], j)
      })
    })

    let numberOfKeys = dataKeys.length
    let extent = [
      Math.min(...utils.arr.flatten(dataValues)) - domainPadding,
      Math.max(...utils.arr.flatten(dataValues)) + domainPadding
    ];
    // radius scale
    scale.domain(extent)//.range([0, radius])



    // now can make spokes since we how many we need
    radialBackgroundContainer.selectAll(`line.${namespace}-spoke`)
    .data(elementValues)
    .enter().append('line').attr('class', `${namespace}-spoke`)
    .merge(radialBackgroundContainer.selectAll(`line.${namespace}-spoke`))
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("x2", 0)
    .attr("y2", 0)
    .attr('stroke', 'black')
    .attr('stroke-width', 1)
    .attr('transform',`translate(${center.x},${center.y})`)
    .transition()
    .duration(transitionDuration)
    .ease(easeFunc)
    .attr("x2", (d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      return polarToCartesian(0, 0, radius, degrees).x
    })
    .attr("y2", (d, i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      return polarToCartesian(0, 0, radius, degrees).y
    })
    // .attr('transform',(d,i)=>{
    //   let degrees = (360 / elementValues.length ) * (i + 1)
    //   let ptc = polarToCartesian(0, 0, 0, degrees)
    //   return `translate(${center.x},${center.y})`
    // })
    radialBackgroundContainer.selectAll(`line.${namespace}-spoke`).exit().remove()


    // add labels to spokes
    radialBackgroundContainer.selectAll(`text.${namespace}-spoke-label`).data(elementValues)
    .enter().append('text').attr('class', `${namespace}-spoke-label`)
    .attr('opacity', 0)
    .attr('x', (d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      // r = radius + config.background["accent-bubbles"].spacing
      return polarToCartesian(0, 0, 0, degrees).x
    })
    .attr('y', (d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      // r = radius + config.background["accent-bubbles"].spacing
      return polarToCartesian(0, 0, 0, degrees).y
    })
    .attr('transform',(d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      let ptc = polarToCartesian(0, 0, 0, degrees)
      let j = -1
      if (i > elementValues.length / 2 - 1)  j *= -1
      return `translate(${center.x},${center.y}) rotate(${j*90 +degrees}, ${ptc.x}, ${ptc.y})`
    })
    .merge(radialBackgroundContainer.selectAll(`text.${namespace}-spoke-label`))
    .text((d)=>d)
    .transition()
    .duration(transitionDuration)
    .ease(easeFunc)
    .attr('x', (d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      // r = radius + config.background["accent-bubbles"].spacing
      return polarToCartesian(0, 0, radius+labelPadding, degrees).x
    })
    .attr('y', (d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      // r = radius + config.background["accent-bubbles"].spacing
      return polarToCartesian(0, 0, radius+labelPadding, degrees).y
    })
    .attr('transform',(d,i)=>{
      let degrees = (360 / elementValues.length ) * (i + 1)
      let ptc = polarToCartesian(0, 0, radius+labelPadding, degrees)
      let j = -1
      if (i > elementValues.length / 2 - 1)  j *= -1
      return `translate(${center.x},${center.y}) rotate(${j*90 +degrees}, ${ptc.x}, ${ptc.y})`
    })
    .attr('text-anchor', (d,i) => i > elementValues.length / 2 ? 'end' : 'start')
    .attr('opacity', 1)
    radialBackgroundContainer.selectAll(`${namespace}-spoke-label`).exit().remove()



    // background set up, now onto shapes

    colorFunction = colorFunction.colorBy() == 'index'
    ? colorFunction.dataExtent([0, elementValues.length])
    : colorFunction.dataExtent(extent)



    let selc = container.selectAll(`g.${namespace}`).data(dataKeys)
    .enter().append('g').attr('class', namespace)
    .merge(container.selectAll(`g.${namespace}`))
    let exit = selc.exit().remove()

    selc.each(function(k, i){

      let dthis = d3.select(this)
      let polygonPoints = []
      let currentData = data[k]
      let currentValues = dataValues[dataKeys.indexOf(k)]
      let currentElementKeys = elementExtractor(currentData, i)

      let fillColor = colorFunction(k, currentValues, i, 'fill')
      let strokeColor = colorFunction(k, currentValues, i,  'stroke')

      elementValues.forEach((e, j)=>{
        let r = scale(radius * valueExtractor(currentData[e], currentElementKeys.indexOf(j)))
        let point = (currentElementKeys.indexOf(e)===-1)
          ? center
          : polarToCartesian(center.x, center.y, r, (360 / elementValues.length ) * (j + 1))
        polygonPoints.push(point)
      })

      let line = d3.line().curve(d3.curveCardinal).x(d=>d.x).y(d=>d.y)
      let poly = dthis.selectAll(`path.${objectClass}`).data([polygonPoints])
      let pentr = poly.enter().append('path').attr('class', objectClass)
      .attr('d', d=>line(d.map(dd=>center)))
      let pexit = poly.exit().remove()
      poly = poly.merge(pentr)
      .transition()
      .ease(easeFunc)
      .duration(transitionDuration)
      .attr('d', d=>line(d))
      .attr("stroke", strokeColor)
      .attr("stroke-width", shapeStrokeWidth)
      .attr("fill", fillColor)
      .attr('opacity', shapeOpacity)
    })

    d3.selectAll(`path.${objectClass}`).on('mouseover', function(d, j){
        d3.select(this)
        .transition()
        // .ease(easeFunc)
        // .duration(transitionDuration)
        .attr('opacity', 1)
        .attr("stroke-width", shapeStrokeWidth*2)
    })
    .on('mouseleave', function(d, j){
      d3.select(this)
      .transition()
      // .ease(easeFunc)
      // .duration(transitionDuration)
      .attr('opacity', shapeOpacity)
      .attr("stroke-width", shapeStrokeWidth)
    })

    tooltip.selection(container.selectAll(`g.${namespace}`))
    .data(data)

    tooltip()


  }
  return starmap
}


// Short cut for converting
export function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  let angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  };
}
