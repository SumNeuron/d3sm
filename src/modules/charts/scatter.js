import * as d3 from 'd3'
import utils from '../utils/index.js'
import CF from '../color-function';
import TTip from '../tooltip';
/*******************************************************************************
**                                                                            **
**                                                                            **
**                                 SCATTER                                    **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Creates a scatter
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/scatter/index.html Demo}
 * @constructor scatter
 * @param {d3.selection} selection
 * @namespace scatter
 * @returns {function} scatter
 */
export default function scatter ( selection ) {

  var
  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a point
  * (see {@link scatter#data})
  * @param {Object} [data=undefined]
  * @memberof scatter#
  * @property
  */
  data,
  /**
  * Amount of horizontal space (in pixels) avaible to render the scatter in
  * (see {@link scatter#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof scatter#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the scatter in
  * (see {@link scatter.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof scatter#
  * @property
  */
  spaceY,

  /**
  * The scale for which scatter x values should be transformed by
  * @param {d3.scale} [scaleX=d3.scaleLinear]
  * @memberof scatter#
  * @property
  */
  scaleX = d3.scaleLinear(),
  /**
  * The padding for the domain of the scaleX (see {@link scatter#scaleX})
  * @param {number} [domainPaddingX=0.5]
  * @memberof scatter#
  * @property
  */
  domainPaddingX = 0.5,
  /**
  * The function for getting the x value of the current point
  * @param {function} [valueExtractorX=function(d, i){return data[d]['x']}]
  * @memberof scatter#
  * @property
  */
  valueExtractorX = function(d, i) {return data[d]['x']},

  /**
  * The scale for which scatter y values should be transformed by
  * @param {d3.scale} [scaleY=d3.scaleLinear]
  * @memberof scatter#
  * @property
  */
  scaleY = d3.scaleLinear(),
  /**
  * The padding for the domain of the scaleY (see {@link scatter#scaleY})
  * @param {number} [domainPaddingY=0.5]
  * @memberof scatter#
  * @property
  */
  domainPaddingY = 0.5,
  /**
  * The function for getting the y value of the current point
  * @param {function} [valueExtractorY=function(d, i){return data[d]['y']}]
  * @memberof scatter#
  * @property
  */
  valueExtractorY = function(d, i) {return data[d]['y']},


  /**
  * The scale for which scatter r values should be transformed by
  * @param {d3.scale} [scaleR=d3.scaleLinear]
  * @memberof scatter#
  * @property
  */
  scaleR = d3.scaleLinear(),
  /**
  * The padding for the domain of the scaleR (see {@link scatter#scaleR})
  * @param {number} [domainPaddingR=0.5]
  * @memberof scatter#
  * @property
  */
  domainPaddingR = 0.5,
  /**
  * The function for getting the r value of the current point
  * @param {function} [valueExtractorR=function(d, i){return 2}]
  * @memberof scatter#
  * @property
  */
  valueExtractorR = function(d, i) {return 2},
  /**
  * The min radius a point can have
  * @param {function} [minRadius=2]
  * @memberof scatter#
  * @property
  */
  minRadius = 2,
  /**
  * The min radius a point can have
  * @param {function} [maxRadius=10]
  * @memberof scatter#
  * @property
  */
  maxRadius = 10,

  /**
  * The stroke width of the points
  * @param {number} [pointStrokeWidth=2]
  * @memberof scatter#
  * @property
  */
  pointStrokeWidth = 2,
  /**
  * Instance of ColorFunction
  * @param {function} [colorFunction = colorFunction()]
  * @memberof scatter#
  * @property
  */
  colorFunction = CF(),
  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof scatter#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of scatter
  * @param {string} [namespace="d3sm-scatter"]
  * @memberof scatter#
  * @property
  */
  namespace = 'd3sm-scatter',
  /**
  * Class name for scatter container (<circle> element)
  * @param {string} [objectClass="scatter-point"]
  * @memberof scatter#
  * @property
  */
  objectClass = 'scatter-point',
  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof scatter#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof scatter#
  * @property
  */
  easeFunc = d3.easeExp,

  // useful values to extract to prevent re-calculation
  /**
  * The keys of the points
  * @param {string[]} [pointKeys=undefined]
  * @memberof scatter#
  * @property
  */
  pointKeys,
  /**
  * The x values of the points
  * @param {number[]} [valuesX=undefined]
  * @memberof scatter#
  * @property
  */
  valuesX,
  /**
  * The y values of the points
  * @param {number[]} [valuesY=undefined]
  * @memberof scatter#
  * @property
  */
  valuesY,
  /**
  * The r values of the points
  * @param {number[]} [valuesR=undefined]
  * @memberof scatter#
  * @property
  */
  valuesR,

  /**
  * Instance of Tooltip
  * @param {function} [tooltip=tooltip()]
  * @memberof scatter#
  * @property
  */
  tooltip = TTip()

  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {scatter | d3.selection}
   * @memberof scatter
   * @property
   * by default selection = selection
   */
  scatter.selection = function(_) { return arguments.length ? (selection =_, scatter) : selection}
  /**
   * Gets or sets the data
   * (see {@link scatter#data})
   * @param {number} [_=none]
   * @returns {scatter | object}
   * @memberof scatter
   * @property
   */
  scatter.data = function(_) { return arguments.length ? (data =_, scatter) : data}
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link scatter#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default spaceX = undefined
   */
  scatter.spaceX = function(_) { return arguments.length ? (spaceX =_, scatter) : spaceX}
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link scatter#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default spaceY = undefined
   */
  scatter.spaceY = function(_) { return arguments.length ? (spaceY =_, scatter) : spaceY}



  /**
   * Gets / sets the x scale for which the scatter x values should be transformed by
   * (see {@link scatter#scaleX})
   * @param {d3.scale} [_=none]
   * @returns {scatter | d3.scale}
   * @memberof scatter
   * @property
   * by default scaleX = d3.scaleLinear()
   */
  scatter.scaleX = function(_) { return arguments.length ? (scaleX =_, scatter) : scaleX}
  /**
   * Gets / sets the padding for the domain of the x scale
   * (see {@link scatter#domainPaddingX})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default domainPaddingX = 0.5
   */
  scatter.domainPaddingX = function(_) { return arguments.length ? (domainPaddingX =_, scatter) : domainPaddingX}
  /**
   * Gets / sets the valueExtractorX
   * (see {@link scatter#valueExtractorX})
   * @param {function} [_=none]
   * @returns {scatter | function}
   * @memberof scatter
   * @property
   * by default valueExtractorX = function(key, index) { return data[key]['x'] }
   */
  scatter.valueExtractorX = function(_) { return arguments.length ? (valueExtractorX =_, scatter) : valueExtractorX}


  /**
   * Gets / sets the y scale for which the scatter y values should be transformed by
   * (see {@link scatter#scaleY})
   * @param {d3.scale} [_=none]
   * @returns {scatter | d3.scale}
   * @memberof scatter
   * @property
   * by default scaleY = d3.scaleLinear()
   */
  scatter.scaleY = function(_) { return arguments.length ? (scaleY =_, scatter) : scaleY}
  /**
   * Gets / sets the padding for the domain of the y scale
   * (see {@link scatter#domainPaddingY})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default domainPaddingY = 0.5
   */
  scatter.domainPaddingY = function(_) { return arguments.length ? (domainPaddingY =_, scatter) : domainPaddingY}
  /**
   * Gets / sets the valueExtractorY
   * (see {@link scatter#valueExtractorY})
   * @param {function} [_=none]
   * @returns {scatter | function}
   * @memberof scatter
   * @property
   * by default valueExtractorY = function(key, index) { return data[key]['y'] }
   */
  scatter.valueExtractorY = function(_) { return arguments.length ? (valueExtractorY =_, scatter) : valueExtractorY}


  /**
   * Gets / sets the r scale for which the scatter r values should be transformed by
   * (see {@link scatter#scaleR})
   * @param {d3.scale} [_=none]
   * @returns {scatter | d3.scale}
   * @memberof scatter
   * @property
   * by default scaleR = d3.scaleLinear()
   */
  scatter.scaleR = function(_) { return arguments.length ? (scaleR =_, scatter) : scaleR}
  /**
   * Gets / sets the padding for the domain of the r scale
   * (see {@link scatter#domainPaddingR})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default domainPaddingR = 0.5
   */
  scatter.domainPaddingR = function(_) { return arguments.length ? (domainPaddingR =_, scatter) : domainPaddingR}
  /**
   * Gets / sets the valueExtractorR
   * (see {@link scatter#valueExtractorR})
   * @param {function} [_=none]
   * @returns {scatter | function}
   * @memberof scatter
   * @property
   * by default valueExtractorR = function(key, index) { return data[key]['r'] }
   */
  scatter.valueExtractorR = function(_) { return arguments.length ? (valueExtractorR =_, scatter) : valueExtractorR}
  /**
   * Gets / sets the minRadius
   * (see {@link scatter#minRadius})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default minRadius = 2
   */
  scatter.minRadius = function(_) { return arguments.length ? (minRadius =_, scatter) : minRadius}
  /**
   * Gets / sets the maxRadius
   * (see {@link scatter#maxRadius})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default maxRadius = 10
   */
  scatter.maxRadius = function(_) { return arguments.length ? (maxRadius =_, scatter) : maxRadius}

  /**
   * Gets / sets the pointStrokeWidth
   * (see {@link scatter#pointStrokeWidth})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default pointStrokeWidth = 2
   */
  scatter.pointStrokeWidth = function(_) { return arguments.length ? (pointStrokeWidth =_, scatter) : pointStrokeWidth}
  /**
   * Gets / sets the colorFunction
   * (see {@link scatter#colorFunction})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default colorFunction = colorFunction()
   */
  scatter.colorFunction = function(_) { return arguments.length ? (colorFunction =_, scatter) : colorFunction}
  /**
   * Gets / sets the backgroundFill
   * (see {@link scatter#backgroundFill})
   * @param {string} [_=none]
   * @returns {scatter | string}
   * @memberof scatter
   * @property
   * by default backgroundFill = 'transparent'
   */
  scatter.backgroundFill = function(_) { return arguments.length ? (backgroundFill =_, scatter) : backgroundFill}
  /**
   * Gets / sets the namespace
   * (see {@link scatter#namespace})
   * @param {string} [_=none]
   * @returns {scatter | string}
   * @memberof scatter
   * @property
   * by default namespace = 'd3sm-scatter'
   */
  scatter.namespace = function(_) { return arguments.length ? (namespace =_, scatter) : namespace}
  /**
   * Gets / sets the objectClass
   * (see {@link scatter#objectClass})
   * @param {string} [_=none]
   * @returns {scatter | string}
   * @memberof scatter
   * @property
   * by default objectClass = 'tick-group'
   */
  scatter.objectClass = function(_) { return arguments.length ? (objectClass =_, scatter) : objectClass}
  /**
   * Gets / sets the transitionDuration
   * (see {@link scatter#transitionDuration})
   * @param {number} [_=none]
   * @returns {scatter | number}
   * @memberof scatter
   * @property
   * by default transitionDuration = 1000
   */
  scatter.transitionDuration = function(_) { return arguments.length ? (transitionDuration =_, scatter) : transitionDuration}
  /**
   * Gets / sets the easeFunc
   * (see {@link scatter#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {scatter | d3.ease}
   * @memberof scatter
   * @property
   * by default easeFunc = d3.easeExp
   */
  scatter.easeFunc = function(_) { return arguments.length ? (easeFunc =_, scatter) : easeFunc}

  /**
   * Gets / sets the pointKeys
   * (see {@link scatter#pointKeys})
   * @param {string[]} [_=none]
   * @returns {scatter | string[]}
   * @memberof scatter
   * @property
   * by default pointKeys = undefined
   */
  scatter.pointKeys = function(_) { return arguments.length ? (pointKeys =_, scatter) : pointKeys}
  /**
   * Gets / sets the valuesX
   * (see {@link scatter#valuesX})
   * @param {number[]} [_=none]
   * @returns {scatter | number[]}
   * @memberof scatter
   * @property
   * by default valuesX = undefined
   */
  scatter.valuesX = function(_) { return arguments.length ? (valuesX =_, scatter) : valuesX}
  /**
   * Gets / sets the valuesY
   * (see {@link scatter#valuesY})
   * @param {number[]} [_=none]
   * @returns {scatter | number[]}
   * @memberof scatter
   * @property
   * by default valuesY = undefined
   */
  scatter.valuesY = function(_) { return arguments.length ? (valuesY =_, scatter) : valuesY}
  /**
   * Gets / sets the valuesR
   * (see {@link scatter#valuesR})
   * @param {number[]} [_=none]
   * @returns {scatter | number[]}
   * @memberof scatter
   * @property
   * by default valuesR = undefined
   */
  scatter.valuesR = function(_) { return arguments.length ? (valuesR =_, scatter) : valuesR}
  /**
   * Gets / sets the tooltip
   * (see {@link scatter#tooltip})
   * @param {tooltip} [_=none]
   * @returns {scatter | tooltip}
   * @memberof scatter
   * @property
   * by default tooltip = tooltip()
   */

  scatter.tooltip = function(_) { return arguments.length ? (tooltip =_, scatter) : tooltip}


  function scatter() {
    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );


    pointKeys = d3.keys(data)
    valuesX = pointKeys.map(valueExtractorX)
    valuesY = pointKeys.map(valueExtractorY)
    valuesR = pointKeys.map(valueExtractorR)

    var numberOfObjects = pointKeys.length
    var extentX = [Math.min(...valuesX) - domainPaddingX, Math.max(...valuesX) + domainPaddingX]
    var extentY = [Math.min(...valuesY) - domainPaddingY, Math.max(...valuesY) + domainPaddingY]
    var extentR = [Math.min(...valuesR) - domainPaddingR, Math.max(...valuesR) + domainPaddingR]

    scaleX.domain(extentX).range([0, spaceX])
    scaleY.domain(extentY).range([spaceY, 0])
    scaleR.domain(extentR).range([minRadius, maxRadius])

    var points = container.selectAll('.'+objectClass)
    points = points.data(pointKeys)
    var pEnter = points.enter().append('circle')
    .attr('class', objectClass)
    .attr('cx', 0).attr('cy', spaceY).attr('r', 0)

    var pExit = points.exit()

    points = points.merge(pEnter)

    points.each(function(key, i){
      var t = d3.select(this),
      currentData = data[key],
      x = valuesX[i],
      y = valuesY[i],
      r = valuesR[i],
      fillColor = colorFunction(key, currentData, i, 'fill'),
      strokeColor = colorFunction(key, currentData, i, 'stroke')

      t.transition().duration(transitionDuration).ease(easeFunc)
      .attr('cx', scaleX(x))
      .attr('cy', scaleY(y))
      .attr('r', scaleR(r))
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', pointStrokeWidth)



      t.on('mouseover', function(d, i){
        points.style('opacity', 0.2)
        t.style('opacity', 1)
        t.transition().duration(transitionDuration/2).ease(easeFunc)
        .attr('stroke-width', pointStrokeWidth*2)
        .attr('r', scaleR(r) * 1.5)

      })
      t.node().addEventListener('mouseout', function(){
        container.selectAll('.'+objectClass).style('opacity', 1)
        t.transition().duration(transitionDuration/2).ease(easeFunc)
        .attr('stroke-width', pointStrokeWidth)
        .attr('r', scaleR(r))

      })

    })



    pExit.transition().duration(transitionDuration).ease(easeFunc)
    .attr('cx', 0).attr('cy', spaceY).attr('r', 0)
    .remove()

    tooltip.selection(points)
    .data(data)

    tooltip()
  }


  return scatter
}
