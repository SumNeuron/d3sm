import scatter from './scatter'
import bar from './bar'
import bubble from './bubble-heatmap'
import boxwhisker from './box-whisker'
import heatmap from './heatmap'
import violin from './violin'
import {neededViolinValues} from './violin'
import upset from './upset'
import starmap from './starmap'
import {polarToCartesian} from './starmap'

let charts = {
  scatter, bar, bubble, boxwhisker, heatmap, violin, neededViolinValues, upset,
  starmap, polarToCartesian
}

export {
  scatter, bar, bubble, boxwhisker, heatmap, violin, neededViolinValues, upset,
  starmap, polarToCartesian
}
export default charts
