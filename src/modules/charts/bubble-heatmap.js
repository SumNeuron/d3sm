import * as d3 from 'd3'
import utils from '../utils/index.js'
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';


/**
 * Creates a bubbleHeatmap
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/bubble-heatmap/index.html Demo}
 * @constructor bubbleHeatmap
 * @param {d3.selection} selection
 * @namespace bubbleHeatmap
 * @returns {function} bubbleHeatmap
 */
export default function bubbleHeatmap( selection ) {
  var
  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a cell
  * (see {@link bubbleHeatmap#data})
  * @param {Object} [data=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  data,

  /**
  * Amount of horizontal space (in pixels) avaible to render the bubbleHeatmap in
  * (see {@link bubbleHeatmap#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the bubbleHeatmap in
  * (see {@link bubbleHeatmap.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  spaceY,

  /**
  * The internal key of the cell specifiying to which x axis key it belongs
  * (see {@link bubbleHeatmap.xKey})
  * @param {string} [xKey='x']
  * @memberof bubbleHeatmap#
  * @property
  */
  xKey = 'x',
  /**
  * The internal key of the cell specifiying to which y axis key it belongs
  * (see {@link bubbleHeatmap.yKey})
  * @param {string} [yKey='y']
  * @memberof bubbleHeatmap#
  * @property
  */
  yKey = 'y',
  /**
  * The internal key of the cell specifiying what value to use to determine the radius
  * (see {@link bubbleHeatmap.rKey})
  * @param {string} [rKey='r']
  * @memberof bubbleHeatmap#
  * @property
  */
  rKey = 'r',
  /**
  * The internal key of the cell specifiying what value to use to determine the color
  * (see {@link bubbleHeatmap.vKey})
  * @param {string} [vKey='v']
  * @memberof bubbleHeatmap#
  * @property
  */
  vKey = 'v',

  /**
  * Function for extracting the the value from xKey.
  * (see {@link bubbleHeatmap.xExtractor})
  * @param {function} [xExtractor=function(key, i) { return data[key][xKey] }]
  * @returns {string}
  * @memberof bubbleHeatmap#
  * @property
  */
  xExtractor = function(key, i) {return data[key][xKey] },
  /**
  * Function for extracting the the value from yKey.
  * (see {@link bubbleHeatmap.yExtractor})
  * @param {function} [yExtractor=function(key, i) { return data[key][yKey] }]
  * @returns {string}
  * @memberof bubbleHeatmap#
  * @property
  */
  yExtractor = function(key, i) { return data[key][yKey] },
  /**
  * Function for extracting the the value from rKey.
  * (see {@link bubbleHeatmap.rExtractor})
  * @param {function} [rExtractor=function(key, i) { return data[key][rKey] }]
  * @returns {number}
  * @memberof bubbleHeatmap#
  * @property
  */
  rExtractor = function(key, i) { return data[key][rKey] },
  /**
  * Function for extracting the the value from vKey.
  * (see {@link bubbleHeatmap.vExtractor})
  * @param {function} [vExtractor=function(key, i) { return data[key][vKey] }]
  * @returns {number}
  * @memberof bubbleHeatmap#
  * @property
  */
  vExtractor = function(key, i) { return data[key][vKey] },


  /**
  * Whether or not to allow bubbleHeatmap to render elements pass the main spatial dimension
  * given the orientation (see {@link bubbleHeatmap#orient}), where {@link bubbleHeatmap#orient}="bottom" or {@link bubbleHeatmap#orient}="top"
  * the main dimension is {@link bubbleHeatmap#spaceX} and where {@link bubbleHeatmap#orient}="left" or {@link bubbleHeatmap#orient}="right"
  * the main dimension is {@link bubbleHeatmap#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof bubbleHeatmap#
  * @property
  */
  overflowQ = false,

  /**
  * The scale for which the radius values should be transformed by
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof bubbleHeatmap#
  * @property
  */
  scale = d3.scaleLinear(),
  /**
  * The padding for the domain of the scale (see {@link bubbleHeatmap#scale})
  * @param {number} [domainPadding=0.5]
  * @memberof bubbleHeatmap#
  * @property
  */
  domainPadding = 0.5,

  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link bubbleHeatmap#orient}), where {@link bubbleHeatmap#orient}="horizontal"
  * the main dimension is {@link bubbleHeatmap#spaceX} and where {@link bubbleHeatmap#orient}="vertical"
  * the main dimension is {@link bubbleHeatmap#spaceY} between bubbles
  * @param {number} [objectSpacer=0.0]
  * @memberof bubbleHeatmap#
  * @property
  */
  objectSpacer = 0.0,
  /**
  * The minimum size that an object can be
  * @param {number} [minObjectSize=50]
  * @memberof bubbleHeatmap#
  * @property
  */
  minObjectSize = 50,
  /**
  * The maximum size that an object can be
  * @param {number} [maxObjectSize=100]
  * @memberof bubbleHeatmap#
  * @property
  */
  maxObjectSize = 100,


  /**
  * The stroke width of the bubbles
  * @param {number} [bubbleStrokeWidth=2]
  * @memberof bubbleHeatmap#
  * @property
  */
  bubbleStrokeWidth = 2,
  // colorFunc = colorFunction(),

  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof bubbleHeatmap#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of bubbleHeatmap
  * @param {string} [namespace="d3sm-bubble"]
  * @memberof bubbleHeatmap#
  * @property
  */
  namespace = 'd3sm-bubble',
  /**
  * Class name for bubble container (<g> element)
  * @param {string} [objectClass="bubble"]
  * @memberof bubbleHeatmap#
  * @property
  */
  objectClass = 'bubble',
  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof bubbleHeatmap#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof bubbleHeatmap#
  * @property
  */
  easeFunc = d3.easeExp,

  /**
  * Stores the keys of all the cells
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [cellKeys=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  cellKeys,
  /**
  * Stores the list of utils.arr.unique xValues
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [xValues=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  xValues,
  /**
  * Stores the list of utils.arr.unique yValues
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [yValues=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  yValues,
  /**
  * Stores the list of utils.arr.unique rValues
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [rValues=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  rValues,
  /**
  * Stores the list of utils.arr.unique vValues
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [vValues=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  vValues,

  xKeySortingFunction = function(a, b) { return xExtractor(a) - xExtractor(b) },
  yKeySortingFunction = function(a, b) { return yExtractor(a) - yExtractor(b) },
  rKeySortingFunction = function(a, b) { return rExtractor(a) - rExtractor(b) },
  vKeySortingFunction = function(a, b) { return vExtractor(a) - vExtractor(b) },

  /**
  * Instance of ColorFunction with .colorBy set to 'category'
  * @function colorFunction
  * @memberof bubbleHeatmap#
  * @property
  */
  colorFunction = CF().colorBy('value'),
  /**
  * Instance of Tooltip
  * @function tooltip
  * @memberof bubbleHeatmap#
  * @property
  */
  tooltip = TTip(),

  /**
  * store the size the bubble could be in the x dimension
  * the actuall size of the bubble will be the min of xSize and {@link bubbleHeatmap#ySize}
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [xSize=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  xSize,
  /**
  * store the size of the spacer in the x dimension
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [xSpacerSize=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  xSpacerSize,

  /**
  * store the size the bubble could be in the y dimension
  * the actuall size of the bubble will be the min of xSize and {@link bubbleHeatmap#xSize}
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [ySize=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  ySize,
  /**
  * store the size of the spacer in the y dimension.
  * Calculated after bubbleHeatmap called.
  * @param {string[]} [xSpacerSize=undefined]
  * @memberof bubbleHeatmap#
  * @property
  */
  ySpacerSize

  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {bubbleHeatmap | d3.selection}
   * @memberof bubbleHeatmap
   * @property
   * by default selection = selection
   */
  bhm.selection = function(_) { return arguments.length ? (selection = _, bhm) : selection; }
  /**
   * Gets or sets the data
   * (see {@link bubbleHeatmap#data})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | object}
   * @memberof bubbleHeatmap
   * @property
   */
  bhm.data = function(_) { return arguments.length ? (data = _, bhm) : data; }
  // bhm.orient = function(_) { return arguments.length ? (orient = _, bhm) : orient; }
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link bubbleHeatmap#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default spaceX = undefined
   */
  bhm.spaceX = function(_) { return arguments.length ? (spaceX = _, bhm) : spaceX; }
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link bubbleHeatmap#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default spaceY = undefined
   */
  bhm.spaceY = function(_) { return arguments.length ? (spaceY = _, bhm) : spaceY; }

  /**
   * Gets or sets the xKey
   * (see {@link bubbleHeatmap#xKey})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default xKey = 'x'
   */
  bhm.xKey = function(_) { return arguments.length ? (xKey = _, bhm) : xKey; }
  /**
   * Gets or sets the yKey
   * (see {@link bubbleHeatmap#yKey})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default yKey = 'y'
   */
  bhm.yKey = function(_) { return arguments.length ? (yKey = _, bhm) : yKey; }
  /**
   * Gets or sets the rKey
   * (see {@link bubbleHeatmap#rKey})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default rKey = 'r'
   */
  bhm.rKey = function(_) { return arguments.length ? (rKey = _, bhm) : rKey; }
  /**
   * Gets or sets the vKey
   * (see {@link bubbleHeatmap#vKey})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default vKey = 'y'
   */
  bhm.vKey = function(_) { return arguments.length ? (vKey = _, bhm) : vKey; }

  /**
   * Gets or sets the cellKeys
   * (see {@link bubbleHeatmap#cellKeys})
   * @param {string[]} [_=none]
   * @returns {bubbleHeatmap | string[]}
   * @memberof bubbleHeatmap
   * @property
   * by default cellKeys = undefined
   */
  bhm.cellKeys = function(_) { return arguments.length ? (cellKeys = _, bhm) : cellKeys; }
  /**
   * Gets or sets the xValues
   * (see {@link bubbleHeatmap#xValues})
   * @param {string[]} [_=none]
   * @returns {bubbleHeatmap | string[]}
   * @memberof bubbleHeatmap
   * @property
   * by default xValues = undefined
   */
  bhm.xValues = function(_) { return arguments.length ? (xValues = _, bhm) : xValues; }
  /**
   * Gets or sets the yValues
   * (see {@link bubbleHeatmap#yValues})
   * @param {string[]} [_=none]
   * @returns {bubbleHeatmap | string[]}
   * @memberof bubbleHeatmap
   * @property
   * by default yValues = undefined
   */
  bhm.yValues = function(_) { return arguments.length ? (yValues = _, bhm) : yValues; }
  /**
   * Gets or sets the rValues
   * (see {@link bubbleHeatmap#rValues})
   * @param {number[]} [_=none]
   * @returns {bubbleHeatmap | number[]}
   * @memberof bubbleHeatmap
   * @property
   * by default rValues = undefined
   */
  bhm.rValues = function(_) { return arguments.length ? (rValues = _, bhm) : rValues; }
  /**
   * Gets or sets the vValues
   * (see {@link bubbleHeatmap#vValues})
   * @param {number[]} [_=none]
   * @returns {bubbleHeatmap | number[]}
   * @memberof bubbleHeatmap
   * @property
   * by default vValues = undefined
   */
  bhm.vValues = function(_) { return arguments.length ? (vValues = _, bhm) : vValues; }


  /**
   * Gets or sets the xExtractor
   * (see {@link bubbleHeatmap#xExtractor})
   * @param {function} [_=none]
   * @returns {bubbleHeatmap | function}
   * @memberof bubbleHeatmap
   * @property
   * by default xExtractor = undefined
   */
  bhm.xExtractor = function(_) { return arguments.length ? (xExtractor = _, bhm) : xExtractor; }
  /**
   * Gets or sets the yExtractor
   * (see {@link bubbleHeatmap#yExtractor})
   * @param {function} [_=none]
   * @returns {bubbleHeatmap | function}
   * @memberof bubbleHeatmap
   * @property
   * by default yExtractor = undefined
   */
  bhm.yExtractor = function(_) { return arguments.length ? (yExtractor = _, bhm) : yExtractor; }
  /**
   * Gets or sets the rExtractor
   * (see {@link bubbleHeatmap#rExtractor})
   * @param {function} [_=none]
   * @returns {bubbleHeatmap | function}
   * @memberof bubbleHeatmap
   * @property
   * by default rExtractor = undefined
   */
  bhm.rExtractor = function(_) { return arguments.length ? (rExtractor = _, bhm) : rExtractor; }
  /**
   * Gets or sets the vExtractor
   * (see {@link bubbleHeatmap#vExtractor})
   * @param {function} [_=none]
   * @returns {bubbleHeatmap | function}
   * @memberof bubbleHeatmap
   * @property
   * by default vExtractor = undefined
   */
  bhm.vExtractor = function(_) { return arguments.length ? (vExtractor = _, bhm) : vExtractor; }

  /**
   * Gets / sets whether or not bubbleHeatmap is allowed to go beyond specified dimensions
   * (see {@link bubbleHeatmap#spaceX})
   * @param {boolean} [_=none]
   * @returns {bubbleHeatmap | boolean}
   * @memberof bubbleHeatmap
   * @property
   * by default overflowQ = false
   */
  bhm.overflowQ = function(_) { return arguments.length ? (overflowQ = _, bhm) : overflowQ; }
  /**
   * Gets / sets the scale for which the radius of bubbles should be transformed by
   * (see {@link bubbleHeatmap#scale})
   * @param {d3.scale} [_=none]
   * @returns {bubbleHeatmap | d3.scale}
   * @memberof bubbleHeatmap
   * @property
   * by default scale = d3.scaleLinear()
   */
  bhm.scale = function(_) { return arguments.length ? (scale = _, bhm) : scale; };
  /**
   * Gets / sets the padding for the domain of the scale
   * (see {@link bubbleHeatmap#domainPadding})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default domainPadding = 0.5
   */
  bhm.domainPadding = function(_) { return arguments.length ? (domainPadding = _, bhm) : domainPadding; };
  /**
   * Gets / sets objectSpacer
   * (see {@link bubbleHeatmap#objectSpacer})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default objectSpacer = 0.0
   */
  bhm.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, objectSpacer) : data; }
  /**
   * Gets / sets the minObjectSize
   * (see {@link bubbleHeatmap#minObjectSize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default minObjectSize = 50
   */
  bhm.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, bhm) : minObjectSize; }
  /**
   * Gets / sets the maxObjectSize
   * (see {@link bubbleHeatmap#maxObjectSize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default maxObjectSize = 100
   */
  bhm.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, bhm) : maxObjectSize; }
  /**
   * Gets / sets the bubbleStrokeWidth
   * (see {@link bubbleHeatmap#bubbleStrokeWidth})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default bubbleStrokeWidth = 2
   */
  bhm.bubbleStrokeWidth = function(_) { return arguments.length ? (bubbleStrokeWidth = _, bhm) : bubbleStrokeWidth; }
  /**
   * Gets / sets the backgroundFill
   * (see {@link bubbleHeatmap#backgroundFill})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default backgroundFill = 'transparent'
   */
  bhm.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, bhm) : backgroundFill; }
  /**
   * Gets / sets the namespace
   * (see {@link bubbleHeatmap#namespace})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default namespace = 'd3sm-bubbleHeatmap'
   */
  bhm.namespace = function(_) { return arguments.length ? (namespace = _, bhm) : namespace; }
  /**
   * Gets / sets the objectClass
   * (see {@link bubbleHeatmap#objectClass})
   * @param {string} [_=none]
   * @returns {bubbleHeatmap | string}
   * @memberof bubbleHeatmap
   * @property
   * by default objectClass = 'tick-group'
   */
  bhm.objectClass = function(_) { return arguments.length ? (objectClass = _, bhm) : objectClass; }
  /**
   * Gets / sets the transitionDuration
   * (see {@link bubbleHeatmap#transitionDuration})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default transitionDuration = 1000
   */
  bhm.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, bhm) : transitionDuration; }
  /**
   * Gets / sets the easeFunc
   * (see {@link bubbleHeatmap#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {bubbleHeatmap | d3.ease}
   * @memberof bubbleHeatmap
   * @property
   * by default easeFunc = d3.easeExp
   */
  bhm.easeFunc = function(_) { return arguments.length ? (easeFunc = _, bhm) : easeFunc; }

  /**
   * Gets / sets the tooltip
   * (see {@link bubbleHeatmap#tooltip})
   * @param {tooltip} [_=none]
   * @returns {bubbleHeatmap | tooltip}
   * @memberof bubbleHeatmap
   * @property
   * by default tooltip = tooltip()
   */
  bhm.tooltip = function(_) { return arguments.length ? (tooltip = _, bhm) : tooltip; }

  /**
   * Gets / sets the colorFunction
   * (see {@link bubbleHeatmap#colorFunction})
   * @param {colorFunction} [_=none]
   * @returns {bubbleHeatmap | colorFunction}
   * @memberof bubbleHeatmap
   * @property
   * by default colorFunction = colorFunction()
   */
  bhm.colorFunction = function(_) { return arguments.length ? (colorFunction = _, bhm) : colorFunction; }

  /**
   * Gets / sets the xSize
   * (see {@link bubbleHeatmap#xSize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default xSize = undefined
   */
  bhm.xSize = function(_) { return arguments.length ? (xSize = _, bhm) : xSize; }
  /**
   * Gets / sets the xSpacerSize
   * (see {@link bubbleHeatmap#xSpacerSize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default xSpacerSize = undefined
   */
  bhm.xSpacerSize = function(_) { return arguments.length ? (xSpacerSize = _, bhm) : xSpacerSize; }
  /**
   * Gets / sets the ySize
   * (see {@link bubbleHeatmap#ySize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default ySize = undefined
   */
  bhm.ySize = function(_) { return arguments.length ? (ySize = _, bhm) : ySize; }
  /**
   * Gets / sets the ySpacerSize
   * (see {@link bubbleHeatmap#ySpacerSize})
   * @param {number} [_=none]
   * @returns {bubbleHeatmap | number}
   * @memberof bubbleHeatmap
   * @property
   * by default ySpacerSize = undefined
   */
  bhm.ySpacerSize = function(_) { return arguments.length ? (ySpacerSize = _, bhm) : ySpacerSize; }
  // bhm.yKeySortingFunction = function(_) { return arguments.length ? (yKeySortingFunction = _, bhm) : yKeySortingFunction; }
  // bhm.xKeySortingFunction = function(_) { return arguments.length ? (xKeySortingFunction = _, bhm) : xKeySortingFunction; }



  function bhm() {
    var horizontalQ = true; // no orientation in heatmaps. Aids as placeholder in functions that take orient as a parameter
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    cellKeys = d3.keys(data);
    cellKeys.sort(function(a, b){ return xKeySortingFunction(a, b) || yKeySortingFunction(a, b) })
    utils.con.log('bubbleHeatmap', 'cells are sorted by', cellKeys)



    xValues = utils.arr.unique(cellKeys.map(xExtractor));
    yValues = utils.arr.unique(cellKeys.map(yExtractor));
    rValues = utils.arr.unique(cellKeys.map(rExtractor));
    vValues = utils.arr.unique(cellKeys.map(vExtractor));
    utils.con.log('bubbleHeatmap', 'x and y keys are', {x: xValues, y:yValues})


    var xDim = xValues.length, yDim = yValues.length;


    var extent = [Math.min(...rValues) - domainPadding,Math.max(...rValues) + domainPadding];


    ySize = utils.math.calculateWidthOfObject(spaceY, yDim, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    xSize = utils.math.calculateWidthOfObject(spaceX, xDim, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    ySpacerSize = utils.math.calculateWidthOfSpacer(yValues, spaceY, ySize, yDim, objectSpacer, overflowQ)
    xSpacerSize = utils.math.calculateWidthOfSpacer(xValues, spaceX, xSize, xDim, objectSpacer, overflowQ)
    utils.con.log('bubbleHeatmap', 'size of', {x: xSize, y: ySize})


    scale.domain(extent).range([Math.min(minObjectSize/2,   Math.min(ySize, xSize)/2), Math.min(ySize, xSize)/2])

    var ySpacer = groupingSpacer()
    .horizontalQ(false)
    .moveby('category').numberOfObjects(yDim)
    .objectClass(utils.str.hypenate(objectClass, 'row'))
    .objectSize(ySize).spacerSize(ySpacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace('row')

    var xSpacer = groupingSpacer()
    .horizontalQ(true)
    .moveby('category').numberOfObjects(xDim)
    .objectClass(objectClass)
    .objectSize(xSize).spacerSize(xSpacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)


    ySpacer(container, yValues, 0)
    container.selectAll('g.'+utils.str.hypenate(objectClass, 'row'))
    .each(function(d, i){
      xSpacer(d3.select(this), xValues, 0)
    })
    var cells = container.selectAll('g:not(.to-remove).'+objectClass).data(cellKeys);

    var parentIndexArray = []
    cells.each(function(d, i){ parentIndexArray.push(Number(d3.select(this).attr('parent-index'))) })


    colorFunction = colorFunction.colorBy() == 'index'
    ? colorFunction.dataExtent([0, Math.max(...parentIndexArray)])
    : colorFunction.dataExtent([0, Math.max(...vValues)])

    cells.each(function(key, i) {
      utils.con.log('bubbleHeatmap', 'each cell', {key: key, index: i, node: d3.select(this).node()})

      var t = d3.select(this),
      currentData = data[key],
      value = vExtractor(key, i),
      radius= rExtractor(key, i),
      i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
      fillColor = colorFunction(key, value, i, 'fill'), // prevent duplicate computation
      strokeColor = colorFunction(key, value, i,  'stroke')

      utils.con.log('bubbleHeatmap', 'radius',{radius: radius, scaled: scale(radius), extent: extent, range:scale.range()})

      var c = utils.sel.safeSelect(t, 'circle', utils.str.hypenate(objectClass,'circle'))
      c.attr('cx', xSize / 2)
      .attr('cy', ySize / 2 )
      .attr('r', scale(radius))
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', bubbleStrokeWidth)

    })

    tooltip.selection(cells.selectAll('circle.'+utils.str.hypenate(objectClass, 'circle')))
    .data(data)
    // .keys(['r', 'v'])
    // .header(function(d, i){return utils.str.hypenate(data[d][xKey], data[d][yKey]) })

    tooltip()


  }

  return bhm;
}
