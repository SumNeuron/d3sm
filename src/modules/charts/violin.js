import * as d3 from 'd3'
import utils from '../utils/index.js';
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';

/*******************************************************************************
**                                                                            **
**                                                                            **
**                                 VIOLIN                                     **
**                                                                            **
**                                                                            **
*******************************************************************************/

/**
 * Creates a violin
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/basic-violins/index.html Demo}
 * @constructor violin
 * @param {d3.selection} selection
 * @namespace violin
 * @returns {function} violin
 */
export default function violin( selection ) {
  var
  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a violin
  * (see {@link violin#data})
  * @param {Object} [data=undefined]
  * @memberof violin#
  * @property
  */
  data,
  /**
  * Which direction to render the bars in
  * (see {@link violin#orient})
  * @param {number} [orient='horizontal']
  * @memberof violin#
  * @property
  */
  orient='horizontal',
  /**
  * Amount of horizontal space (in pixels) avaible to render the violin in
  * (see {@link violin#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof violin#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the violin in
  * (see {@link violin.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof violin#
  * @property
  */
  spaceY,
  /**
  * Whether or not to allow violin to render elements pass the main spatial dimension
  * given the orientation (see {@link violin#orient}), where {@link violin#orient}="horizontal"
  * the main dimension is {@link violin#spaceX} and where {@link violin#orient}="vertical"
  * the main dimension is {@link violin#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof violin#
  * @property
  */
  overflowQ = true,
  /**
  * Whether or not to display points inside the points
  * @param {boolean} [pointsQ=false]
  * @memberof violin#
  * @property
  */
  pointsQ = true,
  /**
  * An array - putatively of other arrays - depicting how bars should be arranged
  * @param {Array[]} [grouping=undefined]
  * @memberof violin#
  * @property
  */
  grouping,
  /**
  * How to get the value of the violin
  * @param {function} [valueExtractor=function(key, index) { return data[key] }]
  * @memberof violin#
  * @property
  */
  valueExtractor = function(key, index) {return data[key] },
  /**
  * How to sort the bars - if {@link violin#grouping} is not provided.
  * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
  * @memberof violin#
  * @property
  */
  sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},

  /**
  * The scale for which violin values should be transformed by
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof violin#
  * @property
  */
  scale = d3.scaleLinear(),
  /**
  * The padding for the domain of the scale (see {@link violin#scale})
  * @param {number} [domainPadding=0.5]
  * @memberof violin#
  * @property
  */
  domainPadding = 0.5,
  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link violin#orient}), where {@link violin#orient}="horizontal"
  * the main dimension is {@link violin#spaceX} and where {@link violin#orient}="vertical"
  * the main dimension is {@link violin#spaceY} between bars
  * @param {number} [objectSpacer=0.05]
  * @memberof violin#
  * @property
  */
  objectSpacer = 0.05,
  /**
  * The minimum size that an object can be
  * @param {number} [minObjectSize=50]
  * @memberof violin#
  * @property
  */
  minObjectSize = 50,
  /**
  * The maximum size that an object can be
  * @param {number} [maxObjectSize=100]
  * @memberof violin#
  * @property
  */
  maxObjectSize = 100,

  /**
  * The stroke width of the bars
  * @param {number} [barStrokeWidth=2]
  * @memberof violin#
  * @property
  */
  objectStrokeWidth = 2,
  /**
  * Instance of ColorFunction
  * @param {function} [colorFunction = colorFunction()]
  * @memberof violin#
  * @property
  */
  colorFunction = CF(),
  /**
  * Instance of ColorFunction modified by a scale for the points
  * @param {function} [pointColorFunc = colorFunction()]
  * @memberof violin#
  * @property
  */
  pointColorFunc = function (d, type, base, min, max) {
    var minMaxHexScale = d3.scaleLinear().domain([min, max]).range([-0.25, 0.05])
    var scaledColor = utils.color.modifyHexidecimalColorLuminance(base.replace('#', ''), minMaxHexScale(d))
    var mod = type == "stroke" ? 0 : 0.25
    return utils.color.modifyHexidecimalColorLuminance(scaledColor.replace('#', ''), mod)
  },

  /**
  * The radius of a point
  * @param {number} [pointRadius=3]
  * @memberof violin#
  * @property
  */
  pointRadius = 3,
  /**
  * The stroke width of the oints
  * @param {number} [pointStrokeWidth=2]
  * @memberof violin#
  * @property
  */
  pointStrokeWidth = 2,

  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof violin#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of violin
  * @param {string} [namespace="d3sm-violin"]
  * @memberof violin#
  * @property
  */
  namespace = 'd3sm-violin',
  /**
  * Class name for violin container (<g> element)
  * @param {string} [objectClass="violin"]
  * @memberof violin#
  * @property
  */
  objectClass = 'violin',
  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof violin#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof violin#
  * @property
  */
  easeFunc = d3.easeExp,

  /**
  * The key containing the utils.math.quartiles
  * @param {string} [quartilesKey=undefined]
  * @memberof violin#
  * @property
  */
  quartilesKey = "quartiles",
  /**
  * The keys corresponding to each quartile
  * @param {string[]} [quartileKeys=["Q0", "Q1", "Q2", "Q3", "Q4"]]
  * @memberof violin#
  * @property
  */
  quartileKeys = ["Q0", "Q1", "Q2", "Q3", "Q4"],

  /**
  * The keys of the bars
  * @param {string[]} [violinKeys=undefined]
  * @memberof violin#
  * @property
  */
  violinKeys,
  /**
  * The values of the bars
  * @param {number[]} [violinValues=undefined]
  * @memberof violin#
  * @property
  */
  violinValues,
  /**
  * The objectSize (actual width) used by the bars
  * @param {number} [objectSize=undefined]
  * @memberof violin#
  * @property
  */
  objectSize,
  /**
  * The spacerSize (actual width) used by the spacers between the bars
  * @param {number} [spacerSize=undefined]
  * @memberof violin#
  * @property
  */
  spacerSize,

  /**
  * Instance of Tooltip
  * @param {function} [tooltip=tooltip()]
  * @memberof violin#
  * @property
  */
  tooltip = TTip().keys([quartileKeys[4], quartileKeys[3], quartileKeys[2], quartileKeys[1], quartileKeys[0]]),
  pointsTooltip = TTip(),
  // pointKeyExtractor = function(violinKey, violinData, violinValues) {return d3.keys(violinValues[violinKey].values)},
  // pointValueExtractor = function(pointKey, violinKey, violinData, violinValues) {return violinValues[violinKey].values[pointKey]},


  /**
  * Function which given the key of the violin and that key's associated value
  * returns the object consiting of the points of the violin
  * (see {@link violin#violinPointsExtractor})
  * @param {Object} [violinPointsExtractor=function(violinKey, violinData) { return violinData.points }]
  * @memberof violin#
  * @property
  */
  violinPointsExtractor = function (violinKey, violinData) {return violinData.points },
  /**
  * Function which given the key of the current point and the object of points for the
  * violin, returns the numerical value of the point
  * (see {@link violin#violinPointValueExtractor})
  * @param {Object} [violinPointValueExtractor=function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }]
  * @memberof violin#
  * @property
  */
  violinPointValueExtractor = function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }


  /**
   * Gets or sets the violinPointsExtractor
   * @param {function} [_=none]
   * @returns {violin | function}
   * @memberof violin
   * @property
   * by default violinPointsExtractor = function(violinKey, violinData) { return violinData.points }
   */
  violin.violinPointsExtractor = function(_) { return arguments.length ? (violinPointsExtractor = _, violin) : violinPointsExtractor; };
  /**
   * Gets or sets the violinPointValueExtractor
   * @param {function} [_=none]
   * @returns {violin | function}
   * @memberof violin
   * @property
   * by default violinPointsExtractor = function(pointKey, violinKey, violinData, violinValues) {return violinValues[violinKey].values[pointKey]}
   */
  violin.violinPointValueExtractor = function(_) { return arguments.length ? (violinPointValueExtractor = _, violin) : violinPointValueExtractor; };


  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {violin | d3.selection}
   * @memberof violin
   * @property
   * by default selection = selection
   */
  violin.selection = function(_) { return arguments.length ? (selection = _, violin) : selection; };
  /**
   * Gets or sets the data
   * (see {@link violin#data})
   * @param {number} [_=none]
   * @returns {violin | object}
   * @memberof violin
   * @property
   */
  violin.data = function(_) { return arguments.length ? (data = _, violin) : data; };
  /**
   * Gets or sets the orient of the boxes
   * (see {@link violin#orient})
   * @param {number} [_=none]
   * @returns {violin | object}
   * @memberof violin
   * @property
   */
  violin.orient = function(_) { return arguments.length ? (orient = _, violin) : orient; };
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link violin#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default spaceX = undefined
   */
  violin.spaceX = function(_) { return arguments.length ? (spaceX = _, violin) : spaceX; };
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link violin#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default spaceY = undefined
   */
  violin.spaceY = function(_) { return arguments.length ? (spaceY = _, violin) : spaceY; };


  /**
   * Gets / sets whether or not violin is allowed to go beyond specified dimensions
   * (see {@link violin#overflowQ})
   * @param {boolean} [_=none]
   * @returns {violin | boolean}
   * @memberof violin
   * @property
   * by default overflowQ = false
   */
  violin.overflowQ = function(_) { return arguments.length ? (overflowQ = _, violin) : overflowQ; };
  /**
   * Gets / sets whether or not to plot points with the violins
   * (see {@link violin#pointsQ})
   * @param {boolean} [_=none]
   * @returns {violin | boolean}
   * @memberof violin
   * @property
   * by default pointsQ = false
   */
  violin.pointsQ = function(_) { return arguments.length ? (pointsQ = _, violin) : pointsQ; };


  /**
   * Gets / sets the grouping of the boxes
   * (see {@link violin#grouping})
   * @param {Array[]} [_=none]
   * @returns {violin | Array[]}
   * @memberof violin
   * @property
   * by default grouping = undefined
   */
  violin.grouping = function(_) { return arguments.length ? (grouping = _, violin) : grouping; };
  /**
   * Gets / sets the valueExtractor
   * (see {@link violin#valueExtractor})
   * @param {function} [_=none]
   * @returns {violin | function}
   * @memberof violin
   * @property
   */
  violin.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, violin) : valueExtractor; };
  /**
   * Gets / sets the sortingFunction
   * (see {@link violin#sortingFunction})
   * @param {function} [_=none]
   * @returns {violin | function}
   * @memberof violin
   * @property
   */
  violin.sortingFunction = function(_) { return arguments.length ? (sortingFunction = _, violin) : sortingFunction; };

  /**
   * Gets / sets the scale for which the violin values should be transformed by
   * (see {@link violin#scale})
   * @param {d3.scale} [_=none]
   * @returns {violin | d3.scale}
   * @memberof violin
   * @property
   * by default scale = d3.scaleLinear()
   */
  violin.scale = function(_) { return arguments.length ? (scale = _, violin) : scale; };
  /**
   * Gets / sets the padding for the domain of the scale
   * (see {@link violin#domainPadding})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default domainPadding = 0.5
   */
  violin.domainPadding = function(_) { return arguments.length ? (domainPadding = _, violin) : domainPadding; };


  /**
   * Gets / sets objectSpacer
   * (see {@link violin#objectSpacer})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default objectSpacer = 0.05
   */
  violin.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, violin) : objectSpacer; };
  /**
   * Gets / sets the minObjectSize
   * (see {@link violin#minObjectSize})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default minObjectSize = 15
   */
  violin.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, violin) : minObjectSize; };
  /**
   * Gets / sets the maxObjectSize
   * (see {@link violin#maxObjectSize})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default maxObjectSize = 50
   */
  violin.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, violin) : maxObjectSize; };

  /**
   * Gets / sets the objectStrokeWidth
   * (see {@link violin#objectStrokeWidth})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default objectStrokeWidth = 2
   */
  violin.objectStrokeWidth = function(_) { return arguments.length ? (objectStrokeWidth = _, violin) : objectStrokeWidth; };


  /**
   * Gets / sets the colorFunction
   * (see {@link violin#colorFunction})
   * @param {colorFunction} [_=none]
   * @returns {violin | colorFunction}
   * @memberof violin
   * @property
   * by default colorFunction = colorFunction()
   */
  violin.colorFunction = function(_) { return arguments.length ? (colorFunction = _, violin) : colorFunction; };
  /**
   * Gets / sets the colorFunction
   * (see {@link violin#colorFunction})
   * @param {colorFunction} [_=none]
   * @returns {violin | colorFunction}
   * @memberof violin
   * @property
   * by default colorFunction = colorFunction()
   */
  violin.pointColorFunc = function(_) { return arguments.length ? (pointColorFunc = _, violin) : pointColorFunc; };


  /**
   * Gets / sets the pointRadius
   * (see {@link violin#pointRadius})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default pointRadius = 2
   */
  violin.pointRadius = function(_) { return arguments.length ? (pointRadius = _, violin) : pointRadius; };
  /**
   * Gets / sets the pointStrokeWidth
   * (see {@link violin#pointStrokeWidth})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default pointStrokeWidth = 2
   */
  violin.pointStrokeWidth = function(_) { return arguments.length ? (pointStrokeWidth = _, violin) : pointStrokeWidth; };


  /**
   * Gets / sets the backgroundFill
   * (see {@link violin#backgroundFill})
   * @param {string} [_=none]
   * @returns {violin | string}
   * @memberof violin
   * @property
   * by default backgroundFill = 'transparent'
   */
  violin.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, violin) : backgroundFill; };
  /**
   * Gets / sets the namespace
   * (see {@link violin#namespace})
   * @param {string} [_=none]
   * @returns {violin | string}
   * @memberof violin
   * @property
   * by default namespace = 'd3sm-violin'
   */
  violin.namespace = function(_) { return arguments.length ? (namespace = _, violin) : namespace; };
  /**
   * Gets / sets the objectClass
   * (see {@link violin#objectClass})
   * @param {string} [_=none]
   * @returns {violin | string}
   * @memberof violin
   * @property
   * by default objectClass = 'tick-group'
   */
  violin.objectClass = function(_) { return arguments.length ? (objectClass = _, violin) : objectClass; };


  /**
   * Gets / sets the transitionDuration
   * (see {@link violin#transitionDuration})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default transitionDuration = 1000
   */
  violin.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, violin) : transitionDuration; };
  /**
   * Gets / sets the easeFunc
   * (see {@link violin#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {violin | d3.ease}
   * @memberof violin
   * @property
   * by default easeFunc = d3.easeExp
   */
  violin.easeFunc = function(_) { return arguments.length ? (easeFunc = _, violin) : easeFunc; };


  /**
   * Gets / sets the quartileKey
   * (see {@link violin#quartileKey})
   * @param {string} [_=none]
   * @returns {violin | string}
   * @memberof violin
   * @property
   * by default quartileKey = "utils.math.quartiles"
   */
  violin.quartileKey = function(_) { return arguments.length ? (quartileKey = _, violin) : quartileKey; };
  /**
   * Gets / sets the quartileKeys
   * (see {@link violin#quartileKeys})
   * @param {string[]} [_=none]
   * @returns {violin | string[]}
   * @memberof violin
   * @property
   * by default quartileKeys = ["Q0","Q1","Q2","Q3","Q4"]
   */
  violin.quartileKeys = function(_) { return arguments.length ? (quartileKeys = _, violin) : quartileKeys; };


  /**
   * Gets / sets the violinKeys
   * (see {@link violin#violinKeys})
   * @param {string[]} [_=none]
   * @returns {violin | string[]}
   * @memberof violin
   * @property
   * by default violinKeys = undefined
   */
  violin.violinKeys = function(_) { return arguments.length ? (violinKeys = _, violin) : violinKeys; };
  /**
   * Gets / sets the violinValues
   * (see {@link violin#violinValues})
   * @param {Object[]} [_=none]
   * @returns {violin | Object[]}
   * @memberof violin
   * @property
   * by default violinValues = undefined
   */
  violin.violinValues = function(_) { return arguments.length ? (violinValues = _, violin) : violinValues; };

  /**
   * Gets / sets the objectSize
   * (see {@link violin#objectSize})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default objectSize = undefined
   */
  violin.objectSize = function(_) { return arguments.length ? (objectSize = _, violin) : objectSize; };
  /**
   * Gets / sets the spacerSize
   * (see {@link violin#spacerSize})
   * @param {number} [_=none]
   * @returns {violin | number}
   * @memberof violin
   * @property
   * by default spacerSize = undefined
   */
  violin.spacerSize = function(_) { return arguments.length ? (spacerSize = _, violin) : spacerSize; };
  /**
   * Gets / sets the tooltip
   * (see {@link violin#tooltip})
   * @param {tooltip} [_=none]
   * @returns {violin | tooltip}
   * @memberof violin
   * @property
   * by default tooltip = tooltip()
   */
  violin.tooltip = function(_) { return arguments.length ? (tooltip = _, violin) : tooltip; };
  /**
   * Gets / sets the pointsTooltip
   * (see {@link violin#pointsTooltip})
   * @param {tooltip} [_=none]
   * @returns {violin | tooltip}
   * @memberof violin
   * @property
   * by default pointsTooltip = tooltip()
   */
  violin.pointsTooltip = function(_) { return arguments.length ? (pointsTooltip = _, violin) : pointsTooltip; };





  function violin () {
    // for convenience in handling orientation specific values
    var horizontalQ = (orient == 'horizontal') ? true : false
    var verticalQ = !horizontalQ

    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    // if grouping is undefined sort violinKeys by sortingFunction
    var ordered = (grouping == undefined) ? d3.keys(data).sort(sortingFunction) : grouping

    // console.log(ordered)

    violinKeys = utils.arr.flatten(ordered)


    var calcValues = neededViolinValues()
    .horizontalQ(horizontalQ)
    .quartileKeys(quartileKeys)
    .violinPointsExtractor(violinPointsExtractor)
    .violinPointValueExtractor(violinPointValueExtractor)



    // augment valus
    violinValues = {}
    violinKeys.forEach(function(vk, i){
      violinValues[vk] = calcValues(vk, data)
      // console.log(data, violinValues[vk] )
    })


    var numberOfObjects = violinKeys.length

    var min = [].concat(...violinKeys.map(function(k, i){return violinValues[k].quartiles[quartileKeys[0]]}))
                .filter(x=>x!==undefined)
    var max = [].concat(...violinKeys.map(function(k, i){return violinValues[k].quartiles[quartileKeys[quartileKeys.length - 1]]}))
                .filter(x=>x!==undefined)
    var extent = [Math.min(...min) - domainPadding, Math.max(...max) + domainPadding]
    // console.log(min, max, extent, violinValues, ordered)

    // set the scale
    scale.domain(extent).range(horizontalQ ? [0,spaceY] : [0, spaceX])
    var space = horizontalQ ? spaceX : spaceY
    // calculate object size
    objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    // calculate spacer size if needed
    spacerSize = utils.math.calculateWidthOfSpacer(ordered, space, objectSize, numberOfObjects, objectSpacer, overflowQ)
    // make the nested groups
    var spacerFunction = groupingSpacer()
    .horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects)
    .objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace(namespace)

    // move stuff
    spacerFunction(container, ordered, 0)
    // console.log(violinKeys, ordered, container.selectAll('g:not(.to-remove).'+objectClass).nodes())

    // for color function
    var parentIndexArray = []
    container.selectAll('g:not(.to-remove).'+objectClass)
    .each(function(d, i){if (utils.arr.hasQ(violinKeys, d)){ parentIndexArray.push(Number(d3.select(this).attr('parent-index')))}})

    // update color function
    colorFunction = colorFunction.colorBy() == 'index'
    ? colorFunction.dataExtent([0, Math.max(...parentIndexArray)])
    : colorFunction.dataExtent(extent)

    /* violiin specific needs */


    var frequencyMax = Math.max(...[].concat(...violinKeys.map(function(k, i){return d3.max(violinValues[k].frequencies)})))
    var vScale = d3.scaleLinear().domain([0, frequencyMax]).range([0, objectSize / 2])

    var lArea = d3.line()
    .x(function(d, i){ return horizontalQ ? -vScale(d.x) : scale(d.x)})
    .y(function(d, i){ return horizontalQ ? scale(extent[1]) - scale(d.y) : -vScale(d.y)})
    .curve(d3.curveBasis)
    var rArea = d3.line()
    .x(function(d, i){ return horizontalQ ? vScale(d.x) : scale(d.x)})
    .y(function(d, i){ return horizontalQ ? scale(extent[1]) - scale(d.y) : vScale(d.y)})
    .curve(d3.curveBasis)






    container.selectAll('g:not(.to-remove).'+objectClass).each(function(key, i){
      var t = d3.select(this),
      currentData = data[key],
      currentViolinData = violinValues[key]
      // needed because bug in selecting .to-remove
      if (!utils.arr.hasQ(violinKeys, key)) {return}
      var
      i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
      fillColor = colorFunction(key, currentData, i, 'fill'), // prevent duplicate computation
      strokeColor = colorFunction(key, currentData, i, 'stroke'),
      area = utils.sel.safeSelect(t, 'g', 'area'),
      la = utils.sel.safeSelect(area, 'path', 'left'),
      ra = utils.sel.safeSelect(area, 'path', 'right'),
      quarts = utils.sel.safeSelect(t, 'g', 'quarts'),
      lq3 = utils.sel.safeSelect(quarts, 'line', 'q3'),
      lq1 = utils.sel.safeSelect(quarts, 'line', 'q1'),
      q3 = currentViolinData.quartiles[quartileKeys[3]],
      q2 = currentViolinData.quartiles[quartileKeys[2]],
      q1 = currentViolinData.quartiles[quartileKeys[1]]

      t.attr('transform', horizontalQ ? 'translate('+objectSize / 2+',0)' : 'translate(0,'+objectSize / 2+')'  )
      // draw curve
      la.transition().duration(transitionDuration).attr('d', function(dd, ii){ return lArea(currentViolinData.contour)})
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', objectStrokeWidth)

      ra.transition().duration(transitionDuration).attr('d', function(dd, ii){ return rArea(currentViolinData.contour)})
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', objectStrokeWidth)

      area.node().addEventListener('mouseover', function(dd, ii){
        container.selectAll('g.'+objectClass).style('opacity', 0.2)
        t.style('opacity', 1)
        la.attr('stroke-width',objectStrokeWidth*2)
        ra.attr('stroke-width',objectStrokeWidth*2)
      })
      area.node().addEventListener('mouseout', function(dd, ii){
        container.selectAll('g.'+objectClass).style('opacity', 1)
        la.attr('stroke-width',objectStrokeWidth)
        ra.attr('stroke-width',objectStrokeWidth)
      })

      if (pointsQ) {
        var ptsContainer = utils.sel.safeSelect(t, 'g', 'points')
        var pts = ptsContainer.selectAll('.point').data(currentViolinData.pointKeys)
        pts.on('mouseover', null)


        var ptsExit = pts.exit().transition().ease(easeFunc).duration(transitionDuration)
        .attr('r', 0)
        .attr('cy', horizontalQ ? scale(extent[1]) - scale(q2) : vScale(0))
        .attr('cx', horizontalQ ? vScale(0) : scale(q2)).remove()

        var ptsEnter = pts.enter().append('circle').attr('class', 'point').attr('r', 0)
        .attr('cx', horizontalQ ? 0 : scale(q2))
        .attr('cy', horizontalQ ? scale(q2) : 0)

        pts = pts.merge(ptsEnter)

        // console.log(pointsTooltip.header())

        var pTTips = pointsTooltip.selection(pts)
        .data(violinPointsExtractor(key, currentData))

        pTTips()

        var pMin = d3.min(currentViolinData.pointValues), pMax = d3.max(currentViolinData.pointValues)



        pts.transition().duration(transitionDuration).ease(easeFunc).attr('r', pointRadius)
        .attr('cy', function(pointKey, ii){
          var dd = currentViolinData.pointValues[ii]
          if (horizontalQ) { return scale(extent[1]) - scale(dd) }
          var j = utils.arr.whichBin(currentViolinData.binned, dd)
          var r = Math.random()
          var n = vScale(r * currentViolinData.frequencies[j] * 0.5)
          var k = Math.random() > 0.5 ? n : -n
          return k
        })
        .attr('cx', function(pointKey, ii){
          var dd = currentViolinData.pointValues[ii]
          if (horizontalQ) {
            var j = utils.arr.whichBin(currentViolinData.binned, dd)
            var r = Math.random()
            var n = vScale(r * currentViolinData.frequencies[j] * 0.5)
            var k = Math.random() > 0.5 ? n : -n
            return k
          }
          return scale(dd)
        })
        .attr('stroke', function(dd, ii) { var dd = currentViolinData.pointValues[ii]; return pointColorFunc(dd, 'stroke', strokeColor, pMin, pMax) })
        .attr('fill'  , function(dd, ii) { var dd = currentViolinData.pointValues[ii]; return pointColorFunc(dd, 'fill'  , strokeColor, pMin, pMax) })
        .attr('stroke-width', pointStrokeWidth)

        ptsContainer.selectAll('circle.point').on('mouseover', function(dd, ii){
          container.selectAll('g.'+objectClass).style('opacity', 0.2)
          t.style('opacity', 1)
          la.attr('stroke-width',objectStrokeWidth*2)
          ra.attr('stroke-width',objectStrokeWidth*2)

          container.selectAll('.point').style('opacity', 0.2)
          d3.select(this).style('opacity', 1).attr('r', pointRadius * 2).attr('stroke-width',pointStrokeWidth*2)
        })
        ptsContainer.selectAll('circle.point').on('mouseout', function(dd, ii){
          var e = document.createEvent('SVGEvents')
          e.initEvent('mouseout',true,true);
          area.node().dispatchEvent(e)

          container.selectAll('.point').style('opacity', 1)
          d3.select(this).attr('stroke-width', pointStrokeWidth).attr('r', pointRadius)
        })
      }
      else {
        cV.selectAll('.point')
        .transition().duration(transitionDuration).ease(easeFunc)
        .attr('r', 0)
        .attr('cy', horizontalQ ? scale(extent[1]) - scale(q2) : vScale(0))
        .attr('cx', horizontalQ ? vScale(0) : scale(q2))
        .remove()
      }


    })

    tooltip.selection(container.selectAll('g:not(.to-remove).'+objectClass + ' .area'))
    if (tooltip.data() == undefined) {tooltip.data(data)}
    tooltip()
    if (tooltip.values() == undefined) {
      tooltip.values([
        function(currentKey, currentData, tooltipKey){ return violinValues[currentKey].quartiles[tooltipKey] },
        function(currentKey, currentData, tooltipKey){ return violinValues[currentKey].quartiles[tooltipKey] },
        function(currentKey, currentData, tooltipKey){ return violinValues[currentKey].quartiles[tooltipKey] },
        function(currentKey, currentData, tooltipKey){ return violinValues[currentKey].quartiles[tooltipKey] },
        function(currentKey, currentData, tooltipKey){ return violinValues[currentKey].quartiles[tooltipKey] },
      ])

    }

  }

  return violin
}





/**
* Produces the function which manipulates the violin data to have values needed
* for rendering the violins as svg.
* @returns {function} calculateViolinValues
* @namespace neededViolinValues
*/
export function neededViolinValues() {
  var
  /**
  * Whether or not the orientation of the violins are horizontal
  * (see {@link violin#orient})
  * @param {Object} [horizontalQ=true]
  * @memberof neededViolinValues#
  * @property
  */
  horizontalQ = true,
  /**
  * Keys to be put into the utils.math.quartiles if they need to be calculated.
  * (see {@link violin#quartileKeys})
  * @param {Object} [quartileKeys=['Q0', 'Q1', 'Q2', 'Q3', 'Q4']]
  * @memberof neededViolinValues#
  * @property
  */
  quartileKeys = ['Q0', 'Q1', 'Q2', 'Q3', 'Q4'],
  /**
  * Function which given the key of the violin and that key's associated value
  * returns the object consiting of the points of the violin
  * (see {@link violin#violinPointsExtractor})
  * @param {Object} [violinPointsExtractor=function(violinKey, violinData) { return violinData.points }]
  * @memberof neededViolinValues#
  * @property
  */
  violinPointsExtractor,
  /**
  * Function which given the key of the current point and the object of points for the
  * violin, returns the numerical value of the point
  * (see {@link violin#violinPointValueExtractor})
  * @param {Object} [violinPointValueExtractor=function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }]
  * @memberof neededViolinValues#
  * @property
  */
  violinPointValueExtractor


  /**
   * Gets / sets the horizontalQ
   * (see {@link violin#orient})
   * @param {boolean} [_=none]
   * @returns {calculateViolinValues | boolean}
   * @memberof calculateViolinValues
   * @property
   *
   * by default horizontalQ = true
   */
  calculateViolinValues.horizontalQ = function(_) { return arguments.length ? (horizontalQ=_, calculateViolinValues) : horizontalQ }
  /**
   * Gets / sets the quartileKeys
   * (see {@link violin#quartileKeys})
   * @param {string[]} [_=none]
   * @returns {calculateViolinValues | string[]}
   * @memberof calculateViolinValues
   * @property
   *
   * by default quartileKeys = ["Q0","Q1","Q2","Q3","Q4"]
   */
  calculateViolinValues.quartileKeys = function(_) { return arguments.length ? (quartileKeys=_, calculateViolinValues) : quartileKeys }
  /**
   * Gets / sets the violinPointsExtractor
   * (see {@link violin#violinPointsExtractor})
   * @param {function} [_=none]
   * @returns {calculateViolinValues | function}
   * @memberof calculateViolinValues
   * @property
   *
   * by default violinPointsExtractor = function(violinKey, violinData) { return violinData.points }
   */
  calculateViolinValues.violinPointsExtractor = function(_) { return arguments.length ? (violinPointsExtractor=_, calculateViolinValues) : violinPointsExtractor }
  /**
   * Gets / sets the violinPointValueExtractor
   * (see {@link violin#violinPointValueExtractor})
   * @param {function} [_=none]
   * @returns {calculateViolinValues | function}
   * @memberof calculateViolinValues
   * @property
   *
   * by default violinPointValueExtractor = function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }
   */
  calculateViolinValues.violinPointValueExtractor = function(_) { return arguments.length ? (violinPointValueExtractor=_, calculateViolinValues) : violinPointValueExtractor }


  /**
  * The function produced by neededViolinValues.
  *
  * Adds the data need to render the violin as an svg
  * @param {string} violinKey the key of the current violin
  * @param {object} data the object consisting of violinKey - values (violin data) pairs
  * @returns {none} this function manipulates the passed data object adding the following keys
  *
  * data[violinKey].binned // the binned values of the points
  *
  * data[violinKey].frequencies // the list consisting of the length of each bin
  *
  * data[violinKey].contour // the points depicting the contour of 1/2 of the violin
  *
  * data[violinKey].utils.math.quartiles // the utils.math.quartiles of the points' values
  *
  * data[violinKey].pointKeys // the keys associated with the points
  *
  * data[violinKey].pointValues // the numerical values of the points
  *
  * @memberof neededViolinValues#
  * @property
  */
  function calculateViolinValues(violinKey, data) {
    // data for the current violin
    var violinData = {}
    // Object.assign(violinData, data[violinKey])
    // console.log(data[violinKey], violinData)
    // the object of points
    var violinPoints = violinPointsExtractor(violinKey,  data[violinKey]);
    //
    var violinPointsKeys = d3.keys(violinPoints);
    // the numerical values of those points
    var violinPointsValues = violinPointsKeys.map(function(pk, i){return violinPointValueExtractor(pk, violinPoints)})

    // utils.math.quartiles of those points
    var pointQuartiles = utils.math.quartiles(violinPointsValues, quartileKeys)

    // binned points
    var binned = d3.histogram()(violinPointsValues)
    // length of bins
    var frequencies = binned.map(bin=>bin.length)
    // min and max countour points for nice drawings
    var minContourPoint = horizontalQ ? {x: 0, y: d3.min(violinPointsValues)} :  {x: d3.min(violinPointsValues), y: 0}
    var maxContourPoint = horizontalQ ? {x: 0, y: d3.max(violinPointsValues)} :  {x: d3.max(violinPointsValues), y: 0}
    var violinContourPoints = binned.map(function(bin, i) {
        return horizontalQ
        ? {y: (bin.length) ? d3.median(bin): d3.median([bin.x0, bin.x1]), x: frequencies[i]}
        : {x: (bin.length) ? d3.median(bin): d3.median([bin.x0, bin.x1]), y: frequencies[i]}
      })
    // points along which to draw the violin shpe
    violinContourPoints = [minContourPoint].concat(violinContourPoints).concat([maxContourPoint])

    // set data
    violinData.binned = binned;
    violinData.frequencies = frequencies
    violinData.contour = violinContourPoints
    violinData.quartiles = pointQuartiles
    violinData.pointKeys = violinPointsKeys
    violinData.pointValues = violinPointsValues
    return violinData
  }

  return calculateViolinValues
}
