/**
* Helper function for Array.filter to get unique elements of the array
* @param {*} value current value as mapping over array (self)
* @param {number} index current index in the array
* @param {Array} self passed array from Array.filter method
* @returns {boolean} whether or not value is the first of its kind (i.e. indexOf(value) == index)
*/
export function uniqueElements(value, index, self) { return self.indexOf(value) === index; }

/**
* This function tests to see if all elements of the passed array are true.
* @param {Array} array of values
* @param {Function} [func function(value){return value == true;}] is applied to each value of the array and should return a boolean.
* @returns {boolean} if all values are true by function
*/
export function all( array, func ) {
  if (func == undefined) { return array.every( function(value) { return value === true; }); }
  return array.every( function(value) { return func(value); } );
}

/**
* Counts the number of occurances of each element in the given array.
* @param {Array} array of elements
* @returns {Object} of key: value pairs where key is an element in the array and value is the number of times it occurs.
*/
export function tally( array ) {
  var tallies = {};
  array.map( function ( element ) {
    if ( hasQ(Object.keys(tallies), element) ) { tallies[element] = 1; }
    else { tallies[element] += 1; }
  });
  return tallies;
}
/**
* Short-hand for array.includes(item);
* @param {Array} array
* @param {*} item to test if contained in  {array}
* @returns {boolean}
*/
export function hasQ( array, item ) { return array.includes(item); }

/**
* Returns first item in array
* @param {Array} array of items
* @returns {*} array[0]
*/
export function first( array ) { return array[0]; }

/**
* Returns last item in array
* @param {Array} array of items
* @returns {*} array[array.length-1]
*/
export function last( array ) { return array[array.length-1]; }

/**
* Calculates the total value of numbers in passed array
* @param {number[]} array of numerical values
* @returns {number} sum over elements in array
*/
export function total( array ) { return array.reduce((a, b) => a + b, 0) };

/**
* Removes duplicates in array
* @param {Array} array of items
* @returns {Array} of items such that item_i != item_j for all i < j
* @see{@link uniqueElements} for the filtering function
*/
export function unique( array ) { return array.filter( uniqueElements ); }

/**
* Filters passed array for specified indicies
* @param {Array} array of items
* @param {number[]} positions of integers such that i < array.length
* @returns {Array} of items such that for any item_i, positions.includes(i) === true
*/
export function get( array, positions ) {
  return array.filter( function( value, index ) { return hasQ(positions, index); } );
}

/**
* Determines if all elements in passed array are arrays themselves.
* @param {Array} array of items
* @returns {boolean} true if Array.isArray(e) is true for all e in array
* @see{@link all}
*/
export function listOfListsQ( array ) {
  return all( array.map( function( element, index ) { return Array.isArray(element) } ) )
}

/**
* Built on top of @see{@link get}, mapping if positions is a list of lists (@see{@link listOfListsQ})
* @param {Array} array of items
* @param {number[] | []number[] } positions of integers or list of positions of integers
* @returns {boolean} returns specified positions from array. If nested positions passed, returns requested items in same structure.
*/
export function cut( array, positions ) {
  if ( listOfListsQ(array) ) { return positions.map(function(pos, i) { return array.get(pos); }); }
  return get( array, positions );
}

/**
* Given an array of objects, constructs new objects where each value is a list
* based on the corresonding key, which is extracted by the parameter by
* @param {Objects[]} array of objects
* @param {string} by key within all objects of passed array
* @param {string[]} [groups] saves some computation if all known values extracted by mapping over the parameter by are passed
* @returns {Object} of key value pairs, where keys are all values of the key by from an object in the passed array and the value are those corresponding objects.
*/
export function groupBy (array, by, groups) {
  if (groups == undefined) {
    groups = unique(array.map(function(elements, index){ return element[by]; }));
    groups.map(function(value, index){groupped[value] = []})
  }

  var groupped = {};
  array.map(function(element, index){groupped[element[by]].push(element)});
  return groupped
}

/**
* Tests if two arrays are equivalent
* @param {Array} array
* @param {Array} other
* @returns {boolean} if every element of array matches that of other
*/
export function arrayEquals(array, other) {
  if (!other)
      return false;
  // compare lengths - can save a lot of time
  if (array.length != other.length)
      return false;

  for (var i = 0, l=array.length; i < l; i++) {
      // Check if we have nested arrays
      if (array[i] instanceof Array && other[i] instanceof Array) {
          // recurse into the nested arrays
          if (!arrayEquals(array[i],other[i]))
              return false;
      }
      else if (array[i] != other[i]) {
          // Warning - two different object instances will never be equal: {x:20} != {x:20}
          return false;
      }
  }
  return true;
}



/**
* Recursively tallies the number of elements at each level of the passed, putatively nested array
* @param {Array} array of items which may include nested arrays
* @param {number} [level=0] current depth in the recursion
* @param {Array} [levelData=[]] keeps track of items seen so far at each depth
* @returns {Array} stating the number of elements (array inclusive) found at each level of the array
*/
export function elementsAtLevels(array, level, levelData) {
  level = level == undefined ? 0 : level + 1;
  levelData = levelData == undefined ? [] : levelData;
  if ( level >= levelData.length ) { levelData.push(array.length)} else {levelData[level] += array.length }
  array.map(function(e, i) {if (Array.isArray(e)){ elementsAtLevels(e, level, levelData) }})
  return levelData
}


/**
* Recursively tallies the number of elements of the passed, putatively nested array
* @param {Array} array of items which may include nested arrays
* @param {number} [elements=0] current number of elements seen so far
* @returns {number} number of elements (array inclusive) found in passed array
*/
export function numberOfElements( array, elements ) {
  elements = elements == undefined ? 0 : elements;
  array.map(function(e, i) {
    if ( Array.isArray(e) ) { elements = numberOfElements(e, elements) }
    else { elements += 1 }
  })
  return elements
}

/**
* Concats all nested arrays in passed array to form a single array
* @param {Array} array of putatively nested arrays
* @param {Array} [flat=[]] current flattened array
* @returns {Array} with every element in the same level
*/
export function flatten( array, flat ) {
  flat = flat == undefined ? [] : flat;
  array.map(function(e, i){
    if (Array.isArray(e)) {flat = flat.concat(flatten(e))}
    else {flat.push(e)}
  })
  return flat;
}

/**
* Search of list of lists to find which - if any - passed value is in
* @param {Array[]} bins list of lists of values
* @param {*} value item to test if in any of the bins
* @returns {number} indicating the index of the bin in which value was found
*/
export function whichBin(bins, value) {
  var i = -1
  for (var j = 0; j < bins.length; j++) { if (hasQ(bins[j],value)) {return j} }
  return i
}
