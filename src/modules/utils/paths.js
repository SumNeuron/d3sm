
/**
* Draws a whisker for @see{@link boxwhisker}
* @param {boolean} dir direction to draw whisker, should be either true (up, top) or false (down or bottom)
* @param {number} x starting x coordinate in which to draw whisker
* @param {number} y starting y coordinate in which to draw whisker
* @param {number} w width of space in which to draw whisker
* @param {number} h height of space in which to draw whisker
* @param {number} per percentage of w or h (depends on o) to make whisker
* @param {boolean} o orientation, true is horizontal and false is vertical
* @returns {string} representing the svg path (i.e. the d attribute for a path tag)
*/
export function whiskerPath(dir, x, y, w, h, per, o) {
  // d = direction (true is up), p = percent width
  if (dir == 'up' || dir == 'top' || dir == true) {dir = true}
  if (dir == 'down' || dir == 'bottom' || dir == false) {dir = false}
  o = o == undefined ? 'horizontal' : o
  per = per == undefined ? 1 : per
  if (o != "horizontal") {
    var hh = h * per ,
    w = dir ? w : -w ,
    a = dir ? x + w : x ,
    b = dir ? x : x + w ,
    c = dir ? a : b
    p = "M " + a + ' ' + (     h / 2      ) + ' '
      + 'L ' + b + ' ' + (     h / 2      ) + ' '
      + 'M ' + c + ' ' + ( h / 2 - hh / 2 ) + ' '
      + 'L ' + c + ' ' + ( h / 2 + hh / 2 ) + ' '

    return p
  }
  var ww = w * per,
  a = dir ? y + h : y  ,
  b = dir ? y : y + h  ,
  p = "M " + (  w / 2  ) + ' ' + a + ' ' // straight line part
    + 'L ' + (  w / 2  ) + ' ' + b + ' ' // straight line part
    + 'h ' + ( -ww / 2 ) + ' ' + 0 + ' ' // horizontal line part
    + 'h ' + (    ww   ) + ' ' + 0 + ' '
  return p
}
