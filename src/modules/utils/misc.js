import {safeSelect} from './selections.js'
import {hypenate} from './strings.js'
export function resizeDebounce(f, wait) {
  var resize = debounce(function(){f()},wait)
  window.addEventListener('resize', resize)
}


export function setget(arg, fn){
  return function (_) { return arguments.length ? (arg = _, fn) : arg; };
}

function debounce(func, wait, immediate) {
  var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

export function setupStandardChartContainers(
  selection,
  namespace,
  container,
  margins={top:0.01, bottom:0.01, left:0.01, right:0.01},
  svg={w:0.8, h:0.6}, // percent of container space for svg
  axes={y:0.1, x:0.1}, // percent of container space for axes,
  leg={x:0, margin:0, pos:'left'} // absolute width of legend and space on either size
)
{
  if (container == undefined) {container = {w:window.innerWidth, h:window.Height}}
  // SVG width and height

  var svgSpace =  {
    w: container.w * svg.w,
    h: container.h * svg.h
  }

  var margPx = {
    top: margins.top * svgSpace.h,
    bottom: margins.bottom * svgSpace.h,
    left: margins.left * svgSpace.w,
    right: margins.right * svgSpace.w
  },



  // Space after removing margins
  chartSpace = {
    w: svgSpace.w - margPx.left - margPx.right,
    h: svgSpace.h - margPx.top - margPx.bottom
  },

  // main dimension of x and y axies
  // e.g. defines how tall x axis is as length is determined by plotRect.w
  axesSpace = {
    x: chartSpace.h * axes.x,
    y: chartSpace.w * axes.y
  },

  // space left for drawing the chart properly (e.g. bars, violins, etc)
  drawingSpace = {
    x: chartSpace.w - axesSpace.y - leg.x - 2*leg.margin,
    y: chartSpace.h - axesSpace.x
  },


  legRect = {
    x: leg.margin + margPx.left + (leg.pos == 'left' ? 0 : drawingSpace.x + axesSpace.y),
    y: margPx.top, // this is soomehow getting calculated incorectly
    w: leg.x,
    h: drawingSpace.y
  },

  yAxisRect = {
    x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2*leg.margin : 0),
    y: margPx.top,
    w: axesSpace.y,
    h: drawingSpace.y
  },

  plotRect = {
    x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2*leg.margin : 0),
    y: margPx.top,
    w: drawingSpace.x,
    h: drawingSpace.y
  },

  xAxisRect = {
    x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2*leg.margin : 0),
    y: margPx.top + drawingSpace.y,
    w: drawingSpace.x,
    h: axesSpace.x
  }



  container = safeSelect(selection, 'svg', namespace)
    .style('width', svgSpace.w+'px')
    .style('height', svgSpace.h+'px')

  var axesC = safeSelect(container, 'g', hypenate(namespace, 'axes'))

  var legC = safeSelect(container, 'g', hypenate(namespace, 'legend'))
  .attr('transform', "translate("+legRect.x+","+legRect.y+")")

  var plot = safeSelect(container, 'g', hypenate(namespace, 'plot'))
    .attr('transform', "translate("+plotRect.x+","+plotRect.y+")")

  var xAxis = safeSelect(axesC, 'g', hypenate(namespace, 'x-axis'))
    .attr('transform', "translate("+xAxisRect.x+","+xAxisRect.y+")")

  var yAxis = safeSelect(axesC, 'g', hypenate(namespace, 'y-axis'))
    .attr('transform', "translate("+yAxisRect.x+","+yAxisRect.y+")")

  return {
    svg: {
      selection: container,
      rect: svgSpace
    },
    plot: {
      selection: plot,
      rect: plotRect
    },
    xAxis: {
      selection: xAxis,
      rect: xAxisRect
    },
    yAxis: {
      selection: yAxis,
      rect: yAxisRect
    },
    legend: {
      selection: legC,
      rect: legRect
    }
  }
}
