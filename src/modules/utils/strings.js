import * as d3 from 'd3'
/**
* Hypenates all strings together
* @param {string[]} arguments
* @returns {string} "arg1-arg2-...-argn"
*/
export function hypenate(){ return Array.prototype.slice.call(arguments).join('-') }


/**
* Trys to reduce text to fit in specified area, made for tick labels as called by
* @see{@link axis}
* @param {d3.selection} t container for specific axis tick
* @param {string} text to be the label of the passed axis tick
* @param {boolean} orient of the axis, true is horizontal, false is vertical
* @param {number} tickLength is the length of the text
* @param {number} space is the amount of availble space for the text and the tick to fit in
* @param {boolean} overflowQ whether or not allowed to go over the alloted space
* @returns {none}
*/
export function truncateText(t, text, orient, tickLength, space, overflowQ) {
  var rect = t.node().getBoundingClientRect()
  t.text(text)
  while (Math.max(rect.width, rect.height) > space - tickLength) {
    text = String(text)
    text = text.slice(0, text.length - 1)
    t.text(text + '...')
    rect = t.node().getBoundingClientRect()
    if (text.length == 0) break
  }
}


export function truncateString(string, space, font) {
  var chars = space / font;
  if (chars < string.length) {
    return string.slice(0, Math.round(chars-5)) + '...'
  } else {
    return string
  }
}
