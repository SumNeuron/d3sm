import * as d3 from 'd3'
import {total} from './array.js'
/**
* Rounds decimals of number to precision
* @param {number} number
* @param {number} precision
* @returns {number} rounded to precision
*/
export function round(number, precision) {
  var shift = function (number, precision, reverseShift) {
    if (reverseShift) {
      precision = -precision;
    }
    var numArray = ('' + number).split('e');
    return +(numArray[0] + 'e' + (numArray[1] ? (+numArray[1] + precision) : precision));
  };
  return shift(Math.round(shift(number, precision, false)), precision, true);
}

/**
* evenly partitions the range [min, max] into n parts
* @param {number} min
* @param {number} max
* @param {number} n
* @returns {number[]} array of length n evenly partitioned between min and max
*/
export function tickRange(min, max, n) {
  var a = [min]
  var d = max-min
  var s = d / (n-1)
  for (var i = 0; i < n-2; i++) { a.push(min + s * (i+1)) }
  a.push(max)
  return a
}

/**
* @deprecated @see{@link tickRange}
* @param {number} min
* @param {number} max
* @param {number} parts
* @returns {number[]} array of length parts evenly partitioned between min and max
*/
export function partitionRangeInto(min, max, parts) {
  var diff = max - min
  return Array(parts).map(function (e, i) { return min + diff / parts * i })
}


export function euclideanDistance(p1, p2){
  var a =  p1[0] - p2[0], b =  p1[1] - p2[1]
  return Math.sqrt(a*a + b*b)
}

/**
* Calculated the quartiles of the passed data and stores them with qKeys
* @param {number[]} data list of numerical values
* @param {string[]} [qKeys=['q0', 'q1', 'q2', 'q3', 'q4']] how returned object with quartiles should be stored
* @returns {Object} with keys qKeys giving only the numerical values for the quartiles
*/
export function quartiles(data, qKeys) {
  var
  q2 = d3.median(data),
  lower = data.filter(x => x < q2),
  upper = data.filter(x => x > q2),

  q1 = d3.median(lower),
  q1 = q1 == undefined ? q2 : q1,

  q0 = d3.min(lower),
  q0 = q0 == undefined ? q1 : q0,

  q3 = d3.median(upper),
  q3 = q3 == undefined ? q2 : q3,

  q4 = d3.max(upper),
  q4 = q4 == undefined ? q3 : q4,

  k0 = 'q0', k1 = 'q1', k2 = 'q2', k3 = 'q3', k4 = 'q4',
  obj = {}
  if (qKeys!=undefined && qKeys.length == 5) { k0 = qKeys[0]; k1 = qKeys[1]; k2 = qKeys[2]; k3 = qKeys[3]; k4 = qKeys[4]; }
  obj[k0] = q0; obj[k1] = q1; obj[k2] = q2; obj[k3] = q3; obj[k4] = q4;

  return obj
}


/**
* determines the width of an object for the calling plotting function
* @param {number} freeSpace how much space is avalible
* @param {number} numberOfObjects how many object do we need
* @param {number} minObjectWidth how small are these objects allowed to be
* @param {number} maxObjectWidth how large are these object allowed to be
* @param {number} sizeOfSpacer percent of freeSpace that a single spacer should take up (need numberOfObjects - 1 spacers)
* @param {boolean} overflowQ can we go beyond alloted space
* @returns {number} how large object should be
* function tries to keep object within min / max width, but wil default to
* 5e-10 (smallest consistenly visible by svg size of element) if overflowQ is false
*/
export function calculateWidthOfObject(freeSpace, numberOfObjects, minObjectWidth, maxObjectWidth, sizeOfSpacer, overflowQ) {
  var sizeOfSpacer =
  sizeOfSpacer == 0 || sizeOfSpacer > 1
  ? sizeOfSpacer
  : freeSpace * sizeOfSpacer

  var numberOfSpacers = numberOfObjects - 1
  var spaceTakenBySpacers = numberOfSpacers * sizeOfSpacer
  var remainingSpace = freeSpace - spaceTakenBySpacers
  remainingSpace = remainingSpace < 0 ? 0 : remainingSpace
  var objectWidth = remainingSpace / numberOfObjects

  if ( overflowQ && minObjectWidth != undefined && objectWidth < minObjectWidth ) { objectWidth = minObjectWidth }
  // if ( maxObjectWidth != undefined && objectWidth > maxObjectWidth ) { objectWidth = maxObjectWidth }
  if ( overflowQ && maxObjectWidth != undefined && objectWidth < maxObjectWidth ) { objectWidth = maxObjectWidth }
  return Math.max(objectWidth, 5e-10)
}

/**
* @param {Array[]} data list data (can be nested). If nested will create more complex spacer size
* @param {number} freeSpace how much space is avalible
* @param {number} objectWidth @see{@link calculateWidthOfObject}
* @param {number} numberOfObjects how many object do we need
* @param {number} baseSpacerSize percent of freeSpace that a single spacer should take up (need numberOfObjects - 1 spacers)
* @param {boolean} overflowQ can we go beyond alloted space
* @returns {number} returns size that spacer should be at level=0
*/
export function calculateWidthOfSpacer(data, freeSpace, objectWidth, numberOfObjects, baseSpacerSize, overflowQ) {
  if (overflowQ) {
    // var limitedNumberOfObjects = numberOfObjects > 6 ? 6 : numberOfObjects
    // var spaceLeft = freeSpace - limitedNumberOfObjects * objectWidth
    // return spaceLeft / (limitedNumberOfObjects - 1)
    return freeSpace * baseSpacerSize
  }
  var spacersAtEachLevel = spacersNeededAtEachLevel(data)
  var totalSpacerPercent = total(spacersAtEachLevel.map(function(e, i) {return e * 1 / (i+1)}))
  var baseSpacerSize = (freeSpace - (objectWidth * numberOfObjects)) / totalSpacerPercent
  // console.log(freeSpace, objectWidth, numberOfObjects, totalSpacerPercent)
  // console.log(totalSpacerPercent, baseSpacerSize, totalSpacerPercent * baseSpacerSize)
  return isNaN(baseSpacerSize) ? 0 : baseSpacerSize
}

/**
* Calculates number of spacers needed to seperate elements at each level.
* @param {Array[]} array list data (can be nested). If nested will create more complex spacer size
* @param {number} [level=0] current level, used in recusrion
* @param {Array} [levelData=[]] how many spacers needed at a given level
* @returns {Array} levelData
*
* @example
* array = [[1,2], [3,4]]
* // returns [1, 2]
* as at level=0 the only spacer needed is between [1,2] and [3,4]
* and at level=1 the only two spacers needed is between 1 and 2 as well as
* 3 and 4 since the spacer between 2 and 3 is handled at level=0
*/
export function spacersNeededAtEachLevel (array, level, levelData ) {
  if ( level == undefined ) { level = 0;  } else { level += 1 }
  if ( levelData == undefined ) { levelData = []; }
  if ( level >= levelData.length ) { levelData.push(array.length - 1) }
  else { levelData[level] += array.length - 1 }
  array.map(function(e, i) { if (Array.isArray(e)) { spacersNeededAtEachLevel(e, level, levelData) } } )
  return levelData
}
