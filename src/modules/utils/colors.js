import * as d3 from 'd3'
/**
* Modifies luminance of hexidecimal number
* @param {string} hex should be hexidecimal value with or without the proceeding octotrope
* @param {number} lum value to increase or decrease luminosity by
* @returns {string} updated hexidecimal value without the proceeding octotrope
*/
export function modifyHexidecimalColorLuminance(hex, lum) {
  // validate hex string
  var hex = String(hex).replace(/[^0-9a-f]/gi, '');

  if (hex.length < 6) {
    hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	}
	lum = lum || 0;

	// convert to decimal and change luminosity
	var rgb = '#', c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i*2,2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ('00'+c).substr(c.length);
	}

	return rgb;
}

/**
* Maps arguments in to d3.interpolateRgbBasis
* @param arguments
* @returns {Function}
*/
export function interpolateColors(){return d3.interpolateRgbBasis(arguments)}
