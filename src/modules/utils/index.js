import * as arr from './array.js'
import * as color from './colors.js'
import * as math from './math.js'
import * as misc from './misc.js'
import * as sel from './selections.js'
import * as str from './strings.js'
import * as con from './console.js'
import * as paths from './paths.js'


let utils = {
  arr, color, math, misc, sel, str, con, paths
}

export {arr, color, math, misc, sel, str, con, paths}
export default utils
