/**
 * calls console.group if d3sm.debugQ == true
 * @param {string} name of the group
 * @returns {undefined}
 */
export function consoleGroup(name) {
  if (window.d3sm.debugQ === true){
    console.group(name)
  }
}

/**
 * calls console.groupEnd if d3sm.debugQ == true
 * @returns {undefined}
 */
export function consoleGroupEnd() {
  if (window.d3sm.debugQ === true){
    console.groupEnd()
  }
}

/**
 * Calls console.log if d3sm.debugQ == true
 * @param {string} func name of the function logging
 * @param {string} msg to log
 * @param {Object} data to be logged along side the message
 * @returns {undefined}
 */
export function log(func, msg, data) {
  if (window.d3sm.debugQ === true){
    console.log(
      `%c[d3sm::${func}]:\t${msg}`,
      [
        'background: #6cd1ef',
        'border-radius: 5000px',
        'padding: 0px 2px',
        'font-size: 14px'
      ].join(';')
    )
    console.table(data)
    // console.trace()
  }
}

/**
 * Calls console.warn if d3sm.debugQ == true
 * @param {string} func name of the function warning
 * @param {string} msg to display
 * @param {Object} data to be displayed along side the message
 * @returns {undefined}
 */
export function warn(func, msg, data) {
  if (window.d3sm.debugQ === true)
    console.warn(
      `%c[d3sm::${func}]:\t${msg}`,
      [
        'background: #ffd53e',
        'border-radius: 5000px',
        'padding: 0px 2px',
        'font-size: 14px'
      ].join(';')
    )
    console.table(data)
}
/**
 * Calls the console.info if d3sm.debugQ == true
 * @param {string} func name of the function providing info
 * @param {string} msg to display
 * @param {Object} data to be displayed along side the message
 * @returns {undefined}
 */
export function info(func, msg, data) {
  if (window.d3sm.debugQ)
    console.info(
      `%c[d3sm::${func}]:\t${msg}`,
      [
        'background: #009ccd',
        'border-radius: 5000px',
        'padding: 0px 2px',
        'font-size: 14px'
      ].join(';')
    )
    console.table(data)
}


/**
 * Calls console.error if d3sm.debugQ == true
 * @param {string} func name of the function which sends the error
 * @param {string} msg to display
 * @param {Object} data to be displayed along side the message
 * @returns {undefined}
 */
export function error(func, msg, data) {
  if (window.d3sm.debugQ)
    console.error(`[d3sm::${func}]:\t${msg}\t%o`,data)
}
