import * as d3 from 'd3'
import {hypenate} from './strings.js'
/**
* Extracts x and y of translate from transform property
* @param {string} transform transform property of svg element
* @returns {number[]} x, y of translate(x, y)
*/
export function getTranslation(transform) {
  // Create a dummy g for calculation purposes only. This will never
  // be appended to the DOM and will be discarded once this function
  // returns.
  var g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
  // Set the transform attribute to the provided string value.
  transform = transform == undefined ? 'translate(0,0)' : transform;
  g.setAttributeNS(null, 'transform', transform);
  // consolidate the SVGTransformList containing all transformations
  // to a single SVGTransform of type SVG_TRANSFORM_MATRIX and get
  // its SVGMatrix.
  var matrix = g.transform.baseVal.consolidate().matrix;
  // As per definition values e and f are the ones for the translation.
  return [matrix.e, matrix.f];
}

/**
* recursively ascends element.parentElement to find a svg tag
* @param {Element} element
* @returns {Element | undefined}
*/
export function getContainingSVG(element) {
  var parent = element.parentElement
  // if (parent == null || parent == undefined) {
  //   parent = element.node().parentElement
  // }
  var tag = parent.tagName.toLowerCase()
  if (tag === 'svg') { return parent; }
  if (tag === 'html') { return undefined; }
  return getContainingSVG(parent);
}


/**
* Trys to use d3.selection to get element, if it doesnt exist, makes one
* @param {d3.selection} sel selection in which to try and find object
* @param {string} tag tag of which to try and select
* @param {string} [cls=''] class of tag to try and grab
* @returns {d3.selection} of either append or selected tag.cls within sel
*/
export function safeSelect(sel, tag, cls) {
  var clsStr = cls == undefined ? '' : '.'+cls;
  var sSel = sel.select(tag+clsStr).empty()
  ? sel.append(tag)
  : sel.select(tag+clsStr)
  return sSel
  .classed(clsStr.replace('.', ''), true)
  .attr('transform', sSel.attr('transform') == undefined ? 'translate(0,0)' : sSel.attr('transform'))
}



/**
* Adds a clip-path rect and binds it to container
* @param {d3.selection} container in which to add the clip-path and to which to bind the cliping path to
* @param {Object} rect the coordinates (x, y, width, height) of the clip-path
* @param {string} namespace
* @returns {d3.selection} of the clip-path rect
*/
export function cpRect(container, rect, namespace) {
  var defs = safeSelect(container, 'defs', hypenate(namespace, 'definitions'))
  var cp = safeSelect(defs, 'clipPath', hypenate(namespace, 'clip-path'))
  .attr('id', hypenate(namespace, 'clip-path'))

  var cpRect = safeSelect(cp, 'rect')
  .attr('x', rect.x)
  .attr('y', rect.y)
  .attr('width', rect.width)
  .attr('height', rect.height)

  defs.raise()
  // set clipping path to container
  container.attr('clip-path', 'url(#'+ hypenate(namespace, 'clip-path')+')')

  return cpRect
}


/**
* Adds a background rect t to container
* @param {d3.selection} container in which to add the background rectangle
* @param {Object} rect the coordinates (x, y, width, height) of the background
* @param {string} fill the color of the background
* @returns {d3.selection} of the background fill
*/
export function bgRect(container, rect, fill) {
  return safeSelect(container, 'rect', 'bg')
  .attr('x', rect.x)
  .attr('y', rect.y)
  .attr('width', rect.width)
  .attr('height', rect.height)
  .attr('fill', fill)
}


/**
* Sets up the container for making chart elements. This includes making
* a clip-path rect bound to the passed container, a background rect, and
* a g element with class <namespace>-object-container.
* @param {d3.selection} container in which to add the clip-path and background
* @param {string} namespace
* @param {Object} rect the coordinates (x, y, width, height) of the background and clip-path
* @param {string} fill the color of the background
* @returns {d3.selection} of g.<namespace>-object-container
*
* @see{@link bgRect}
* @see{@link cpRect}
*/
export function setupContainer(selection, namespace, rect, fill) {
  // the container for three main items, bg, defs, and object-container
  var
  container = safeSelect(selection, 'g', namespace),
  bg = bgRect(container, rect, fill),
  cp = cpRect(container, rect, namespace),
  objectContainer = safeSelect(container, 'g', hypenate(namespace, 'object-container'))
  return objectContainer
}



export function absolutePosition(selection) {
    var element = selection.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = getContainingSVG(element)
    var svgPosition = containerSVG.getBoundingClientRect();

    return {
        top:    elementPosition.top    - svgPosition.top,
        left:   elementPosition.left   - svgPosition.left,
        bottom: elementPosition.bottom - svgPosition.top,
        right:  elementPosition.right  - svgPosition.left,
        height: elementPosition.height,
        width:  elementPosition.width
    };

}
