import * as d3 from 'd3'
import utils from '../utils/index.js'
import CF from '../color-function';
import groupingSpacer from '../grouping-spacer';


export default function categoricLegend( selection ) {
  var
  categories,

  /**
  * Data to plot. Assumed to be a object, where each key corresponds to a legend
  * (see {@link legend#data})
  * @param {Object} [data=undefined]
  * @memberof legend#
  * @property
  */
  data,
  /**
  * Which direction to render the bars in
  * (see {@link legend#orient})
  * @param {number} [orient='horizontal']
  * @memberof legend#
  * @property
  */
  orient='horizontal',
  /**
  * Amount of horizontal space (in pixels) avaible to render the legend in
  * (see {@link legend#spaceX})
  * @param {number} [spaceX=undefined]
  * @memberof legend#
  * @property
  */
  spaceX,
  /**
  * Amount of vertical space (in pixels) avaible to render the legend in
  * (see {@link legend.spaceY})
  * @param {number} [spaceY=undefined]
  * @memberof legend#
  * @property
  */
  spaceY,

  /**
  * Whether or not to allow legend to render elements pass the main spatial dimension
  * given the orientation (see {@link legend#orient}), where {@link legend#orient}="horizontal"
  * the main dimension is {@link legend#spaceX} and where {@link legend#orient}="vertical"
  * the main dimension is {@link legend#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof legend#
  * @property
  */
  overflowQ = false,

  /**
  * An array - putatively of other arrays - depicting how bars should be arranged
  * @param {Array[]} [grouping=undefined]
  * @memberof legend#
  * @property
  */
  grouping,

  /**
  * How to get the value of the legend
  * @param {function} [valueExtractor=function(key, index) { return data[key] }]
  * @memberof legend#
  * @property
  */
  valueExtractor = function(key, index) { return data[key] },
  /**
  * How to sort the bars - if {@link bar#grouping} is not provided.
  * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
  * @memberof bar#
  * @property
  */
  sortingFunction = function(keyA, keyB) {return d3.ascending(keyA, keyB)},
  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link legend#orient}), where {@link legend#orient}="horizontal"
  * the main dimension is {@link legend#spaceX} and where {@link legend#orient}="vertical"
  * the main dimension is {@link legend#spaceY} between bars
  * @param {number} [objectSpacer=0.05]
  * @memberof legend#
  * @property
  */
  objectSpacer = 0.05,
  /**
  * The minimum size that an object can be
  * @param {number} [minObjectSize=50]
  * @memberof legend#
  * @property
  */
  minObjectSize = 10,
  /**
  * The maximum size that an object can be
  * @param {number} [maxObjectSize=100]
  * @memberof legend#
  * @property
  */
  maxObjectSize = 100,

  /**
  * The stroke width of the bars
  * @param {number} [barStrokeWidth=2]
  * @memberof legend#
  * @property
  */
  bubbleStrokeWidth = 2,
  /**
  * Instance of ColorFunction
  * @param {function} [colorFunction = colorFunction()]
  * @memberof legend#
  * @property
  */
  colorFunction = CF(),

  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof legend#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of legend
  * @param {string} [namespace="d3sm-legend"]
  * @memberof legend#
  * @property
  */
  namespace = 'd3sm-legend',
  /**
  * Class name for legend container (<g> element)
  * @param {string} [objectClass="legend"]
  * @memberof legend#
  * @property
  */
  objectClass = 'legend',

  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof legend#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof legend#
  * @property
  */
  easeFunc = d3.easeExp


  legend.categories = function(_) { return arguments.length ? (categories=_, legend) : categories };

  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {legend | d3.selection}
   * @memberof legend
   * @property
   * by default selection = selection
   */
  legend.selection = function(_) { return arguments.length ? (selection = _, legend) : selection; };
  /**
   * Gets or sets the data
   * (see {@link legend#data})
   * @param {number} [_=none]
   * @returns {legend | object}
   * @memberof legend
   * @property
   */
  legend.data = function(_) { return arguments.length ? (data = _, legend) : data; };
  /**
   * Gets or sets the orient of the bars
   * (see {@link legend#orient})
   * @param {number} [_=none]
   * @returns {legend | object}
   * @memberof legend
   * @property
   */
  legend.orient = function(_) { return arguments.length ? (orient = _, legend) : orient; };
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link legend#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default spaceX = undefined
   */
  legend.spaceX = function(_) { return arguments.length ? (spaceX = _, legend) : spaceX; };
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link legend#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default spaceY = undefined
   */
  legend.spaceY = function(_) { return arguments.length ? (spaceY = _, legend) : spaceY; };

  /**
   * Gets / sets whether or not legend is allowed to go beyond specified dimensions
   * (see {@link legend#spaceX})
   * @param {boolean} [_=none]
   * @returns {legend | boolean}
   * @memberof legend
   * @property
   * by default overflowQ = false
   */
  legend.overflowQ = function(_) { return arguments.length ? (overflowQ = _, legend) : overflowQ; };
  /**
   * Gets / sets the grouping of the bars
   * (see {@link legend#grouping})
   * @param {Array[]} [_=none]
   * @returns {legend | Array[]}
   * @memberof legend
   * @property
   * by default grouping = undefined
   */
  legend.grouping = function(_) { return arguments.length ? (grouping = _, legend) : grouping; };
  /**
   * Gets / sets the valueExtractor
   * (see {@link legend#valueExtractor})
   * @param {function} [_=none]
   * @returns {legend | function}
   * @memberof legend
   * @property
   * by default valueExtractor = function(key, index) { return data[key] },
   */
  legend.valueExtractor = function(_) { return arguments.length ? (valueExtractor = _, legend) : valueExtractor; };
  /**
   * Gets / sets the sortingFunction
   * (see {@link bar#sortingFunction})
   * @param {function} [_=none]
   * @returns {bar | function}
   * @memberof bar
   * @property
   * by default sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},
   */
  legend.sortingFunction = function(_) { return arguments.length ? (sortingFunction = _, legend) : sortingFunction; };
  /**
   * Gets / sets objectSpacer
   * (see {@link legend#objectSpacer})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default objectSpacer = 0.05
   */
  legend.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, legend) : objectSpacer; };
  /**
   * Gets / sets the minObjectSize
   * (see {@link legend#minObjectSize})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default minObjectSize = 50
   */
  legend.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, legend) : minObjectSize; };
  /**
   * Gets / sets the maxObjectSize
   * (see {@link legend#maxObjectSize})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default maxObjectSize = 100
   */
  legend.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, legend) : maxObjectSize; };

  /**
   * Gets / sets the barStrokeWidth
   * (see {@link legend#barStrokeWidth})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default barStrokeWidth = 2
   */
  legend.bubbleStrokeWidth = function(_) { return arguments.length ? (bubbleStrokeWidth = _, legend) : bubbleStrokeWidth; };
  /**
   * Gets / sets the colorFunction
   * (see {@link legend#colorFunction})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default colorFunction = colorFunction()
   */
  legend.colorFunction = function(_) { return arguments.length ? (colorFunction = _, legend) : colorFunction; };

  /**
   * Gets / sets the backgroundFill
   * (see {@link legend#backgroundFill})
   * @param {string} [_=none]
   * @returns {legend | string}
   * @memberof legend
   * @property
   * by default backgroundFill = 'transparent'
   */
  legend.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, legend) : backgroundFill; };
  /**
   * Gets / sets the namespace
   * (see {@link legend#namespace})
   * @param {string} [_=none]
   * @returns {legend | string}
   * @memberof legend
   * @property
   * by default namespace = 'd3sm-legend'
   */
  legend.namespace = function(_) { return arguments.length ? (namespace = _, legend) : namespace; };
  /**
   * Gets / sets the objectClass
   * (see {@link legend#objectClass})
   * @param {string} [_=none]
   * @returns {legend | string}
   * @memberof legend
   * @property
   * by default objectClass = 'tick-group'
   */
  legend.objectClass = function(_) { return arguments.length ? (objectClass = _, legend) : objectClass; };
  /**
   * Gets / sets the transitionDuration
   * (see {@link legend#transitionDuration})
   * @param {number} [_=none]
   * @returns {legend | number}
   * @memberof legend
   * @property
   * by default transitionDuration = 1000
   */
  legend.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, legend) : transitionDuration; };
  /**
   * Gets / sets the easeFunc
   * (see {@link legend#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {legend | d3.ease}
   * @memberof legend
   * @property
   * by default easeFunc = d3.easeExp
   */
  legend.easeFunc = function(_) { return arguments.length ? (easeFunc = _, legend) : easeFunc; };


  function legend() {
    var horizontalQ = (orient == 'horizontal') ? true : false
    var verticalQ = !horizontalQ
    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );


    colorFunction.dataExtent([0, categories.length - 1])
    .colorBy('categories')
    .categoryExtractor(function(k, v, i){return v})

    var r = Math.min(spaceX, spaceY) / 2
    var numberOfObjects = categories.length

    // if grouping is undefined sort barKeys by sortingFunction
    var ordered = (grouping == undefined) ? categories.sort(sortingFunction) : grouping
    // ordered might be nested depending on grouping
    var catKeys = utils.arr.flatten(ordered)

    var space = horizontalQ ? spaceX : spaceY
    // calculate object size
    var objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    // calculate spacer size if needed
    var spacerSize = utils.math.calculateWidthOfSpacer(catKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ)
    // make the nested groups
    var spacerFunction = groupingSpacer()
    .horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects)
    .objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace(namespace)

    spacerFunction(container, ordered, 0)
    var r = Math.min(objectSize, spaceX, spaceY) / 2 - bubbleStrokeWidth

    container.selectAll('g:not(.to-remove).'+objectClass).each(function(cat, i) {
      var t = d3.select(this)
      var c = utils.sel.safeSelect(t, 'circle')
      var fillColor = colorFunction(undefined, cat, i, 'fill'), // prevent duplicate computation
      strokeColor = colorFunction(undefined, cat, i,  'stroke')

      var cx = horizontalQ
        ? r+bubbleStrokeWidth
        : (spaceX - r*2) / 2 + r
      var cy = verticalQ
        ? r+bubbleStrokeWidth
        : (spaceX - r*2) / 2 + r

      c.attr("r", r)
      .attr('cx', cx)
      .attr('cy', cy)
      .attr('fill', fillColor)
      .attr('stroke', strokeColor)
      .attr('stroke-width', bubbleStrokeWidth)

      var text = utils.sel.safeSelect(t, 'text')
      text.text(cat)
      .attr('text-anchor', 'middle')
      .attr("transform", function(d, i){
        var
        x = cx,
        y = cy + text.node().getBoundingClientRect().height / 4,
        t = 'translate('+x+','+y+')'
        return t
      })

    })


  }

  return legend
}
