import * as d3 from 'd3'
import utils from '../utils/index.js'
import CF from '../color-function';

export default function numericLegend( selection ) {

  var
  min=0,
  max=1,
  spaceX,
  spaceY,
  colorFunction = CF(),
  namespace='d3sm-linear-vertical-gradient',
  fontSize = 12,
  backgroundFill = 'transparent',
  textColor = 'black',
  roundTo = 2


  legend.min = function(_) { return arguments.length ? (min=_, legend) : min }
  legend.max = function(_) { return arguments.length ? (max=_, legend) : max }
  legend.spaceX = function(_) { return arguments.length ? (spaceX=_, legend) : spaceX }
  legend.spaceY = function(_) { return arguments.length ? (spaceY=_, legend) : spaceY }
  legend.namespace = function(_) { return arguments.length ? (namespace=_, legend) : namespace }
  legend.fontSize = function(_) { return arguments.length ? (fontSize=_, legend) : fontSize }
  legend.backgroundFill = function(_) { return arguments.length ? (backgroundFill=_, legend) : backgroundFill }
  legend.colorFunction = function(_) { return arguments.length ? (colorFunction=_, legend) : colorFunction }
  legend.textColor = function(_) { return arguments.length ? (textColor=_, legend) : textColor }
  legend.roundTo = function(_) { return arguments.length ? (roundTo=_, legend) : roundTo }

  function legend() {
    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    var defs = utils.sel.safeSelect(selection, 'defs')
    var linearGradient = utils.sel.safeSelect(defs, 'linearGradient')
    .attr("x1", "0%")
    .attr("y1", "100%")
    .attr("x2", "0%")
    .attr("y2", "0%")
    .attr('id', utils.str.hypenate(namespace,'numerical-legend-gradient'))


    colorFunction.dataExtent([min, max])
    .colorBy('value')
    .valueExtractor(function(k, v, i){return v})


    linearGradient.selectAll('stop')
    .data( colorFunction.colors() )
    .enter()
    .append('stop')
    .attr("offset", function(d, i){ return i / (colorFunction.colors().length - 1) })
    .attr('stop-color', function(d) {return d})




    var rect = utils.sel.safeSelect(container, 'rect', 'legend')
    .attr('transform', 'translate(0,'+fontSize+')')
    .style("fill", "url(#"+utils.str.hypenate(namespace,'numerical-legend-gradient')+")")
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', spaceX)
    .attr('height', spaceY - fontSize*2)
    .on('mousemove', function(d, i){legendMousemove(d, i, rect, d3.select(this))})
    .on('mouseout', function(d, i){ d3.select("#"+utils.str.hypenate(namespace,'legend-tooltip')).remove() })

    var minText = utils.sel.safeSelect(container, 'text', 'min')
    .text(utils.math.round(min, 2))
    .attr('text-anchor', 'middle')
    .attr("font-size", fontSize+'px')
    .attr('transform', function(d, i){
      var
      x = spaceX / 2,
      y = spaceY - fontSize / 4,
      t = 'translate('+x+','+y+')'
      return t
    })

    var maxText = utils.sel.safeSelect(container, 'text', 'max')
    .text(utils.math.round(max, 2))
    .attr('text-anchor', 'middle')
    .attr("font-size", fontSize+'px')
    .attr('transform', function(d, i){
      var
      x = spaceX / 2,
      y = fontSize,
      t = 'translate('+x+','+y+')'
      return t
    })




  }

  function legendMousemove(d, i, rect, t) {
    var s = d3.scaleLinear()
    .domain([0, rect.attr('height')])
    .range([max, min])
    var m = d3.mouse(rect.node())
    var v = utils.math.round(s(m[1]),roundTo)

    var strokeColor = colorFunction(undefined, v, undefined, 'stroke')
    var fillColor = colorFunction(undefined, v, undefined, 'fill')

    var div = utils.sel.safeSelect(d3.select('body'), 'div', utils.str.hypenate(namespace,'legend-tooltip'))
    .attr('id', utils.str.hypenate(namespace,'legend-tooltip'))
    .style('position', 'absolute')
    .style('left', (d3.event.pageX+15)+'px')
    .style('top', (d3.event.pageY+15)+'px')
    .style('background-color', fillColor)
    .style('border-color', strokeColor)

    .style('min-width', (fontSize * (String(max).split('.')[0].length+3))+'px')
    .style('min-height', (fontSize * (String(max).split('.')[0].length+3))+'px')
    .style('border-radius', '50%')
    .style('border-radius', '5000px')

    .style('display', 'flex')
    .style('justify-content', 'center')
    .style('text-align', 'middle')
    .style('padding', 2+"px")

    .style('border-style', 'solid')
    .style('border-width', 2)

    var text = utils.sel.safeSelect(div, 'div')
    .text(v)
    .style('color', textColor)
    .style('align-self', 'center')
  }

  return legend
}
