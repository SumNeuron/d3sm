import categorical from './categorical-legend'
import numeric from './numeric-legend'

let legends = {
  categorical, numeric
}

export {categorical, numeric}
export default legends
