import utils from '../utils/index.js'
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';
import lasso from './lasso';
import '../d3-prototypes';

export default function lassoWidget( selection ) {
  var
  namespace = 'd3sm-lasso',
  selection = selection,
  svg,
  chartContainer,
  objectContainer,
  objectClass,
  lassoLine = d3.line()
  .x(function(d, i){return d[0]})
  .y(function(d, i){return d[1]})
  .curve(d3.curveLinearClosed),
  path,
  colorFunction = CF(),
  maxNumberOfGroups,
  dataExtractor,
  xScale,
  yScale

  function onError(msg, style){
    console.log(msg, style)
  }
  // setup the container
  setupDataselectContainer()

  lassoWidget.objectClass = function(_){return arguments.length ? (objectClass='.'+_.replace('.',''), lassoWidget) : objectClass}
  lassoWidget.svg = function(_){return arguments.length ? (svg=_, lassoWidget) : svg}
  lassoWidget.submit = function(_){return arguments.length ? (submit=_, lassoWidget) : submit}
  lassoWidget.maxNumberOfGroups = function(_){return arguments.length ? (maxNumberOfGroups=_, lassoWidget) : maxNumberOfGroups}
  lassoWidget.onError = function(_){return arguments.length ? (onError=_, lassoWidget) : onError}
  lassoWidget.objectContainer = function(_){return arguments.length ? (objectContainer=_, lassoWidget) : objectContainer}
  lassoWidget.chartContainer = function(_){return arguments.length ? (chartContainer=_, lassoWidget) : chartContainer}
  lassoWidget.dataExtractor = function(_){return arguments.length ? (dataExtractor=_, lassoWidget) : dataExtractor}
  lassoWidget.xScale = function(_){return arguments.length ? (xScale=_, lassoWidget) : xScale}
  lassoWidget.yScale = function(_){return arguments.length ? (yScale=_, lassoWidget) : yScale}

  function styles() {
    var s = d3.select("html").select("style."+namespace+'lasso-widget')
    if (s.empty()) {
      d3.select("html").append("style")
      .classed(namespace+'lasso-widget', true)
      .html(
        "."+utils.str.hypenate(namespace, "data-table") + "{\
          height:100px;\
          overflow:auto;\
        }"
      )
    }
  }


  function lassoWidget() {
    styles()

  }

  function submit(data) {
    console.log(data)
  }

  function plusTab(tabList) {
    var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'plus-tab'))
    .classed('ml-auto', true)
    .classed('nav-item', true)
    .on('click', makeNewGroup),

    anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
    icon = utils.sel.safeSelect(anchor, 'i', 'fa')
    .classed('fa-plus fa-2x text-success', true)
  }

  function sendTab(tabList) {
    var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'send-tab'))
    .classed('ml-auto', true)
    .classed('nav-item', true)
    .on('click', clickSend)
    ,

    anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
    icon = utils.sel.safeSelect(anchor, 'i', 'fa')
    .classed('fa-paper-plane-o fa-2x text-primary', true)
  }

  function clickSend() {
    var data = gatherDataLists()
    submit(data)
  }

  function closeTab(tabList) {
    var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'close-tab'))
    .classed('ml-auto', true)
    .classed('nav-item', true)
    .on('click', function(){
      var m = d3.mouse(d3.select("html").node())
      selection.select('div.card').style("position", 'relative')
      .transition().duration(1000)
      .ease(d3.easeBack)
      .style('left', window.outerWidth +'px')
      .remove()
    })
    ,

    anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
    icon = utils.sel.safeSelect(anchor, 'i', 'fa')
    .classed('fa-window-close-o fa-2x text-danger', true)
  }



  function setupDataselectContainer() {
    var
    card = utils.sel.safeSelect(selection, 'div', 'card'),
    cardHeader = utils.sel.safeSelect(card, 'div', 'card-header'),

    tabList = utils.sel.safeSelect(cardHeader, 'ul', 'nav')
    .classed('nav-tabs card-header-tabs', true)
    .classed(utils.str.hypenate(namespace, 'tab-list'), true)
    .attr('role', 'tablist'),

    cardBody = utils.sel.safeSelect(card, 'div', 'card-body'),
    tabContent = utils.sel.safeSelect(cardBody, 'div', 'tab-content')
    .classed(utils.str.hypenate(namespace, 'tab-content'), true),

    // leftTabs = utils.sel.safeSelect(tabList, 'div', 'nav mr-auto left-aligned-tabs')
    rightTabs = utils.sel.safeSelect(tabList, 'div', 'right-aligned-tabs').classed('nav ml-auto ', true)

    plusTab(rightTabs)
    sendTab(rightTabs)
    closeTab(rightTabs)
    var
    defaultTab = utils.sel.safeSelect(tabContent, 'div', 'tab-pane')
    .classed(utils.str.hypenate(namespace,'default-tab'), true)
    .classed("active", true)
    .classed('text-left', true),

    p = utils.sel.safeSelect(defaultTab, 'div')
    .html(
      "Click <kbd><i class='fa fa-plus text-success'></i></kbd> to add a new group.<br>"+
      "Click <kbd><i class='fa fa-paper-plane-o text-primary'></i></kbd> to submit for re-analysis.<br>"+
      "Click <kbd><i class='fa fa-window-close-o text-danger'></i></kbd> to close the dataselect widget."
    )
    // .text('Click the green plus to add a new group.')
  }


  function makeRemoveButton(paneBtnList) {
    var
    btn = utils.sel.safeSelect(paneBtnList, 'button', 'remove-btn')
    .classed('btn btn-danger', true)
    .on('click', removeBtnClickToRemove),
    i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-trash-o', true),
    s = utils.sel.safeSelect(btn, 'span').text('Remove group')
    return btn
  }

  function removeBtnClickToRemove(d, i) {
    var
    tab = selection.select('#'+utils.str.hypenate(namespace,'tab',d)),
    pane = selection.select('#'+utils.str.hypenate(namespace,'tab','pane',d))

    tab.remove()
    pane.remove()

    showRemainingPanes()
    updateTabAndPaneNumbers()
  }


  function togglePaneById(id) {
    var panes = selection.select('.'+utils.str.hypenate(namespace, 'tab-content'))
    panes.selectAll('div.tab-pane')
    .each(function(d, i){
      d3.select(this).classed('active show', d3.select(this).attr('id') == id)
    })
  }

  function insertTab(tabs, n) {

    // var li = tabs.insert("li", ':nth-child('+(n)+')')
    var li = tabs.insert("li", '.right-aligned-tabs')
    .classed('nav-item', true)
    .classed('alert', true)
    .classed('alert-secondary', true)
    .classed('alert-dismissable', true)
    .classed('fade', true)
    .classed('show', true)
    // .classed('mr-auto', true)
    .attr("role", 'alert')
    .attr("id", utils.str.hypenate(namespace,'tab',n))
    .classed(utils.str.hypenate(namespace,'tab'), true)

    var a = utils.sel.safeSelect(li, 'a')
    .attr('data-toggle', 'tab')
    .text('Group ' + n)
    .attr('href', '#'+utils.str.hypenate(namespace,'tab','pane',n))
    .on('dblclick', function(d, i){
      var t = d3.select(this)
      t.attr('contenteditable', true)
      d3.select(t.node().parentNode)
      .classed('alert-secondary', false)
      .classed('alert-warning', true)

      // d3.select("html").on("click", dispatchBlur)
      //
      // function dispatchBlur(d, i) {
      //   if (d3.event.target != d3.select(this)) {
      //     d3.select(this).dispatch("blur")
      //   }
      // }


    })
    .on('blur', function(d, i){

      var t = d3.select(this)
      t.attr('contenteditable', false)
      d3.select(t.node().parentNode)
      .classed('alert-secondary', true)
      .classed('alert-warning', false)

    })
    .on('input', function(d, i){
      var t = d3.select(this)
      var txt = t.text()
      d3.select(t.attr('href')).select("p.lead").text(txt)
    })


    return li
  }

  function makeTabPane(panes, n) {
    var pane = panes.append('div')
    .datum(n)
    .attr('role', 'tabpanel')
    .classed('tab-pane', true)
    .classed('text-left', true)
    .classed(utils.str.hypenate(namespace,'tab','pane'), true)
    .attr('id', utils.str.hypenate(namespace,'tab','pane',n))
    return pane
  }

  function populatePane(pane, n) {
    var
    lead = utils.sel.safeSelect(pane, 'p', 'lead').text('Group '+n),

    tableContainer = utils.sel.safeSelect(pane, 'div', 'table-responsive')
    .attr("class", utils.str.hypenate(namespace, "data-table")),

    table = utils.sel.safeSelect(tableContainer, "table", "table")
    .classed("table-sm", true).classed("table-hover", true),

    caption = utils.sel.safeSelect(table, "caption", "caption").html("List of selected"),

    btns = utils.sel.safeSelect(pane, 'div', 'text-right'),

    cN = n % colorFunction.colors().length
    if (n % 2 != 0) { cN = (colorFunction.colors().length - 1) - cN }





    var lassoBtn = makeLassoButton(btns)
    var currentLasso = makeLassoFunction(n, colorFunction.colors()[cN])
    var clearBtn = makeClearButton(btns)

    bindButtons(lassoBtn, clearBtn, currentLasso, table)

    var removeBtn = makeRemoveButton(btns)

    lead.on("mouseover", function(){
      currentLasso.draw()
    })
    lead.on("mouseout", function(){
      currentLasso.remove()
    })

  }

  function makeLassoButton(btns) {
    var btn = utils.sel.safeSelect(btns, 'button', 'lasso-btn')
    .classed('btn btn-info', true)
    .classed(namespace, true)
    // .datum([lasso])
    var i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-hand-pointer-o', true)
    var s = utils.sel.safeSelect(btn, 'span').text('Lasso select')
    return btn

  }

  function makeClearButton(btns) {
    var btn = utils.sel.safeSelect(btns, 'button', 'clear-btn')
    .classed('btn btn-light', true)
    .classed(namespace, true)
    var i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-eraser', true)
    var s = utils.sel.safeSelect(btn, 'span').text('Clear selection')
    return btn
  }

  function makeLassoFunction(instance, color) {
    var currentLasso = lasso()
    .namespace(namespace)
    .svg(svg)
    .objectClass(objectClass)
    .chartContainer(chartContainer)
    .objectContainer(objectContainer)
    .instance(instance)
    .color(color)
    .yScale(yScale)
    .xScale(xScale)

    return currentLasso
  }

  function bindButtons(lassoBtn, clearBtn, currentLasso, table) {
    currentLasso.eventCatcher(lassoBtn)
    lassoBtn.node().addEventListener("click", lassoBtnToggle)

    lassoBtn.datum([currentLasso])
    .on(utils.str.hypenate(namespace,'render'), function(){
        d3.select(this).datum()[0].draw()
    })
    .on(utils.str.hypenate(namespace,"drag"), function(las){
      var nodes = chartContainer.selectAll(".in-lasso-"+las[0].instance()).nodes()
      var tableData = nodes.map(function(d, i){
        var extracted = dataExtractor(d3.select(d).datum())
        extracted["__node"] = d
        return extracted
      })


      makeTable(table, tableData, currentLasso)
    })

    clearBtn.on('click', function(){
      currentLasso.allPoints([])
      currentLasso.currentPoints([])
      lassoBtn.dispatch(utils.str.hypenate(namespace,"drag"))

    })

  }

  function lassoBtnToggle() {
    var that = d3.select(this)
    var las = that.datum()[0]

    las.toggle()
    var activeQ = las.activeQ()

    if (las.activeQ()) {
      that.classed('btn-info', !activeQ)
      that.classed('btn-warning', activeQ)
      that.select("span").text("Lasso select (active)")
      selection.selectAll("."+namespace+".lasso-btn").dispatch(utils.str.hypenate(namespace,'render'))
      d3.select("html").node().addEventListener('mousedown', monitorLassoButtonState)
    } else {
      that.classed('btn-info', !activeQ)
      that.classed('btn-warning', activeQ)
      that.select("span").text("Lasso select")
      d3.select("html").node().removeEventListener('mousedown', monitorLassoButtonState)
    }

    function monitorLassoButtonState(event) {
      /*
      activeLasso stops event stopPropagation, so this event will not register in
      that case. Thus we only need to ensure that the usre is clicking on the lasso button
      otherwise, de-spawn lasso.
      */
      if (
        event.target != that.node() &&
        event.target != that.select("span").node() &&
        event.target != that.select("i").node()
      ) {
        // event.preventDefault()
        // event.stopPropagation()
        las.toggle(false)
        that.classed('btn-info', true)
        that.classed('btn-warning', false)
        that.select("span").text("Lasso select")
        // console.log(that, that.node())
        // that.dispatch("focusout")
      }
    }

  }


  function updateTableHeaderColumns(headR, tableData) {
    var headerKeys = d3.keys(tableData[0]).filter(k=>k!="__node")
    if (headerKeys.length > 0) {
      // headerKeys = ["remove"].concat(headerKeys)
      headerKeys.push("remove")
    }

    var headerCols = headR.selectAll("th")

    headerCols = headerCols.data(headerKeys)
    headerCols.exit().remove()
    headerCols = headerCols.merge(headerCols.enter().append("th").attr("scope","col"))
    .text(function(d, i){return d})

    return headerKeys
  }

  function updateTableRows(body, tableData) {
    var bodyRows = body.selectAll("tr")

    bodyRows = bodyRows.data(tableData)
    bodyRows.exit().remove()
    bodyRows = bodyRows.merge(bodyRows.enter().append("tr"))

    return bodyRows
  }

  function updateTableRowColumns(cols, headerKeys, rowData, tableData, lasso, table) {
    cols = cols.data(headerKeys)
    cols.exit().remove()
    cols = cols.merge(cols.enter().append("td"))

    cols.html(function(d, i){
      if (d != "remove") { return rowData[d] }
      return "<i class='fa fa-close'></i>"
    }).classed("text-left", function(d, i){
      if (d != "remove") { return false }
      return true
    })
    // .attr("scope",function(d, i){
    //   if (d != "remove") { return false }
    //   return true
    // })

    cols.select("i.fa-close").on("click", function(d, i){
      var node = rowData["__node"],
      n = d3.select(node)
      n.classed("in-lasso", false)
      n.classed("in-lasso-"+lasso.instance(), false)

      tableData.map(function(dd, j){
        if (dd["__node"] == node) {
          tableData.splice(j, 1)
          makeTable(table, tableData, lasso)
        }
      })

    })
  }

  function makeTable(table, tableData, lasso) {
    var
    head = utils.sel.safeSelect(table, "thead"),
    headR = utils.sel.safeSelect(head, "tr"),
    body = utils.sel.safeSelect(table, "tbody"),

    headerKeys = updateTableHeaderColumns(headR, tableData),

    bodyRows = updateTableRows(body, tableData)

    bodyRows.each(function(rowData, i){
      var t = d3.select(this)
      var cols = t.selectAll("td")

      updateTableRowColumns(cols, headerKeys, rowData, tableData, lasso, table)

      t.on("mouseover", function(d, i) {
        lasso.applyObjectAttributes(d3.select(d["__node"]),true)
      })
      .on("mouseout", function(d, i) {
        lasso.applyObjectAttributes(d3.select(d["__node"]),false)
      })
    })


  }







  function makeNewGroup(d, i) {

    var tabs = selection.select('.'+utils.str.hypenate(namespace, 'tab-list')),
    panes = selection.select('.'+utils.str.hypenate(namespace, 'tab-content')),
    n = newGroupNumber()

    if (tabs.selectAll('.'+utils.str.hypenate(namespace,'tab')).size() == maxNumberOfGroups) {
      onError('only '+maxNumberOfGroups+' allowed.', 'warning')
      return
    }

    var
    tab = insertTab(tabs, n),
    pane = makeTabPane(panes, n)

    populatePane(pane, n)
    togglePaneById(pane.attr('id'))

  }

  function newGroupNumber() {
    var tabs = selection.select('.'+utils.str.hypenate(namespace, 'tab-list'))//,
    var
    n = tabs.selectAll('li').size() - 3, // minus 1 for plus tab
    s = 'Group ' + n,
    gs = []
    tabs.each(function(d, i){ return gs.push(d3.select(this).select("a").text()) })
    while (gs.includes(s)) { n+=1; s = 'Group ' + n; }
    return n
  }

  function updateTabAndPaneNumbers() {
    var
    tabs = selection.select('.'+utils.str.hypenate(namespace, 'tab-list')),
    panes = selection.select('.'+utils.str.hypenate(namespace, 'tab-content'))

    tabs = tabs.selectAll('.'+utils.str.hypenate(namespace,'tab'))
    panes = panes.selectAll('.'+utils.str.hypenate(namespace,'tab', 'pane'))

    tabs.each(function(d, i){
      d3.select(this).datum(i)
      .attr("id", utils.str.hypenate(namespace,'tab',i))
      .select('a')
      .attr('href', '#'+utils.str.hypenate(namespace,'tab','pane',i))
      .text(function(dd, ii){
        var curText = d3.select(this).text();
        if (curText.split(' ')[0] == 'Group') {
            return 'Group ' + i
        }
        return curText
      })
    })

    panes.each(function(d, i){
      d3.select(this).datum(i)
      .attr('id', utils.str.hypenate(namespace,'tab','pane',i))
      utils.sel.safeSelect(d3.select(this), 'p', 'lead')
      .text(function(dd, ii){
        var curText = d3.select(this).text();
        if (curText.split(' ')[0] == 'Group') {
            return 'Group ' + i
        }
        return curText
      })
      utils.sel.safeSelect(d3.select(this), 'button', 'remove-btn')
      .on('click', removeBtnClickToRemove)
    })

  }

  function gatherDataLists() {
    var tabs = selection.select('.'+utils.str.hypenate(namespace, 'tab-list'))
    var panes = selection.select('.'+utils.str.hypenate(namespace, 'tab-content'))
    var tables = panes.selectAll('.'+utils.str.hypenate(namespace, "data-table"))
    var data = {}

    var textGroups = tabs.selectAll('li.'+utils.str.hypenate(namespace,'tab') + ' > a')
    .nodes().map(function(d, i){return d3.select(d).text()})

    textGroups.map(function(e, i){
      data[e] = []
    })


    tables.each(function(d, i){
      var table = d3.select(this).select("tbody")
      data[textGroups[i]] = table.selectAll('tr').data()
    })

    return data
  }

  function showRemainingPanes() {
    var
    tabs = selection.select('.'+utils.str.hypenate(namespace, 'tab-list')),
    panes = selection.select('.'+utils.str.hypenate(namespace, 'tab-content')),
    remainingTabs = tabs.selectAll('.'+utils.str.hypenate(namespace,'tab'))

    if (remainingTabs.size() == 0) {
      panes.select('.'+utils.str.hypenate(namespace,'default-tab'))
      .classed("active", true)
      .classed('text-left', true)
    }
    else {
      var lastTab = remainingTabs.nodes()[remainingTabs.size()-1],
      lastPaneId = d3.select(lastTab).select('a').attr('href')
      panes.select(lastPaneId)
      .classed("active", true)
      .classed('text-left', true)
    }
  }


  return lassoWidget
}
