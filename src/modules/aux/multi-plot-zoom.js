import utils from '../utils/index.js'
/*******************************************************************************

**                                                                            **
**                                                                            **
**                                PLOTZOOM                                    **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Creates an plotZoom instance, which can handle drag and scroll events
 * @constructor plotZoom
 * @param {function} chart a function instance of one of the d3sm plots (e.g. bar, boxwhisker, bubbleHeatmap, violin, etc)
 * @param {axis}  xAxis the axis instance responsible for the x axis
 * @param {axis} yAxis the axis instance responsible for the y axis
 * @namespace plotZoom
 * @returns {function} zoom
 */
export default function multiPlotZoom( chart ) {
  var
  /**
  * The event on which to fire
  * (see {@link plotZoom#eventType})
  * @param {string} eventType which event it should handle. Currently supports scroll and wheel
  * @memberof plotZoom#
  * @property
  */
  eventType,
  /**
  * A scaling factor for the wheel "speed"
  * (see {@link plotZoom#wheelSpeed})
  * @param {number} [wheelSpeed=20] scales the wheel translation by wheelSpeed
  * @memberof plotZoom#
  * @property
  */
  wheelSpeed = 20,
  /**
  * The orientation in which to allow scrolling: 'horizontal', 'vertical', or '2D'
  * (see {@link plotZoom#orient})
  * @param {string} [orient=chart.orient() || 'horizontal']
  * @memberof plotZoom#
  * @property
  */
  orient = (chart.orient == undefined) ? 'horizontal' : chart.orient(),
  /**
  * The max distance allowed to scroll in the x direction
  * (see {@link plotZoom#xLock})
  * @param {number} [xLock=chart.spaceX()] ideally chart.overflowQ() == true and this value is the
  * bounding rect across all elements in the chart  minus the space in which to show.
  * @memberof plotZoom#
  * @property
  */
  xLock=chart.spaceX(),
  /**
  * The max distance allowed to scroll in the y direction
  * (see {@link plotZoom#yLock})
  * @param {number} [yLock=chart.spaceY()] ideally chart.overflowQ() == true and this value is the
  * bounding rect across all elements in the chart  minus the space in which to show.
  * @memberof plotZoom#
  * @property
  */
  yLock=chart.spaceY(),

  chartSel = chart.selection(),

  svg = d3.select(utils.sel.getContainingSVG(chartSel.node())),//.thisSVG()),

  xComponents = [],
  yComponents = []


  /**
   * Gets or sets the event type in which to respond
   * (see {@link plotZoom#eventType})
   * @param {string} [_=none] should be 'drag' or 'wheel'
   * @returns {zoom | string}
   * @memberof plotZoom
   * @property
   * by default plotZoom=undefined
   */
  zoom.eventType = function(_) { return arguments.length ? (eventType = _, zoom) : eventType; };
  /**
   * Gets or sets the wheel speed in which to scale the wheel scroll transform
   * (see {@link plotZoom#wheelSpeed})
   * @param {number} [_=none]
   * @returns {zoom | number}
   * @memberof plotZoom
   * @property
   * by default wheelSpeed=20
   */
  zoom.wheelSpeed = function(_) { return arguments.length ? (wheelSpeed = _, zoom) : wheelSpeed; };
  /**
   * Gets or sets the orientation in which items are manipulated
   * (see {@link plotZoom#orient})
   * @param {string} [_=none] should be horizontal, vertical, or 2D
   * @returns {zoom | string}
   * @memberof plotZoom
   * @property
   * by default orient=chart.orient() || 'horizontal'
   */
  zoom.orient = function(_) { return arguments.length ? (orient = _, zoom) : orient; };

  /**
   * Gets or sets the max distance in which to scroll X
   * (see {@link plotZoom#xLock})
   * @param {number} [_=none] should be a positive value
   * @returns {zoom | number}
   * @memberof plotZoom
   * @property
   * by default xLock=chart.spaceX()
   */
  zoom.xLock = function(_) { return arguments.length ? (xLock = _, zoom) : xLock; };
  /**
   * Gets or sets the max distance in which to scroll Y
   * (see {@link plotZoom#yLock})
   * @param {number} [_=none]  should be a positive value
   * @returns {zoom | number}
   * @memberof plotZoom
   * @property
   * by default yLock=chart.spaceY()
   */
  zoom.yLock = function(_) { return arguments.length ? (yLock = _, zoom) : yLock; };

  zoom.xComponents = function(_) { return arguments.length ? (xComponents = _, zoom) : xComponents; };
  zoom.yComponents = function(_) { return arguments.length ? (yComponents = _, zoom) : yComponents; };


  function setLocks() {
    var chartObjSel = chartSel.select('.'+utils.str.hypenate(chart.namespace(),'object-container'))
    var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'))
    var cos = chartObjSel.attr('transform', 'translate(0,0)')

    xLock = chartSel.node().getBBox().width - chart.spaceX()// * .9
    yLock = chartSel.node().getBBox().height - chart.spaceY()// * .9
    cos.attr('transform', 'translate('+chartObjTrans[0]+','+chartObjTrans[1]+')')
    utils.con.log('plotZoom', 'setLocks', {xLock:xLock, yLock:yLock})
  }


  /**
   * Sets the x and y locks (how far one can scroll)
   * (see {@link plotZoom#xLock} and {@link plotZoom#yLock})
   * @function plotZoom.setLocks
   * @returns {undefined}
   * @memberof plotZoom
   * @property
   */
  zoom.setLocks = setLocks

  function zoom() {
    setLocks()

    var
    xComponentsSel = xComponents.map(function(d, i){return d.selection()}),
    yComponentsSel = yComponents.map(function(d, i){return d.selection()})

    var horizontalQ, verticalQ
    if (orient == '2D') {horizontalQ = true; verticalQ = true;}
    if (orient == 'horizontal') {horizontalQ = true; verticalQ = false;}
    if (orient == 'vertical') {verticalQ = true; horizontalQ = false;}

    // capture transform event
    var transform = d3.event.transform

    var chartBox = chartSel.node().getBBox()
    var xComponentsBox = xComponentsSel.map(function(d, i){return d.node().getBBox()})
    var yComponentsBox = xComponentsSel.map(function(d, i){return d.node().getBBox()})


    var chartWidth = chartBox.width - chartBox.x
    var chartHeight = chartBox.height - chartBox.y

    // enable wheel event
    if (eventType == "wheel") {
      var e = d3.event
      // prevent page scrolling
      e.preventDefault()
      // event delta is very very slow, so speed it up with wheel speed
      var w = d3.event.deltaY * wheelSpeed
      var shiftQ = d3.event.shiftKey

      // d3 has no way to make custom transform, so make an object and add
      // the necessary functions to make wheel event compatible with drag events
      if (orient == '2D') {
        transform = shiftQ ? {k: 1, x: w, y: 0} : {k: 1, x: 0, y: w}
      } else {
        transform = horizontalQ ? {k: 1, x: w, y: 0} : {k: 1, x: 0, y: w}
      }
      // * -1 is invert
      transform.applyX = function(x) { return x * this.k + this.x *-1; }
      transform.applyY =  function(y) { return y * this.k + this.y *-1; }
    }



    var chartObjSel = chartSel.select('.'+utils.str.hypenate(chart.namespace(),'object-container'))
    var xComponentObjSel = xComponentsSel.map(function(d, i){
      return d.select('.'+utils.str.hypenate(xComponents[i].namespace(),'object-container'))
    })
    var yComponentObjSel = yComponentsSel.map(function(d, i){
      return d.select('.'+utils.str.hypenate(yComponents[i].namespace(),'object-container'))
    })

    var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'))
    var xComponentsObjTrans = xComponentObjSel.map(function(d, i){
      return utils.sel.getTranslation(d.attr('transform'))
    })
    var yComponentsObjTrans = yComponentObjSel.map(function(d, i){
      return utils.sel.getTranslation(d.attr('transform'))
    })

    var x = horizontalQ ? transform.applyX(chartObjTrans[0]) : 0
    if (horizontalQ) {x = x < -xLock ? (transform.x = 0, -xLock) : (transform.x = 0, Math.min(x, 0)) }

    var y = verticalQ ? transform.applyY(chartObjTrans[1]) : 0
    if (verticalQ) {y = y < -yLock ? (transform.y = 0, -yLock): (transform.y = 0, Math.min(y, 0))}

    chartObjSel.attr('transform', 'translate('+x+','+y+')')
    if (horizontalQ) {
      // xAxisObjSel.attr('transform', 'translate('+x+','+0+')')
      xComponentObjSel.map(function(d, i){ d.attr('transform', 'translate('+x+','+0+')')  })
    }
    if (verticalQ) {
      // yAxisObjSel.attr('transform', 'translate('+0+','+y+')')
      yComponentObjSel.map(function(d, i){ d.attr('transform', 'translate('+0+','+y+')')  })

    }


  }

  zoom.reset = function() {
    var horizontalQ, verticalQ
    if (orient == '2D') {horizontalQ = true; verticalQ = true;}
    if (orient == 'horizontal') {horizontalQ = true; verticalQ = false;}
    if (orient == 'vertical') {verticalQ = true; horizontalQ = false;}

    var chartObjSel = chartSel.select('.'+utils.str.hypenate(chart.namespace(),'object-container'))
    var xAxisObjSel = xAxisSel.select('.'+utils.str.hypenate(xAxis.namespace(),'object-container'))
    var yAxisObjSel = yAxisSel.select('.'+utils.str.hypenate(yAxis.namespace(),'object-container'))
    chartObjSel.attr('transform', 'translate('+0+','+0+')')
    xAxisObjSel.attr('transform', 'translate('+0+','+0+')')
    yAxisObjSel.attr('transform', 'translate('+0+','+0+')')
  }

  return zoom
}
