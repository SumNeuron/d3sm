import lasso from './lasso'
import lassoWidget from './lasso-widget'
import multiPlotZoom from './multi-plot-zoom'
import plotZoom from './plot-zoom'
import datatoggle from './data-toggle'
import selectFilter from './select-filter'

let aux = {
  lasso, plotZoom, multiPlotZoom, datatoggle, selectFilter, lassoWidget
}

export {lasso, plotZoom, multiPlotZoom, datatoggle, selectFilter, lassoWidget}
export default aux
