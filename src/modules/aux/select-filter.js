import utils from '../utils/index.js'

export default function selectFilter(selection) {

  var
  data,
  namespace = 'd3sm-select-filter',
  selectionName = 'Select options:',
  defaultValue = undefined




  var lastValue = undefined

  selectFilter.data = function(_) { return arguments.length ? (data = _, selectFilter) : data}
  selectFilter.namespace = function(_) { return arguments.length ? (namespace = _, selectFilter) : namespace}
  selectFilter.selectionName = function(_) { return arguments.length ? (selectionName = _, selectFilter) : selectionName}
  selectFilter.defaultValue = function(_) { return arguments.length ? (defaultValue = _, selectFilter) : defaultValue}
  selectFilter.currentOption = currentOption

  function selectFilter() {
    var
    container = utils.sel.safeSelect(selection, 'div', 'input-group').classed(utils.str.hypenate(namespace,'container'),true),

      selectPrepend = utils.sel.safeSelect(container, 'div', 'select-prepend').classed('input-group-prepend', true),
        selectPrependSpan = utils.sel.safeSelect(selectPrepend, 'span', 'input-group-text').text(selectionName),

      select = utils.sel.safeSelect(container, 'select', 'custom-select').classed(utils.str.hypenate(namespace,'select'),true),

      selectAppend = utils.sel.safeSelect(container, 'div', 'select-append').classed('input-group-prepend', true),
        selectAppendButton = utils.sel.safeSelect(selectAppend, 'a', 'filter-button').classed('btn btn-outline-secondary', true),
          filterButtonIcon = utils.sel.safeSelect(selectAppendButton, 'i', 'fa fa-filter'),

      inputGroup = utils.sel.safeSelect(container, 'div', 'filter-input-group').classed('input-group',true).classed('d-none', true),
        inputPrepend = utils.sel.safeSelect(inputGroup, 'div', 'input-group-prepend'),
          inputPrependSpan = utils.sel.safeSelect(inputPrepend, 'span', 'input-group-text').classed('search-button', true),
            inputPrependSpanIcon = utils.sel.safeSelect(inputPrependSpan,'i','fa fa-search'),

        input = utils.sel.safeSelect(inputGroup, 'input', 'form-control').attr('placeholder', 'all').attr('type', 'text'),
        inputAppend = utils.sel.safeSelect(inputGroup, 'div', 'input-group-append'),
          inputAppendButton = utils.sel.safeSelect(inputAppend, 'a', 'close-button').classed('btn btn-outline-secondary', true),
            inputAppendButtonIcon = utils.sel.safeSelect(inputAppendButton, 'i', 'fa fa-close')


    var keys = d3.keys(data),
    options = select.selectAll('option')

    options = options.data(d3.keys(data))
    options = options.merge(options.enter().append('option'))
    .attr('value', function(d, i){return d})
    .text(function(d, i){return d})

    var
    filterButton = selectAppendButton,
    closeButton = inputAppendButton

    filterButton.on('click', function(d, i){
      var currentStyle = inputGroup.classed('d-none')
      inputGroup.classed('d-none', !currentStyle)
    })

    closeButton.on('click', function(d, i){
      input.property('value', '').dispatch('input')
    })

    input.on('input', function(d, i){
      var
      val = input.property('value'),
      reg = new RegExp(val, 'gi'),
      use

      if (val == '') {use = keys}
      else {
        use = []
        d3.keys(data).map(function(option, j){
          var match = option.match(reg)
          if (match == null || match.join('') == '') {}
          else { use.push(option) }
        })
      }

      options = select.selectAll('option')
      options = options.data(use)
      options.exit().remove()
      options = options.merge(options.enter().append('option'))
      .attr('value', function(d, i){return d})
      .text(function(d, i){return d})

      var current = currentOption()
      if (lastValue != current) {
        lastValue = current
        select.dispatch('change')
      }
    })


  }

  function currentOption() {
    var val = selection.select("select").property('value')
    return val == undefined || val == ''
    ? defaultValue == undefined
      ? d3.keys(data)[0]
      : defaultValue
    : val
  }

  return selectFilter
}
