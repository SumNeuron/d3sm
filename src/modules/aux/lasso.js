import utils from '../utils/index.js';
import groupingSpacer from '../grouping-spacer';
import CF from '../color-function';
import TTip from '../tooltip';
import '../d3-prototypes';

function getTranslation(selection){
  var transform = selection.attr('transform')
  var [junk, xy] =transform.split('translate(')
  var [x, y] = xy.split(',')
  y, junk = y.split(')')
  return [parseFloat(x), parseFloat(y)]
}

export default function lasso( selection ) {
  var
  svg, // svg that is target of events
  objectContainer, // container which houses objects we are selecting (allows for transform to be applied to lasso)
  objectClass, // class of object we are selecting
  namespace="d3sm-lasso",
  chartContainer,
  chartOffset,
  objectsOffset,
  eventCatcher,

  xScale, // optional scale for the lasso currentPoints
  yScale, // optional scale for the lasso currentPoints

  activeQ = false, // whether or not lasso is active

  currentPoints=[], // mouse points for current lasso
  allPoints=[], // list of lists for all points of lassos

  line = d3.line()
  .x(function(d, i){
    var x
    if (xScale != undefined) { x = xScale(d[0]) }
    else {x = d[0]}
    return x //- chartOffset[0]// - objectsOffset[0]
  })
  .y(function(d, i){
    var y
    if (yScale != undefined) { y= yScale(d[1]) }
    else {y =  d[1]}
    return y// - chartOffset[1]// - objectsOffset[1]
  })
  .curve(d3.curveLinearClosed),

  instance=0,    // an indentifier for which instance this lasso is under the current svg

  tickDistance = 10,

  // styles for lasso path
  color = '#17a2b8',
  animationRate = '10s',
  opacity=0.3,
  dashArray = '5, 10',
  stroke = 'black',
  strokeWidth=2,

  // styles for lassoed objects
  lassoedFill = "white",
  lassoedStroke = 'black',
  lassoedStrokeWidth = 3,

  transitionDuration = 1000,
  easeFunc = d3.easeExp

  var path

  lasso.svg = function(_) { return arguments.length ? (svg = _, lasso) : svg; }
  lasso.chartContainer = function(_) { return arguments.length ? (chartContainer = _, lasso) : chartContainer; }
  lasso.objectContainer = function(_) { return arguments.length ? (objectContainer = _, lasso) : objectContainer; }
  lasso.objectClass = function(_) { return arguments.length ? (objectClass = _, lasso) : objectClass; }
  lasso.namespace = function(_) { return arguments.length ? (namespace = _, lasso) : namespace; }
  lasso.xScale = function(_) { return arguments.length ? (xScale = _, lasso) : xScale; }
  lasso.yScale = function(_) { return arguments.length ? (yScale = _, lasso) : yScale; }
  lasso.activeQ = function(_) { return arguments.length ? (activeQ = _, lasso) : activeQ; }
  lasso.currentPoints = function(_) { return arguments.length ? (currentPoints = _, lasso) : currentPoints; }
  lasso.allPoints = function(_) { return arguments.length ? (allPoints = _, lasso) : allPoints; }
  lasso.instance = function(_) { return arguments.length ? (instance = _, lasso) : instance; }
  lasso.tickDistance = function(_) { return arguments.length ? (tickDistance = _, lasso) : tickDistance; }
  lasso.color = function(_) { return arguments.length ? (color = _, lasso) : color; }
  lasso.animationRate = function(_) { return arguments.length ? (animationRate = _, lasso) : animationRate; }
  lasso.opacity = function(_) { return arguments.length ? (opacity = _, lasso) : opacity; }
  lasso.dashArray = function(_) { return arguments.length ? (dashArray = _, lasso) : dashArray; }
  lasso.stroke = function(_) { return arguments.length ? (stroke = _, lasso) : stroke; }
  lasso.lassoedFill = function(_) { return arguments.length ? (lassoedFill = _, lasso) : lassoedFill; }
  lasso.lassoedStroke = function(_) { return arguments.length ? (lassoedStroke = _, lasso) : lassoedStroke; }
  lasso.lassoedStrokeWidth = function(_) { return arguments.length ? (lassoedStrokeWidth = _, lasso) : lassoedStrokeWidth; }
  lasso.eventCatcher = function(_) { return arguments.length ? (eventCatcher = _, lasso) : eventCatcher; }

  lasso.drag = drag
  lasso.draw = draw
  lasso.tick = tick
  lasso.detect = detect
  lasso.toggle = toggle
  lasso.remove = remove
  lasso.render = render
  lasso.keyFrames = keyFrames
  lasso.updateObjects = updateObjects
  lasso.applyPathAttributes = applyPathAttributes
  lasso.applyObjectAttributes = applyObjectAttributes

  keyFrames()

  function lasso() {
    // add a dash animation if needed
    if (activeQ) { transitionDraw() }
  }

  function toggle(state) {
    // use optional param to set state, otherwise toggle state
    activeQ = (state!=undefined) ? state : !activeQ
    chartOffset = getTranslation(chartContainer) //utils.sel.getTranslation(chartContainer)
    objectsOffset = getTranslation(objectContainer) //utils.sel.getTranslation(objectContainer)

    if (activeQ) {
      svg.node().addEventListener('mousedown', render, true)
    } else {
      svg.node().removeEventListener('mousedown', render, true)
      remove()
    }

  }

  function draw() {
    chartOffset = getTranslation(chartContainer) //utils.sel.getTranslation(chartContainer)
    objectsOffset = getTranslation(objectContainer) //utils.sel.getTranslation(objectContainer)

    var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container')
    var paths = container.selectAll('path[instance="'+instance+'"]')

    // update
    paths = paths.data(allPoints)

    // remove excess
    var pExit = paths.exit().remove()
    // add needed paths
    var pEnter = paths.enter().append('path')

    // merge
    paths = paths.merge(pEnter)

    // apply
    applyPathAttributes(paths)
  }

  function remove() {
    var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container')
    var paths = container.selectAll('path[instance="'+instance+'"]').remove()
    container.remove()
    objectContainer.selectAll(objectClass).classed("in-lasso", false)
    updateObjects()
  }

  function render( event ) {
    // nothing can interefer with drawing the lasso
    event.preventDefault(); event.stopPropagation();

    var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container')

    /*
    each time the user presses down, while the state is active, the lasso
    the lasso should make a seperate segment.
    */
    currentPoints = [];

    svg.node().addEventListener('mousemove', drag)
    svg.node().addEventListener('mouseup', function(event) {
      svg.node().removeEventListener('mousemove', drag)
      allPoints.push(currentPoints)
      // BUG:  somehow this is pushing currentPoints n times where n is the nth lasso path for the current instance
      // NOTE: allPoints = utils.arr.unique(allPoints) is a temporary and inefficient fix
      allPoints = utils.arr.unique(allPoints)
    })

    path = container.append('path').data([currentPoints])
    applyPathAttributes(path)
  }

  function transitionDraw() {
    var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container')
    var paths = container.selectAll('path[instance="'+instance+'"]')

    // update
    paths = paths.data(allPoints)

    // remove excess
    var pExit = paths.exit().remove()
    // add needed paths
    var pEnter = paths.enter().append('path')

    // merge
    paths = paths.merge(pEnter)
    .transition().duration(transitionDuration)
    .ease(easeFunc)
    applyPathAttributes(paths)

  }

  function applyPathAttributes(path) {
    path
    .attr("class", utils.str.hypenate(namespace, "lasso-path"))
    .style('opacity', opacity)
    .attr('fill', color)
    .attr("d", line)
    .attr('instance', instance)
    .style("stroke-dasharray", dashArray)
    .attr("stroke", stroke)
    .attr("stroke-width", strokeWidth)
    .style('animation', 'lassoDash '+animationRate+' linear')
    .style("animation-iteration-count", "infinite")
  }

  function drag(event) {
    /*
    effectively create a mouse down and move event (which normally is inteperated
    as 'drag' by the browser) by dynamically adding / removing this event on
    mouse down / mouse up.
    */

    if (eventCatcher != undefined) {eventCatcher.dispatch(utils.str.hypenate(namespace,"drag"))}
    // d3.dispatch(utils.str.hypenate(namespace,"drag"))

    if (event.which != 1) {return} // ensures left mouse button set
    d3.event = event
    var pt = d3.mouse(objectContainer.node());
    var pt = d3.mouse(svg.node());

    if (xScale != undefined) {pt[0] = xScale.invert(pt[0])}
    if (yScale != undefined) {pt[1] = yScale.invert(pt[1])}
    pt[0] = pt[0] - chartOffset[0] - objectsOffset[0]
    pt[1] = pt[1] - chartOffset[1] - objectsOffset[1]

    /* if we have a point already, test if it passes a minimum distance to prevent overwhelming with too many tick functions */
    if (currentPoints.length) {
      var lastPt = currentPoints[currentPoints.length - 1]
      var a = [pt[0], pt[1]], b = [lastPt[0], lastPt[1]]

      if (xScale) {b[0] = xScale(b[0]); a[0] = xScale(a[0])}
      if (yScale) {b[1] = yScale(b[1]); a[1] = yScale(a[1])}

      var dist = utils.math.euclideanDistance(b, a)
      if (dist > tickDistance) { tick(pt) }
    }
    else { tick(pt) }
  }


  function tick (pt) {
    /*
    If a point is provided update data and objects.
    Otherwise just call on data we already have.

    Why like this?:
    1. currentPoints is current points to allow disjunct lassos, currentPoints is only pushed to
    allPoints after mouseup.
    2. to allow render of objects in the lasso class / updating the data list
    just by toggling the button
    */

    if (pt != undefined) {
      currentPoints.push(pt);
      path.attr("d", line);
      if (currentPoints.length < 3) {return} // need at least 3 points to detect anything.
      detect(allPoints.concat([currentPoints]))
    } else {
      detect(allPoints)
    }
  }


  function detect(lassos) {
    if (lassos == undefined) {lassos = allPoints}
    objectContainer.selectAll(objectClass).each(function(d, i){
      var current = d3.select(this),

      box = utils.sel.absolutePosition(current)//.absolutePosition(),
      // box = current.relativePositionTo(objectContainer.node()),

      boxPts = [
        [
          box.left - chartOffset[0] - objectsOffset[0],
          box.top - chartOffset[1] - objectsOffset[1]
        ],
        [
          box.right - chartOffset[0] - objectsOffset[0],
          box.top - chartOffset[1] - objectsOffset[1]
        ],
        [
          box.left - chartOffset[0] - objectsOffset[0],
          box.bottom - chartOffset[1] - objectsOffset[1]
        ],
        [
          box.right - chartOffset[0] - objectsOffset[0],
          box.bottom - chartOffset[1] - objectsOffset[1]
        ]
      ]

      if (xScale != undefined) {
        boxPts[0][0] = xScale.invert(boxPts[0][0])
        boxPts[1][0] = xScale.invert(boxPts[1][0])
        boxPts[2][0] = xScale.invert(boxPts[2][0])
        boxPts[3][0] = xScale.invert(boxPts[3][0])
      }
      if (yScale != undefined) {
        boxPts[0][1] = yScale.invert(boxPts[0][1])
        boxPts[1][1] = yScale.invert(boxPts[1][1])
        boxPts[2][1] = yScale.invert(boxPts[2][1])
        boxPts[3][1] = yScale.invert(boxPts[3][1])
      }


      /*
      flag needed as we have to test multiple lasso segments, and if the point
      is not in one segment, it does not mean it is not in any
      */
      var inAnyLassoQ = false;
      for (var i = 0; i < lassos.length; i++) {
        var lassoPoints = lassos[i]
        // .map(function(pt){
        //   var x, y = pt
        //   if (xScale!=undefined) {x = xScale(x)}
        //   if (yScale!=undefined) {y = yScale(y)}
        //   return [x, y]
        // })
        var boxInLassoQ = boxPts.every(coord => d3.polygonContains(lassoPoints, coord))

        if (boxInLassoQ) { inAnyLassoQ = true; } // only update flag in the positive case.
      }

      current.classed('in-lasso', inAnyLassoQ)
      current.classed('in-lasso-'+instance, inAnyLassoQ)
    })

    updateObjects()
    return objectContainer.selectAll('.in-lasso-'+instance)
  }



  function updateObjects() {
    objectContainer.selectAll(objectClass).each(function(d, i) {
      var t = d3.select(this)
      applyObjectAttributes(t, t.classed('in-lasso'))
    })
  }

  function applyObjectAttributes(obj, setQ) {
    var
    preLassoFill = obj.attr('_pre_lasso_fill'),
    preLassoStroke = obj.attr('_pre_lasso_stroke'),
    preLassoStrokeWidth = obj.attr('_pre_lasso_stroke-width')

    if (setQ) {
      obj.classed("in-lasso", true)
      obj.classed('in-lasso-'+instance, true)
      if (preLassoFill == undefined) { obj.attr('_pre_lasso_fill', obj.attr('fill')) }
      if (preLassoStroke == undefined) { obj.attr('_pre_lasso_stroke', obj.attr('stroke')) }
      if (preLassoStrokeWidth == undefined) { obj.attr('_pre_lasso_stroke-width', obj.attr('stroke-width')) }

      obj
      //BUG: when .raise()
      .attr('fill', lassoedFill)
      .attr('stroke', lassoedStroke)
      .attr('stoke-width', lassoedStrokeWidth)

    } else {
      obj.classed("in-lasso", false)
      obj.classed('in-lasso-'+instance, false)
      if (preLassoFill != undefined) { obj.attr('fill', preLassoFill) }
      if (preLassoStroke != undefined) { obj.attr('stroke', preLassoStroke) }
      if (preLassoStrokeWidth != undefined) { obj.attr('stroke-width', preLassoStrokeWidth) }
    }
  }

  function keyFrames() {
    var style =
    d3.select("html").select('style.'+utils.str.hypenate(namespace,"lasso-dash"))
    if (style.empty()) {
      d3.select("html").append('style')
      .classed(utils.str.hypenate(namespace,"lasso-dash"), true)
      .html("@keyframes lassoDash {to { stroke-dashoffset: 1000;}}")
    }

  }
  return lasso
}
