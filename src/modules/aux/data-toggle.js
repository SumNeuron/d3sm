import utils from '../utils/index.js'
import selectFilter from './select-filter';

/*******************************************************************************
**                                                                            **
**                                                                            **
**                                DATATOGGLE                                  **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Creates a datatoggle
 * @constructor datatoggle
 * @param {d3.selection} selection
 * @namespace datatoggle
 * @returns {function} datatoggle
 */
export default function datatoggle( selection ) {
  var
  /**
  * Keys to make toggle-able options
  * (see {@link datatoggle#keys})
  * @param {string[]} [keys=undefined]
  * @memberof datatoggle#
  * @property
  */
  keys,

  /**
  * What to do when a different key is clicked
  * (see {@link datatoggle#updateFunction})
  * @param {function} [updateFunction=function(){}]
  * @memberof datatoggle#
  * @property
  */
  updateFunction = function(){},
  /**
  * Namespace for all items made by this instance of datatoggle
  * @param {string} [namespace="d3sm-databar"]
  * @memberof datatoggle#
  * @property
  */
  namespace='d3sm-databar',
  /**
  * Currently toggled key
  * @param {string} [currentKey=undefined]
  * @memberof datatoggle#
  * @property
  */
  currentKey,



  xAxisSelectQ = false,
  xAxisOptions,
  yAxisSelectQ = false,
  yAxisOptions,
  data
  toggle.xAxisSelectQ = function(_){return arguments.length ? (xAxisSelectQ = _, toggle) : xAxisSelectQ}
  toggle.yAxisSelectQ = function(_){return arguments.length ? (yAxisSelectQ = _, toggle) : yAxisSelectQ}
  toggle.xAxisOptions = function(_){return arguments.length ? (xAxisOptions = _, toggle) : xAxisOptions}
  toggle.yAxisOptions = function(_){return arguments.length ? (yAxisOptions = _, toggle) : yAxisOptions}
  toggle.data = function(_){return arguments.length ? (data = _, toggle) : data}
  toggle.keys = function(_){return arguments.length ? (keys = _, toggle) : keys}
  toggle.currentKey = function(_){return arguments.length ? (currentKey = _, toggle) : currentKey}

  /**
   * Gets / sets the updateFunction
   * (see {@link datatoggle#updateFunction})
   * @function datatoggle.updateFunction
   * @param {function} [_=none]
   * @returns {datatoggle | function}
   * @memberof datatoggle
   * @property
   * by default updateFunction = function(){}
   */
  toggle.updateFunction = function(_){return arguments.length ? (updateFunction = _, toggle) : updateFunction}
  /**
   * Gets / sets the namespace
   * (see {@link datatoggle#namespace})
   * @function datatoggle.namespace
   * @param {string} [_=none]
   * @returns {datatoggle | string}
   * @memberof datatoggle
   * @property
   * by default namespace = 'd3sm-databar'
   */
  toggle.namespace = function(_){return arguments.length ? (namespace = _, toggle) : namespace}
  /**
   * Gets / sets the currentKey
   * (see {@link datatoggle#currentKey})
   * @function datatoggle.currentKey
   * @param {string} [_=none]
   * @returns {datatoggle | string}
   * @memberof datatoggle
   * @property
   * by default currentKey = undefined
   */
  toggle.currentKeys =
  function () {
    var vals = {}
    d3.keys(filterSelects).map(function(k, i){
      vals[k]= filterSelects[k].currentOption()
    })
    return vals
  }

  var filterSelects = {}
  function toggle() {
    // selection options

    // selection.classed('d-flex flex-row', true)
    // var filterButton = utils.sel.safeSelect(selection, 'a', 'slider-buttons')
    // var filterI = utils.sel.safeSelect(filterButton, 'i', 'fa fa-sliders')

    /*BUG: unexpected behavior.
      - when using bootstrap-eque way for collapse, clicking button submits to
      the same page in applications (but not in demo), so using anchor (<a>)
      - when using anchor, cause page to jump to that location
      - when using show, the first open works, but the close does not.
    */
    // filterButton.attr('class', 'btn btn-secondary')
    // .attr('data-toggle', 'collapse')
    // .attr('href', '#'+utils.str.hypenate(namespace, 'data-toggle'))
    // .attr('target', '_blank')
    // .html(filterButton.html()+' Filters')
    // .on('click', function(d, i){
    //   d3.event.preventDefault()
    //   d3.event.stopPropagation()
    //   var dt = d3.select("#"+utils.str.hypenate(namespace, 'data-toggle'))
    //   dt.classed('show', !dt.classed('show'))
    //   dt.classed('d-inline-flex', dt.classed('show'))
    //
    //   filterButton.classed('btn-primary', dt.classed('show'))
    //   filterButton.classed('btn-secondary', !dt.classed('show'))
    // })
    // .classed('d-inline-flex', true)
    // .style("margin-right", '10px')



    // var datatoggleCollapse = utils.sel.safeSelect(selection, 'div', utils.str.hypenate(namespace,'collapse'))
    // .attr('id', utils.str.hypenate(namespace, 'data-toggle'))
    // .classed('collapse', true)

    // var flexRow = utils.sel.safeSelect(datatoggleCollapse, 'div', 'd-inline-flex flex-row flex-wrap')
    var flexRow = utils.sel.safeSelect(selection, 'div', 'd-inline-flex flex-row flex-wrap')

    var dataopts = flexRow.selectAll('div.'+utils.str.hypenate(namespace,'select-filter'))
    // remove excess
    dataopts.exit().remove()
    // bind data
    dataopts = dataopts.data(d3.keys(data))
    //enter
    var doEnter = dataopts.enter().append('div')
    .attr('class', 'select-filter')

    dataopts = dataopts.merge(doEnter).style('margin-right', "10px")

    dataopts.each(function(d, i){
      var t = d3.select(this)
      var sf = selectFilter(t)
      .data(data[d])
      .namespace(utils.str.hypenate(namespace, d))
      .selectionName(d)
      sf()
      filterSelects[d] = sf
    })


    selection.selectAll('select')
    .on('change', function(){updateFunction()})
    // bind update function
    // d3.selectAll
    return toggle
  }


  function onlyOne() {
    // d3.event.preventDefault()
    // d3.event.stopPropagation()
    var dataopts = selection.selectAll('div.data-option')
    currentKey = dataopts.select(':checked').datum()
    updateFunction()

  }

  function axisSelectFilter(selection, axis="x-axis", axisData) {
    if (axisData == undefined) {
      axisData = {
        'linear': d3.scaleLinear(),
        'log': d3.scaleLog(),
        'pow': d3.scalePow(),
        'sqrt': d3.scaleSqrt()
      }
    }

    var sf = selectFilter(selection)
    .data(axisData)
    .namespace(axis)
    .selectionName(axis+' scale')
    .defaultValue(d3.scaleLinear())


    sf()

  }

  return toggle
}
