import * as d3 from 'd3'
import utils from './utils/index.js'
import groupingSpacer from './grouping-spacer';
/*******************************************************************************
**                                                                            **
**                                                                            **
**                                  AXIS                                      **
**                                                                            **
**                                                                            **
*******************************************************************************/

/**
 * Creates an axis
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/axes/index.html Demo}
 * @constructor axis
 * @param {d3.selection} selection
 * @namespace axis
 * @returns {function} axis
 */
export default function axis ( selection ) {
  var
  /**
  * The orientation of the axis
  * (see {@link axis#orient})
  * @param {string} [orient='bottom']
  * @memberof axis#
  * @property
  */
  orient = 'bottom',       // direction of the axis

  /**
  * Amount of horizontal space (in pixels) avaible to render the axis in
  * (see {@link axis#spaceX})
  * @param {number} [spaceX=0]
  * @memberof axis#
  * @property
  */
  spaceX=0,
  /**
  * Amount of vertical space (in pixels) avaible to render the axis in
  * (see {@link axis.spaceY})
  * @param {number} [spaceY=0]
  * @memberof axis#
  * @property
  */
  spaceY=0,


  /**
  * Whether or not to allow axis to render elements pass the main spatial dimension
  * given the orientation (see {@link axis#orient}), where {@link axis#orient}="bottom" or {@link axis#orient}="top"
  * the main dimension is {@link axis#spaceX} and where {@link axis#orient}="left" or {@link axis#orient}="right"
  * the main dimension is {@link axis#spaceY}
  * @param {boolean} [overflowQ=false]
  * @memberof axis#
  * @property
  */
  overflowQ = false,    // whether or not to allow overflow
  /**
  * Whether or not the axis labels are for categorical data. If false,
  * will use {@link axis#scale} to position ticks.
  * @param {boolean} [categoricalQ=false]
  * @memberof axis#
  * @property
  */
  categoricalQ = false, // whether or not the axis is showing values or groups
  /**
  * Whether or not the axis ticks should have guidelines
  * @param {boolean} [categoricalQ=false]
  * @memberof axis#
  * @property
  */
  guideLinesQ = false,    // whether or not to allow overflow


  /**
  * How to group the tick labels
  * @param {Array[]} [grouping=undefined] list of putatively other lists, which should correspond to tickLabels
  * will space tick labels in nested lists closer together than outer lists
  * @memberof axis#
  * @property
  */
  grouping,

  /**
  * The scale for which non-categorial (see {@link axis#categoricalQ}) ticks should be spaced
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof axis#
  * @property
  */

  scale = d3.scaleLinear(),
  /**
  * The padding for the domain of the scale (see {@link axis#scale})
  * @param {d3.scale} [scale=d3.scaleLinear]
  * @memberof axis#
  * @property
  */
  domainPadding = 0.5,


  /**
  * Default space for the spacer (percentage) of main dimension given the orientation
  * (see {@link axis#orient}), where {@link axis#orient}="bottom" or {@link axis#orient}="top"
  * the main dimension is {@link axis#spaceX} and where {@link axis#orient}="left" or {@link axis#orient}="right"
  * the main dimension is {@link axis#spaceY}between ticks
  * @param {number} [objectSpacer=0.05]
  * @memberof axis#
  * @property
  */
  objectSpacer = 0.05,
  /**
  * The minimum size that an object can be if {@link axis#categoricalQ} is set to true
  * @param {number} [minObjectSize=15]
  * @memberof axis#
  * @property
  */
  minObjectSize = 15,
  /**
  * The maximum size that an object can be if {@link axis#categoricalQ} is set to true
  * @param {number} [maxObjectSize=15]
  * @memberof axis#
  * @property
  */
  maxObjectSize = 50,

  /**
  * Color of the background
  * @param {string} [backgroundFill="transparent"]
  * @memberof axis#
  * @property
  */
  backgroundFill = 'transparent',
  /**
  * Namespace for all items made by this instance of axis
  * @param {string} [namespace="d3sm-axis"]
  * @memberof axis#
  * @property
  */
  namespace = 'd3sm-axis',
  /**
  * Class name for tick container (<g> element)
  * @param {string} [objectClass="tick-group"]
  * @memberof axis#
  * @property
  */
  objectClass = 'tick-group',

  /**
  * Values to show at each tick. Only used if categoricalQ is set true. See {@link axis#categoricalQ}
  * @param {string[]} [tickLabels=undefined]
  * @memberof axis#
  * @property
  */
  tickLabels,   // what to place at ticks
  /**
  * Values to show at each tick. Only used if categoricalQ is set false. See {@link axis#categoricalQ}
  * @param {string[] | number[]} [objectClass=undefined]
  * @memberof axis#
  * @property
  */
  tickValues,   // where to place ticks if not
  /**
  * Number of ticks to display if categoricalQ is false. See {@link axis#categoricalQ}
  * @param {number} [numberOfTicks=5]
  * @memberof axis#
  * @property
  */
  numberOfTicks = 5,


  /**
  * Stroke color of the main axis line
  * @param {string} [lineStroke='black']
  * @memberof axis#
  * @property
  */
  lineStroke = 'black',
  /**
  * Stroke width of the main axis line
  * @param {number} [lineStrokeWidth=3]
  * @memberof axis#
  * @property
  */
  lineStrokeWidth = 3,


  /**
  * Stroke color of ticks
  * @param {string} [tickStroke='black']
  * @memberof axis#
  * @property
  */
  tickStroke = 'black',
  /**
  * Stroke number of ticks
  * @param {string} [tickStrokeWidth=2]
  * @memberof axis#
  * @property
  */
  tickStrokeWidth = 2,
  /**
  * Length - in pixels - of ticks
  * @param {number} [tickLength=10]
  * @memberof axis#
  * @property
  */
  tickLength = 10,

  tickTickLabelSpacer = 10,
  tickLabelMargin = 10,


  /**
  * Font size of tick labels
  * @param {number} [tickLabelFontSize=14]
  * @memberof axis#
  * @property
  */
  tickLabelFontSize = 14,
  /**
  * Min font size of tick labels
  * @param {number} [tickLabelMinFontSize=8]
  * @memberof axis#
  * @property
  */
  tickLabelMinFontSize = 8,
  /**
  * Max font size of tick labels
  * @param {number} [tickLabelMaxFontSize=20]
  * @memberof axis#
  * @property
  */
  tickLabelMaxFontSize = 20,


  /**
  * Text anchor of tick labels
  * @param {string} [tickLabelTextAnchor="middle"]
  * @memberof axis#
  * @property
  */
  tickLabelTextAnchor,
  /**
  * Rotation of tick labels
  * @param {number} [tickLabelRotation=0]
  * @memberof axis#
  * @property
  */
  tickLabelRotation,
  /**
  * Optional function for extracting the tick label from data
  * @param {function} [tickLabelFunc=undefined]
  * @memberof axis#
  * @property
  */
  tickLabelFunc = undefined,

  /**
  * Optional function for what to do when label is clicked
  * @param {function} [tickLabelOnClick=function(d, i){}]
  * @memberof axis#
  * @property
  */
  tickLabelOnClick = function(d, i){},

  /**
  * Optional function for what to do when label is hovered
  * @param {function} [tickLabelOnHoverFunc=function(d, i){}]
  * @memberof axis#
  * @property
  */
  tickLabelOnHoverFunc = function(d, i){
    return String(d).replace('-', ' ').replace('_', ' ')
  },


  /**
  * Length of guidelines
  * @param {function} [guidelineSpace=undefined]
  * @memberof axis#
  * @property
  */
  guidelineSpace,
  /**
  * Stroke color of guidlines
  * @param {string} [guidelineSpace="#333333"]
  * @memberof axis#
  * @property
  */
  guideLineStroke = '#333333',
  /**
  * Stroke width of guidlines
  * @param {number} [guidelineSpace=2]
  * @memberof axis#
  * @property
  */
  guideLineStrokeWidth = 2,

  /**
  * Duration of all transitions of this element
  * @param {number} [transitionDuration=1000]
  * @memberof axis#
  * @property
  */
  transitionDuration = 1000,
  /**
  * Easing function for transitions
  * @param {d3.ease} [easeFunc=d3.easeExp]
  * @memberof axis#
  * @property
  */
  easeFunc = d3.easeExp,


  /**
  * Closure variable for getting object size after calculation
  * @param {number} [objectSize=undefined]
  * @memberof axis#
  * @property
  */
  objectSize,
  /**
  * Closure variable for getting spacer size after calculation
  * @param {number} [spacerSize=undefined]
  * @memberof axis#
  * @property
  */
  spacerSize,

  /**
  * Decimal percision to round numerical tick labels to
  * @param {number} [roundTo=2]
  * @memberof axis#
  * @property
  */
  roundTo = 2,

  label,


  reverseScaleQ = false

  axis.label = function(_) { return arguments.length ? (label = _, axis) : label; };
  axis.tickTickLabelSpacer = function(_) { return arguments.length ? (tickTickLabelSpacer = _, axis) : tickTickLabelSpacer; };
  axis.tickLabelMargin = function(_) { return arguments.length ? (tickLabelMargin = _, axis) : tickLabelMargin; };
  /**
   * Gets or sets the selection in which items are manipulated
   * @param {d3.selection} [_=none]
   * @returns {axis | d3.selection}
   * @memberof axis
   * @property
   * by default selection = selection
   */

  axis.selection = function(_) { return arguments.length ? (selection = _, axis) : selection; };

  /**
   * Gets or sets the orientation in which items are manipulated
   * (see {@link axis#orient})
   * @param {string} [_=none] should be horizontal or vertical
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default orient="bottom"
   */
  axis.orient = function(_) { return arguments.length ? (orient = _, axis) : orient; };
  /**
   * Gets or sets the amount of horizontal space in which items are manipulated
   * (see {@link axis#spaceX})
   * @param {number} [_=none] should be a number > 0
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default spaceX = undefined
   */
  axis.spaceX = function(_) { return arguments.length ? (spaceX = _, axis) : spaceX; };
  /**
   * Gets or sets the amount of vertical space in which items are manipulated
   * (see {@link axis#spaceY})
   * @param {number} [_=none] should be a number > 0
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default spaceY = undefined
   */
  axis.spaceY = function(_) { return arguments.length ? (spaceY = _, axis) : spaceY; };


  /**
   * Gets / sets whether or not axis is allowed to go beyond specified dimensions
   * (see {@link axis#spaceX})
   * @param {boolean} [_=none]
   * @returns {axis | boolean}
   * @memberof axis
   * @property
   * by default overflowQ = false
   */
  axis.overflowQ = function(_) { return arguments.length ? (overflowQ = _, axis) : overflowQ; };
  /**
   * Gets / sets whether or not axis will display categorial ticks or by numerical value
   * (see {@link axis#categoricalQ})
   * @param {boolean} [_=none]
   * @returns {axis | boolean}
   * @memberof axis
   * @property
   * by default categoricalQ = false
   */
  axis.categoricalQ = function(_) { return arguments.length ? (categoricalQ = _, axis) : categoricalQ; };
  /**
   * Gets / sets whether or not axis ticks should have guidelines
   * (see {@link axis#guideLinesQ})
   * @param {boolean} [_=none]
   * @returns {axis | boolean}
   * @memberof axis
   * @property
   * by default guideLinesQ = false
   */
  axis.guideLinesQ = function(_) { return arguments.length ? (guideLinesQ = _, axis) : guideLinesQ; };


  /**
   * Gets / sets how ticks should be groupped
   * (see {@link axis#grouping})
   * @param {Array[]} [_=none] list of putatively other lists, which should correspond to tickLabels
   * will space tick labels in nested lists closer together than outer lists
   * @returns {axis | Array[]}
   * @memberof axis
   * @property
   * by default grouping = undefined
   */
  axis.grouping = function(_) { return arguments.length ? (grouping = _, axis) : grouping; };


  /**
   * Gets / sets the scale for which non-categorial  ticks should
   * be spaced
   * (see {@link axis#scale})
   * @param {d3.scale} [_=none]
   * @returns {axis | d3.scale}
   * @memberof axis
   * @property
   * by default scale = d3.scaleLinear()
   */
  axis.scale = function(_) { return arguments.length ? (scale = _, axis) : scale; };
  /**
   * Gets / sets the padding for the domain of the scale
   * (see {@link axis#domainPadding})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default domainPadding = 0.5
   */
  axis.domainPadding = function(_) { return arguments.length ? (domainPadding = _, axis) : domainPadding; };


  /**
   * Gets / sets objectSpacer
   * (see {@link axis#objectSpacer})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default objectSpacer = 0.05
   */
  axis.objectSpacer = function(_) { return arguments.length ? (objectSpacer = _, axis) : objectSpacer; };
  /**
   * Gets / sets the minObjectSize
   * (see {@link axis#minObjectSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default minObjectSize = 15
   */
  axis.minObjectSize = function(_) { return arguments.length ? (minObjectSize = _, axis) : minObjectSize; };
  /**
   * Gets / sets the maxObjectSize
   * (see {@link axis#maxObjectSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default maxObjectSize = 50
   */
  axis.maxObjectSize = function(_) { return arguments.length ? (maxObjectSize = _, axis) : maxObjectSize; };


  /**
   * Gets / sets the namespace
   * (see {@link axis#namespace})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default namespace = 'd3sm-axis'
   */
  axis.namespace = function(_) { return arguments.length ? (namespace = _, axis) : namespace; };
  /**
   * Gets / sets the backgroundFill
   * (see {@link axis#backgroundFill})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default backgroundFill = 'transparent'
   */
  axis.backgroundFill = function(_) { return arguments.length ? (backgroundFill = _, axis) : backgroundFill; };
  /**
   * Gets / sets the objectClass
   * (see {@link axis#objectClass})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default objectClass = 'tick-group'
   */
  axis.objectClass = function(_) { return arguments.length ? (objectClass = _, axis) : objectClass; };


  /**
   * Gets / sets the tickLabels
   * (see {@link axis#tickLabels})
   * @param {string[]} [_=none]
   * @returns {axis | string[]}
   * @memberof axis
   * @property
   * by default tickLabels = undefined
   */
  axis.tickLabels = function(_) { return arguments.length ? (tickLabels = _, axis) : tickLabels; };
  /**
   * Gets / sets the tickValues
   * (see {@link axis#tickValues})
   * @param {number[]} [_=none]
   * @returns {axis | number[]}
   * @memberof axis
   * @property
   * by default tickValues = undefined
   */
  axis.tickValues = function(_) { return arguments.length ? (tickValues = _, axis) : tickValues; };
  /**
   * Gets / sets the tickValues
   * (see {@link axis#numberOfTicks})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default numberOfTicks = 5
   */
  axis.numberOfTicks = function(_) { return arguments.length ? (numberOfTicks = _, axis) : numberOfTicks; };


  /**
   * Gets / sets the lineStroke
   * (see {@link axis#lineStroke})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default lineStroke = 'black'
   */
  axis.lineStroke = function(_) { return arguments.length ? (lineStroke = _, axis) : lineStroke; };
  /**
   * Gets / sets the lineStrokeWidth
   * (see {@link axis#lineStrokeWidth})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default lineStrokeWidth = 3
   */
  axis.lineStrokeWidth = function(_) { return arguments.length ? (lineStrokeWidth = _, axis) : lineStrokeWidth; };


  /**
   * Gets / sets the tickStroke
   * (see {@link axis#tickStroke})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default tickStroke = 'black'
   */
  axis.tickStroke = function(_) { return arguments.length ? (tickStroke = _, axis) : tickStroke; };
  /**
   * Gets / sets the tickStrokeWidth
   * (see {@link axis#tickStrokeWidth})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickStrokeWidth = 2
   */
  axis.tickStrokeWidth = function(_) { return arguments.length ? (tickStrokeWidth = _, axis) : tickStrokeWidth; };
  /**
   * Gets / sets the tickLength
   * (see {@link axis#tickLength})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickLength = 10
   */
  axis.tickLength = function(_) { return arguments.length ? (tickLength = _, axis) : tickLength; };


  /**
   * Gets / sets the tickLabelFontSize
   * (see {@link axis#tickLabelFontSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickLabelFontSize = 14
   */
  axis.tickLabelFontSize = function(_) { return arguments.length ? (tickLabelFontSize = _, axis) : tickLabelFontSize; };
  /**
   * Gets / sets the tickLabelMinFontSize
   * (see {@link axis#tickLabelMinFontSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickLabelMinFontSize = 8
   */
  axis.tickLabelMinFontSize = function(_) { return arguments.length ? (tickLabelMinFontSize = _, axis) : tickLabelMinFontSize; };
  /**
   * Gets / sets the tickLabelMaxFontSize
   * (see {@link axis#tickLabelMaxFontSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickLabelMaxFontSize = 20
   */
  axis.tickLabelMaxFontSize = function(_) { return arguments.length ? (tickLabelMaxFontSize = _, axis) : tickLabelMaxFontSize;};


  /**
   * Gets / sets the tickLabelTextAnchor
   * (see {@link axis#tickLabelTextAnchor})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default tickLabelTextAnchor = 'center'
   */
  axis.tickLabelTextAnchor = function(_) { return arguments.length ? (tickLabelTextAnchor = _, axis) : tickLabelTextAnchor; };
  /**
   * Gets / sets the tickLabelRotation
   * (see {@link axis#tickLabelRotation})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default tickLabelRotation = 0
   */
  axis.tickLabelRotation = function(_) { return arguments.length ? (tickLabelRotation = _, axis) : tickLabelRotation; };
  /**
   * Gets / sets the tickLabelFunc
   * (see {@link axis#tickLabelFunc})
   * @param {function} [_=none]
   * @returns {axis | function}
   * @memberof axis
   * @property
   * by default tickLabelFunc = undefined
   */
  axis.tickLabelFunc = function(_) { return arguments.length ? (tickLabelFunc = _, axis) : tickLabelFunc; };


  /**
  * Gets / sets the tickLabelOnClick
  * (see {@link axis#tickLabelOnClick})
  * @param {function} [_=none]
  * @returns {axis | function}
  * @memberof axis
  * @property
  */
  axis.tickLabelOnClick = function(_) { return arguments.length ? (tickLabelOnClick = _, axis) : tickLabelOnClick; };


  /**
   * Gets / sets the guidelineSpace
   * (see {@link axis#guidelineSpace})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default guidelineSpace = undefined
   */
  axis.guidelineSpace = function(_) { return arguments.length ? (guidelineSpace = _, axis) : guidelineSpace; };
  /**
   * Gets / sets the guideLineStroke
   * (see {@link axis#guideLineStroke})
   * @param {string} [_=none]
   * @returns {axis | string}
   * @memberof axis
   * @property
   * by default guideLineStroke = "#333333"
   */
  axis.guideLineStroke = function(_) { return arguments.length ? (guideLineStroke = _, axis) : guideLineStroke; };
  /**
   * Gets / sets the guideLineStrokeWidth
   * (see {@link axis#guideLineStrokeWidth})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default guideLineStrokeWidth = 2
   */
  axis.guideLineStrokeWidth = function(_) { return arguments.length ? (guideLineStrokeWidth = _, axis) : guideLineStrokeWidth; };


  /**
   * Gets / sets the transitionDuration
   * (see {@link axis#transitionDuration})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default transitionDuration = 1000
   */
  axis.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, axis) : transitionDuration; };
  /**
   * Gets / sets the easeFunc
   * (see {@link axis#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {axis | d3.ease}
   * @memberof axis
   * @property
   * by default easeFunc = d3.easeExp
   */
  axis.easeFunc = function(_) { return arguments.length ? (easeFunc = _, axis) : easeFunc; };


  /**
   * Gets / sets the objectSize
   * (see {@link axis#objectSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default objectSize = undefined
   */
  axis.objectSize = function(_) { return arguments.length ? (objectSize = _, axis) : objectSize; };
  /**
   * Gets / sets the spacerSize
   * (see {@link axis#spacerSize})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default spacerSize = undefined
   */
  axis.spacerSize = function(_) { return arguments.length ? (spacerSize = _, axis) : spacerSize; };

  /**
   * Gets / sets the roundTo
   * (see {@link axis#roundTo})
   * @param {number} [_=none]
   * @returns {axis | number}
   * @memberof axis
   * @property
   * by default roundTo = 2
   */
   axis.roundTo = function(_) { return arguments.length ? (roundTo = _, axis) : roundTo; };
   axis.reverseScaleQ = function(_) { return arguments.length ? (reverseScaleQ = _, axis) : reverseScaleQ; };


   axis.tickLabelOnHoverFunc = function(_) {return arguments.length ? (tickLabelOnHoverFunc = _, axis) : tickLabelOnHoverFunc; };


  function axis () {
    // for convenience in handling orientation specific values
    var horizontalQ = utils.arr.hasQ(['top', 'bottom', 'horizontal'], orient) ? true : false
    var verticalQ = !horizontalQ

    // background cliping rectangle
    var bgcpRect = {x:0, y:0, width: spaceX, height:spaceY}
    // modify the rect based on axis orientation
    if (orient == "left") {
      bgcpRect.x -= spaceX;
      if(guideLinesQ) { bgcpRect.width += guidelineSpace };
      /* these two lines increase the clipping rect to allow for text at the edge of the axis */
      bgcpRect.y -= tickLabelMaxFontSize;
      bgcpRect.height += 2*tickLabelMaxFontSize
    }
    if (orient == "bottom"){
      bgcpRect.y = bgcpRect.y;
      if(guideLinesQ) { bgcpRect.y -= guidelineSpace; bgcpRect.height += guidelineSpace; };
      /* these two lines increase the clipping rect to allow for text at the edge of the axis */
      bgcpRect.x -= tickLabelMaxFontSize;
      bgcpRect.width += 2*tickLabelMaxFontSize
    }
    if (orient == "top") {
      bgcpRect.y -= spaceY;
      if(guideLinesQ) { bgcpRect.height += guidelineSpace };
      /* these two lines increase the clipping rect to allow for text at the edge of the axis */
      bgcpRect.y -= tickLabelMaxFontSize;
      bgcpRect.height += 2*tickLabelMaxFontSize
    }
    if (orient == "right") { bgcpRect.x = 0;
      if(guideLinesQ) { bgcpRect.width += guidelineSpace; bgcpRect.x -= guidelineSpace };
      /* these two lines increase the clipping rect to allow for text at the edge of the axis */
      bgcpRect.y -= tickLabelMaxFontSize;
      bgcpRect.height += 2*tickLabelMaxFontSize
    }


    var container = utils.sel.setupContainer( selection, namespace, bgcpRect, backgroundFill );

    // defaults for text-anchor and text rotation
    if (orient == 'top') {
      tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'start' : tickLabelTextAnchor
      tickLabelRotation = tickLabelRotation == undefined ? -90 : tickLabelRotation
    }
    if (orient == 'bottom') {
      tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'end' : tickLabelTextAnchor
      tickLabelRotation = tickLabelRotation == undefined ? -90 : tickLabelRotation
    }
    if (orient == 'left') {
      tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'end' : tickLabelTextAnchor
      tickLabelRotation = tickLabelRotation == undefined ? 0 : tickLabelRotation
    }
    if (orient == 'right') {
      tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'start' : tickLabelTextAnchor
      tickLabelRotation = tickLabelRotation == undefined ? 0 : tickLabelRotation
    }

    /*
    If categorical:
      -> use grouping if defined,
      -> else use the labels provided
    else:
      if grouping undefined
        and no specified number of tickes
          -> make numberOfTick ticks
          -> else use provided tick values
        -> use grouping
    */
    var tickData = categoricalQ
    ? (grouping == undefined)
      ? tickLabels
      : grouping
    : (grouping == undefined)
      ? (numberOfTicks != undefined)
      // ? (tickValues.length < numberOfTicks)
        ? (utils.math.tickRange(...d3.extent(tickValues), numberOfTicks))
        : tickValues
      : grouping


    var flatTickData = utils.arr.flatten(tickData)
    var numberOfObjects = flatTickData.length
    var space = horizontalQ ? spaceX : spaceY
    var extent = d3.extent(flatTickData)


    if (reverseScaleQ) {extent.reverse()}
    var domain = reverseScaleQ
    ? [extent[0] + domainPadding, extent[1] - domainPadding]
    : [extent[0] - domainPadding, extent[1] + domainPadding]

    scale
    .domain(domain)
    .range([horizontalQ ? 0 : spaceY, horizontalQ ? spaceX : 0])


    /*
    Scales are based on the values of the chart and correspond to the spacings of the
    chart. If the chart has already been rendered, these values (expensive to caluclate) can
    be passed to axis to prevent recalculation.
    */

    // calculate object size if needed
    objectSize = (objectSize == undefined)
    ? utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ)
    : objectSize

    // calculate spacer size if needed
    spacerSize = (spacerSize == undefined)
    ? utils.math.calculateWidthOfSpacer(flatTickData, space, objectSize, numberOfObjects, objectSpacer, overflowQ)
    : spacerSize


    var objClass = utils.str.hypenate(namespace, categoricalQ ? objectClass+'-categorical' : objectClass)

    var spacerFunction = groupingSpacer()
    .horizontalQ(horizontalQ).scale(scale).moveby((categoricalQ?'category':'scale')).numberOfObjects(numberOfObjects)
    .objectClass(objClass).objectSize(objectSize).spacerSize(spacerSize)
    .transitionDuration(transitionDuration).easeFunc(easeFunc)
    .namespace(namespace)

    var tickEnterAnimation = function(sel){
      var mt = scale(sel.datum()),
      dist = scale(extent[1]) * 2,
      k = (mt < extent[1] / 2) ? 1 : -1
      k = horizontalQ ? k * -1 : k
      sel.attr('transform', function (d, i) {
        var
        x = horizontalQ ?  dist * k : 0,
        y = !horizontalQ ? dist * k : 0,
        t = 'translate('+x+','+y+')'
        return t
      })
    }
    var tickExitAnimation = function(sel) {
      var mt = scale(sel.datum()),
      dist = scale(extent[1]) * 2,
      k = (mt < extent[1] / 2) ? 1 : -1
      k = horizontalQ ? k * -1 : k
      sel.transition().duration(transitionDuration).ease(easeFunc)
      .style('opacity', 0)
      .attr('transform', function (d, i) {
        var

        x = horizontalQ ?  dist * k  : 0,
        y = !horizontalQ ? dist * k : 0,
        t = 'translate('+x+','+y+')'
        return t
      }).remove()
    }

    if (!categoricalQ){
      spacerFunction.enterFunction(tickEnterAnimation)
      spacerFunction.exitFunction(tickExitAnimation)
    }

    // move tick containers
    spacerFunction(container, tickData, 0)

    // move by for x and y needed to center categorical ticks, labels, and guidelines
    function moveXBy(d, i, horizontalQ, categoricalQ, objectSize){
      return (horizontalQ)
      ? (categoricalQ)
        ? objectSize / 2
        : 0
      : 0
    }

    function moveYBy(d, i, verticalQ, categoricalQ, objectSize){
      return (verticalQ)
      ? (categoricalQ)
        ? objectSize / 2
        : 0
      : 0
    }



    var labelNameGroup = utils.sel.safeSelect(selection, 'g', utils.str.hypenate(namespace,'axis-name'))



    var labelElement = utils.sel.safeSelect(labelNameGroup, 'text', utils.str.hypenate(namespace, 'name'))
    if (labelElement != undefined) {
      labelElement.text(label)

      if (orient == 'left' || orient == 'right') {
        labelElement.attr('transform', 'rotate(-90)')
      }

      var bbox = labelElement.node().getBoundingClientRect()
      labelNameGroup.attr('transform',function(d, i){
        var
        x = 0,
        y = 0,
        t

        if (orient == 'bottom') {
          x = spaceX - bbox.width - tickLabelMargin
          y = - tickTickLabelSpacer
        }
        else if (orient == 'top') {
          x = spaceX - bbox.width - tickLabelMargin
          x = tickLabelMargin
          y = bbox.height + tickTickLabelSpacer
        }
        else if (orient == 'left') {
          x = bbox.width + tickTickLabelSpacer
          y = bbox.height + tickLabelMargin
        } else if (orient == 'right') {
          x = -(bbox.width + tickTickLabelSpacer)
          y = bbox.height + tickLabelMargin
        } else {

        }
        t = 'translate('+x+','+y+')'
        return t
      })



    } else {
      labelElement.remove()
    }
    /*
    Idea from Stack Overflow
    https://stackoverflow.com/questions/50579535/d3-js-v4-truncate-text-to-fit-in-fixed-space/50585022?noredirect=1#comment88235562_50585022
    to use clip path to make things fit in fixed size. Have yet got this to work nicely.
    */
    // var defs = d3.select(container.node().parentNode).select('defs')
    // var tickLabelClipPath = utils.sel.safeSelect(defs, 'clipPath', utils.str.hypenate(namespace,'tick-label-clip-path')).attr('id',  utils.str.hypenate(namespace,'tick-label-clip-path'))
    // var tickLabelClipPathRect = utils.sel.safeSelect(tickLabelClipPath, 'rect',  utils.str.hypenate(namespace,'tick-label-clip-path-rect'))
    // .attr('x', 0)
    // .attr('y', 0)
    // .attr('width', function(d, i){
    //   if (horizontalQ) { return tickLabelFontSize }
    //   if (verticalQ) { return spaceX - tickLength }
    // })
    // .attr('height', function(d, i){
    //   if (verticalQ) { return tickLabelFontSize }
    //   if (horizontalQ) { return spaceY - tickLength }
    // })


    // for each tick container
    var ticks = container.selectAll('g:not(.to-remove).'+objClass).each(function(d, i){
      var that = d3.select(this).style('opacity', 1)

      // make and move tick
      var tick = utils.sel.safeSelect(that, 'line', utils.str.hypenate(namespace,'tick'))
      .attr("x1", 0)
      .attr("x2", horizontalQ ? 0 : orient == "left" ? -tickLength : tickLength)
      .attr("y1", 0)
      .attr('y2',  verticalQ ? 0 : orient == "top" ? -tickLength : tickLength)
      .attr('stroke', tickStroke)
      .attr('stroke-width', tickStrokeWidth)
      .attr('transform', function(d, i) {
        var
        x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
        y = moveYBy(d, i, verticalQ, categoricalQ, objectSize),
        t = 'translate('+x+','+y+')'
        return t
      })

      // make and move label
      var label = utils.sel.safeSelect(that, 'text', utils.str.hypenate(namespace,'label'))
      .text(function(d, i){
        var s = typeof d == 'number' ? utils.math.round(d, roundTo) : d
        s = utils.str.truncateString(String(s), (horizontalQ ? spaceY : spaceX) - tickLength-tickLabelMargin-tickTickLabelSpacer, tickLabelFontSize * 0.45)
        return s
      })
      .attr('font-size', tickLabelFontSize)
      .attr('text-anchor', tickLabelTextAnchor)
      // utils.str.truncateText(label, label.text(), orient, tickLength, horizontalQ ? spaceY : spaceX, overflowQ)

      label.attr('transform', function(d, i) {
        var
        rect = d3.select(this).node().getBoundingClientRect(),
        leng = d3.select(this).node().getComputedTextLength(),
        x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
        y = moveYBy(d, i, verticalQ, categoricalQ, objectSize)
        // on recall, rect changes because of rotation so need Math.min(rect.height, rect.width)

        var s = Math.sin(tickLabelRotation) * leng * 0

        if (orient == 'top') {
          y = -(tickLength+tickTickLabelSpacer);
          // y = tickLength+tickTickLabelSpacer;

          // y -= Math.max(rect.height, rect.width);
          x += Math.min(rect.height, rect.width) * 0.25
          // x -= leng * 0.25 + s
        }
        if (orient == 'bottom') {
          y = tickLength+tickTickLabelSpacer;
          x += Math.min(rect.height, rect.width) * 0.25
          // x += leng * 0.25 - s
        }
        if (orient == 'left') {
          x -= (tickLength+tickTickLabelSpacer);
          // y += rect.height * 0.5; y-= rect.height/4
          y += Math.min(rect.height, rect.width) * 0.25
          // y += leng * 0.25
        }
        if (orient == 'right') {
          x += (tickLength+tickTickLabelSpacer);
          // y += Math.min(rect.height, rect.width) * 0.25
          // y += leng * 0.25
          y += rect.height * 0.5; y-= rect.height/4
        }

        var
        t = 'translate('+x+','+y+')',
        r = 'rotate('+tickLabelRotation+')'
        return t + r
      })
      .on('mousemove', labelHover)
      .on('mouseout', labelHoverOff)
      .on('click', tickLabelOnClick)
      // .attr('clip-path', 'url(#'+utils.str.hypenate(namespace,'tick-label-clip-path')+')')

      // add guidlines as needed
       if (guideLinesQ) {
         var gline = utils.sel.safeSelect(that, 'line', utils.str.hypenate(namespace, 'guideline'))
         .transition().duration(transitionDuration).ease(easeFunc)
         .attr("x1", 0)
         .attr("x2", horizontalQ ? 0 : orient == "left" ? guidelineSpace : -guidelineSpace)
         .attr("y1", 0)
         .attr('y2',  verticalQ ? 0 : orient == "top" ? guidelineSpace : -guidelineSpace)
         .attr('transform', function(d, i) {
           var
           x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
           y = moveYBy(d, i, verticalQ, categoricalQ, objectSize),
           t = 'translate('+x+','+y+')'
           return t
         })
       } else { that.select('line.'+utils.str.hypenate(namespace, 'guideline')).remove() }

    })

    // apply alternating guidline thickness
    if (guideLinesQ) {
      container.selectAll('.'+utils.str.hypenate(namespace,'guideline'))
      .attr('stroke', function(d, i){
        if (i % 2 == 0) { return utils.color.modifyHexidecimalColorLuminance(guideLineStroke, 0.8) }
        return guideLineStroke
      })
      .attr('stroke-width', function(d, i){
        if (i % 2 == 0) { return guideLineStrokeWidth *0.8}
        return guideLineStrokeWidth
      })
      .attr('minor', function(d, i){return i%2 == 0})
    }


    /***************************************************************************
    ** Make the line of the axis
    ***************************************************************************/
    var line = utils.sel.safeSelect(selection, 'path', utils.str.hypenate(namespace,'line'))
    // .attr('x1', 0)
    // .attr('x2', horizontalQ ? spaceX : 0)
    // .attr('y1', 0)
    // .attr('y2', horizontalQ ? 0 : spaceY)
    .attr('d',
      horizontalQ
      ? 'M 0,0 H' + spaceX + ',0'
      : 'M 0,0 V 0,' + spaceY
    )
    .attr('stroke', lineStroke)
    .attr('stroke-width', lineStrokeWidth)
    .classed('axis-line', true)


  }

  // hover of label show full text label in case it is truncated
  function labelHover(d, i){
    var t = d3.select(this).style('fill', 'red')
    d3.select(t.node().parentNode).select("line."+utils.str.hypenate(namespace,'tick'))
    .attr("stroke", 'red')
    .attr("stroke-width", tickStrokeWidth*2)

    if (guideLinesQ) {
      d3.select(t.node().parentNode).select('line.'+utils.str.hypenate(namespace, 'guideline'))
      .attr('stroke', 'red')
      .attr('stroke-width', guideLineStrokeWidth*2)
    }

    var s = typeof d == 'number' ? utils.math.round(d, roundTo) : d

    var m = d3.mouse(d3.select('html').node())
    var div = utils.sel.safeSelect(d3.select('body'), 'div', utils.str.hypenate(namespace,'guideline-tooltip'))
    .attr('id', utils.str.hypenate(namespace,'guideline-tooltip'))
    .style('position', 'absolute')
    .style('left', (d3.event.pageX+15)+'px')
    .style('top', (d3.event.pageY+15)+'px')
    .style('background-color', 'white')
    .style('border-color', 'black')
    // .style('min-width', (tickLabelFontSize * (String(s).split('.')[0].length+3))+'px')
    // .style('min-height', (tickLabelFontSize * (String(s).split('.')[0].length+3))+'px')
    .style('border-radius', '10px')
    .style('display', 'flex')
    .style('justify-content', 'center')
    .style('text-align', 'middle')
    .style('padding', 4+"px")

    .style('border-style', 'solid')
    .style('border-width', 2)

    var text = utils.sel.safeSelect(div, 'div')
    .text(tickLabelOnHoverFunc(s, i))
    .style('color', 'black')
    .style('align-self', 'center')

    var bbox = div.node().getBoundingClientRect()
    if (bbox.x + bbox.width > window.innerWidth) {
      div.style('left', (d3.event.pageX-15-300)+'px')
    }
  }

  function labelHoverOff(d, i){
    var t = d3.select(this).style('fill', 'black')
    d3.select(t.node().parentNode).select("line."+utils.str.hypenate(namespace,'tick'))
    .attr("stroke", tickStroke)
    .attr("stroke-width", tickStrokeWidth)

    if (guideLinesQ) {
      var gline = d3.select(t.node().parentNode).select('line.'+utils.str.hypenate(namespace, 'guideline'))
      var minorQ = gline.attr('minor')
      gline.attr('stroke', function(d, ii){
        if (minorQ == 'true') { return utils.color.modifyHexidecimalColorLuminance(guideLineStroke, 0.8) }
        return guideLineStroke
      })
      .attr('stroke-width', function(d, ii){
        if (minorQ == 'true') { return guideLineStrokeWidth *0.8}
        return guideLineStrokeWidth
      })
    }
    d3.select("#"+utils.str.hypenate(namespace,'guideline-tooltip')).remove()
  }



  return axis
}
