import * as d3 from 'd3'
import {con} from './utils/index.js'
/*******************************************************************************
**                                                                            **
**                                                                            **
**                              SPACEGROUPING                                 **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Produces a function for spacing objects by an arbitrarly complex grouping
 * @returns {recursivelyPosition} the function for moving the objects
 * (see {@link groupingSpacer#recursivelyPosition})
 * @namespace groupingSpacer
 */
export default function groupingSpacer() {
  var
  /*@var {boolean} horizontalQ @default*/

  /**
  * Whether or not to space objects horizontally or vertically.
  * (see {@link groupingSpacer.horizontalQ})
  * @param {boolean} [horizontalQ=true]
  * @memberof groupingSpacer#
  * @instance
  */
  horizontalQ = true,
  /**
  * The scale to use to position elements if {@link groupingSpacer#moveby}="string"
  * (see {@link groupingSpacer.scale})
  * @param {d3.scale} [scale=d3.scaleLinear()]
  * @memberof groupingSpacer#
  * @instance
  */
  scale = d3.scaleLinear(),
  /**
  * How elements in the complex grouping should be moved over by.
  * By default, moveby="category", which moves objects by the complex grouping
  * But objects can also be moved over by scale.
  * (see {@link groupingSpacer.moveby})
  * @param {string} [moveby="category"]
  * @memberof groupingSpacer#
  * @instance
  */
  moveby = 'category',
  /**
  * How many objects are there in total
  * (see {@link groupingSpacer.numberOfObjects})
  * @param {number} [numberOfObjects=none]
  * @memberof groupingSpacer#
  * @instance
  */
  numberOfObjects,
  /**
  * The class given to an nested <g> tag whose parent(s) have the correct transition
  * properties
  * (see {@link groupingSpacer.numberOfObjects})
  * @param {string} [numberOfObjects='d3sm-groupped-item']
  * @memberof groupingSpacer#
  * @instance
  */
  objectClass = 'd3sm-groupped-item',
  /**
  * The size of the objects being positioned
  * (see {@link groupingSpacer.objectSize})
  * @param {number} [objectSize=none]
  * @memberof groupingSpacer#
  * @instance
  */
  objectSize,
  /**
  * The size of the un-nested spacer between objects
  * (see {@link groupingSpacer.spacerSize})
  * @param {number} [spacerSize=none]
  * @memberof groupingSpacer#
  * @instance
  */
  spacerSize,
  /**
  * The duration of transitions in ms
  * (see {@link groupingSpacer.transitionDuration})
  * @param {number} [transitionDuration=1000]
  * @memberof groupingSpacer#
  * @instance
  */
  transitionDuration = 1000,
  /**
  * The ease function for the transitions
  * (see {@link groupingSpacer.easeFunc})
  * @param {d3.ease} [easeFunc=d3.easeSin]
  * @memberof groupingSpacer#
  * @instance
  */
  easeFunc = d3.easeSin,
  /**
  * The namespace for the objects being moved
  * (see {@link groupingSpacer.namespace})
  * @param {string} [namespace='spacer']
  * @memberof groupingSpacer#
  * @instance
  */
  namespace = 'spacer',
  /**
  * The animation for new objects being added
  * (see {@link groupingSpacer.enterFunction})
  * @param {function} enterFunction
  * @memberof groupingSpacer#
  * @instance
  * @example
  * // by default
  * function(newObjectSelection) {
  *  newObjectSelection.attr('transform', function(d, i){
  *    var
  *    x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
  *    y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
  *    t = 'translate('+x+','+y+')'
  *    return t
  *  })
  * }
  */
  enterFunction = function(cur) {
    cur.attr('transform', function(d, i){
      var
      // x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
      // y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
      x = horizontalQ ? window.outerWidth : 0,
      y = !horizontalQ ? window.outerWidth : 0,
      t = 'translate('+x+','+y+')'
      // if(y == undefined) {console.con.log(cur.node(), y, d)}
      return t
    })
  },
  /**
  * The animation for old objects being removed
  * (see {@link groupingSpacer.exitFunction})
  * @param {function} exitFunction
  * @memberof groupingSpacer#
  * @instance
  * @example
  * // by default
  * oldObjectSelection.transition().duration(transitionDuration).ease(easeFunc)
  * .attr('transform', function(d, i){
  *     var
  *   x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
  *   y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
  *   t = 'translate('+x+','+y+')'
  *   return t
  * }).remove()
  */
  exitFunction = function(cur){
    con.log("groupingSpacer", "exiting with", {current: cur, currentNode: cur.node()})
    cur.selectAll('g').classed('to-remove', true)

    cur.transition().duration(transitionDuration*0.9).ease(easeFunc)
    .attr('transform', function(d, i){
      var
      // x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
      // y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
      x = horizontalQ ? window.outerWidth : 0,
      y = !horizontalQ ? window.outerWidth : 0,
      t = 'translate('+x+','+y+')'
      // if(y == undefined) {console.con.log(cur.node(), y, d)}
      return t
    }).remove()
  }

  /**
   * Gets / sets horizontalQ (whether or not to space objects horizontally or vertically).
   * (see {@link groupingSpacer#horizontalQ})
   * @param {string} [_=none]
   * @returns {groupingSpacer | string}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.horizontalQ = function(_) { return arguments.length ? (horizontalQ = _, recursivelyPosition) : horizontalQ }
  /**
   * Gets / sets the scale to use to position elements if {@link groupingSpacer#moveby}="string"
   * (see {@link groupingSpacer#scale})
   * @param {d3.scale} [_=none]
   * @returns {groupingSpacer | d3.scale}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.scale = function(_) { return arguments.length ? (scale = _, recursivelyPosition) : scale }
  /**
   * Gets / sets moveby (whether or not to move by scale or by grouping).
   * (see {@link groupingSpacer#moveby})
   * @param {string} [_=none]
   * @returns {groupingSpacer | string}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.moveby = function(_) { return arguments.length ? (moveby = _, recursivelyPosition) : moveby }
  /**
   * Gets / sets numberOfObjects.
   * (see {@link groupingSpacer#numberOfObjects})
   * @param {number} [_=none]
   * @returns {groupingSpacer | number}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.numberOfObjects = function(_) { return arguments.length ? (numberOfObjects = _, recursivelyPosition) : numberOfObjects }
  /**
   * Gets / sets the objectClass (will be applied to <g> elements).
   * (see {@link groupingSpacer#objectClass})
   * @param {string} [_=none]
   * @returns {groupingSpacer | string}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.objectClass = function(_) { return arguments.length ? (objectClass = _, recursivelyPosition) : objectClass }
  /**
   * Gets / sets the objectSize.
   * (see {@link groupingSpacer#objectSize})
   * @param {number} [_=none]
   * @returns {groupingSpacer | number}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.objectSize = function(_) { return arguments.length ? (objectSize = _, recursivelyPosition) : objectSize }
  /**
   * Gets / sets the spacerSize.
   * (see {@link groupingSpacer#spacerSize})
   * @param {number} [_=none]
   * @returns {groupingSpacer | number}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.spacerSize = function(_) { return arguments.length ? (spacerSize = _, recursivelyPosition) : spacerSize }
  /**
   * Gets / sets the transitionDuration.
   * (see {@link groupingSpacer#transitionDuration})
   * @param {number} [_=none]
   * @returns {groupingSpacer | number}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.transitionDuration = function(_) { return arguments.length ? (transitionDuration = _, recursivelyPosition) : transitionDuration }
  /**
   * Gets / sets the easeFunc.
   * (see {@link groupingSpacer#easeFunc})
   * @param {d3.ease} [_=none]
   * @returns {groupingSpacer | d3.ease}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.easeFunc = function(_) { return arguments.length ? (easeFunc = _, recursivelyPosition) : easeFunc }
  /**
   * Gets / sets the namespace.
   * (see {@link groupingSpacer#namespace})
   * @param {string} [_=none]
   * @returns {groupingSpacer | string}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.namespace = function(_) { return arguments.length ? (namespace = _, recursivelyPosition) : namespace }
  /**
   * Gets / sets the enterFunction.
   * (see {@link groupingSpacer#enterFunction})
   * @param {function} [_=none]
   * @returns {groupingSpacer | function}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.enterFunction = function(_) { return arguments.length ? (enterFunction = _, recursivelyPosition) : enterFunction }
  /**
   * Gets / sets the exitFunction.
   * (see {@link groupingSpacer#exitFunction})
   * @param {function} [_=none]
   * @returns {groupingSpacer | function}
   * @memberof groupingSpacer
   * @static
   */
  recursivelyPosition.exitFunction = function(_) { return arguments.length ? (exitFunction = _, recursivelyPosition) : exitFunction }


  /**
   * recursively position the objects inside of the selection.
   * @param {d3.selection} selection
   * @param {Object} data
   * @param {level} [level=0] recursion depth
   * @returns {number} (how much to move next element)
   * @memberof groupingSpacer#
   */
  function recursivelyPosition(selection, data, level) {
    if ( level == undefined ) { level = 0;  }

    var currentSelection = selection.selectAll('g.'+namespace+'[level="'+level+'"]').data(data)
    var enter = currentSelection.enter().append('g').attr('level', level).attr('class', namespace)
    var exit = currentSelection.exit()
    currentSelection = currentSelection.merge(enter)


    if (typeof exitFunction == 'function' ){ exit.each(function(d, i){ exitFunction(d3.select(this))}) }
    else{exit.remove()}
    // spacer for current level
    var levelSpacer = spacerSize / (level+1)
    // movement for current level
    var move = 0
    currentSelection.each(function(currentElement, index) {
      var t = d3.select(this)
      if (t.attr('transform') == undefined && typeof enterFunction == 'function') { enterFunction(t) }

      t.transition().duration(transitionDuration).ease(easeFunc)
      .attr('transform', function(d, i) {
        var
        x = horizontalQ ? (moveby =="scale" ? scale(d) : move) : 0,
        y = !horizontalQ ? (moveby =="scale" ? scale(d) : move): 0,
        t = 'translate('+x+','+y+')'
        return t
      })

      if (Array.isArray(currentElement)) {
        move += recursivelyPosition(t, currentElement, level+1)
        var toRemove = t.selectAll('g.'+namespace+'[level="'+(level)+'"] > g.'+objectClass+'.'+namespace)
        if (typeof exitFunction == 'function' ){ toRemove.each(function(d, i){ exitFunction(d3.select(this))}) }
        else{toRemove.remove()}
      }
      else {
        move += objectSize
        var obj = t.select('g.'+namespace+'[level="'+level+'"] > g.'+objectClass+'.'+namespace)
        if (obj.empty()) { obj = t.append('g').attr('class', objectClass).classed(namespace, true) }
        obj.attr('parent-index', index)
        var toRemove = t.selectAll('g.'+namespace+'[level="'+(level+1)+'"]')

        if (typeof exitFunction == 'function' ){ toRemove.each(function(d, i){ exitFunction(d3.select(this))}) }
        else{toRemove.remove()}
      }
      move += (index == currentSelection.size()-1) ? 0 : levelSpacer
    })
    return move
  }
  return recursivelyPosition
}
