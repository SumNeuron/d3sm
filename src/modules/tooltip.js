// import { event, mouse} from 'd3-selection';
import * as d3 from 'd3'
import {sel, con, math} from './utils/index.js'
/*******************************************************************************
**                                                                            **
**                                                                            **
**                                 TOOLTIP                                    **
**                                                                            **
**                                                                            **
*******************************************************************************/
/**
 * Produces a function for handling the tooltip
 *
 * {@link https://sumneuron.gitlab.io/d3sm/demos/tooltip-design/index.html Demo}
 * @param {d3.selection} selection
 * @returns {tooltip}
 * @namespace tooltip
 */
export default function tooltip( selection ) {

  var
  keys,
  values,
  header,
  data,
  selection

  /**
   * Gets / sets the keys to be displayed in the tooltip.
   * If not set, uses d3.keys(data[key])
   * @param {string[]} [_=none]
   * @returns {tooltip | string[]}
   * @memberof tooltip
   */
  tooltip.keys = function(_){return arguments.length ? (keys = _, tooltip) : keys};
  /**
   * Gets / sets the values to be displayed next to the keys.
   * If not set, uses data[key][keys[i]].
   * If a function, gets passed currentData (data[key]) and keys[i].
   * @param {*[]} [_=none]
   * @returns {tooltip | *[]}
   * @memberof tooltip
   */
  tooltip.values = function(_){return arguments.length ? (values = _, tooltip) : values};
  /**
   * Gets / sets the header to be displayed in the tooltip.
   * If not set, uses key
   * @param {string} [_=none]
   * @returns {tooltip | string}
   * @memberof tooltip
   */
  tooltip.header = function(_){return arguments.length ? (header = _, tooltip) : header};
  /**
   * Gets / sets the data (over the selection) to be used for the tooltip
   * @param {Object} [_=none]
   * @returns {tooltip | Object}
   * @memberof tooltip
   */
  tooltip.data = function(_){return arguments.length ? (data = _, tooltip) : data};
  /**
   * Gets / sets the selection for the tooltip to be applied on
   * @param {d3.selection} [_=none]
   * @returns {tooltip | d3.selection}
   * @memberof tooltip
   */
  tooltip.selection = function(_){return arguments.length ? (selection = _, tooltip) : selection};

  tooltip.mousemove = function(_){return arguments.length ? (mousemove = _, tooltip) : mousemove};
  tooltip.mouseout = function(_){return arguments.length ? (mouseout = _, tooltip) : mouseout};

  /**
   * Bind, via selection.on(), the mousemove and mouseout events
   * @returns undefined
   */
  function tooltip( ) {
    selection.on('mouseover', mousemove)
    selection.on('mousemove', mousemove)
    selection.on('mouseout',  mouseout)
    selection.on('mouseleave', mouseout)
  }

  function mouseout(key, i) {
    d3.selectAll(".d3sm-tooltip").remove()
  }


  /**
   * Produces the tooltip on mousemove
   * @param {string} key of the object targeted by the mousemove
   * @param {number} i (index) of the object targeted by mousemove
   * @memberof tooltip
   * @private
   */
  function mousemove(key, i) {
    con.consoleGroup('d3sm-tooltip')
    var currentData = data[key]

    var [x, y] = d3.mouse(d3.select("html").node())
    con.log('tooltip', 'mousemove detected',{key: key, index: i, x:x, y:y})
    con.log('tooltip', 'current data', currentData)



    var div = sel.safeSelect(d3.select('html'), 'tooltip', 'd3sm-tooltip')
    .classed('card', true)
    .style('max-width', '300px')
    .style('background-color', "#212529")
    .style('color', 'white')



    var cardBody = sel.safeSelect(div, 'div', 'card-body')
    var cardTitle = sel.safeSelect(cardBody, 'h5', 'card-title')
    .text(header == undefined ? key : typeof header == 'function' ? header(key, currentData, i) : header)
    .style('color', 'cyan')


    var table = sel.safeSelect(cardBody, 'table', 'table').classed('table-dark', true)
    var tBody = sel.safeSelect(table, 'tbody')

    tBody = tBody.selectAll('tr')
    tBody= tBody.data(keys == undefined ? d3.keys(currentData): keys)
    tBody.exit().remove()


    var tr = tBody.enter().append('tr').style('max-width', '300px')
    tr.append('td').attr('class', function(d, i){return 'tooltip-key'})
    tr.append('td').attr('class',  function(d, i, j){return 'tooltip-value'})
    .attr('tooltip-row-index', function(d, i){return i})

    // tBody = tBody.merge(tr)
    con.consoleGroup('tooltip-rows')
    tBody.selectAll('.tooltip-key').text(function(d, i){return d})
    tBody.selectAll('tr .tooltip-value')
    .text(function(d, i){
      con.log('tooltip', 'trying to set value', {rowKey: d, rowIndex: i})
      var i = d3.select(this).attr('tooltip-row-index')
      var v = currentData[d];


      if (values != undefined) {v = values[i]; if(typeof v == "function") {v = v(key, currentData, d)}}
      return  typeof v == 'number' ? math.round(v, 5) : v
    })
    con.consoleGroupEnd()
    con.consoleGroupEnd()

    x += 15
    // x += 15
    var bbox = div.node().getBoundingClientRect()
    if (x + bbox.width > window.innerWidth - window.scrollX) { x = d3.event.pageX - bbox.width - 15 }
    if (y + bbox.height > window.innerHeight  - window.scrollY) { y = d3.event.pageY - bbox.height - 15 }
    div.style('position') == "relative"
    ? div.style('position', 'absolute').style('left', x+'px').style('top', y+'px')
    : div.style('left', x+'px').style('top', y+'px')
    // .transition().duration(200).ease(d3.easeSin)

    // if (bbox.x + bbox.width > window.innerWidth) {
    //   div.style('left', (d3.event.pageX-15-bbox.width)+'px')
    // }
    // if (bbox.y + bbox.height > window.innerHeight) {
    //   div.style('top', (d3.event.pageY-15-bbox.height)+'px')
    // }

    div.attr('z-index', 10000)
  }

  return tooltip
}
