function heatmap ( selection ) {
  var
  chart,
  xAxis,
  yAxis,
  legend,

  data,
  namespace = 'heatmap',
  title = 'Heatmap'

  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.title = function(_){return arguments.length ? (title = _, plot) : title; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };

  plot.chart = function(_){return arguments.length ? (chart = _, plot) : chart; };
  plot.xAxis = function(_){return arguments.length ? (xAxis = _, plot) : xAxis; };
  plot.yAxis = function(_){return arguments.length ? (yAxis = _, plot) : yAxis; };
  plot.legend = function(_){return arguments.length ? (legend = _, plot) : legend; };

  function plot(){
    var
    orient = '2D',

    titleContainer = d3sm.utils.sel.safeSelect(selection, 'h5', d3sm.utils.str.hypenate(namespace,'title')).text(title),

    selections = d3sm.utils.misc.setupStandardChartContainers(
      selection,
      namespace,
      {w:Math.min(window.innerWidth,1200), h:Math.min(window.innerHeight,700)},
      {top:0.01, bottom:0.01, left:0.01, right:0.01},
      {w:1, h:1}, // percent of container space for svg
      {y:0.1,x:0.2}, // percent of container space for axes,
      {x:25, margin:05} // absolute width of legendSelect and space on either size
    ),

    container = selections.svg.selection,
    chartSelection = selections.plot.selection,
    xAxisSelection = selections.xAxis.selection,
    yAxisSelection = selections.yAxis.selection,
    legendSelect = selections.legend.selection


    if (chart == undefined){ chart = d3sm.charts.heatmap(chartSelection) }
    if (yAxis == undefined) { yAxis = d3sm.axis(yAxisSelection) }
    if (xAxis == undefined) { xAxis = d3sm.axis(xAxisSelection) }
    if (legend == undefined) {
      legend = d3sm.legends.numeric(legendSelect)
      .spaceX(selections.legend.rect.w).spaceY(selections.plot.rect.h)

    }

    // d3sm.debugQ = true
    var overflowQ = true

    chart
    .spaceX(selections.plot.rect.w)
    .spaceY(selections.plot.rect.h)
    .xKey('ykey')
    .yKey('xkey')
    .vKey('padj')
    // .minObjectSize(25)
    // .maxObjectSize(100)
    // .domainPadding(0.01)
    .overflowQ(overflowQ)
    .data(data)

    chart.tooltip()
    .header(function(k, cD){return cD.experimentId})
    .keys(['padj', 'log2FoldChange', 'xkey', 'ykey'])

    chart.colorFunction().colorBy('value')
    .valueExtractor(function(k, v, i){ return data[k].padj })

    chart()

    legend
    .min(d3.min(chart.vValues()))
    .max(d3.max(chart.vValues()))
    .colorFunction(chart.colorFunction())
    .roundTo(5)
    legend()

    var xk = chart.xValues(), yk = chart.yValues()

    // console.log(xk, yk)
    yAxis
    .spaceX(selections.yAxis.rect.w)
    .spaceY(selections.yAxis.rect.h)
    .orient("left")
    .overflowQ(overflowQ)
    .categoricalQ(true)
    .tickLabels(yk)
    .namespace(d3sm.utils.str.hypenate(namespace,'axis-left'))
    .minObjectSize(chart.yMinObjectSize())
    .maxObjectSize(chart.yMaxObjectSize())
    .objectSpacer(chart.objectSpacer())
    .spacerSize(0)
    .objectSize(chart.ySize() + chart.ySpacerSize())
    .guideLinesQ(false)
    .guidelineSpace(selections.xAxis.rect.w)

    yAxis()

    xAxis = d3sm.axis(xAxisSelection)
    .spaceX(selections.xAxis.rect.w)
    .spaceY(selections.xAxis.rect.h)
    .orient("bottom")
    .overflowQ(overflowQ)
    .categoricalQ(true)
    .tickLabels(xk)
    .namespace(d3sm.utils.str.hypenate(namespace,'axis-bottom'))
    .minObjectSize(chart.xMinObjectSize())
    .maxObjectSize(chart.xMaxObjectSize())
    .objectSpacer(chart.objectSpacer())
    .spacerSize(0)
    .objectSize(chart.xSize() + chart.xSpacerSize())
    .guideLinesQ(false)
    .guidelineSpace(selections.yAxis.rect.h)
    xAxis()

    var zoom = d3sm.aux.plotZoom(chart, xAxis, yAxis).eventType('drag')
    .orient("vertical")

    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){zoom.eventType('drag')()})
    chartSelection.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })

  }


return plot
}
