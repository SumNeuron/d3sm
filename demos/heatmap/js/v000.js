function heatmap( selection ) {
  var data,
  namespace
  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };

  function plot(){
    var
    selections = d3sm.setupStandardChartContainers(selection, namespace, {w: 1000, h:750}, undefined, {axes:{x:0.2, y:0.2},space:{w:1,h:1}})
    container = selections.svg.selection,
    bubbles = selections.plot.selection,
    xAxis = selections.xAxis.selection,
    yAxis = selections.yAxis.selection


    d3sm.debugQ = true

    var overflow = true
    // var overflow = false
    var bhm = d3sm.heatmap(bubbles)
      .spaceX(selections.plot.rect.w)
      .spaceY(selections.plot.rect.h)
      .xKey('xKey')
      .yKey('yKey')
      .vKey('val')
      .overflowQ(overflow)
      .objectStrokeWidth(2)
      // .minObjectSize(100)
      .maxObjectSize(100)


      // .yKeySortingFunction()
      // .xKeySortingFunction(function(a,b){return data[a]['xKey']})

      .data(data)

    bhm.tooltip().keys(['tooltip'])
    bhm()
    //
    //
    //
    // // extract keys and values to safe re-computation
    var xk = bhm.xValues(), yk = bhm.yValues()
    // var pd = []
    // vk.map(function(k, i){ pd.push(...d3.values(vv[k].quartiles)) })
    // pd = d3sm.unique(pd.sort(function (a, b) {return d3.ascending(a, b)}))
    //
    // console.log(pd)
    // console.log(yk, bhm.minObjectSize(), bhm.maxObjectSize(), bhm.ySpacerSize())

    var yA = d3sm.axis(yAxis)
    .spaceX(selections.yAxis.rect.w)
    .spaceY(selections.yAxis.rect.h)
    .orient("left")
    .overflowQ(overflow)
    .categoricalQ(true)
    .tickLabels(yk)
    .namespace('axis-left')
    .minObjectSize(bhm.minObjectSize())
    .maxObjectSize(bhm.maxObjectSize())
    .objectSpacer(bhm.ySpacerSize())
    .objectSize(bhm.ySize())

    .guideLinesQ(true)
    .guidelineSpace(selections.xAxis.rect.w)
    yA()

    var xA = d3sm.axis(xAxis)
    .spaceX(selections.xAxis.rect.w)
    .spaceY(selections.xAxis.rect.h)
    .orient("bottom")
    .overflowQ(overflow)
    .categoricalQ(true)
    .tickLabels(xk)
    .namespace('axis-bottom')
    .minObjectSize(bhm.minObjectSize())
    .maxObjectSize(bhm.maxObjectSize())
    .objectSpacer(bhm.xSpacerSize())
    .objectSize(bhm.xSize())
    .guideLinesQ(true)
    .guidelineSpace(selections.yAxis.rect.h)

    xA()

    console.log(bhm.selection().node(), bhm.selection().node().getBBox(), bhm.selection().node().getBoundingClientRect())


    var zoom = d3sm.plotZoom(bhm, xA, yA).eventType('drag')
    .orient('2D')

    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){zoom.eventType('drag')()})
    container.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })

  }


  return plot
}
