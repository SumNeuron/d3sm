function upset ( selection ) {
  var
  data,
  namespace='upset'


  var
  upset,
  upsetAxis,
  setTotalsBarChart,
  setTotalsAxis,
  intersectionTotalsBarChart,
  intersectionTotalsAxis,
  intersectionSetsAxis

  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };
  plot.upset = function(_){return arguments.length ? (upset = _, plot) : upset; };
  plot.upsetAxis = function(_){return arguments.length ? (upsetAxis = _, plot) : upsetAxis; };
  plot.setTotalsBarChart = function(_){return arguments.length ? (setTotalsBarChart = _, plot) : setTotalsBarChart; };
  plot.setTotalsAxis = function(_){return arguments.length ? (setTotalsAxis = _, plot) : setTotalsAxis; };
  plot.intersectionTotalsBarChart = function(_){return arguments.length ? (intersectionTotalsBarChart = _, plot) : intersectionTotalsBarChart; };
  plot.intersectionTotalsAxis = function(_){return arguments.length ? (intersectionTotalsAxis = _, plot) : intersectionTotalsAxis; };



  function plot() {
    var
    svg = d3sm.utils.sel.safeSelect(selection, 'svg'),

    upsetAxisSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-upset-chart-axis'),
    setTotalsAxisSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-set-bar-chart-axis'),
    intersectionTotalsAxisSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-intersection-bar-chart-axis'),
    intersectionSetsAxisSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-intersection-bar-set-axis'),

    upsetSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-upset-chart'),
    setTotalsBarChartSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-set-bar-chart'),
    intersectionTotalsBarChartSelection = d3sm.utils.sel.safeSelect(svg, 'g', namespace+'-intersection-bar-chart')



    if (upset == undefined) { upset = d3sm.charts.upset(upsetSelection) }
    if (upsetAxis == undefined) { upsetAxis = d3sm.axis(upsetAxisSelection) }
    if (setTotalsBarChart == undefined) { setTotalsBarChart = d3sm.charts.bar(setTotalsBarChartSelection) }
    if (setTotalsAxis == undefined) { setTotalsAxis = d3sm.axis(setTotalsAxisSelection) }
    if (intersectionTotalsBarChart == undefined) { intersectionTotalsBarChart = d3sm.charts.bar(intersectionTotalsBarChartSelection) }
    if (intersectionTotalsAxis == undefined) { intersectionTotalsAxis = d3sm.axis(intersectionTotalsAxisSelection) }
    if (intersectionSetsAxis == undefined) { intersectionSetsAxis = d3sm.axis(intersectionSetsAxisSelection) }



    var
    setChartSpacer = 10,
    intersectionChartSpacer = 10,

    percentages = {
      inChart: {w: 0.75, h: 0.5 },
      usChart: {w: 0.75, h: 0.2 },
      stChart: {w: 0.10, h: 0.2 },

      isAxis : {w: 0.75, h: 0.2 },
      inAxis : {w: 0.15, h: 0.5 },
      usAxis : {w: 0.15, h: 0.2 },
      stAxis : {w: 0.10, h: 0.1 },

      svg: {w: 1, h: 0.7}
    }

    var
    page = {w: selection.node().getBoundingClientRect().width, h: window.innerHeight},
    margins = {left: page.w *0.01, right: page.w *0.01, top: page.h*0.01, bottom: page.h*0.01},
    svgRect = {
      _w: page.w * percentages.svg.w,
      _h: page.h * percentages.svg.h,
      w: page.w *  percentages.svg.w - margins.left - margins.right,
      h: page.h *  percentages.svg.h - margins.top - margins.bottom
    },
    chartRect = {
      w: svgRect.w - setChartSpacer*2,
      h: svgRect.h - intersectionChartSpacer*2
    },
    upsetRect = {
      x: margins.left + (chartRect.w * percentages.stChart.w) + (chartRect.w * percentages.usAxis.w) + setChartSpacer*2,
      y: margins.top  + chartRect.h * percentages.inChart.h + (chartRect.h * percentages.isAxis.h) + intersectionChartSpacer,
      w: chartRect.w * percentages.usChart.w,
      h: chartRect.h * percentages.usChart.h
    },
    upsetAxisRect = {
      x: margins.left + (chartRect.w * percentages.stChart.w) + (chartRect.w * percentages.usAxis.w) + setChartSpacer,
      y: margins.top + chartRect.h * percentages.inChart.h + (chartRect.h * percentages.isAxis.h) + intersectionChartSpacer,
      w: chartRect.w * percentages.usAxis.w,
      h: chartRect.h * percentages.usAxis.h
    },
    setTotalsRect = {
      x: margins.left,
      y: margins.top + chartRect.h * percentages.inChart.h + (chartRect.h * percentages.isAxis.h) + intersectionChartSpacer,
      w: chartRect.w * percentages.stChart.w,
      h: chartRect.h * percentages.stChart.h
    },
    setTotalsAxisRect = {
      x: margins.left,
      y: margins.top + (chartRect.h * percentages.inChart.h) + (chartRect.h * percentages.stChart.h) + (chartRect.h * percentages.isAxis.h) + intersectionChartSpacer*2,
      w: chartRect.w * percentages.stAxis.w,
      h: chartRect.h * percentages.stAxis.h
    },
    intersectionTotalsRect = {
      x: margins.left + (chartRect.w * percentages.stChart.w) + (chartRect.w * percentages.usAxis.w) + setChartSpacer*2,
      y: margins.top + (chartRect.h * percentages.isAxis.h),
      w: chartRect.w * percentages.inChart.w,
      h: chartRect.h * percentages.inChart.h
    },
    intersectionTotalsAxisRect = {
      x: margins.left + (chartRect.w * percentages.stChart.w) + (chartRect.w * percentages.usAxis.w) + setChartSpacer,
      y: margins.top + (chartRect.h * percentages.isAxis.h),
      w: chartRect.w * percentages.inAxis.w,
      h: chartRect.h * percentages.inAxis.h
    },
    intersectionSetsAxisRect = {
      x: margins.left + (chartRect.w * percentages.stChart.w) + (chartRect.w * percentages.usAxis.w) + setChartSpacer + intersectionChartSpacer,
      y: margins.top + (chartRect.h * percentages.isAxis.h),
      w: chartRect.w * percentages.isAxis.w,
      h: chartRect.h * percentages.isAxis.h
    }


    svg.attr('width', svgRect._w).attr('height', svgRect._h)
    upsetAxisSelection.attr('transform', 'translate('+upsetAxisRect.x+','+upsetAxisRect.y+')')
    setTotalsAxisSelection.attr('transform', 'translate('+setTotalsAxisRect.x+','+setTotalsAxisRect.y+')')
    intersectionTotalsAxisSelection.attr('transform', 'translate('+intersectionTotalsAxisRect.x+','+intersectionTotalsAxisRect.y+')')
    intersectionSetsAxisSelection.attr('transform', 'translate('+intersectionSetsAxisRect.x+','+intersectionSetsAxisRect.y+')')

    upsetSelection.attr('transform', 'translate('+upsetRect.x+','+upsetRect.y+')')
    setTotalsBarChartSelection.attr('transform', 'translate('+setTotalsRect.x+','+setTotalsRect.y+')')
    intersectionTotalsBarChartSelection.attr('transform', 'translate('+intersectionTotalsRect.x+','+intersectionTotalsRect.y+')')

    var overflowQ = true

    upset
    .data(data)
    .spaceX(upsetRect.w)
    .spaceY(upsetRect.h)
    .xObjectSpacer(0.01)
    .yObjectSpacer(0.01)
    .maxObjectSize(5)
    .minObjectSize(20)
    .overflowQ(overflowQ)
    .intersectionExtractor(function(k, i){ return data[k].intersection.join(';') })
    .intersectionKeySortingFunction(function(a, b){
      return (
        data[a].intersection.length - data[b].intersection.length
        ||
        upset.intersectionValues().indexOf(data[a].intersection.join(';'))
        -upset.intersectionValues().indexOf(data[b].intersection.join(';'))
      )
    })
    // .radius(1)
    upset()

    upsetAxis
    .spaceX(upsetAxisRect.w)
    .spaceY(upsetAxisRect.h)
    .orient('left')
    .categoricalQ(true)
    .tickLabels(upset.setValues())
    .overflowQ(overflowQ)
    .minObjectSize(upset.minObjectSize())
    .maxObjectSize(upset.maxObjectSize())
    .objectSpacer(upset.yObjectSpacer())
    .tickLabelMargin(30)

    upsetAxis()

    var intersectionTotals = upset.intersectionTotals()
    var setTotals = upset.setTotals()


    intersectionSetsAxis
    .tickLabels(upset.intersectionValues())
    .categoricalQ(true)
    .spaceX(intersectionSetsAxisRect.w)
    .spaceY(intersectionSetsAxisRect.h)
    .orient('top')
    .namespace('intersection-sets-name')
    .overflowQ(overflowQ)
    .minObjectSize(upset.minObjectSize())
    .maxObjectSize(upset.maxObjectSize())
    .objectSpacer(upset.xObjectSpacer())
    .objectSize(upset.xObjectSize())
    .spacerSize(upset.xSpacerSize())
    // .tickLabelRotation(-45)
    .tickTickLabelSpacer(0)

    intersectionSetsAxis()

    intersectionTotalsBarChart.data(intersectionTotals)
    .grouping(upset.intersectionValues())
    .spaceX(intersectionTotalsRect.w)
    .spaceY(intersectionTotalsRect.h)
    .namespace('intersections')
    .orient('horizontal')
    .valueExtractor(function(k, i){return intersectionTotals[k]['total']})
    .overflowQ(overflowQ)
    .minObjectSize(upset.minObjectSize())
    .maxObjectSize(upset.maxObjectSize())
    .objectSpacer(upset.xObjectSpacer())
    .objectSize(upset.xObjectSize())
    .spacerSize(upset.xSpacerSize())


    intersectionTotalsBarChart.tooltip().keys(['Amount','Values'])
    .values([
      function(currentData, d){
        return currentData.total
      },
      function(currentData, d){
        var l = currentData.values.length
        // return currentData
        return currentData.values.join(', ').slice(0,100)+ ((l > 100) ? '...': '')
      }
    ])

    // intersectionTotalsBarChart.colorFunction().colors(['cyan', 'blue']).colorBy('value')

    // intersectionSetsAxis()
    intersectionTotalsBarChart()

    intersectionTotalsAxis
    .spaceX(intersectionTotalsAxisRect.w)
    .spaceY(intersectionTotalsAxisRect.h)
    .orient('left')
    .namespace('intersections-axis-left')
    .tickValues(intersectionTotalsBarChart.barValues())
    .guideLinesQ(true)
    .guidelineSpace(intersectionTotalsRect.w + intersectionChartSpacer)
    .overflowQ(overflowQ)

    intersectionTotalsAxis()



    setTotalsBarChart.data(setTotals)
    .grouping(upset.setValues())
    .spaceX(setTotalsRect.w)
    .spaceY(setTotalsRect.h)
    .orient('right')
    .valueExtractor(function(k, i){return setTotals[k]['total']})
    .namespace('set')
    .overflowQ(overflowQ)
    .minObjectSize(upset.minObjectSize())
    .maxObjectSize(upset.maxObjectSize())
    .objectSpacer(upset.yObjectSpacer())
    .objectSize(upset.yObjectSize())
    .spacerSize(upset.ySpacerSize())
    .barPercent(0.75)

    setTotalsBarChart()



    setTotalsAxis
    .spaceX(setTotalsAxisRect.w)
    .spaceY(setTotalsAxisRect.h)
    .namespace('set-axis-bottom')
    .orient('bottom')
    .tickValues(setTotalsBarChart.barValues())
    .reverseScaleQ(true)
    // .tickLabelRotation(0)
    .overflowQ(overflowQ)

    setTotalsAxis()






    var zoom = d3sm.aux.multiPlotZoom(upset).eventType('drag').orient('2D')
    .xComponents([
      intersectionTotalsBarChart,
      intersectionSetsAxis
    ])
    .yComponents([
      upsetAxis,
      setTotalsBarChart
    ])
    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){ zoom.eventType('drag')() })
    upsetSelection.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })
    intersectionTotalsBarChartSelection.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })



    intersectionTotalsBarChartSelection.selectAll('.bar.intersections > rect.bar-rect').on('click', function(d, i){
      console.log( d, i)
    })
    // selection.selectAll('text').attr('pointer-events', 'none')
    // .attr('pointer-events', 'none')

  }

  return plot
}
