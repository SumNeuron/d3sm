function barPlot( selection ) {
  var data,
  namespace
  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };


  function plot() {
    var
    databar = d3sm.d3sm.utils.sel.safeSelect(selection, 'form',d3sm.utils.str.hypenate(namespace,'databar')),
    dataPrint = d3sm.d3sm.utils.sel.safeSelect(selection, 'div', d3sm.utils.str.hypenate(namespace,'data-print')),

    selections = d3sm.setupStandardChartContainers(selection, namespace, {w: 1000, h:500}, undefined, {axes:{x:0.1, y:0.1},space:{w:1,h:1}})
    container = selections.svg.selection,
    bars = selections.plot.selection,
    xAxis = selections.xAxis.selection,
    yAxis = selections.yAxis.selection


    // make data selection
    var dataSelect = d3sm.datatoggle(databar)
    .keys(d3.keys(data.groupings))
    .updateFunction(function(){plot(); zoom.reset()})
    .namespace(namespace)()

    // grab current selected key
    var currentKey = dataSelect.currentKey()
    // // extract subdata
    // var currentData = data[currentKey]
    //


    dataPrint.text(JSON.stringify(data.groupings[currentKey]))
    // make bars
    var b = d3sm.bar(bars)
    .data(data.data)
    .grouping(data.groupings[currentKey])
    .spaceX(selections.plot.rect.w)
    .spaceY(selections.plot.rect.h)
    .overflowQ(true)
    .orient('horizontal')
    .maxObjectSize(20)
    .minObjectSize(10)
    .valueExtractor(function(d, i){return data.data[d]['value'] })


    // b.tooltip().keys(['value'])
    //
    // b.colorFunction().colorBy('index')
    b()

    // extract keys and values to safe re-computation
    var bk = b.barKeys(), bv = b.barValues()

    // left d3sm.axis
    var yA = d3sm.axis(yAxis)
    .spaceX(selections.yAxis.rect.w)
    .spaceY(selections.yAxis.rect.h)
    .orient("left")
    .overflowQ(false)
    .tickValues(bv)
    .guideLinesQ(true)
    .guidelineSpace(selections.xAxis.rect.w)
    .namespace('axis-left')
    .numberOfTicks(5)
    yA()

    // bottom d3sm.axis
    var xA = d3sm.axis(xAxis)
    .spaceX(selections.xAxis.rect.w)
    .spaceY(selections.xAxis.rect.h)
    .orient("bottom")
    .overflowQ(true)
    .categoricalQ(true)
    .tickLabels(bk)
    .namespace('axis-bottom')
    .minObjectSize(b.minObjectSize())
    .maxObjectSize(b.maxObjectSize())
    .grouping(data.groupings[currentKey])
    .objectSpacer(b.objectSpacer())
    xA()

    b.selection().selectAll("rect").nodes().map(function(d, i){
      console.log(d.getBoundingClientRect(), i)
      return this
    }),
    console.log(
      b.selection().node(),
      b.selection().selectAll("rect").nodes(),
      b.selection().selectAll("rect").nodes(),
      'BCR:',b.selection().node().getBoundingClientRect(),
      'BBox:',b.selection().node().getBBox()
    )





    // // set-up zoom, whell and drag
    var zoom = d3sm.plotZoom(b, xA, yA).eventType('drag')
    .orient('horizontal')
    // .xLock(
    //   objs.getBBox().width
    // )

    console.log(zoom)

    // zoom.setLocks()

    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){
      // zoom.setLocks()
      zoom.eventType('drag')()
    })
    container.call(pan).on('wheel', function(){
      // zoom.setLocks()

      zoom.eventType('wheel')()
    })
  }




  return plot
}
