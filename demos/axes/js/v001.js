var data = d3smDemoData['basicViolins']

var
selection = d3.select("#axes-by-value")
namespace = 'axes',
container = d3sm.utils.sel.safeSelect(selection, 'svg', namespace).style('width', '1000px'),
lAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'left-axis')).attr('transform', "translate(100,100)"),
rAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'right-axis')).attr('transform', "translate(900,100)")
uAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'up-axis')).attr('transform', "translate(100,100)")
dAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'down-axis')).attr('transform', "translate(100,400)")


tickValues = [1,2,3,4,5]

var uA = d3sm.axis(uAxis)
.spaceX(800)
.spaceY(100)
.orient("top")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('u')
// .guideLinesQ(true)
.guidelineSpace(300)

var dA = d3sm.axis(dAxis)
.spaceX(800)
.spaceY(100)
.orient("bottom")
.label('bottom')
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('d')
// .guideLinesQ(true)
.guidelineSpace(300)
.tickLabelRotation(315)
// .tickTickLabelSpacer(0)



var lA = d3sm.axis(lAxis)
.spaceX(100)
.spaceY(300)
.orient("left")
.label('left')
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('l')
// .guideLinesQ(true)
.guidelineSpace(800)
// .tickLabelRotation(45)


var rA = d3sm.axis(rAxis)
.spaceX(100)
.spaceY(300)
.orient("right")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('r')
// .guideLinesQ(true)
.guidelineSpace(800)
// .tickLabelRotation(45)


uA()
dA()
lA()
rA()



var
selection = d3.select("#axes-by-cat")
namespace = 'axes',
container = d3sm.utils.sel.safeSelect(selection, 'svg', namespace).style('width', '1000px'),
lAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'left-axis')).attr('transform', "translate(100,100)"),
rAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'right-axis')).attr('transform', "translate(900,100)")
uAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'up-axis')).attr('transform', "translate(100,100)")
dAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'down-axis')).attr('transform', "translate(100,400)")


tickValues = [1,2,3,4,5]
tickLabels = ["this", "this-text", "this-text-gets", "this-text-gets-longer", "this-text-gets-longer-as-we-go-on"]

var uA = d3sm.axis(uAxis)
.spaceX(800)
.spaceY(100)
.label('top')
.orient("top")
.overflowQ(false)
.categoricalQ(true)
.tickLabels(tickLabels)
.numberOfTicks(5)
.namespace('u-c')
// .guideLinesQ(true)
.guidelineSpace(300)

var dA = d3sm.axis(dAxis)
.spaceX(800)
.spaceY(100)
.orient("bottom")
.overflowQ(false)
.categoricalQ(true)
.tickLabels(tickLabels)
.numberOfTicks(5)
.namespace('d-c')
// .guideLinesQ(true)
.guidelineSpace(300)
// .tickLabelRotation(315)
.tickTickLabelSpacer(0)


var lA = d3sm.axis(lAxis)
.spaceX(100)
.spaceY(300)
.orient("left")
.overflowQ(false)
.categoricalQ(true)
.tickLabels(tickLabels)
.numberOfTicks(5)
.namespace('l-c')
// .guideLinesQ(true)
.guidelineSpace(800)

var rA = d3sm.axis(rAxis)
.spaceX(100)
.spaceY(300)
.label('right')
.orient("right")
.overflowQ(false)
.categoricalQ(true)
.tickLabels(tickLabels)
.numberOfTicks(5)
.namespace('r-c')
// .guideLinesQ(true)
.guidelineSpace(800)

uA()
dA()
lA()
rA()



var
selection = d3.select("#axes-by-val-g")
namespace = 'axes',
container = d3sm.utils.sel.safeSelect(selection, 'svg', namespace).style('width', '1000px'),
lAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'left-axis')).attr('transform', "translate(100,100)"),
rAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'right-axis')).attr('transform', "translate(900,100)")
uAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'up-axis')).attr('transform', "translate(100,100)")
dAxis = d3sm.utils.sel.safeSelect(container, 'g', d3sm.utils.str.hypenate(namespace, 'down-axis')).attr('transform', "translate(100,400)")


tickValues = [1,2,3,4,5]

var uA = d3sm.axis(uAxis)
.spaceX(800)
.spaceY(100)
.orient("top")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('u-g')
.guideLinesQ(true)
.guidelineSpace(300)

var dA = d3sm.axis(dAxis)
.spaceX(800)
.spaceY(100)
.orient("bottom")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('d-g')
.guideLinesQ(true)
.guidelineSpace(300)


var lA = d3sm.axis(lAxis)
.spaceX(100)
.spaceY(300)
.orient("left")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('l-g')
.guideLinesQ(true)
.guidelineSpace(800)

var rA = d3sm.axis(rAxis)
.spaceX(100)
.spaceY(300)
.orient("right")
.overflowQ(false)
.tickValues(tickValues)
.numberOfTicks(5)
.namespace('r-g')
.guideLinesQ(true)
.guidelineSpace(800)

uA()
dA()
lA()
rA()
