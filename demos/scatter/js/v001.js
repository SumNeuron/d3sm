//<kbd class='code-version'>VERSION: 0.0.1</kbd>
function scatterPlot( selection ) {
  var data,
  namespace
  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };


  function plot() {
    var
    databar = d3sm.safeSelect(selection, 'form', d3sm.hypenate(namespace,'databar')),

    selections = d3sm.setupStandardChartContainers(selection, namespace, {w: 1000, h:500}, undefined, {axes:{x:0.1, y:0.1},space:{w:1,h:1}})
    container = selections.svg.selection,
    chart = selections.plot.selection,
    xAxis = selections.xAxis.selection,
    yAxis = selections.yAxis.selection

    leg = d3sm.safeSelect(container, 'g', d3sm.hypenate(namespace,'legend'))
    .attr('transform','translate(10, 24)')

    var dataSelect = d3sm.datatoggle(databar)
    .keys(d3.keys(data.groupings))
    .namespace(namespace)
    .updateFunction(function(){ plot(); zoom.reset(); })
    ()

    var currentKey = dataSelect.currentKey()

    var currentData = d3.keys(data.data).reduce( (acc, k) => {
      if (data.groupings[currentKey].includes(k)) { acc[k] = data.data[k] }
      return acc
    }, {})


    var scale = currentKey == "linearScale" ? d3.scaleLinear() : d3.scaleSqrt()
    var ticks = currentKey == "linearScale" ? 5 : 10

    var s = d3sm.scatter(chart)
    .data(currentData)
    .spaceX(selections.plot.rect.w)
    .spaceY(selections.plot.rect.h)
    .domainPaddingX(0.1)
    .domainPaddingY(0.1)
    .valueExtractorR(function(d, i){return currentData[d]['r']})
    .maxRadius(20)
    .minRadius(1)
    .scaleY(scale)
    .scaleR(d3.scalePow())

    var rs = d3.keys(currentData).map(function(d, i){return currentData[d].r})

    s.colorFunction().colorBy('value').valueExtractor(function(k, v, i){ return v.r })
    .dataExtent( [d3.min(rs), d3.max(rs)])

    s()

    var xV = s.valuesX(), yV = s.valuesY()

    var yA = d3sm.axis(yAxis)
    .spaceX(selections.yAxis.rect.w)
    .spaceY(selections.yAxis.rect.h)
    .orient("left")
    .overflowQ(false)
    .tickValues(yV)
    .guideLinesQ(true)
    .guidelineSpace(selections.xAxis.rect.w)
    .namespace('axis-left')
    .numberOfTicks(ticks)
    .domainPadding(s.domainPaddingY())
    .scale(scale)
    yA()

    var xA = d3sm.axis(xAxis)
    .spaceX(selections.xAxis.rect.w)
    .spaceY(selections.xAxis.rect.h)
    .orient("bottom")
    .overflowQ(false)
    .tickValues(xV)
    .guideLinesQ(true)
    .guidelineSpace(selections.yAxis.rect.h)
    .namespace('axis-bottom')
    .numberOfTicks(ticks)
    .domainPadding(s.domainPaddingX())

    xA()

    // set-up zoom, whell and drag
    var zoom = d3sm.plotZoom(s, xA, yA).eventType('drag').orient('2D')
    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){ zoom.eventType('drag')() })
    container.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })

    l = d3sm.numericLegend(leg)
    .min(d3.min(s.valuesR()))
    .max(d3.max(s.valuesR()))
    .spaceX(25)
    .spaceY(300)

    l()

  }
  return plot
}
