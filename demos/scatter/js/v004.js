//<kbd class='code-version'>VERSION: 0.0.4</kbd>
function scatterPlot( selection ) {
  var
  data,
  namespace,
  chart,
  xAxis,
  yAxis,
  dataSelect,
  legend,
  lasso,
  lassoWidget

  plot.data = function(_){return arguments.length ? (data = _, plot) : data; };
  plot.namespace = function(_){return arguments.length ? (namespace = _, plot) : namespace; };
  plot.chart = function(_){return arguments.length ? (chart = _, plot) : chart; };
  plot.lasso = function(_){return arguments.length ? (lasso = _, plot) : lasso; };

  function plot() {
    var objectClass= 'item'

    var
    lassoDiv = d3sm.utils.sel.safeSelect(selection, 'div', d3sm.utils.str.hypenate(namespace,'lassoWidget')),
    databar = d3sm.utils.sel.safeSelect(selection, 'form', d3sm.utils.str.hypenate(namespace,'databar')),
    selections = d3sm.utils.misc.setupStandardChartContainers(
      selection,
      namespace,
      {w:window.innerWidth, h:window.innerHeight},
      {top:0.01, bottom:0.01, left:0.01, right:0.01},
      {w: 0.8, h:0.8}, // percent of container space for svg
      {y:0.1,x:0.1}, // percent of container space for axes,
      {x:25, margin:05} // absolute width of legendSelect and space on either size
    ),

    // selections = d3sm.setupStandardChartContainers(selection, namespace, {w: 1000, h:500}, undefined, {axes:{x:0.1, y:0.1},space:{w:1,h:1}})
    container = selections.svg.selection,
    chartSelection = selections.plot.selection,
    xAxisSelection = selections.xAxis.selection,
    yAxisSelection = selections.yAxis.selection,
    legendSelect = selections.legend.selection


    var
    xAxisToggle = { 'linear': d3.scaleLinear(), 'sqrt': d3.scaleSqrt() },
    yAxisToggle = { 'linear': d3.scaleLinear(), 'sqrt': d3.scaleSqrt() },
    rAxisToggle = { 'linear': d3.scaleLinear(), 'sqrt': d3.scaleSqrt(), 'pow': d3.scalePow() }

    //
    if (dataSelect == undefined) { dataSelect =  d3sm.aux.datatoggle(databar)
      .data({
        'dataset': data.groupings,
        'x-axis': xAxisToggle,
        'y-axis': yAxisToggle,
        'radius scale': rAxisToggle
      })
      .namespace(namespace)
      .updateFunction(function(){ plot(); zoom.reset(); })
      dataSelect()
    }

    if (chart == undefined) { chart = d3sm.charts.scatter(chartSelection) }
    if (yAxis == undefined) { yAxis = d3sm.axis(yAxisSelection) }
    if (xAxis == undefined) { xAxis = d3sm.axis(xAxisSelection) }
    if (legend == undefined) {legend = d3sm.legends.numeric(legendSelect) }


    var currentKeys = dataSelect.currentKeys()
    var xScale = xAxisToggle[currentKeys['x-axis']]
    var yScale = yAxisToggle[currentKeys['y-axis']]
    var rScale = rAxisToggle[currentKeys['radius scale']]
    var currentDataKey = currentKeys['dataset']

    //
    // var xScale = xAxisToggle['linear']
    // var yScale = yAxisToggle['linear']
    // var rScale = rAxisToggle['linear']
    // var currentDataKey = 'datasetOne'



    var currentKey = dataSelect.currentKeys()
    var currentData = d3.keys(data.data).reduce( (acc, k) => {
      if (data.groupings[currentDataKey].includes(k)) { acc[k] = data.data[k] }
      return acc
    }, {})


    chart
    .data(currentData)
    .spaceX(selections.plot.rect.w)
    .spaceY(selections.plot.rect.h)
    .domainPaddingX(0.1)
    .domainPaddingY(0.1)
    .valueExtractorR(function(d, i){return currentData[d]['r']})
    .maxRadius(20)
    .minRadius(1)
    .scaleY(yScale)
    .scaleX(xScale)
    .scaleR(rScale)
    .namespace(namespace)
    .objectClass(objectClass)

    var rs = d3.keys(currentData).map(function(d, i){return currentData[d].r})

    chart.colorFunction().colorBy('value').valueExtractor(function(k, v, i){ return v.r })
    .dataExtent( [d3.min(rs), d3.max(rs)])
    chart()

    var xV = chart.valuesX(), yV = chart.valuesY()

    yAxis
    .spaceX(selections.yAxis.rect.w)
    .spaceY(selections.yAxis.rect.h)
    .orient("left")
    .overflowQ(false)
    .tickValues(yV)
    .guideLinesQ(true)
    .guidelineSpace(selections.xAxis.rect.w)
    .namespace('axis-left')
    .numberOfTicks(5)
    .domainPadding(chart.domainPaddingY())
    .scale(yScale)
    yAxis()

    xAxis = d3sm.axis(xAxisSelection)
    .spaceX(selections.xAxis.rect.w)
    .spaceY(selections.xAxis.rect.h)
    .orient("bottom")
    .overflowQ(false)
    .tickValues(xV)
    .guideLinesQ(true)
    .guidelineSpace(selections.yAxis.rect.h)
    .namespace('axis-bottom')
    .numberOfTicks(5)
    .domainPadding(chart.domainPaddingX())
    .scale(xScale)

    xAxis()


    legend
    .min(d3.min(chart.valuesR()))
    .max(d3.max(chart.valuesR()))
    .spaceX(selections.legend.rect.w)
    .spaceY(selections.legend.rect.h)

    legend()



    // set-up zoom, whell and drag
    var zoom = d3sm.aux.plotZoom(chart, xAxis, yAxis).eventType('drag').orient('2D')
    var pan = d3.zoom().scaleExtent([1,1]).on('zoom', function(){ zoom.eventType('drag')() })
    chartSelection.call(pan).on('wheel', function(){ zoom.eventType('wheel')() })


    var svg = selection.select('svg')
    var objs = svg.select('g.'+namespace+'-object-container')
    var objC = '.chart-point'

    if (lasso == undefined) { lasso = d3sm.aux.lasso( )
      .svg(svg)
      .objectClass(objC)
      .objectContainer(objs)
      .namespace('test')
      .chartContainer(chartSelection)
     }

    lasso
    .xScale(chart.scaleX())
    .yScale(chart.scaleY())

    lasso()


    if (lassoWidget == undefined) {lassoWidget = d3sm.aux.lassoWidget(lassoDiv)
      .svg(selections.svg.selection)
      .objectClass("."+objectClass)
      .chartContainer(chartSelection)
      .objectContainer(chartSelection.select('g.'+namespace+'-object-container'))
      .maxNumberOfGroups(2)
      .dataExtractor(function(nodeData){
        return {"name": nodeData}
      })
      lassoWidget()
    }

  }




  return plot
}
