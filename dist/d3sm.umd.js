(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('d3'), require('d3-selection')) :
  typeof define === 'function' && define.amd ? define(['exports', 'd3', 'd3-selection'], factory) :
  (factory((global.d3sm = global.d3sm || {}),global.d3,global['d3-selection']));
}(this, (function (exports,d3$1,d3Selection) { 'use strict';

  /**
  * Helper function for Array.filter to get unique elements of the array
  * @param {*} value current value as mapping over array (self)
  * @param {number} index current index in the array
  * @param {Array} self passed array from Array.filter method
  * @returns {boolean} whether or not value is the first of its kind (i.e. indexOf(value) == index)
  */
  function uniqueElements(value, index, self) {
    return self.indexOf(value) === index;
  }
  /**
  * This function tests to see if all elements of the passed array are true.
  * @param {Array} array of values
  * @param {Function} [func function(value){return value == true;}] is applied to each value of the array and should return a boolean.
  * @returns {boolean} if all values are true by function
  */

  function all(array, func) {
    if (func == undefined) {
      return array.every(function (value) {
        return value === true;
      });
    }

    return array.every(function (value) {
      return func(value);
    });
  }
  /**
  * Counts the number of occurances of each element in the given array.
  * @param {Array} array of elements
  * @returns {Object} of key: value pairs where key is an element in the array and value is the number of times it occurs.
  */

  function tally(array) {
    var tallies = {};
    array.map(function (element) {
      if (hasQ(Object.keys(tallies), element)) {
        tallies[element] = 1;
      } else {
        tallies[element] += 1;
      }
    });
    return tallies;
  }
  /**
  * Short-hand for array.includes(item);
  * @param {Array} array
  * @param {*} item to test if contained in  {array}
  * @returns {boolean}
  */

  function hasQ(array, item) {
    return array.includes(item);
  }
  /**
  * Returns first item in array
  * @param {Array} array of items
  * @returns {*} array[0]
  */

  function first(array) {
    return array[0];
  }
  /**
  * Returns last item in array
  * @param {Array} array of items
  * @returns {*} array[array.length-1]
  */

  function last(array) {
    return array[array.length - 1];
  }
  /**
  * Calculates the total value of numbers in passed array
  * @param {number[]} array of numerical values
  * @returns {number} sum over elements in array
  */

  function total(array) {
    return array.reduce(function (a, b) {
      return a + b;
    }, 0);
  }
  /**
  * Removes duplicates in array
  * @param {Array} array of items
  * @returns {Array} of items such that item_i != item_j for all i < j
  * @see{@link uniqueElements} for the filtering function
  */

  function unique(array) {
    return array.filter(uniqueElements);
  }
  /**
  * Filters passed array for specified indicies
  * @param {Array} array of items
  * @param {number[]} positions of integers such that i < array.length
  * @returns {Array} of items such that for any item_i, positions.includes(i) === true
  */

  function get(array, positions) {
    return array.filter(function (value, index) {
      return hasQ(positions, index);
    });
  }
  /**
  * Determines if all elements in passed array are arrays themselves.
  * @param {Array} array of items
  * @returns {boolean} true if Array.isArray(e) is true for all e in array
  * @see{@link all}
  */

  function listOfListsQ(array) {
    return all(array.map(function (element, index) {
      return Array.isArray(element);
    }));
  }
  /**
  * Built on top of @see{@link get}, mapping if positions is a list of lists (@see{@link listOfListsQ})
  * @param {Array} array of items
  * @param {number[] | []number[] } positions of integers or list of positions of integers
  * @returns {boolean} returns specified positions from array. If nested positions passed, returns requested items in same structure.
  */

  function cut(array, positions) {
    if (listOfListsQ(array)) {
      return positions.map(function (pos, i) {
        return array.get(pos);
      });
    }

    return get(array, positions);
  }
  /**
  * Given an array of objects, constructs new objects where each value is a list
  * based on the corresonding key, which is extracted by the parameter by
  * @param {Objects[]} array of objects
  * @param {string} by key within all objects of passed array
  * @param {string[]} [groups] saves some computation if all known values extracted by mapping over the parameter by are passed
  * @returns {Object} of key value pairs, where keys are all values of the key by from an object in the passed array and the value are those corresponding objects.
  */

  function groupBy(array, by, groups) {
    if (groups == undefined) {
      groups = unique(array.map(function (elements, index) {
        return element[by];
      }));
      groups.map(function (value, index) {
        groupped[value] = [];
      });
    }

    var groupped = {};
    array.map(function (element, index) {
      groupped[element[by]].push(element);
    });
    return groupped;
  }
  /**
  * Tests if two arrays are equivalent
  * @param {Array} array
  * @param {Array} other
  * @returns {boolean} if every element of array matches that of other
  */

  function arrayEquals(array, other) {
    if (!other) return false; // compare lengths - can save a lot of time

    if (array.length != other.length) return false;

    for (var i = 0, l = array.length; i < l; i++) {
      // Check if we have nested arrays
      if (array[i] instanceof Array && other[i] instanceof Array) {
        // recurse into the nested arrays
        if (!arrayEquals(array[i], other[i])) return false;
      } else if (array[i] != other[i]) {
        // Warning - two different object instances will never be equal: {x:20} != {x:20}
        return false;
      }
    }

    return true;
  }
  /**
  * Recursively tallies the number of elements at each level of the passed, putatively nested array
  * @param {Array} array of items which may include nested arrays
  * @param {number} [level=0] current depth in the recursion
  * @param {Array} [levelData=[]] keeps track of items seen so far at each depth
  * @returns {Array} stating the number of elements (array inclusive) found at each level of the array
  */

  function elementsAtLevels(array, level, levelData) {
    level = level == undefined ? 0 : level + 1;
    levelData = levelData == undefined ? [] : levelData;

    if (level >= levelData.length) {
      levelData.push(array.length);
    } else {
      levelData[level] += array.length;
    }

    array.map(function (e, i) {
      if (Array.isArray(e)) {
        elementsAtLevels(e, level, levelData);
      }
    });
    return levelData;
  }
  /**
  * Recursively tallies the number of elements of the passed, putatively nested array
  * @param {Array} array of items which may include nested arrays
  * @param {number} [elements=0] current number of elements seen so far
  * @returns {number} number of elements (array inclusive) found in passed array
  */

  function numberOfElements(array, elements) {
    elements = elements == undefined ? 0 : elements;
    array.map(function (e, i) {
      if (Array.isArray(e)) {
        elements = numberOfElements(e, elements);
      } else {
        elements += 1;
      }
    });
    return elements;
  }
  /**
  * Concats all nested arrays in passed array to form a single array
  * @param {Array} array of putatively nested arrays
  * @param {Array} [flat=[]] current flattened array
  * @returns {Array} with every element in the same level
  */

  function flatten(array, flat) {
    flat = flat == undefined ? [] : flat;
    array.map(function (e, i) {
      if (Array.isArray(e)) {
        flat = flat.concat(flatten(e));
      } else {
        flat.push(e);
      }
    });
    return flat;
  }
  /**
  * Search of list of lists to find which - if any - passed value is in
  * @param {Array[]} bins list of lists of values
  * @param {*} value item to test if in any of the bins
  * @returns {number} indicating the index of the bin in which value was found
  */

  function whichBin(bins, value) {
    var i = -1;

    for (var j = 0; j < bins.length; j++) {
      if (hasQ(bins[j], value)) {
        return j;
      }
    }

    return i;
  }

  var arr = /*#__PURE__*/Object.freeze({
    uniqueElements: uniqueElements,
    all: all,
    tally: tally,
    hasQ: hasQ,
    first: first,
    last: last,
    total: total,
    unique: unique,
    get: get,
    listOfListsQ: listOfListsQ,
    cut: cut,
    groupBy: groupBy,
    arrayEquals: arrayEquals,
    elementsAtLevels: elementsAtLevels,
    numberOfElements: numberOfElements,
    flatten: flatten,
    whichBin: whichBin
  });

  /**
  * Modifies luminance of hexidecimal number
  * @param {string} hex should be hexidecimal value with or without the proceeding octotrope
  * @param {number} lum value to increase or decrease luminosity by
  * @returns {string} updated hexidecimal value without the proceeding octotrope
  */

  function modifyHexidecimalColorLuminance(hex, lum) {
    // validate hex string
    var hex = String(hex).replace(/[^0-9a-f]/gi, '');

    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }

    lum = lum || 0; // convert to decimal and change luminosity

    var rgb = '#',
        c,
        i;

    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
      rgb += ('00' + c).substr(c.length);
    }

    return rgb;
  }
  /**
  * Maps arguments in to d3.interpolateRgbBasis
  * @param arguments
  * @returns {Function}
  */

  function interpolateColors() {
    return d3$1.interpolateRgbBasis(arguments);
  }

  var color = /*#__PURE__*/Object.freeze({
    modifyHexidecimalColorLuminance: modifyHexidecimalColorLuminance,
    interpolateColors: interpolateColors
  });

  /**
  * Rounds decimals of number to precision
  * @param {number} number
  * @param {number} precision
  * @returns {number} rounded to precision
  */

  function round(number, precision) {
    var shift = function shift(number, precision, reverseShift) {
      if (reverseShift) {
        precision = -precision;
      }

      var numArray = ('' + number).split('e');
      return +(numArray[0] + 'e' + (numArray[1] ? +numArray[1] + precision : precision));
    };

    return shift(Math.round(shift(number, precision, false)), precision, true);
  }
  /**
  * evenly partitions the range [min, max] into n parts
  * @param {number} min
  * @param {number} max
  * @param {number} n
  * @returns {number[]} array of length n evenly partitioned between min and max
  */

  function tickRange(min, max, n) {
    var a = [min];
    var d = max - min;
    var s = d / (n - 1);

    for (var i = 0; i < n - 2; i++) {
      a.push(min + s * (i + 1));
    }

    a.push(max);
    return a;
  }
  /**
  * @deprecated @see{@link tickRange}
  * @param {number} min
  * @param {number} max
  * @param {number} parts
  * @returns {number[]} array of length parts evenly partitioned between min and max
  */

  function partitionRangeInto(min, max, parts) {
    var diff = max - min;
    return Array(parts).map(function (e, i) {
      return min + diff / parts * i;
    });
  }
  function euclideanDistance(p1, p2) {
    var a = p1[0] - p2[0],
        b = p1[1] - p2[1];
    return Math.sqrt(a * a + b * b);
  }
  /**
  * Calculated the quartiles of the passed data and stores them with qKeys
  * @param {number[]} data list of numerical values
  * @param {string[]} [qKeys=['q0', 'q1', 'q2', 'q3', 'q4']] how returned object with quartiles should be stored
  * @returns {Object} with keys qKeys giving only the numerical values for the quartiles
  */

  function quartiles(data, qKeys) {
    var q2 = d3$1.median(data),
        lower = data.filter(function (x) {
      return x < q2;
    }),
        upper = data.filter(function (x) {
      return x > q2;
    }),
        q1 = d3$1.median(lower),
        q1 = q1 == undefined ? q2 : q1,
        q0 = d3$1.min(lower),
        q0 = q0 == undefined ? q1 : q0,
        q3 = d3$1.median(upper),
        q3 = q3 == undefined ? q2 : q3,
        q4 = d3$1.max(upper),
        q4 = q4 == undefined ? q3 : q4,
        k0 = 'q0',
        k1 = 'q1',
        k2 = 'q2',
        k3 = 'q3',
        k4 = 'q4',
        obj = {};

    if (qKeys != undefined && qKeys.length == 5) {
      k0 = qKeys[0];
      k1 = qKeys[1];
      k2 = qKeys[2];
      k3 = qKeys[3];
      k4 = qKeys[4];
    }

    obj[k0] = q0;
    obj[k1] = q1;
    obj[k2] = q2;
    obj[k3] = q3;
    obj[k4] = q4;
    return obj;
  }
  /**
  * determines the width of an object for the calling plotting function
  * @param {number} freeSpace how much space is avalible
  * @param {number} numberOfObjects how many object do we need
  * @param {number} minObjectWidth how small are these objects allowed to be
  * @param {number} maxObjectWidth how large are these object allowed to be
  * @param {number} sizeOfSpacer percent of freeSpace that a single spacer should take up (need numberOfObjects - 1 spacers)
  * @param {boolean} overflowQ can we go beyond alloted space
  * @returns {number} how large object should be
  * function tries to keep object within min / max width, but wil default to
  * 5e-10 (smallest consistenly visible by svg size of element) if overflowQ is false
  */

  function calculateWidthOfObject(freeSpace, numberOfObjects, minObjectWidth, maxObjectWidth, sizeOfSpacer, overflowQ) {
    var sizeOfSpacer = sizeOfSpacer == 0 || sizeOfSpacer > 1 ? sizeOfSpacer : freeSpace * sizeOfSpacer;
    var numberOfSpacers = numberOfObjects - 1;
    var spaceTakenBySpacers = numberOfSpacers * sizeOfSpacer;
    var remainingSpace = freeSpace - spaceTakenBySpacers;
    remainingSpace = remainingSpace < 0 ? 0 : remainingSpace;
    var objectWidth = remainingSpace / numberOfObjects;

    if (overflowQ && minObjectWidth != undefined && objectWidth < minObjectWidth) {
      objectWidth = minObjectWidth;
    } // if ( maxObjectWidth != undefined && objectWidth > maxObjectWidth ) { objectWidth = maxObjectWidth }


    if (overflowQ && maxObjectWidth != undefined && objectWidth < maxObjectWidth) {
      objectWidth = maxObjectWidth;
    }

    return Math.max(objectWidth, 5e-10);
  }
  /**
  * @param {Array[]} data list data (can be nested). If nested will create more complex spacer size
  * @param {number} freeSpace how much space is avalible
  * @param {number} objectWidth @see{@link calculateWidthOfObject}
  * @param {number} numberOfObjects how many object do we need
  * @param {number} baseSpacerSize percent of freeSpace that a single spacer should take up (need numberOfObjects - 1 spacers)
  * @param {boolean} overflowQ can we go beyond alloted space
  * @returns {number} returns size that spacer should be at level=0
  */

  function calculateWidthOfSpacer(data, freeSpace, objectWidth, numberOfObjects, baseSpacerSize, overflowQ) {
    if (overflowQ) {
      // var limitedNumberOfObjects = numberOfObjects > 6 ? 6 : numberOfObjects
      // var spaceLeft = freeSpace - limitedNumberOfObjects * objectWidth
      // return spaceLeft / (limitedNumberOfObjects - 1)
      return freeSpace * baseSpacerSize;
    }

    var spacersAtEachLevel = spacersNeededAtEachLevel(data);
    var totalSpacerPercent = total(spacersAtEachLevel.map(function (e, i) {
      return e * 1 / (i + 1);
    }));
    var baseSpacerSize = (freeSpace - objectWidth * numberOfObjects) / totalSpacerPercent; // console.log(freeSpace, objectWidth, numberOfObjects, totalSpacerPercent)
    // console.log(totalSpacerPercent, baseSpacerSize, totalSpacerPercent * baseSpacerSize)

    return isNaN(baseSpacerSize) ? 0 : baseSpacerSize;
  }
  /**
  * Calculates number of spacers needed to seperate elements at each level.
  * @param {Array[]} array list data (can be nested). If nested will create more complex spacer size
  * @param {number} [level=0] current level, used in recusrion
  * @param {Array} [levelData=[]] how many spacers needed at a given level
  * @returns {Array} levelData
  *
  * @example
  * array = [[1,2], [3,4]]
  * // returns [1, 2]
  * as at level=0 the only spacer needed is between [1,2] and [3,4]
  * and at level=1 the only two spacers needed is between 1 and 2 as well as
  * 3 and 4 since the spacer between 2 and 3 is handled at level=0
  */

  function spacersNeededAtEachLevel(array, level, levelData) {
    if (level == undefined) {
      level = 0;
    } else {
      level += 1;
    }

    if (levelData == undefined) {
      levelData = [];
    }

    if (level >= levelData.length) {
      levelData.push(array.length - 1);
    } else {
      levelData[level] += array.length - 1;
    }

    array.map(function (e, i) {
      if (Array.isArray(e)) {
        spacersNeededAtEachLevel(e, level, levelData);
      }
    });
    return levelData;
  }

  var math = /*#__PURE__*/Object.freeze({
    round: round,
    tickRange: tickRange,
    partitionRangeInto: partitionRangeInto,
    euclideanDistance: euclideanDistance,
    quartiles: quartiles,
    calculateWidthOfObject: calculateWidthOfObject,
    calculateWidthOfSpacer: calculateWidthOfSpacer,
    spacersNeededAtEachLevel: spacersNeededAtEachLevel
  });

  /**
  * Hypenates all strings together
  * @param {string[]} arguments
  * @returns {string} "arg1-arg2-...-argn"
  */

  function hypenate() {
    return Array.prototype.slice.call(arguments).join('-');
  }
  /**
  * Trys to reduce text to fit in specified area, made for tick labels as called by
  * @see{@link axis}
  * @param {d3.selection} t container for specific axis tick
  * @param {string} text to be the label of the passed axis tick
  * @param {boolean} orient of the axis, true is horizontal, false is vertical
  * @param {number} tickLength is the length of the text
  * @param {number} space is the amount of availble space for the text and the tick to fit in
  * @param {boolean} overflowQ whether or not allowed to go over the alloted space
  * @returns {none}
  */

  function truncateText(t, text, orient, tickLength, space, overflowQ) {
    var rect = t.node().getBoundingClientRect();
    t.text(text);

    while (Math.max(rect.width, rect.height) > space - tickLength) {
      text = String(text);
      text = text.slice(0, text.length - 1);
      t.text(text + '...');
      rect = t.node().getBoundingClientRect();
      if (text.length == 0) break;
    }
  }
  function truncateString(string, space, font) {
    var chars = space / font;

    if (chars < string.length) {
      return string.slice(0, Math.round(chars - 5)) + '...';
    } else {
      return string;
    }
  }

  var str = /*#__PURE__*/Object.freeze({
    hypenate: hypenate,
    truncateText: truncateText,
    truncateString: truncateString
  });

  /**
  * Extracts x and y of translate from transform property
  * @param {string} transform transform property of svg element
  * @returns {number[]} x, y of translate(x, y)
  */

  function getTranslation(transform) {
    // Create a dummy g for calculation purposes only. This will never
    // be appended to the DOM and will be discarded once this function
    // returns.
    var g = document.createElementNS('http://www.w3.org/2000/svg', 'g'); // Set the transform attribute to the provided string value.

    transform = transform == undefined ? 'translate(0,0)' : transform;
    g.setAttributeNS(null, 'transform', transform); // consolidate the SVGTransformList containing all transformations
    // to a single SVGTransform of type SVG_TRANSFORM_MATRIX and get
    // its SVGMatrix.

    var matrix = g.transform.baseVal.consolidate().matrix; // As per definition values e and f are the ones for the translation.

    return [matrix.e, matrix.f];
  }
  /**
  * recursively ascends element.parentElement to find a svg tag
  * @param {Element} element
  * @returns {Element | undefined}
  */

  function getContainingSVG(element) {
    var parent = element.parentElement; // if (parent == null || parent == undefined) {
    //   parent = element.node().parentElement
    // }

    var tag = parent.tagName.toLowerCase();

    if (tag === 'svg') {
      return parent;
    }

    if (tag === 'html') {
      return undefined;
    }

    return getContainingSVG(parent);
  }
  /**
  * Trys to use d3.selection to get element, if it doesnt exist, makes one
  * @param {d3.selection} sel selection in which to try and find object
  * @param {string} tag tag of which to try and select
  * @param {string} [cls=''] class of tag to try and grab
  * @returns {d3.selection} of either append or selected tag.cls within sel
  */

  function safeSelect(sel, tag, cls) {
    var clsStr = cls == undefined ? '' : '.' + cls;
    var sSel = sel.select(tag + clsStr).empty() ? sel.append(tag) : sel.select(tag + clsStr);
    return sSel.classed(clsStr.replace('.', ''), true).attr('transform', sSel.attr('transform') == undefined ? 'translate(0,0)' : sSel.attr('transform'));
  }
  /**
  * Adds a clip-path rect and binds it to container
  * @param {d3.selection} container in which to add the clip-path and to which to bind the cliping path to
  * @param {Object} rect the coordinates (x, y, width, height) of the clip-path
  * @param {string} namespace
  * @returns {d3.selection} of the clip-path rect
  */

  function cpRect(container, rect, namespace) {
    var defs = safeSelect(container, 'defs', hypenate(namespace, 'definitions'));
    var cp = safeSelect(defs, 'clipPath', hypenate(namespace, 'clip-path')).attr('id', hypenate(namespace, 'clip-path'));
    var cpRect = safeSelect(cp, 'rect').attr('x', rect.x).attr('y', rect.y).attr('width', rect.width).attr('height', rect.height);
    defs.raise(); // set clipping path to container

    container.attr('clip-path', 'url(#' + hypenate(namespace, 'clip-path') + ')');
    return cpRect;
  }
  /**
  * Adds a background rect t to container
  * @param {d3.selection} container in which to add the background rectangle
  * @param {Object} rect the coordinates (x, y, width, height) of the background
  * @param {string} fill the color of the background
  * @returns {d3.selection} of the background fill
  */

  function bgRect(container, rect, fill) {
    return safeSelect(container, 'rect', 'bg').attr('x', rect.x).attr('y', rect.y).attr('width', rect.width).attr('height', rect.height).attr('fill', fill);
  }
  /**
  * Sets up the container for making chart elements. This includes making
  * a clip-path rect bound to the passed container, a background rect, and
  * a g element with class <namespace>-object-container.
  * @param {d3.selection} container in which to add the clip-path and background
  * @param {string} namespace
  * @param {Object} rect the coordinates (x, y, width, height) of the background and clip-path
  * @param {string} fill the color of the background
  * @returns {d3.selection} of g.<namespace>-object-container
  *
  * @see{@link bgRect}
  * @see{@link cpRect}
  */

  function setupContainer(selection, namespace, rect, fill) {
    // the container for three main items, bg, defs, and object-container
    var container = safeSelect(selection, 'g', namespace),
        bg = bgRect(container, rect, fill),
        cp = cpRect(container, rect, namespace),
        objectContainer = safeSelect(container, 'g', hypenate(namespace, 'object-container'));
    return objectContainer;
  }
  function absolutePosition(selection) {
    var element = selection.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = getContainingSVG(element);
    var svgPosition = containerSVG.getBoundingClientRect();
    return {
      top: elementPosition.top - svgPosition.top,
      left: elementPosition.left - svgPosition.left,
      bottom: elementPosition.bottom - svgPosition.top,
      right: elementPosition.right - svgPosition.left,
      height: elementPosition.height,
      width: elementPosition.width
    };
  }

  var sel = /*#__PURE__*/Object.freeze({
    getTranslation: getTranslation,
    getContainingSVG: getContainingSVG,
    safeSelect: safeSelect,
    cpRect: cpRect,
    bgRect: bgRect,
    setupContainer: setupContainer,
    absolutePosition: absolutePosition
  });

  function resizeDebounce(f, wait) {
    var resize = debounce(function () {
      f();
    }, wait);
    window.addEventListener('resize', resize);
  }
  function setget(arg, fn) {
    return function (_) {
      return arguments.length ? (arg = _, fn) : arg;
    };
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;

      var later = function later() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  function setupStandardChartContainers(selection, namespace, container) {
    var margins = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {
      top: 0.01,
      bottom: 0.01,
      left: 0.01,
      right: 0.01
    };
    var svg = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {
      w: 0.8,
      h: 0.6
    };
    var axes = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {
      y: 0.1,
      x: 0.1
    };
    var leg = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : {
      x: 0,
      margin: 0,
      pos: 'left' // absolute width of legend and space on either size

    };

    if (container == undefined) {
      container = {
        w: window.innerWidth,
        h: window.Height
      };
    } // SVG width and height


    var svgSpace = {
      w: container.w * svg.w,
      h: container.h * svg.h
    };
    var margPx = {
      top: margins.top * svgSpace.h,
      bottom: margins.bottom * svgSpace.h,
      left: margins.left * svgSpace.w,
      right: margins.right * svgSpace.w
    },
        // Space after removing margins
    chartSpace = {
      w: svgSpace.w - margPx.left - margPx.right,
      h: svgSpace.h - margPx.top - margPx.bottom
    },
        // main dimension of x and y axies
    // e.g. defines how tall x axis is as length is determined by plotRect.w
    axesSpace = {
      x: chartSpace.h * axes.x,
      y: chartSpace.w * axes.y
    },
        // space left for drawing the chart properly (e.g. bars, violins, etc)
    drawingSpace = {
      x: chartSpace.w - axesSpace.y - leg.x - 2 * leg.margin,
      y: chartSpace.h - axesSpace.x
    },
        legRect = {
      x: leg.margin + margPx.left + (leg.pos == 'left' ? 0 : drawingSpace.x + axesSpace.y),
      y: margPx.top,
      // this is soomehow getting calculated incorectly
      w: leg.x,
      h: drawingSpace.y
    },
        yAxisRect = {
      x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2 * leg.margin : 0),
      y: margPx.top,
      w: axesSpace.y,
      h: drawingSpace.y
    },
        plotRect = {
      x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2 * leg.margin : 0),
      y: margPx.top,
      w: drawingSpace.x,
      h: drawingSpace.y
    },
        xAxisRect = {
      x: axesSpace.y + margPx.left + (leg.pos == 'left' ? leg.x + 2 * leg.margin : 0),
      y: margPx.top + drawingSpace.y,
      w: drawingSpace.x,
      h: axesSpace.x
    };
    container = safeSelect(selection, 'svg', namespace).style('width', svgSpace.w + 'px').style('height', svgSpace.h + 'px');
    var axesC = safeSelect(container, 'g', hypenate(namespace, 'axes'));
    var legC = safeSelect(container, 'g', hypenate(namespace, 'legend')).attr('transform', "translate(" + legRect.x + "," + legRect.y + ")");
    var plot = safeSelect(container, 'g', hypenate(namespace, 'plot')).attr('transform', "translate(" + plotRect.x + "," + plotRect.y + ")");
    var xAxis = safeSelect(axesC, 'g', hypenate(namespace, 'x-axis')).attr('transform', "translate(" + xAxisRect.x + "," + xAxisRect.y + ")");
    var yAxis = safeSelect(axesC, 'g', hypenate(namespace, 'y-axis')).attr('transform', "translate(" + yAxisRect.x + "," + yAxisRect.y + ")");
    return {
      svg: {
        selection: container,
        rect: svgSpace
      },
      plot: {
        selection: plot,
        rect: plotRect
      },
      xAxis: {
        selection: xAxis,
        rect: xAxisRect
      },
      yAxis: {
        selection: yAxis,
        rect: yAxisRect
      },
      legend: {
        selection: legC,
        rect: legRect
      }
    };
  }

  var misc = /*#__PURE__*/Object.freeze({
    resizeDebounce: resizeDebounce,
    setget: setget,
    setupStandardChartContainers: setupStandardChartContainers
  });

  /**
   * calls console.group if d3sm.debugQ == true
   * @param {string} name of the group
   * @returns {undefined}
   */
  function consoleGroup(name) {
    if (window.d3sm.debugQ === true) {
      console.group(name);
    }
  }
  /**
   * calls console.groupEnd if d3sm.debugQ == true
   * @returns {undefined}
   */

  function consoleGroupEnd() {
    if (window.d3sm.debugQ === true) {
      console.groupEnd();
    }
  }
  /**
   * Calls console.log if d3sm.debugQ == true
   * @param {string} func name of the function logging
   * @param {string} msg to log
   * @param {Object} data to be logged along side the message
   * @returns {undefined}
   */

  function log(func, msg, data) {
    if (window.d3sm.debugQ === true) {
      console.log("%c[d3sm::".concat(func, "]:\t").concat(msg), ['background: #6cd1ef', 'border-radius: 5000px', 'padding: 0px 2px', 'font-size: 14px'].join(';'));
      console.table(data); // console.trace()
    }
  }
  /**
   * Calls console.warn if d3sm.debugQ == true
   * @param {string} func name of the function warning
   * @param {string} msg to display
   * @param {Object} data to be displayed along side the message
   * @returns {undefined}
   */

  function warn(func, msg, data) {
    if (window.d3sm.debugQ === true) console.warn("%c[d3sm::".concat(func, "]:\t").concat(msg), ['background: #ffd53e', 'border-radius: 5000px', 'padding: 0px 2px', 'font-size: 14px'].join(';'));
    console.table(data);
  }
  /**
   * Calls the console.info if d3sm.debugQ == true
   * @param {string} func name of the function providing info
   * @param {string} msg to display
   * @param {Object} data to be displayed along side the message
   * @returns {undefined}
   */

  function info(func, msg, data) {
    if (window.d3sm.debugQ) console.info("%c[d3sm::".concat(func, "]:\t").concat(msg), ['background: #009ccd', 'border-radius: 5000px', 'padding: 0px 2px', 'font-size: 14px'].join(';'));
    console.table(data);
  }
  /**
   * Calls console.error if d3sm.debugQ == true
   * @param {string} func name of the function which sends the error
   * @param {string} msg to display
   * @param {Object} data to be displayed along side the message
   * @returns {undefined}
   */

  function error(func, msg, data) {
    if (window.d3sm.debugQ) console.error("[d3sm::".concat(func, "]:\t").concat(msg, "\t%o"), data);
  }

  var con = /*#__PURE__*/Object.freeze({
    consoleGroup: consoleGroup,
    consoleGroupEnd: consoleGroupEnd,
    log: log,
    warn: warn,
    info: info,
    error: error
  });

  /**
  * Draws a whisker for @see{@link boxwhisker}
  * @param {boolean} dir direction to draw whisker, should be either true (up, top) or false (down or bottom)
  * @param {number} x starting x coordinate in which to draw whisker
  * @param {number} y starting y coordinate in which to draw whisker
  * @param {number} w width of space in which to draw whisker
  * @param {number} h height of space in which to draw whisker
  * @param {number} per percentage of w or h (depends on o) to make whisker
  * @param {boolean} o orientation, true is horizontal and false is vertical
  * @returns {string} representing the svg path (i.e. the d attribute for a path tag)
  */
  function whiskerPath(dir, x, y, w, h, per, o) {
    // d = direction (true is up), p = percent width
    if (dir == 'up' || dir == 'top' || dir == true) {
      dir = true;
    }

    if (dir == 'down' || dir == 'bottom' || dir == false) {
      dir = false;
    }

    o = o == undefined ? 'horizontal' : o;
    per = per == undefined ? 1 : per;

    if (o != "horizontal") {
      var hh = h * per,
          w = dir ? w : -w,
          a = dir ? x + w : x,
          b = dir ? x : x + w,
          c = dir ? a : b;
      p = "M " + a + ' ' + h / 2 + ' ' + 'L ' + b + ' ' + h / 2 + ' ' + 'M ' + c + ' ' + (h / 2 - hh / 2) + ' ' + 'L ' + c + ' ' + (h / 2 + hh / 2) + ' ';
      return p;
    }

    var ww = w * per,
        a = dir ? y + h : y,
        b = dir ? y : y + h,
        p = "M " + w / 2 + ' ' + a + ' ' // straight line part
    + 'L ' + w / 2 + ' ' + b + ' ' // straight line part
    + 'h ' + -ww / 2 + ' ' + 0 + ' ' // horizontal line part
    + 'h ' + ww + ' ' + 0 + ' ';
    return p;
  }

  var paths = /*#__PURE__*/Object.freeze({
    whiskerPath: whiskerPath
  });

  var utils = {
    arr: arr,
    color: color,
    math: math,
    misc: misc,
    sel: sel,
    str: str,
    con: con,
    paths: paths
  };

  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                              SPACEGROUPING                                 **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Produces a function for spacing objects by an arbitrarly complex grouping
   * @returns {recursivelyPosition} the function for moving the objects
   * (see {@link groupingSpacer#recursivelyPosition})
   * @namespace groupingSpacer
   */

  function groupingSpacer() {
    var
    /*@var {boolean} horizontalQ @default*/

    /**
    * Whether or not to space objects horizontally or vertically.
    * (see {@link groupingSpacer.horizontalQ})
    * @param {boolean} [horizontalQ=true]
    * @memberof groupingSpacer#
    * @instance
    */
    horizontalQ = true,

    /**
    * The scale to use to position elements if {@link groupingSpacer#moveby}="string"
    * (see {@link groupingSpacer.scale})
    * @param {d3.scale} [scale=d3.scaleLinear()]
    * @memberof groupingSpacer#
    * @instance
    */
    scale = d3$1.scaleLinear(),

    /**
    * How elements in the complex grouping should be moved over by.
    * By default, moveby="category", which moves objects by the complex grouping
    * But objects can also be moved over by scale.
    * (see {@link groupingSpacer.moveby})
    * @param {string} [moveby="category"]
    * @memberof groupingSpacer#
    * @instance
    */
    moveby = 'category',

    /**
    * How many objects are there in total
    * (see {@link groupingSpacer.numberOfObjects})
    * @param {number} [numberOfObjects=none]
    * @memberof groupingSpacer#
    * @instance
    */
    numberOfObjects,

    /**
    * The class given to an nested <g> tag whose parent(s) have the correct transition
    * properties
    * (see {@link groupingSpacer.numberOfObjects})
    * @param {string} [numberOfObjects='d3sm-groupped-item']
    * @memberof groupingSpacer#
    * @instance
    */
    objectClass = 'd3sm-groupped-item',

    /**
    * The size of the objects being positioned
    * (see {@link groupingSpacer.objectSize})
    * @param {number} [objectSize=none]
    * @memberof groupingSpacer#
    * @instance
    */
    objectSize,

    /**
    * The size of the un-nested spacer between objects
    * (see {@link groupingSpacer.spacerSize})
    * @param {number} [spacerSize=none]
    * @memberof groupingSpacer#
    * @instance
    */
    spacerSize,

    /**
    * The duration of transitions in ms
    * (see {@link groupingSpacer.transitionDuration})
    * @param {number} [transitionDuration=1000]
    * @memberof groupingSpacer#
    * @instance
    */
    transitionDuration = 1000,

    /**
    * The ease function for the transitions
    * (see {@link groupingSpacer.easeFunc})
    * @param {d3.ease} [easeFunc=d3.easeSin]
    * @memberof groupingSpacer#
    * @instance
    */
    easeFunc = d3$1.easeSin,

    /**
    * The namespace for the objects being moved
    * (see {@link groupingSpacer.namespace})
    * @param {string} [namespace='spacer']
    * @memberof groupingSpacer#
    * @instance
    */
    namespace = 'spacer',

    /**
    * The animation for new objects being added
    * (see {@link groupingSpacer.enterFunction})
    * @param {function} enterFunction
    * @memberof groupingSpacer#
    * @instance
    * @example
    * // by default
    * function(newObjectSelection) {
    *  newObjectSelection.attr('transform', function(d, i){
    *    var
    *    x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
    *    y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
    *    t = 'translate('+x+','+y+')'
    *    return t
    *  })
    * }
    */
    enterFunction = function enterFunction(cur) {
      cur.attr('transform', function (d, i) {
        var // x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
        // y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
        x = horizontalQ ? window.outerWidth : 0,
            y = !horizontalQ ? window.outerWidth : 0,
            t = 'translate(' + x + ',' + y + ')'; // if(y == undefined) {console.con.log(cur.node(), y, d)}

        return t;
      });
    },

    /**
    * The animation for old objects being removed
    * (see {@link groupingSpacer.exitFunction})
    * @param {function} exitFunction
    * @memberof groupingSpacer#
    * @instance
    * @example
    * // by default
    * oldObjectSelection.transition().duration(transitionDuration).ease(easeFunc)
    * .attr('transform', function(d, i){
    *     var
    *   x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
    *   y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
    *   t = 'translate('+x+','+y+')'
    *   return t
    * }).remove()
    */
    exitFunction = function exitFunction(cur) {
      log("groupingSpacer", "exiting with", {
        current: cur,
        currentNode: cur.node()
      });
      cur.selectAll('g').classed('to-remove', true);
      cur.transition().duration(transitionDuration * 0.9).ease(easeFunc).attr('transform', function (d, i) {
        var // x = horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
        // y = !horizontalQ ? objectSize * numberOfObjects + spacerSize * (numberOfObjects - 1) : 0,
        x = horizontalQ ? window.outerWidth : 0,
            y = !horizontalQ ? window.outerWidth : 0,
            t = 'translate(' + x + ',' + y + ')'; // if(y == undefined) {console.con.log(cur.node(), y, d)}

        return t;
      }).remove();
    };
    /**
     * Gets / sets horizontalQ (whether or not to space objects horizontally or vertically).
     * (see {@link groupingSpacer#horizontalQ})
     * @param {string} [_=none]
     * @returns {groupingSpacer | string}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.horizontalQ = function (_) {
      return arguments.length ? (horizontalQ = _, recursivelyPosition) : horizontalQ;
    };
    /**
     * Gets / sets the scale to use to position elements if {@link groupingSpacer#moveby}="string"
     * (see {@link groupingSpacer#scale})
     * @param {d3.scale} [_=none]
     * @returns {groupingSpacer | d3.scale}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.scale = function (_) {
      return arguments.length ? (scale = _, recursivelyPosition) : scale;
    };
    /**
     * Gets / sets moveby (whether or not to move by scale or by grouping).
     * (see {@link groupingSpacer#moveby})
     * @param {string} [_=none]
     * @returns {groupingSpacer | string}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.moveby = function (_) {
      return arguments.length ? (moveby = _, recursivelyPosition) : moveby;
    };
    /**
     * Gets / sets numberOfObjects.
     * (see {@link groupingSpacer#numberOfObjects})
     * @param {number} [_=none]
     * @returns {groupingSpacer | number}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.numberOfObjects = function (_) {
      return arguments.length ? (numberOfObjects = _, recursivelyPosition) : numberOfObjects;
    };
    /**
     * Gets / sets the objectClass (will be applied to <g> elements).
     * (see {@link groupingSpacer#objectClass})
     * @param {string} [_=none]
     * @returns {groupingSpacer | string}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.objectClass = function (_) {
      return arguments.length ? (objectClass = _, recursivelyPosition) : objectClass;
    };
    /**
     * Gets / sets the objectSize.
     * (see {@link groupingSpacer#objectSize})
     * @param {number} [_=none]
     * @returns {groupingSpacer | number}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.objectSize = function (_) {
      return arguments.length ? (objectSize = _, recursivelyPosition) : objectSize;
    };
    /**
     * Gets / sets the spacerSize.
     * (see {@link groupingSpacer#spacerSize})
     * @param {number} [_=none]
     * @returns {groupingSpacer | number}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.spacerSize = function (_) {
      return arguments.length ? (spacerSize = _, recursivelyPosition) : spacerSize;
    };
    /**
     * Gets / sets the transitionDuration.
     * (see {@link groupingSpacer#transitionDuration})
     * @param {number} [_=none]
     * @returns {groupingSpacer | number}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, recursivelyPosition) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc.
     * (see {@link groupingSpacer#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {groupingSpacer | d3.ease}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, recursivelyPosition) : easeFunc;
    };
    /**
     * Gets / sets the namespace.
     * (see {@link groupingSpacer#namespace})
     * @param {string} [_=none]
     * @returns {groupingSpacer | string}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.namespace = function (_) {
      return arguments.length ? (namespace = _, recursivelyPosition) : namespace;
    };
    /**
     * Gets / sets the enterFunction.
     * (see {@link groupingSpacer#enterFunction})
     * @param {function} [_=none]
     * @returns {groupingSpacer | function}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.enterFunction = function (_) {
      return arguments.length ? (enterFunction = _, recursivelyPosition) : enterFunction;
    };
    /**
     * Gets / sets the exitFunction.
     * (see {@link groupingSpacer#exitFunction})
     * @param {function} [_=none]
     * @returns {groupingSpacer | function}
     * @memberof groupingSpacer
     * @static
     */


    recursivelyPosition.exitFunction = function (_) {
      return arguments.length ? (exitFunction = _, recursivelyPosition) : exitFunction;
    };
    /**
     * recursively position the objects inside of the selection.
     * @param {d3.selection} selection
     * @param {Object} data
     * @param {level} [level=0] recursion depth
     * @returns {number} (how much to move next element)
     * @memberof groupingSpacer#
     */


    function recursivelyPosition(selection, data, level) {
      if (level == undefined) {
        level = 0;
      }

      var currentSelection = selection.selectAll('g.' + namespace + '[level="' + level + '"]').data(data);
      var enter = currentSelection.enter().append('g').attr('level', level).attr('class', namespace);
      var exit = currentSelection.exit();
      currentSelection = currentSelection.merge(enter);

      if (typeof exitFunction == 'function') {
        exit.each(function (d, i) {
          exitFunction(d3$1.select(this));
        });
      } else {
        exit.remove();
      } // spacer for current level


      var levelSpacer = spacerSize / (level + 1); // movement for current level

      var move = 0;
      currentSelection.each(function (currentElement, index) {
        var t = d3$1.select(this);

        if (t.attr('transform') == undefined && typeof enterFunction == 'function') {
          enterFunction(t);
        }

        t.transition().duration(transitionDuration).ease(easeFunc).attr('transform', function (d, i) {
          var x = horizontalQ ? moveby == "scale" ? scale(d) : move : 0,
              y = !horizontalQ ? moveby == "scale" ? scale(d) : move : 0,
              t = 'translate(' + x + ',' + y + ')';
          return t;
        });

        if (Array.isArray(currentElement)) {
          move += recursivelyPosition(t, currentElement, level + 1);
          var toRemove = t.selectAll('g.' + namespace + '[level="' + level + '"] > g.' + objectClass + '.' + namespace);

          if (typeof exitFunction == 'function') {
            toRemove.each(function (d, i) {
              exitFunction(d3$1.select(this));
            });
          } else {
            toRemove.remove();
          }
        } else {
          move += objectSize;
          var obj = t.select('g.' + namespace + '[level="' + level + '"] > g.' + objectClass + '.' + namespace);

          if (obj.empty()) {
            obj = t.append('g').attr('class', objectClass).classed(namespace, true);
          }

          obj.attr('parent-index', index);
          var toRemove = t.selectAll('g.' + namespace + '[level="' + (level + 1) + '"]');

          if (typeof exitFunction == 'function') {
            toRemove.each(function (d, i) {
              exitFunction(d3$1.select(this));
            });
          } else {
            toRemove.remove();
          }
        }

        move += index == currentSelection.size() - 1 ? 0 : levelSpacer;
      });
      return move;
    }

    return recursivelyPosition;
  }

  function _toConsumableArray(arr$$1) { return _arrayWithoutHoles(arr$$1) || _iterableToArray(arr$$1) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                  AXIS                                      **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates an axis
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/axes/index.html Demo}
   * @constructor axis
   * @param {d3.selection} selection
   * @namespace axis
   * @returns {function} axis
   */

  function axis(selection) {
    var
    /**
    * The orientation of the axis
    * (see {@link axis#orient})
    * @param {string} [orient='bottom']
    * @memberof axis#
    * @property
    */
    orient = 'bottom',
        // direction of the axis

    /**
    * Amount of horizontal space (in pixels) avaible to render the axis in
    * (see {@link axis#spaceX})
    * @param {number} [spaceX=0]
    * @memberof axis#
    * @property
    */
    spaceX = 0,

    /**
    * Amount of vertical space (in pixels) avaible to render the axis in
    * (see {@link axis.spaceY})
    * @param {number} [spaceY=0]
    * @memberof axis#
    * @property
    */
    spaceY = 0,

    /**
    * Whether or not to allow axis to render elements pass the main spatial dimension
    * given the orientation (see {@link axis#orient}), where {@link axis#orient}="bottom" or {@link axis#orient}="top"
    * the main dimension is {@link axis#spaceX} and where {@link axis#orient}="left" or {@link axis#orient}="right"
    * the main dimension is {@link axis#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof axis#
    * @property
    */
    overflowQ = false,
        // whether or not to allow overflow

    /**
    * Whether or not the axis labels are for categorical data. If false,
    * will use {@link axis#scale} to position ticks.
    * @param {boolean} [categoricalQ=false]
    * @memberof axis#
    * @property
    */
    categoricalQ = false,
        // whether or not the axis is showing values or groups

    /**
    * Whether or not the axis ticks should have guidelines
    * @param {boolean} [categoricalQ=false]
    * @memberof axis#
    * @property
    */
    guideLinesQ = false,
        // whether or not to allow overflow

    /**
    * How to group the tick labels
    * @param {Array[]} [grouping=undefined] list of putatively other lists, which should correspond to tickLabels
    * will space tick labels in nested lists closer together than outer lists
    * @memberof axis#
    * @property
    */
    grouping,

    /**
    * The scale for which non-categorial (see {@link axis#categoricalQ}) ticks should be spaced
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof axis#
    * @property
    */
    scale = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scale (see {@link axis#scale})
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof axis#
    * @property
    */
    domainPadding = 0.5,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link axis#orient}), where {@link axis#orient}="bottom" or {@link axis#orient}="top"
    * the main dimension is {@link axis#spaceX} and where {@link axis#orient}="left" or {@link axis#orient}="right"
    * the main dimension is {@link axis#spaceY}between ticks
    * @param {number} [objectSpacer=0.05]
    * @memberof axis#
    * @property
    */
    objectSpacer = 0.05,

    /**
    * The minimum size that an object can be if {@link axis#categoricalQ} is set to true
    * @param {number} [minObjectSize=15]
    * @memberof axis#
    * @property
    */
    minObjectSize = 15,

    /**
    * The maximum size that an object can be if {@link axis#categoricalQ} is set to true
    * @param {number} [maxObjectSize=15]
    * @memberof axis#
    * @property
    */
    maxObjectSize = 50,

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof axis#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of axis
    * @param {string} [namespace="d3sm-axis"]
    * @memberof axis#
    * @property
    */
    namespace = 'd3sm-axis',

    /**
    * Class name for tick container (<g> element)
    * @param {string} [objectClass="tick-group"]
    * @memberof axis#
    * @property
    */
    objectClass = 'tick-group',

    /**
    * Values to show at each tick. Only used if categoricalQ is set true. See {@link axis#categoricalQ}
    * @param {string[]} [tickLabels=undefined]
    * @memberof axis#
    * @property
    */
    tickLabels,
        // what to place at ticks

    /**
    * Values to show at each tick. Only used if categoricalQ is set false. See {@link axis#categoricalQ}
    * @param {string[] | number[]} [objectClass=undefined]
    * @memberof axis#
    * @property
    */
    tickValues,
        // where to place ticks if not

    /**
    * Number of ticks to display if categoricalQ is false. See {@link axis#categoricalQ}
    * @param {number} [numberOfTicks=5]
    * @memberof axis#
    * @property
    */
    numberOfTicks = 5,

    /**
    * Stroke color of the main axis line
    * @param {string} [lineStroke='black']
    * @memberof axis#
    * @property
    */
    lineStroke = 'black',

    /**
    * Stroke width of the main axis line
    * @param {number} [lineStrokeWidth=3]
    * @memberof axis#
    * @property
    */
    lineStrokeWidth = 3,

    /**
    * Stroke color of ticks
    * @param {string} [tickStroke='black']
    * @memberof axis#
    * @property
    */
    tickStroke = 'black',

    /**
    * Stroke number of ticks
    * @param {string} [tickStrokeWidth=2]
    * @memberof axis#
    * @property
    */
    tickStrokeWidth = 2,

    /**
    * Length - in pixels - of ticks
    * @param {number} [tickLength=10]
    * @memberof axis#
    * @property
    */
    tickLength = 10,
        tickTickLabelSpacer = 10,
        tickLabelMargin = 10,

    /**
    * Font size of tick labels
    * @param {number} [tickLabelFontSize=14]
    * @memberof axis#
    * @property
    */
    tickLabelFontSize = 14,

    /**
    * Min font size of tick labels
    * @param {number} [tickLabelMinFontSize=8]
    * @memberof axis#
    * @property
    */
    tickLabelMinFontSize = 8,

    /**
    * Max font size of tick labels
    * @param {number} [tickLabelMaxFontSize=20]
    * @memberof axis#
    * @property
    */
    tickLabelMaxFontSize = 20,

    /**
    * Text anchor of tick labels
    * @param {string} [tickLabelTextAnchor="middle"]
    * @memberof axis#
    * @property
    */
    tickLabelTextAnchor,

    /**
    * Rotation of tick labels
    * @param {number} [tickLabelRotation=0]
    * @memberof axis#
    * @property
    */
    tickLabelRotation,

    /**
    * Optional function for extracting the tick label from data
    * @param {function} [tickLabelFunc=undefined]
    * @memberof axis#
    * @property
    */
    tickLabelFunc = undefined,

    /**
    * Optional function for what to do when label is clicked
    * @param {function} [tickLabelOnClick=function(d, i){}]
    * @memberof axis#
    * @property
    */
    tickLabelOnClick = function tickLabelOnClick(d, i) {},

    /**
    * Optional function for what to do when label is hovered
    * @param {function} [tickLabelOnHoverFunc=function(d, i){}]
    * @memberof axis#
    * @property
    */
    tickLabelOnHoverFunc = function tickLabelOnHoverFunc(d, i) {
      return String(d).replace('-', ' ').replace('_', ' ');
    },

    /**
    * Length of guidelines
    * @param {function} [guidelineSpace=undefined]
    * @memberof axis#
    * @property
    */
    guidelineSpace,

    /**
    * Stroke color of guidlines
    * @param {string} [guidelineSpace="#333333"]
    * @memberof axis#
    * @property
    */
    guideLineStroke = '#333333',

    /**
    * Stroke width of guidlines
    * @param {number} [guidelineSpace=2]
    * @memberof axis#
    * @property
    */
    guideLineStrokeWidth = 2,

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof axis#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof axis#
    * @property
    */
    easeFunc = d3$1.easeExp,

    /**
    * Closure variable for getting object size after calculation
    * @param {number} [objectSize=undefined]
    * @memberof axis#
    * @property
    */
    objectSize,

    /**
    * Closure variable for getting spacer size after calculation
    * @param {number} [spacerSize=undefined]
    * @memberof axis#
    * @property
    */
    spacerSize,

    /**
    * Decimal percision to round numerical tick labels to
    * @param {number} [roundTo=2]
    * @memberof axis#
    * @property
    */
    roundTo = 2,
        label,
        reverseScaleQ = false;

    axis.label = function (_) {
      return arguments.length ? (label = _, axis) : label;
    };

    axis.tickTickLabelSpacer = function (_) {
      return arguments.length ? (tickTickLabelSpacer = _, axis) : tickTickLabelSpacer;
    };

    axis.tickLabelMargin = function (_) {
      return arguments.length ? (tickLabelMargin = _, axis) : tickLabelMargin;
    };
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {axis | d3.selection}
     * @memberof axis
     * @property
     * by default selection = selection
     */


    axis.selection = function (_) {
      return arguments.length ? (selection = _, axis) : selection;
    };
    /**
     * Gets or sets the orientation in which items are manipulated
     * (see {@link axis#orient})
     * @param {string} [_=none] should be horizontal or vertical
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default orient="bottom"
     */


    axis.orient = function (_) {
      return arguments.length ? (orient = _, axis) : orient;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link axis#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default spaceX = undefined
     */


    axis.spaceX = function (_) {
      return arguments.length ? (spaceX = _, axis) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link axis#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default spaceY = undefined
     */


    axis.spaceY = function (_) {
      return arguments.length ? (spaceY = _, axis) : spaceY;
    };
    /**
     * Gets / sets whether or not axis is allowed to go beyond specified dimensions
     * (see {@link axis#spaceX})
     * @param {boolean} [_=none]
     * @returns {axis | boolean}
     * @memberof axis
     * @property
     * by default overflowQ = false
     */


    axis.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, axis) : overflowQ;
    };
    /**
     * Gets / sets whether or not axis will display categorial ticks or by numerical value
     * (see {@link axis#categoricalQ})
     * @param {boolean} [_=none]
     * @returns {axis | boolean}
     * @memberof axis
     * @property
     * by default categoricalQ = false
     */


    axis.categoricalQ = function (_) {
      return arguments.length ? (categoricalQ = _, axis) : categoricalQ;
    };
    /**
     * Gets / sets whether or not axis ticks should have guidelines
     * (see {@link axis#guideLinesQ})
     * @param {boolean} [_=none]
     * @returns {axis | boolean}
     * @memberof axis
     * @property
     * by default guideLinesQ = false
     */


    axis.guideLinesQ = function (_) {
      return arguments.length ? (guideLinesQ = _, axis) : guideLinesQ;
    };
    /**
     * Gets / sets how ticks should be groupped
     * (see {@link axis#grouping})
     * @param {Array[]} [_=none] list of putatively other lists, which should correspond to tickLabels
     * will space tick labels in nested lists closer together than outer lists
     * @returns {axis | Array[]}
     * @memberof axis
     * @property
     * by default grouping = undefined
     */


    axis.grouping = function (_) {
      return arguments.length ? (grouping = _, axis) : grouping;
    };
    /**
     * Gets / sets the scale for which non-categorial  ticks should
     * be spaced
     * (see {@link axis#scale})
     * @param {d3.scale} [_=none]
     * @returns {axis | d3.scale}
     * @memberof axis
     * @property
     * by default scale = d3.scaleLinear()
     */


    axis.scale = function (_) {
      return arguments.length ? (scale = _, axis) : scale;
    };
    /**
     * Gets / sets the padding for the domain of the scale
     * (see {@link axis#domainPadding})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default domainPadding = 0.5
     */


    axis.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, axis) : domainPadding;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link axis#objectSpacer})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default objectSpacer = 0.05
     */


    axis.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, axis) : objectSpacer;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link axis#minObjectSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default minObjectSize = 15
     */


    axis.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, axis) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link axis#maxObjectSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default maxObjectSize = 50
     */


    axis.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, axis) : maxObjectSize;
    };
    /**
     * Gets / sets the namespace
     * (see {@link axis#namespace})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default namespace = 'd3sm-axis'
     */


    axis.namespace = function (_) {
      return arguments.length ? (namespace = _, axis) : namespace;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link axis#backgroundFill})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default backgroundFill = 'transparent'
     */


    axis.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, axis) : backgroundFill;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link axis#objectClass})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default objectClass = 'tick-group'
     */


    axis.objectClass = function (_) {
      return arguments.length ? (objectClass = _, axis) : objectClass;
    };
    /**
     * Gets / sets the tickLabels
     * (see {@link axis#tickLabels})
     * @param {string[]} [_=none]
     * @returns {axis | string[]}
     * @memberof axis
     * @property
     * by default tickLabels = undefined
     */


    axis.tickLabels = function (_) {
      return arguments.length ? (tickLabels = _, axis) : tickLabels;
    };
    /**
     * Gets / sets the tickValues
     * (see {@link axis#tickValues})
     * @param {number[]} [_=none]
     * @returns {axis | number[]}
     * @memberof axis
     * @property
     * by default tickValues = undefined
     */


    axis.tickValues = function (_) {
      return arguments.length ? (tickValues = _, axis) : tickValues;
    };
    /**
     * Gets / sets the tickValues
     * (see {@link axis#numberOfTicks})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default numberOfTicks = 5
     */


    axis.numberOfTicks = function (_) {
      return arguments.length ? (numberOfTicks = _, axis) : numberOfTicks;
    };
    /**
     * Gets / sets the lineStroke
     * (see {@link axis#lineStroke})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default lineStroke = 'black'
     */


    axis.lineStroke = function (_) {
      return arguments.length ? (lineStroke = _, axis) : lineStroke;
    };
    /**
     * Gets / sets the lineStrokeWidth
     * (see {@link axis#lineStrokeWidth})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default lineStrokeWidth = 3
     */


    axis.lineStrokeWidth = function (_) {
      return arguments.length ? (lineStrokeWidth = _, axis) : lineStrokeWidth;
    };
    /**
     * Gets / sets the tickStroke
     * (see {@link axis#tickStroke})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default tickStroke = 'black'
     */


    axis.tickStroke = function (_) {
      return arguments.length ? (tickStroke = _, axis) : tickStroke;
    };
    /**
     * Gets / sets the tickStrokeWidth
     * (see {@link axis#tickStrokeWidth})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickStrokeWidth = 2
     */


    axis.tickStrokeWidth = function (_) {
      return arguments.length ? (tickStrokeWidth = _, axis) : tickStrokeWidth;
    };
    /**
     * Gets / sets the tickLength
     * (see {@link axis#tickLength})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickLength = 10
     */


    axis.tickLength = function (_) {
      return arguments.length ? (tickLength = _, axis) : tickLength;
    };
    /**
     * Gets / sets the tickLabelFontSize
     * (see {@link axis#tickLabelFontSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickLabelFontSize = 14
     */


    axis.tickLabelFontSize = function (_) {
      return arguments.length ? (tickLabelFontSize = _, axis) : tickLabelFontSize;
    };
    /**
     * Gets / sets the tickLabelMinFontSize
     * (see {@link axis#tickLabelMinFontSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickLabelMinFontSize = 8
     */


    axis.tickLabelMinFontSize = function (_) {
      return arguments.length ? (tickLabelMinFontSize = _, axis) : tickLabelMinFontSize;
    };
    /**
     * Gets / sets the tickLabelMaxFontSize
     * (see {@link axis#tickLabelMaxFontSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickLabelMaxFontSize = 20
     */


    axis.tickLabelMaxFontSize = function (_) {
      return arguments.length ? (tickLabelMaxFontSize = _, axis) : tickLabelMaxFontSize;
    };
    /**
     * Gets / sets the tickLabelTextAnchor
     * (see {@link axis#tickLabelTextAnchor})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default tickLabelTextAnchor = 'center'
     */


    axis.tickLabelTextAnchor = function (_) {
      return arguments.length ? (tickLabelTextAnchor = _, axis) : tickLabelTextAnchor;
    };
    /**
     * Gets / sets the tickLabelRotation
     * (see {@link axis#tickLabelRotation})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default tickLabelRotation = 0
     */


    axis.tickLabelRotation = function (_) {
      return arguments.length ? (tickLabelRotation = _, axis) : tickLabelRotation;
    };
    /**
     * Gets / sets the tickLabelFunc
     * (see {@link axis#tickLabelFunc})
     * @param {function} [_=none]
     * @returns {axis | function}
     * @memberof axis
     * @property
     * by default tickLabelFunc = undefined
     */


    axis.tickLabelFunc = function (_) {
      return arguments.length ? (tickLabelFunc = _, axis) : tickLabelFunc;
    };
    /**
    * Gets / sets the tickLabelOnClick
    * (see {@link axis#tickLabelOnClick})
    * @param {function} [_=none]
    * @returns {axis | function}
    * @memberof axis
    * @property
    */


    axis.tickLabelOnClick = function (_) {
      return arguments.length ? (tickLabelOnClick = _, axis) : tickLabelOnClick;
    };
    /**
     * Gets / sets the guidelineSpace
     * (see {@link axis#guidelineSpace})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default guidelineSpace = undefined
     */


    axis.guidelineSpace = function (_) {
      return arguments.length ? (guidelineSpace = _, axis) : guidelineSpace;
    };
    /**
     * Gets / sets the guideLineStroke
     * (see {@link axis#guideLineStroke})
     * @param {string} [_=none]
     * @returns {axis | string}
     * @memberof axis
     * @property
     * by default guideLineStroke = "#333333"
     */


    axis.guideLineStroke = function (_) {
      return arguments.length ? (guideLineStroke = _, axis) : guideLineStroke;
    };
    /**
     * Gets / sets the guideLineStrokeWidth
     * (see {@link axis#guideLineStrokeWidth})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default guideLineStrokeWidth = 2
     */


    axis.guideLineStrokeWidth = function (_) {
      return arguments.length ? (guideLineStrokeWidth = _, axis) : guideLineStrokeWidth;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link axis#transitionDuration})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default transitionDuration = 1000
     */


    axis.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, axis) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link axis#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {axis | d3.ease}
     * @memberof axis
     * @property
     * by default easeFunc = d3.easeExp
     */


    axis.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, axis) : easeFunc;
    };
    /**
     * Gets / sets the objectSize
     * (see {@link axis#objectSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default objectSize = undefined
     */


    axis.objectSize = function (_) {
      return arguments.length ? (objectSize = _, axis) : objectSize;
    };
    /**
     * Gets / sets the spacerSize
     * (see {@link axis#spacerSize})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default spacerSize = undefined
     */


    axis.spacerSize = function (_) {
      return arguments.length ? (spacerSize = _, axis) : spacerSize;
    };
    /**
     * Gets / sets the roundTo
     * (see {@link axis#roundTo})
     * @param {number} [_=none]
     * @returns {axis | number}
     * @memberof axis
     * @property
     * by default roundTo = 2
     */


    axis.roundTo = function (_) {
      return arguments.length ? (roundTo = _, axis) : roundTo;
    };

    axis.reverseScaleQ = function (_) {
      return arguments.length ? (reverseScaleQ = _, axis) : reverseScaleQ;
    };

    axis.tickLabelOnHoverFunc = function (_) {
      return arguments.length ? (tickLabelOnHoverFunc = _, axis) : tickLabelOnHoverFunc;
    };

    function axis() {
      var _utils$math;

      // for convenience in handling orientation specific values
      var horizontalQ = utils.arr.hasQ(['top', 'bottom', 'horizontal'], orient) ? true : false;
      var verticalQ = !horizontalQ; // background cliping rectangle

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY // modify the rect based on axis orientation

      };

      if (orient == "left") {
        bgcpRect.x -= spaceX;

        if (guideLinesQ) {
          bgcpRect.width += guidelineSpace;
        }
        /* these two lines increase the clipping rect to allow for text at the edge of the axis */

        bgcpRect.y -= tickLabelMaxFontSize;
        bgcpRect.height += 2 * tickLabelMaxFontSize;
      }

      if (orient == "bottom") {
        bgcpRect.y = bgcpRect.y;

        if (guideLinesQ) {
          bgcpRect.y -= guidelineSpace;
          bgcpRect.height += guidelineSpace;
        }
        /* these two lines increase the clipping rect to allow for text at the edge of the axis */

        bgcpRect.x -= tickLabelMaxFontSize;
        bgcpRect.width += 2 * tickLabelMaxFontSize;
      }

      if (orient == "top") {
        bgcpRect.y -= spaceY;

        if (guideLinesQ) {
          bgcpRect.height += guidelineSpace;
        }
        /* these two lines increase the clipping rect to allow for text at the edge of the axis */

        bgcpRect.y -= tickLabelMaxFontSize;
        bgcpRect.height += 2 * tickLabelMaxFontSize;
      }

      if (orient == "right") {
        bgcpRect.x = 0;

        if (guideLinesQ) {
          bgcpRect.width += guidelineSpace;
          bgcpRect.x -= guidelineSpace;
        }
        /* these two lines increase the clipping rect to allow for text at the edge of the axis */

        bgcpRect.y -= tickLabelMaxFontSize;
        bgcpRect.height += 2 * tickLabelMaxFontSize;
      }

      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill); // defaults for text-anchor and text rotation

      if (orient == 'top') {
        tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'start' : tickLabelTextAnchor;
        tickLabelRotation = tickLabelRotation == undefined ? -90 : tickLabelRotation;
      }

      if (orient == 'bottom') {
        tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'end' : tickLabelTextAnchor;
        tickLabelRotation = tickLabelRotation == undefined ? -90 : tickLabelRotation;
      }

      if (orient == 'left') {
        tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'end' : tickLabelTextAnchor;
        tickLabelRotation = tickLabelRotation == undefined ? 0 : tickLabelRotation;
      }

      if (orient == 'right') {
        tickLabelTextAnchor = tickLabelTextAnchor == undefined ? 'start' : tickLabelTextAnchor;
        tickLabelRotation = tickLabelRotation == undefined ? 0 : tickLabelRotation;
      }
      /*
      If categorical:
        -> use grouping if defined,
        -> else use the labels provided
      else:
        if grouping undefined
          and no specified number of tickes
            -> make numberOfTick ticks
            -> else use provided tick values
          -> use grouping
      */


      var tickData = categoricalQ ? grouping == undefined ? tickLabels : grouping : grouping == undefined ? numberOfTicks != undefined ? // ? (tickValues.length < numberOfTicks)
      (_utils$math = utils.math).tickRange.apply(_utils$math, _toConsumableArray(d3$1.extent(tickValues)).concat([numberOfTicks])) : tickValues : grouping;
      var flatTickData = utils.arr.flatten(tickData);
      var numberOfObjects = flatTickData.length;
      var space = horizontalQ ? spaceX : spaceY;
      var extent = d3$1.extent(flatTickData);

      if (reverseScaleQ) {
        extent.reverse();
      }

      var domain = reverseScaleQ ? [extent[0] + domainPadding, extent[1] - domainPadding] : [extent[0] - domainPadding, extent[1] + domainPadding];
      scale.domain(domain).range([horizontalQ ? 0 : spaceY, horizontalQ ? spaceX : 0]);
      /*
      Scales are based on the values of the chart and correspond to the spacings of the
      chart. If the chart has already been rendered, these values (expensive to caluclate) can
      be passed to axis to prevent recalculation.
      */
      // calculate object size if needed

      objectSize = objectSize == undefined ? utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ) : objectSize; // calculate spacer size if needed

      spacerSize = spacerSize == undefined ? utils.math.calculateWidthOfSpacer(flatTickData, space, objectSize, numberOfObjects, objectSpacer, overflowQ) : spacerSize;
      var objClass = utils.str.hypenate(namespace, categoricalQ ? objectClass + '-categorical' : objectClass);
      var spacerFunction = groupingSpacer().horizontalQ(horizontalQ).scale(scale).moveby(categoricalQ ? 'category' : 'scale').numberOfObjects(numberOfObjects).objectClass(objClass).objectSize(objectSize).spacerSize(spacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace(namespace);

      var tickEnterAnimation = function tickEnterAnimation(sel$$1) {
        var mt = scale(sel$$1.datum()),
            dist = scale(extent[1]) * 2,
            k = mt < extent[1] / 2 ? 1 : -1;
        k = horizontalQ ? k * -1 : k;
        sel$$1.attr('transform', function (d, i) {
          var x = horizontalQ ? dist * k : 0,
              y = !horizontalQ ? dist * k : 0,
              t = 'translate(' + x + ',' + y + ')';
          return t;
        });
      };

      var tickExitAnimation = function tickExitAnimation(sel$$1) {
        var mt = scale(sel$$1.datum()),
            dist = scale(extent[1]) * 2,
            k = mt < extent[1] / 2 ? 1 : -1;
        k = horizontalQ ? k * -1 : k;
        sel$$1.transition().duration(transitionDuration).ease(easeFunc).style('opacity', 0).attr('transform', function (d, i) {
          var x = horizontalQ ? dist * k : 0,
              y = !horizontalQ ? dist * k : 0,
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }).remove();
      };

      if (!categoricalQ) {
        spacerFunction.enterFunction(tickEnterAnimation);
        spacerFunction.exitFunction(tickExitAnimation);
      } // move tick containers


      spacerFunction(container, tickData, 0); // move by for x and y needed to center categorical ticks, labels, and guidelines

      function moveXBy(d, i, horizontalQ, categoricalQ, objectSize) {
        return horizontalQ ? categoricalQ ? objectSize / 2 : 0 : 0;
      }

      function moveYBy(d, i, verticalQ, categoricalQ, objectSize) {
        return verticalQ ? categoricalQ ? objectSize / 2 : 0 : 0;
      }

      var labelNameGroup = utils.sel.safeSelect(selection, 'g', utils.str.hypenate(namespace, 'axis-name'));
      var labelElement = utils.sel.safeSelect(labelNameGroup, 'text', utils.str.hypenate(namespace, 'name'));

      if (labelElement != undefined) {
        labelElement.text(label);

        if (orient == 'left' || orient == 'right') {
          labelElement.attr('transform', 'rotate(-90)');
        }

        var bbox = labelElement.node().getBoundingClientRect();
        labelNameGroup.attr('transform', function (d, i) {
          var x = 0,
              y = 0,
              t;

          if (orient == 'bottom') {
            x = spaceX - bbox.width - tickLabelMargin;
            y = -tickTickLabelSpacer;
          } else if (orient == 'top') {
            x = spaceX - bbox.width - tickLabelMargin;
            x = tickLabelMargin;
            y = bbox.height + tickTickLabelSpacer;
          } else if (orient == 'left') {
            x = bbox.width + tickTickLabelSpacer;
            y = bbox.height + tickLabelMargin;
          } else if (orient == 'right') {
            x = -(bbox.width + tickTickLabelSpacer);
            y = bbox.height + tickLabelMargin;
          }

          t = 'translate(' + x + ',' + y + ')';
          return t;
        });
      } else {
        labelElement.remove();
      }
      /*
      Idea from Stack Overflow
      https://stackoverflow.com/questions/50579535/d3-js-v4-truncate-text-to-fit-in-fixed-space/50585022?noredirect=1#comment88235562_50585022
      to use clip path to make things fit in fixed size. Have yet got this to work nicely.
      */
      // var defs = d3.select(container.node().parentNode).select('defs')
      // var tickLabelClipPath = utils.sel.safeSelect(defs, 'clipPath', utils.str.hypenate(namespace,'tick-label-clip-path')).attr('id',  utils.str.hypenate(namespace,'tick-label-clip-path'))
      // var tickLabelClipPathRect = utils.sel.safeSelect(tickLabelClipPath, 'rect',  utils.str.hypenate(namespace,'tick-label-clip-path-rect'))
      // .attr('x', 0)
      // .attr('y', 0)
      // .attr('width', function(d, i){
      //   if (horizontalQ) { return tickLabelFontSize }
      //   if (verticalQ) { return spaceX - tickLength }
      // })
      // .attr('height', function(d, i){
      //   if (verticalQ) { return tickLabelFontSize }
      //   if (horizontalQ) { return spaceY - tickLength }
      // })
      // for each tick container


      var ticks = container.selectAll('g:not(.to-remove).' + objClass).each(function (d, i) {
        var that = d3$1.select(this).style('opacity', 1); // make and move tick

        var tick = utils.sel.safeSelect(that, 'line', utils.str.hypenate(namespace, 'tick')).attr("x1", 0).attr("x2", horizontalQ ? 0 : orient == "left" ? -tickLength : tickLength).attr("y1", 0).attr('y2', verticalQ ? 0 : orient == "top" ? -tickLength : tickLength).attr('stroke', tickStroke).attr('stroke-width', tickStrokeWidth).attr('transform', function (d, i) {
          var x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
              y = moveYBy(d, i, verticalQ, categoricalQ, objectSize),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }); // make and move label

        var label = utils.sel.safeSelect(that, 'text', utils.str.hypenate(namespace, 'label')).text(function (d, i) {
          var s = typeof d == 'number' ? utils.math.round(d, roundTo) : d;
          s = utils.str.truncateString(String(s), (horizontalQ ? spaceY : spaceX) - tickLength - tickLabelMargin - tickTickLabelSpacer, tickLabelFontSize * 0.45);
          return s;
        }).attr('font-size', tickLabelFontSize).attr('text-anchor', tickLabelTextAnchor); // utils.str.truncateText(label, label.text(), orient, tickLength, horizontalQ ? spaceY : spaceX, overflowQ)

        label.attr('transform', function (d, i) {
          var rect = d3$1.select(this).node().getBoundingClientRect(),
              leng = d3$1.select(this).node().getComputedTextLength(),
              x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
              y = moveYBy(d, i, verticalQ, categoricalQ, objectSize); // on recall, rect changes because of rotation so need Math.min(rect.height, rect.width)

          if (orient == 'top') {
            y = -(tickLength + tickTickLabelSpacer); // y = tickLength+tickTickLabelSpacer;
            // y -= Math.max(rect.height, rect.width);

            x += Math.min(rect.height, rect.width) * 0.25; // x -= leng * 0.25 + s
          }

          if (orient == 'bottom') {
            y = tickLength + tickTickLabelSpacer;
            x += Math.min(rect.height, rect.width) * 0.25; // x += leng * 0.25 - s
          }

          if (orient == 'left') {
            x -= tickLength + tickTickLabelSpacer; // y += rect.height * 0.5; y-= rect.height/4

            y += Math.min(rect.height, rect.width) * 0.25; // y += leng * 0.25
          }

          if (orient == 'right') {
            x += tickLength + tickTickLabelSpacer; // y += Math.min(rect.height, rect.width) * 0.25
            // y += leng * 0.25

            y += rect.height * 0.5;
            y -= rect.height / 4;
          }

          var t = 'translate(' + x + ',' + y + ')',
              r = 'rotate(' + tickLabelRotation + ')';
          return t + r;
        }).on('mousemove', labelHover).on('mouseout', labelHoverOff).on('click', tickLabelOnClick); // .attr('clip-path', 'url(#'+utils.str.hypenate(namespace,'tick-label-clip-path')+')')
        // add guidlines as needed

        if (guideLinesQ) {
          var gline = utils.sel.safeSelect(that, 'line', utils.str.hypenate(namespace, 'guideline')).transition().duration(transitionDuration).ease(easeFunc).attr("x1", 0).attr("x2", horizontalQ ? 0 : orient == "left" ? guidelineSpace : -guidelineSpace).attr("y1", 0).attr('y2', verticalQ ? 0 : orient == "top" ? guidelineSpace : -guidelineSpace).attr('transform', function (d, i) {
            var x = moveXBy(d, i, horizontalQ, categoricalQ, objectSize),
                y = moveYBy(d, i, verticalQ, categoricalQ, objectSize),
                t = 'translate(' + x + ',' + y + ')';
            return t;
          });
        } else {
          that.select('line.' + utils.str.hypenate(namespace, 'guideline')).remove();
        }
      }); // apply alternating guidline thickness

      if (guideLinesQ) {
        container.selectAll('.' + utils.str.hypenate(namespace, 'guideline')).attr('stroke', function (d, i) {
          if (i % 2 == 0) {
            return utils.color.modifyHexidecimalColorLuminance(guideLineStroke, 0.8);
          }

          return guideLineStroke;
        }).attr('stroke-width', function (d, i) {
          if (i % 2 == 0) {
            return guideLineStrokeWidth * 0.8;
          }

          return guideLineStrokeWidth;
        }).attr('minor', function (d, i) {
          return i % 2 == 0;
        });
      }
      /***************************************************************************
      ** Make the line of the axis
      ***************************************************************************/


      var line = utils.sel.safeSelect(selection, 'path', utils.str.hypenate(namespace, 'line')) // .attr('x1', 0)
      // .attr('x2', horizontalQ ? spaceX : 0)
      // .attr('y1', 0)
      // .attr('y2', horizontalQ ? 0 : spaceY)
      .attr('d', horizontalQ ? 'M 0,0 H' + spaceX + ',0' : 'M 0,0 V 0,' + spaceY).attr('stroke', lineStroke).attr('stroke-width', lineStrokeWidth).classed('axis-line', true);
    } // hover of label show full text label in case it is truncated


    function labelHover(d, i) {
      var t = d3$1.select(this).style('fill', 'red');
      d3$1.select(t.node().parentNode).select("line." + utils.str.hypenate(namespace, 'tick')).attr("stroke", 'red').attr("stroke-width", tickStrokeWidth * 2);

      if (guideLinesQ) {
        d3$1.select(t.node().parentNode).select('line.' + utils.str.hypenate(namespace, 'guideline')).attr('stroke', 'red').attr('stroke-width', guideLineStrokeWidth * 2);
      }

      var s = typeof d == 'number' ? utils.math.round(d, roundTo) : d;
      var m = d3$1.mouse(d3$1.select('html').node());
      var div = utils.sel.safeSelect(d3$1.select('body'), 'div', utils.str.hypenate(namespace, 'guideline-tooltip')).attr('id', utils.str.hypenate(namespace, 'guideline-tooltip')).style('position', 'absolute').style('left', d3$1.event.pageX + 15 + 'px').style('top', d3$1.event.pageY + 15 + 'px').style('background-color', 'white').style('border-color', 'black') // .style('min-width', (tickLabelFontSize * (String(s).split('.')[0].length+3))+'px')
      // .style('min-height', (tickLabelFontSize * (String(s).split('.')[0].length+3))+'px')
      .style('border-radius', '10px').style('display', 'flex').style('justify-content', 'center').style('text-align', 'middle').style('padding', 4 + "px").style('border-style', 'solid').style('border-width', 2);
      var text = utils.sel.safeSelect(div, 'div').text(tickLabelOnHoverFunc(s, i)).style('color', 'black').style('align-self', 'center');
      var bbox = div.node().getBoundingClientRect();

      if (bbox.x + bbox.width > window.innerWidth) {
        div.style('left', d3$1.event.pageX - 15 - 300 + 'px');
      }
    }

    function labelHoverOff(d, i) {
      var t = d3$1.select(this).style('fill', 'black');
      d3$1.select(t.node().parentNode).select("line." + utils.str.hypenate(namespace, 'tick')).attr("stroke", tickStroke).attr("stroke-width", tickStrokeWidth);

      if (guideLinesQ) {
        var gline = d3$1.select(t.node().parentNode).select('line.' + utils.str.hypenate(namespace, 'guideline'));
        var minorQ = gline.attr('minor');
        gline.attr('stroke', function (d, ii) {
          if (minorQ == 'true') {
            return utils.color.modifyHexidecimalColorLuminance(guideLineStroke, 0.8);
          }

          return guideLineStroke;
        }).attr('stroke-width', function (d, ii) {
          if (minorQ == 'true') {
            return guideLineStrokeWidth * 0.8;
          }

          return guideLineStrokeWidth;
        });
      }

      d3$1.select("#" + utils.str.hypenate(namespace, 'guideline-tooltip')).remove();
    }

    return axis;
  }

  function _slicedToArray(arr$$1, i) { return _arrayWithHoles(arr$$1) || _iterableToArrayLimit(arr$$1, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

  function _iterableToArrayLimit(arr$$1, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr$$1[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr$$1) { if (Array.isArray(arr$$1)) return arr$$1; }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                 TOOLTIP                                    **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Produces a function for handling the tooltip
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/tooltip-design/index.html Demo}
   * @param {d3.selection} selection
   * @returns {tooltip}
   * @namespace tooltip
   */

  function tooltip(selection) {
    var keys, values, header, data, selection;
    /**
     * Gets / sets the keys to be displayed in the tooltip.
     * If not set, uses d3.keys(data[key])
     * @param {string[]} [_=none]
     * @returns {tooltip | string[]}
     * @memberof tooltip
     */

    tooltip.keys = function (_) {
      return arguments.length ? (keys = _, tooltip) : keys;
    };
    /**
     * Gets / sets the values to be displayed next to the keys.
     * If not set, uses data[key][keys[i]].
     * If a function, gets passed currentData (data[key]) and keys[i].
     * @param {*[]} [_=none]
     * @returns {tooltip | *[]}
     * @memberof tooltip
     */


    tooltip.values = function (_) {
      return arguments.length ? (values = _, tooltip) : values;
    };
    /**
     * Gets / sets the header to be displayed in the tooltip.
     * If not set, uses key
     * @param {string} [_=none]
     * @returns {tooltip | string}
     * @memberof tooltip
     */


    tooltip.header = function (_) {
      return arguments.length ? (header = _, tooltip) : header;
    };
    /**
     * Gets / sets the data (over the selection) to be used for the tooltip
     * @param {Object} [_=none]
     * @returns {tooltip | Object}
     * @memberof tooltip
     */


    tooltip.data = function (_) {
      return arguments.length ? (data = _, tooltip) : data;
    };
    /**
     * Gets / sets the selection for the tooltip to be applied on
     * @param {d3.selection} [_=none]
     * @returns {tooltip | d3.selection}
     * @memberof tooltip
     */


    tooltip.selection = function (_) {
      return arguments.length ? (selection = _, tooltip) : selection;
    };

    tooltip.mousemove = function (_) {
      return arguments.length ? (mousemove = _, tooltip) : mousemove;
    };

    tooltip.mouseout = function (_) {
      return arguments.length ? (mouseout = _, tooltip) : mouseout;
    };
    /**
     * Bind, via selection.on(), the mousemove and mouseout events
     * @returns undefined
     */


    function tooltip() {
      selection.on('mouseover', mousemove);
      selection.on('mousemove', mousemove);
      selection.on('mouseout', mouseout);
      selection.on('mouseleave', mouseout);
    }

    function mouseout(key, i) {
      d3$1.selectAll(".d3sm-tooltip").remove();
    }
    /**
     * Produces the tooltip on mousemove
     * @param {string} key of the object targeted by the mousemove
     * @param {number} i (index) of the object targeted by mousemove
     * @memberof tooltip
     * @private
     */


    function mousemove(key, i) {
      consoleGroup('d3sm-tooltip');
      var currentData = data[key];

      var _d3$mouse = d3$1.mouse(d3$1.select("html").node()),
          _d3$mouse2 = _slicedToArray(_d3$mouse, 2),
          x = _d3$mouse2[0],
          y = _d3$mouse2[1];

      log('tooltip', 'mousemove detected', {
        key: key,
        index: i,
        x: x,
        y: y
      });
      log('tooltip', 'current data', currentData);
      var div = safeSelect(d3$1.select('html'), 'tooltip', 'd3sm-tooltip').classed('card', true).style('max-width', '300px').style('background-color', "#212529").style('color', 'white');
      var cardBody = safeSelect(div, 'div', 'card-body');
      var cardTitle = safeSelect(cardBody, 'h5', 'card-title').text(header == undefined ? key : typeof header == 'function' ? header(key, currentData, i) : header).style('color', 'cyan');
      var table = safeSelect(cardBody, 'table', 'table').classed('table-dark', true);
      var tBody = safeSelect(table, 'tbody');
      tBody = tBody.selectAll('tr');
      tBody = tBody.data(keys == undefined ? d3$1.keys(currentData) : keys);
      tBody.exit().remove();
      var tr = tBody.enter().append('tr').style('max-width', '300px');
      tr.append('td').attr('class', function (d, i) {
        return 'tooltip-key';
      });
      tr.append('td').attr('class', function (d, i, j) {
        return 'tooltip-value';
      }).attr('tooltip-row-index', function (d, i) {
        return i;
      }); // tBody = tBody.merge(tr)

      consoleGroup('tooltip-rows');
      tBody.selectAll('.tooltip-key').text(function (d, i) {
        return d;
      });
      tBody.selectAll('tr .tooltip-value').text(function (d, i) {
        log('tooltip', 'trying to set value', {
          rowKey: d,
          rowIndex: i
        });
        var i = d3$1.select(this).attr('tooltip-row-index');
        var v = currentData[d];

        if (values != undefined) {
          v = values[i];

          if (typeof v == "function") {
            v = v(key, currentData, d);
          }
        }

        return typeof v == 'number' ? round(v, 5) : v;
      });
      consoleGroupEnd();
      consoleGroupEnd();
      x += 15; // x += 15

      var bbox = div.node().getBoundingClientRect();

      if (x + bbox.width > window.innerWidth - window.scrollX) {
        x = d3$1.event.pageX - bbox.width - 15;
      }

      if (y + bbox.height > window.innerHeight - window.scrollY) {
        y = d3$1.event.pageY - bbox.height - 15;
      }

      div.style('position') == "relative" ? div.style('position', 'absolute').style('left', x + 'px').style('top', y + 'px') : div.style('left', x + 'px').style('top', y + 'px'); // .transition().duration(200).ease(d3.easeSin)
      // if (bbox.x + bbox.width > window.innerWidth) {
      //   div.style('left', (d3.event.pageX-15-bbox.width)+'px')
      // }
      // if (bbox.y + bbox.height > window.innerHeight) {
      //   div.style('top', (d3.event.pageY-15-bbox.height)+'px')
      // }

      div.attr('z-index', 10000);
    }

    return tooltip;
  }

  /**
   * Creates a colorFunction
   * @constructor colorFunction
   * @namespace colorFunction
   * @returns {function} colorFunction
   */

  function colorFunction() {
    var /**
    * Default colors to use
    * @param {number[]} [colors=["#2c7bb6", "#00a6ca", "#00ccbc", "#90eb9d", "#ffff8c", "#f9d057", "#f29e2e", "#e76818", "#d7191c"]]
    * @memberof colorFunction#
    * @property
    */
    colors = ["#2c7bb6", "#00a6ca", "#00ccbc", "#90eb9d", "#ffff8c", "#f9d057", "#f29e2e", "#e76818", "#d7191c"],

    /**
    * Interpolator for colors
    * @param {d3.interpolation} [interpolation=d3.interpolateRgb]
    * @memberof colorFunction#
    * @property
    */
    interpolation = d3$1.interpolateRgb,

    /**
    * Function for modifying color luminance
    * @param {function} [modifyOpacity=modifyHexidecimalColorLuminance]
    * @memberof colorFunction#
    * @property
    */
    modifyOpacity = modifyHexidecimalColorLuminance,

    /**
    * How to modify color for stroke
    * @param {number} [strokeOpacity=0]
    * @memberof colorFunction#
    * @property
    */
    strokeOpacity = 0,

    /**
    * How to modify color for fill
    * @param {number} [fillOpacity=0.4]
    * @memberof colorFunction#
    * @property
    */
    fillOpacity = 0.4,

    /**
    * How to determine the color to use
    * @param {string} [colorBy='index']
    * @memberof colorFunction#
    * @property
    */
    colorBy = 'index',

    /**
    * Sets the scale for interpolating the colors
    * @param {number[]} [dataExtent=[0, colors.length - 1]]
    * @memberof colorFunction#
    * @property
    */
    dataExtent = [0, colors.length - 1],

    /**
    * Extracts the value to color by
    * @param {function} [valueExtractor=function(k, v, i) {return v}]
    * @memberof colorFunction#
    * @property
    */
    valueExtractor = function valueExtractor(k, v, i) {
      return v;
    },

    /**
    * Extracts the category to color by
    * @param {function} [categoryExtractor=function(k, v, i) {return v.category}]
    * @memberof colorFunction#
    * @property
    */
    categoryExtractor = function categoryExtractor(k, v, i) {
      return v.category;
    },

    /**
    * The different type of categories of which to color by
    * @param {string[]} [categories=undefined]
    * @memberof colorFunction#
    * @property
    */
    categories,

    /**
    * Scale for interpolating the colors
    * @param {d3.scale} [scale=d3.scaleLinear()]
    * @memberof colorFunction#
    * @property
    */
    scale = d3$1.scaleLinear().interpolate(interpolation).domain(dataExtent).range(colors),
        helperScale = d3$1.scaleLinear(); // var h = x => '#' + x.match(/\d+/g).map(y = z => ((+z < 16)?'0':'') + (+z).toString(16)).join('');


    var h = function h(x) {
      return "#" + x.match(/\d+/g).map(function (y, i) {
        return (+y < 16 ? '0' : '') + (+y).toString(16);
      }).join('');
    };
    /**
     * Gets or sets the default colors
     * (see {@link colorFunction#colors})
     * @param {number[]} [_=none]
     * @returns {colorFunction | number[]}
     * @memberof colorFunction
     * @property
     */


    colorFunction.colors = function (_) {
      return arguments.length ? (colors = _, scale.range(colors), colorFunction) : colors;
    };
    /**
     * Gets or sets the function for interpolating the colors
     * (see {@link colorFunction#interpolation})
     * @param {d3.interpolation} [_=none]
     * @returns {colorFunction | d3.interpolation}
     * @memberof colorFunction
     * @property
     */


    colorFunction.interpolation = function (_) {
      return arguments.length ? (interpolation = _, scale.interpolate(interpolation).range(colors), colorFunction) : interpolation;
    };
    /**
     * Gets or sets the values for the scale which transforms the value to a color
     * (see {@link colorFunction#dataExtent})
     * @param {number[]} [_=none]
     * @returns {colorFunction | number[]}
     * @memberof colorFunction
     * @property
     */


    colorFunction.dataExtent = function (_) {
      return arguments.length ? (dataExtent = _, scale.domain(dataExtent).interpolate(scale.interpolate()), colorFunction) : dataExtent;
    };
    /**
     * Gets or sets the vthe scale which transforms the value to a color
     * (see {@link colorFunction#scale})
     * @param {d3.scale} [_=none]
     * @returns {colorFunction | d3.scale}
     * @memberof colorFunction
     * @property
     */


    colorFunction.scale = function (_) {
      return arguments.length ? (_ = _.domain(scale.domain()).interpolate(scale.interpolate()).range(scale.range()), scale = _, colorFunction) : scale;
    };
    /**
     * Gets or sets the function for modify opacity
     * (see {@link colorFunction#modifyOpacity})
     * @param {function} [_=none]
     * @returns {colorFunction | function}
     * @memberof colorFunction
     * @property
     */


    colorFunction.modifyOpacity = function (_) {
      return arguments.length ? (modifyOpacity = _, colorFunction) : modifyOpacity;
    };
    /**
     * Gets or sets the value to modify the color for the stroke via {@link colorFunction#modifyOpacity}
     * (see {@link colorFunction#strokeOpacity})
     * @param {number} [_=none]
     * @returns {colorFunction | number}
     * @memberof colorFunction
     * @property
     */


    colorFunction.strokeOpacity = function (_) {
      return arguments.length ? (strokeOpacity = _, colorFunction) : strokeOpacity;
    };
    /**
     * Gets or sets the value to modify the color for the stroke via {@link colorFunction#fillOpacity}
     * (see {@link colorFunction#fillOpacity})
     * @param {number} [_=none]
     * @returns {colorFunction | number}
     * @memberof colorFunction
     * @property
     */


    colorFunction.fillOpacity = function (_) {
      return arguments.length ? (fillOpacity = _, colorFunction) : fillOpacity;
    };
    /**
     * Gets or sets the value to colorBy
     * (see {@link colorFunction#colorBy})
     * @param {string} [_=none]
     * @returns {colorFunction | string}
     * @memberof colorFunction
     * @property
     */


    colorFunction.colorBy = function (_) {
      return arguments.length ? (colorBy = _, colorFunction) : colorBy;
    };
    /**
     * Gets or sets the value of valueExtractor
     * (see {@link colorFunction#valueExtractor})
     * @param {function} [_=none]
     * @returns {colorFunction | function}
     * @memberof colorFunction
     * @property
     */


    colorFunction.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, colorFunction) : valueExtractor;
    };
    /**
     * Gets or sets the value of categoryExtractor
     * (see {@link colorFunction#categoryExtractor})
     * @param {function} [_=none]
     * @returns {colorFunction | function}
     * @memberof colorFunction
     * @property
     */


    colorFunction.categoryExtractor = function (_) {
      return arguments.length ? (categoryExtractor = _, colorFunction) : categoryExtractor;
    };
    /**
     * Gets or sets the value of categoryExtractor
     * (see {@link colorFunction#categories})
     * @param {string[]} [_=none]
     * @returns {colorFunction | string[]}
     * @memberof colorFunction
     * @property
     */


    colorFunction.categories = function (_) {
      return arguments.length ? (categories = _, colorFunction) : categories;
    };

    function colorFunction(key, value, index, type, hoverQ) {
      var c,
          opac = type == "fill" ? fillOpacity : strokeOpacity;
      updateScale();

      if (colorBy == "index") {
        c = type != undefined ? modifyOpacity(h(scale(index)), opac) : h(scale(index));
      } else if (colorBy == 'value') {
        var v = valueExtractor(key, value, index); // if (v < dataExtent[0]) {dataExtent[0] = v; updateScale()}
        // if (v > dataExtent[1]) {dataExtent[1] = v; updateScale()}

        c = type != undefined ? modifyOpacity(h(scale(v)), opac) : h(scale(v));
      } else if (colorBy == 'category') {
        var cat = categoryExtractor(key, value, index);
        var v = categories.indexOf(cat);
        c = type != undefined ? modifyOpacity(h(scale(v)), opac) : h(scale(v));
      } else {
        c = type != undefined ? modifyOpacity(h(scale(index)), opac) : h(scale(index));
      }

      return c;
    }

    function updateScale() {
      helperScale.domain([0, colors.length]);

      if (colorBy == 'category' && categories != undefined) {
        helperScale.range([0, categories.length]);
      } else {
        helperScale.range(dataExtent);
      }

      var a = Array(colors.length).fill(0).map(function (d, i) {
        return helperScale(i);
      });
      scale.domain(a);
    }

    return colorFunction;
  }

  function _toConsumableArray$1(arr$$1) { return _arrayWithoutHoles$1(arr$$1) || _iterableToArray$1(arr$$1) || _nonIterableSpread$1(); }

  function _nonIterableSpread$1() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$1(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$1(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                 SCATTER                                    **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates a scatter
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/scatter/index.html Demo}
   * @constructor scatter
   * @param {d3.selection} selection
   * @namespace scatter
   * @returns {function} scatter
   */

  function scatter(selection) {
    var
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a point
    * (see {@link scatter#data})
    * @param {Object} [data=undefined]
    * @memberof scatter#
    * @property
    */
    data,

    /**
    * Amount of horizontal space (in pixels) avaible to render the scatter in
    * (see {@link scatter#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof scatter#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the scatter in
    * (see {@link scatter.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof scatter#
    * @property
    */
    spaceY,

    /**
    * The scale for which scatter x values should be transformed by
    * @param {d3.scale} [scaleX=d3.scaleLinear]
    * @memberof scatter#
    * @property
    */
    scaleX = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scaleX (see {@link scatter#scaleX})
    * @param {number} [domainPaddingX=0.5]
    * @memberof scatter#
    * @property
    */
    domainPaddingX = 0.5,

    /**
    * The function for getting the x value of the current point
    * @param {function} [valueExtractorX=function(d, i){return data[d]['x']}]
    * @memberof scatter#
    * @property
    */
    valueExtractorX = function valueExtractorX(d, i) {
      return data[d]['x'];
    },

    /**
    * The scale for which scatter y values should be transformed by
    * @param {d3.scale} [scaleY=d3.scaleLinear]
    * @memberof scatter#
    * @property
    */
    scaleY = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scaleY (see {@link scatter#scaleY})
    * @param {number} [domainPaddingY=0.5]
    * @memberof scatter#
    * @property
    */
    domainPaddingY = 0.5,

    /**
    * The function for getting the y value of the current point
    * @param {function} [valueExtractorY=function(d, i){return data[d]['y']}]
    * @memberof scatter#
    * @property
    */
    valueExtractorY = function valueExtractorY(d, i) {
      return data[d]['y'];
    },

    /**
    * The scale for which scatter r values should be transformed by
    * @param {d3.scale} [scaleR=d3.scaleLinear]
    * @memberof scatter#
    * @property
    */
    scaleR = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scaleR (see {@link scatter#scaleR})
    * @param {number} [domainPaddingR=0.5]
    * @memberof scatter#
    * @property
    */
    domainPaddingR = 0.5,

    /**
    * The function for getting the r value of the current point
    * @param {function} [valueExtractorR=function(d, i){return 2}]
    * @memberof scatter#
    * @property
    */
    valueExtractorR = function valueExtractorR(d, i) {
      return 2;
    },

    /**
    * The min radius a point can have
    * @param {function} [minRadius=2]
    * @memberof scatter#
    * @property
    */
    minRadius = 2,

    /**
    * The min radius a point can have
    * @param {function} [maxRadius=10]
    * @memberof scatter#
    * @property
    */
    maxRadius = 10,

    /**
    * The stroke width of the points
    * @param {number} [pointStrokeWidth=2]
    * @memberof scatter#
    * @property
    */
    pointStrokeWidth = 2,

    /**
    * Instance of ColorFunction
    * @param {function} [colorFunction = colorFunction()]
    * @memberof scatter#
    * @property
    */
    colorFunction$$1 = colorFunction(),

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof scatter#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of scatter
    * @param {string} [namespace="d3sm-scatter"]
    * @memberof scatter#
    * @property
    */
    namespace = 'd3sm-scatter',

    /**
    * Class name for scatter container (<circle> element)
    * @param {string} [objectClass="scatter-point"]
    * @memberof scatter#
    * @property
    */
    objectClass = 'scatter-point',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof scatter#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof scatter#
    * @property
    */
    easeFunc = d3$1.easeExp,
        // useful values to extract to prevent re-calculation

    /**
    * The keys of the points
    * @param {string[]} [pointKeys=undefined]
    * @memberof scatter#
    * @property
    */
    pointKeys,

    /**
    * The x values of the points
    * @param {number[]} [valuesX=undefined]
    * @memberof scatter#
    * @property
    */
    valuesX,

    /**
    * The y values of the points
    * @param {number[]} [valuesY=undefined]
    * @memberof scatter#
    * @property
    */
    valuesY,

    /**
    * The r values of the points
    * @param {number[]} [valuesR=undefined]
    * @memberof scatter#
    * @property
    */
    valuesR,

    /**
    * Instance of Tooltip
    * @param {function} [tooltip=tooltip()]
    * @memberof scatter#
    * @property
    */
    tooltip$$1 = tooltip();
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {scatter | d3.selection}
     * @memberof scatter
     * @property
     * by default selection = selection
     */


    scatter.selection = function (_) {
      return arguments.length ? (selection = _, scatter) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link scatter#data})
     * @param {number} [_=none]
     * @returns {scatter | object}
     * @memberof scatter
     * @property
     */


    scatter.data = function (_) {
      return arguments.length ? (data = _, scatter) : data;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link scatter#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default spaceX = undefined
     */


    scatter.spaceX = function (_) {
      return arguments.length ? (spaceX = _, scatter) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link scatter#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default spaceY = undefined
     */


    scatter.spaceY = function (_) {
      return arguments.length ? (spaceY = _, scatter) : spaceY;
    };
    /**
     * Gets / sets the x scale for which the scatter x values should be transformed by
     * (see {@link scatter#scaleX})
     * @param {d3.scale} [_=none]
     * @returns {scatter | d3.scale}
     * @memberof scatter
     * @property
     * by default scaleX = d3.scaleLinear()
     */


    scatter.scaleX = function (_) {
      return arguments.length ? (scaleX = _, scatter) : scaleX;
    };
    /**
     * Gets / sets the padding for the domain of the x scale
     * (see {@link scatter#domainPaddingX})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default domainPaddingX = 0.5
     */


    scatter.domainPaddingX = function (_) {
      return arguments.length ? (domainPaddingX = _, scatter) : domainPaddingX;
    };
    /**
     * Gets / sets the valueExtractorX
     * (see {@link scatter#valueExtractorX})
     * @param {function} [_=none]
     * @returns {scatter | function}
     * @memberof scatter
     * @property
     * by default valueExtractorX = function(key, index) { return data[key]['x'] }
     */


    scatter.valueExtractorX = function (_) {
      return arguments.length ? (valueExtractorX = _, scatter) : valueExtractorX;
    };
    /**
     * Gets / sets the y scale for which the scatter y values should be transformed by
     * (see {@link scatter#scaleY})
     * @param {d3.scale} [_=none]
     * @returns {scatter | d3.scale}
     * @memberof scatter
     * @property
     * by default scaleY = d3.scaleLinear()
     */


    scatter.scaleY = function (_) {
      return arguments.length ? (scaleY = _, scatter) : scaleY;
    };
    /**
     * Gets / sets the padding for the domain of the y scale
     * (see {@link scatter#domainPaddingY})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default domainPaddingY = 0.5
     */


    scatter.domainPaddingY = function (_) {
      return arguments.length ? (domainPaddingY = _, scatter) : domainPaddingY;
    };
    /**
     * Gets / sets the valueExtractorY
     * (see {@link scatter#valueExtractorY})
     * @param {function} [_=none]
     * @returns {scatter | function}
     * @memberof scatter
     * @property
     * by default valueExtractorY = function(key, index) { return data[key]['y'] }
     */


    scatter.valueExtractorY = function (_) {
      return arguments.length ? (valueExtractorY = _, scatter) : valueExtractorY;
    };
    /**
     * Gets / sets the r scale for which the scatter r values should be transformed by
     * (see {@link scatter#scaleR})
     * @param {d3.scale} [_=none]
     * @returns {scatter | d3.scale}
     * @memberof scatter
     * @property
     * by default scaleR = d3.scaleLinear()
     */


    scatter.scaleR = function (_) {
      return arguments.length ? (scaleR = _, scatter) : scaleR;
    };
    /**
     * Gets / sets the padding for the domain of the r scale
     * (see {@link scatter#domainPaddingR})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default domainPaddingR = 0.5
     */


    scatter.domainPaddingR = function (_) {
      return arguments.length ? (domainPaddingR = _, scatter) : domainPaddingR;
    };
    /**
     * Gets / sets the valueExtractorR
     * (see {@link scatter#valueExtractorR})
     * @param {function} [_=none]
     * @returns {scatter | function}
     * @memberof scatter
     * @property
     * by default valueExtractorR = function(key, index) { return data[key]['r'] }
     */


    scatter.valueExtractorR = function (_) {
      return arguments.length ? (valueExtractorR = _, scatter) : valueExtractorR;
    };
    /**
     * Gets / sets the minRadius
     * (see {@link scatter#minRadius})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default minRadius = 2
     */


    scatter.minRadius = function (_) {
      return arguments.length ? (minRadius = _, scatter) : minRadius;
    };
    /**
     * Gets / sets the maxRadius
     * (see {@link scatter#maxRadius})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default maxRadius = 10
     */


    scatter.maxRadius = function (_) {
      return arguments.length ? (maxRadius = _, scatter) : maxRadius;
    };
    /**
     * Gets / sets the pointStrokeWidth
     * (see {@link scatter#pointStrokeWidth})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default pointStrokeWidth = 2
     */


    scatter.pointStrokeWidth = function (_) {
      return arguments.length ? (pointStrokeWidth = _, scatter) : pointStrokeWidth;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link scatter#colorFunction})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default colorFunction = colorFunction()
     */


    scatter.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, scatter) : colorFunction$$1;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link scatter#backgroundFill})
     * @param {string} [_=none]
     * @returns {scatter | string}
     * @memberof scatter
     * @property
     * by default backgroundFill = 'transparent'
     */


    scatter.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, scatter) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link scatter#namespace})
     * @param {string} [_=none]
     * @returns {scatter | string}
     * @memberof scatter
     * @property
     * by default namespace = 'd3sm-scatter'
     */


    scatter.namespace = function (_) {
      return arguments.length ? (namespace = _, scatter) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link scatter#objectClass})
     * @param {string} [_=none]
     * @returns {scatter | string}
     * @memberof scatter
     * @property
     * by default objectClass = 'tick-group'
     */


    scatter.objectClass = function (_) {
      return arguments.length ? (objectClass = _, scatter) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link scatter#transitionDuration})
     * @param {number} [_=none]
     * @returns {scatter | number}
     * @memberof scatter
     * @property
     * by default transitionDuration = 1000
     */


    scatter.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, scatter) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link scatter#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {scatter | d3.ease}
     * @memberof scatter
     * @property
     * by default easeFunc = d3.easeExp
     */


    scatter.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, scatter) : easeFunc;
    };
    /**
     * Gets / sets the pointKeys
     * (see {@link scatter#pointKeys})
     * @param {string[]} [_=none]
     * @returns {scatter | string[]}
     * @memberof scatter
     * @property
     * by default pointKeys = undefined
     */


    scatter.pointKeys = function (_) {
      return arguments.length ? (pointKeys = _, scatter) : pointKeys;
    };
    /**
     * Gets / sets the valuesX
     * (see {@link scatter#valuesX})
     * @param {number[]} [_=none]
     * @returns {scatter | number[]}
     * @memberof scatter
     * @property
     * by default valuesX = undefined
     */


    scatter.valuesX = function (_) {
      return arguments.length ? (valuesX = _, scatter) : valuesX;
    };
    /**
     * Gets / sets the valuesY
     * (see {@link scatter#valuesY})
     * @param {number[]} [_=none]
     * @returns {scatter | number[]}
     * @memberof scatter
     * @property
     * by default valuesY = undefined
     */


    scatter.valuesY = function (_) {
      return arguments.length ? (valuesY = _, scatter) : valuesY;
    };
    /**
     * Gets / sets the valuesR
     * (see {@link scatter#valuesR})
     * @param {number[]} [_=none]
     * @returns {scatter | number[]}
     * @memberof scatter
     * @property
     * by default valuesR = undefined
     */


    scatter.valuesR = function (_) {
      return arguments.length ? (valuesR = _, scatter) : valuesR;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link scatter#tooltip})
     * @param {tooltip} [_=none]
     * @returns {scatter | tooltip}
     * @memberof scatter
     * @property
     * by default tooltip = tooltip()
     */


    scatter.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, scatter) : tooltip$$1;
    };

    function scatter() {
      // background cliping rectangle
      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      pointKeys = d3$1.keys(data);
      valuesX = pointKeys.map(valueExtractorX);
      valuesY = pointKeys.map(valueExtractorY);
      valuesR = pointKeys.map(valueExtractorR);
      var numberOfObjects = pointKeys.length;
      var extentX = [Math.min.apply(Math, _toConsumableArray$1(valuesX)) - domainPaddingX, Math.max.apply(Math, _toConsumableArray$1(valuesX)) + domainPaddingX];
      var extentY = [Math.min.apply(Math, _toConsumableArray$1(valuesY)) - domainPaddingY, Math.max.apply(Math, _toConsumableArray$1(valuesY)) + domainPaddingY];
      var extentR = [Math.min.apply(Math, _toConsumableArray$1(valuesR)) - domainPaddingR, Math.max.apply(Math, _toConsumableArray$1(valuesR)) + domainPaddingR];
      scaleX.domain(extentX).range([0, spaceX]);
      scaleY.domain(extentY).range([spaceY, 0]);
      scaleR.domain(extentR).range([minRadius, maxRadius]);
      var points = container.selectAll('.' + objectClass);
      points = points.data(pointKeys);
      var pEnter = points.enter().append('circle').attr('class', objectClass).attr('cx', 0).attr('cy', spaceY).attr('r', 0);
      var pExit = points.exit();
      points = points.merge(pEnter);
      points.each(function (key, i) {
        var t = d3$1.select(this),
            currentData = data[key],
            x = valuesX[i],
            y = valuesY[i],
            r = valuesR[i],
            fillColor = colorFunction$$1(key, currentData, i, 'fill'),
            strokeColor = colorFunction$$1(key, currentData, i, 'stroke');
        t.transition().duration(transitionDuration).ease(easeFunc).attr('cx', scaleX(x)).attr('cy', scaleY(y)).attr('r', scaleR(r)).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', pointStrokeWidth);
        t.on('mouseover', function (d, i) {
          points.style('opacity', 0.2);
          t.style('opacity', 1);
          t.transition().duration(transitionDuration / 2).ease(easeFunc).attr('stroke-width', pointStrokeWidth * 2).attr('r', scaleR(r) * 1.5);
        });
        t.node().addEventListener('mouseout', function () {
          container.selectAll('.' + objectClass).style('opacity', 1);
          t.transition().duration(transitionDuration / 2).ease(easeFunc).attr('stroke-width', pointStrokeWidth).attr('r', scaleR(r));
        });
      });
      pExit.transition().duration(transitionDuration).ease(easeFunc).attr('cx', 0).attr('cy', spaceY).attr('r', 0).remove();
      tooltip$$1.selection(points).data(data);
      tooltip$$1();
    }

    return scatter;
  }

  function _toConsumableArray$2(arr$$1) { return _arrayWithoutHoles$2(arr$$1) || _iterableToArray$2(arr$$1) || _nonIterableSpread$2(); }

  function _nonIterableSpread$2() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$2(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$2(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                   BAR                                      **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates a bar
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/bar-chart-same-data-complex-grouping/index.html Demo}
   * @constructor bar
   * @param {d3.selection} selection
   * @namespace bar
   * @returns {function} bar
   */

  function bar(selection) {
    /*
    Assumes that data is list an object.
     The keys of data will be bound to the bars.
    The valueExtractor function will extract the value from data[key].
     Grouping can be used if desired. It should be an arbitrary complex list where
    the values are strings matching keys in data.
    */
    var
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a bar
    * (see {@link bar#data})
    * @param {Object} [data=undefined]
    * @memberof bar#
    * @property
    */
    data,

    /**
    * Which direction to render the bars in
    * (see {@link bar#orient})
    * @param {number} [orient='horizontal']
    * @memberof bar#
    * @property
    */
    orient = 'horizontal',

    /**
    * Amount of horizontal space (in pixels) avaible to render the bar in
    * (see {@link bar#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof bar#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the bar in
    * (see {@link bar.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof bar#
    * @property
    */
    spaceY,

    /**
    * Whether or not to allow bar to render elements pass the main spatial dimension
    * given the orientation (see {@link bar#orient}), where {@link bar#orient}="horizontal"
    * the main dimension is {@link bar#spaceX} and where {@link bar#orient}="vertical"
    * the main dimension is {@link bar#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof bar#
    * @property
    */
    overflowQ = false,

    /**
    * An array - putatively of other arrays - depicting how bars should be arranged
    * @param {Array[]} [grouping=undefined]
    * @memberof bar#
    * @property
    */
    grouping,

    /**
    * How to get the value of the bar
    * @param {function} [valueExtractor=function(key, index) { return data[key] }]
    * @memberof bar#
    * @property
    */
    valueExtractor = function valueExtractor(key, index) {
      return data[key];
    },

    /**
    * How to sort the bars - if {@link bar#grouping} is not provided.
    * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
    * @memberof bar#
    * @property
    */
    sortingFunction = function sortingFunction(keyA, keyB) {
      return d3$1.descending(data[keyA], data[keyB]);
    },

    /**
    * The scale for which bar values should be transformed by
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof bar#
    * @property
    */
    scale = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scale (see {@link bar#scale})
    * @param {number} [domainPadding=0.5]
    * @memberof bar#
    * @property
    */
    domainPadding = 0.5,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link bar#orient}), where {@link bar#orient}="horizontal"
    * the main dimension is {@link bar#spaceX} and where {@link bar#orient}="vertical"
    * the main dimension is {@link bar#spaceY} between bars
    * @param {number} [objectSpacer=0.05]
    * @memberof bar#
    * @property
    */
    objectSpacer = 0.05,

    /**
    * The minimum size that an object can be
    * @param {number} [minObjectSize=50]
    * @memberof bar#
    * @property
    */
    minObjectSize = 50,

    /**
    * The maximum size that an object can be
    * @param {number} [maxObjectSize=100]
    * @memberof bar#
    * @property
    */
    maxObjectSize = 100,

    /**
    * The stroke width of the bars
    * @param {number} [barStrokeWidth=2]
    * @memberof bar#
    * @property
    */
    barStrokeWidth = 2,

    /**
    * Instance of ColorFunction
    * @param {function} [colorFunction = colorFunction()]
    * @memberof bar#
    * @property
    */
    colorFunction$$1 = colorFunction(),

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof bar#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of bar
    * @param {string} [namespace="d3sm-bar"]
    * @memberof bar#
    * @property
    */
    namespace = 'd3sm-bar',

    /**
    * Class name for bar container (<g> element)
    * @param {string} [objectClass="bar"]
    * @memberof bar#
    * @property
    */
    objectClass = 'bar',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof bar#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof bar#
    * @property
    */
    easeFunc = d3$1.easeExp,
        // useful values to extract to prevent re-calculation

    /**
    * The keys of the bars
    * @param {string[]} [barKeys=undefined]
    * @memberof bar#
    * @property
    */
    barKeys,

    /**
    * The values of the bars
    * @param {number[]} [barValues=undefined]
    * @memberof bar#
    * @property
    */
    barValues,

    /**
    * The objectSize (actual width) used by the bars
    * @param {number} [objectSize=undefined]
    * @memberof bar#
    * @property
    */
    objectSize,

    /**
    * The spacerSize (actual width) used by the spacers between the bars
    * @param {number} [spacerSize=undefined]
    * @memberof bar#
    * @property
    */
    spacerSize,

    /**
    * Instance of Tooltip
    * @param {function} [tooltip=tooltip()]
    * @memberof bar#
    * @property
    */
    tooltip$$1 = tooltip(),
        barPercent = 1;
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {bar | d3.selection}
     * @memberof bar
     * @property
     * by default selection = selection
     */


    bar.selection = function (_) {
      return arguments.length ? (selection = _, bar) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link bar#data})
     * @param {number} [_=none]
     * @returns {bar | object}
     * @memberof bar
     * @property
     */


    bar.data = function (_) {
      return arguments.length ? (data = _, bar) : data;
    };
    /**
     * Gets or sets the orient of the bars
     * (see {@link bar#orient})
     * @param {number} [_=none]
     * @returns {bar | object}
     * @memberof bar
     * @property
     */


    bar.orient = function (_) {
      return arguments.length ? (orient = _, bar) : orient;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link bar#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default spaceX = undefined
     */


    bar.spaceX = function (_) {
      return arguments.length ? (spaceX = _, bar) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link bar#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default spaceY = undefined
     */


    bar.spaceY = function (_) {
      return arguments.length ? (spaceY = _, bar) : spaceY;
    };
    /**
     * Gets / sets whether or not bar is allowed to go beyond specified dimensions
     * (see {@link bar#spaceX})
     * @param {boolean} [_=none]
     * @returns {bar | boolean}
     * @memberof bar
     * @property
     * by default overflowQ = false
     */


    bar.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, bar) : overflowQ;
    };
    /**
     * Gets / sets the grouping of the bars
     * (see {@link bar#grouping})
     * @param {Array[]} [_=none]
     * @returns {bar | Array[]}
     * @memberof bar
     * @property
     * by default grouping = undefined
     */


    bar.grouping = function (_) {
      return arguments.length ? (grouping = _, bar) : grouping;
    };
    /**
     * Gets / sets the valueExtractor
     * (see {@link bar#valueExtractor})
     * @param {function} [_=none]
     * @returns {bar | function}
     * @memberof bar
     * @property
     * by default valueExtractor = function(key, index) { return data[key] },
     */


    bar.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, bar) : valueExtractor;
    };
    /**
     * Gets / sets the sortingFunction
     * (see {@link bar#sortingFunction})
     * @param {function} [_=none]
     * @returns {bar | function}
     * @memberof bar
     * @property
     * by default sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},
     */


    bar.sortingFunction = function (_) {
      return arguments.length ? (sortingFunction = _, bar) : sortingFunction;
    };
    /**
     * Gets / sets the scale for which the bar values should be transformed by
     * (see {@link bar#scale})
     * @param {d3.scale} [_=none]
     * @returns {bar | d3.scale}
     * @memberof bar
     * @property
     * by default scale = d3.scaleLinear()
     */


    bar.scale = function (_) {
      return arguments.length ? (scale = _, bar) : scale;
    };
    /**
     * Gets / sets the padding for the domain of the scale
     * (see {@link bar#domainPadding})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default domainPadding = 0.5
     */


    bar.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, bar) : domainPadding;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link bar#objectSpacer})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default objectSpacer = 0.05
     */


    bar.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, bar) : objectSpacer;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link bar#minObjectSize})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default minObjectSize = 50
     */


    bar.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, bar) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link bar#maxObjectSize})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default maxObjectSize = 100
     */


    bar.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, bar) : maxObjectSize;
    };
    /**
     * Gets / sets the barStrokeWidth
     * (see {@link bar#barStrokeWidth})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default barStrokeWidth = 2
     */


    bar.barStrokeWidth = function (_) {
      return arguments.length ? (barStrokeWidth = _, bar) : barStrokeWidth;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link bar#colorFunction})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default colorFunction = colorFunction()
     */


    bar.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, bar) : colorFunction$$1;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link bar#backgroundFill})
     * @param {string} [_=none]
     * @returns {bar | string}
     * @memberof bar
     * @property
     * by default backgroundFill = 'transparent'
     */


    bar.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, bar) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link bar#namespace})
     * @param {string} [_=none]
     * @returns {bar | string}
     * @memberof bar
     * @property
     * by default namespace = 'd3sm-bar'
     */


    bar.namespace = function (_) {
      return arguments.length ? (namespace = _, bar) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link bar#objectClass})
     * @param {string} [_=none]
     * @returns {bar | string}
     * @memberof bar
     * @property
     * by default objectClass = 'tick-group'
     */


    bar.objectClass = function (_) {
      return arguments.length ? (objectClass = _, bar) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link bar#transitionDuration})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default transitionDuration = 1000
     */


    bar.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, bar) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link bar#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {bar | d3.ease}
     * @memberof bar
     * @property
     * by default easeFunc = d3.easeExp
     */


    bar.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, bar) : easeFunc;
    };
    /**
     * Gets / sets the barKeys
     * (see {@link bar#barKeys})
     * @param {string[]} [_=none]
     * @returns {bar | string[]}
     * @memberof bar
     * @property
     * by default barKeys = undefined
     */


    bar.barKeys = function (_) {
      return arguments.length ? (barKeys = _, bar) : barKeys;
    };
    /**
     * Gets / sets the barValues
     * (see {@link bar#barValues})
     * @param {number[]} [_=none]
     * @returns {bar | number[]}
     * @memberof bar
     * @property
     * by default barValues = undefined
     */


    bar.barValues = function (_) {
      return arguments.length ? (barValues = _, bar) : barValues;
    };
    /**
     * Gets / sets the objectSize
     * (see {@link bar#objectSize})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default objectSize = undefined
     */


    bar.objectSize = function (_) {
      return arguments.length ? (objectSize = _, bar) : objectSize;
    };
    /**
     * Gets / sets the spacerSize
     * (see {@link bar#spacerSize})
     * @param {number} [_=none]
     * @returns {bar | number}
     * @memberof bar
     * @property
     * by default spacerSize = undefined
     */


    bar.spacerSize = function (_) {
      return arguments.length ? (spacerSize = _, bar) : spacerSize;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link bar#tooltip})
     * @param {tooltip} [_=none]
     * @returns {bar | tooltip}
     * @memberof bar
     * @property
     * by default tooltip = tooltip()
     */


    bar.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, bar) : tooltip$$1;
    };

    bar.barPercent = function (_) {
      return arguments.length ? (barPercent = _, bar) : barPercent;
    };

    function bar() {
      // for convenience in handling orientation specific values
      var horizontalQ = orient == 'horizontal' || orient == 'bottom' || orient == 'top' ? true : false;
      var verticalQ = !horizontalQ; // background cliping rectangle

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill); // to prevent re-calculation and getters to be passed to axes

      barKeys = d3$1.keys(data);
      barValues = barKeys.map(valueExtractor); // if grouping is undefined sort barKeys by sortingFunction

      var ordered = grouping == undefined ? barKeys.sort(sortingFunction) : grouping; // ordered might be nested depending on grouping

      barKeys = utils.arr.flatten(ordered);
      var numberOfObjects = barKeys.length;
      var extent = [Math.min.apply(Math, _toConsumableArray$2(barValues)) - domainPadding, Math.max.apply(Math, _toConsumableArray$2(barValues)) + domainPadding]; // set the scale

      scale.domain(extent).range(horizontalQ ? [0, spaceY] : orient == 'right' ? [0, spaceX] : [spaceX, 0]);
      var space = horizontalQ ? spaceX : spaceY; // calculate object size

      objectSize = objectSize == undefined ? utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ) : objectSize; // calculate spacer size if needed

      spacerSize = spacerSize == undefined ? utils.math.calculateWidthOfSpacer(barKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ) : spacerSize; // make the nested groups

      var spacerFunction = groupingSpacer().horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects).objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace(namespace); // safe default function

      var defaultExit = spacerFunction.exitFunction();
      spacerFunction.exitFunction(function (sel$$1) {
        // use default to move objects off screen
        // console.log("EXIT", sel.nodes(), objectSize, scale(extent[1]))
        if (objectSize == undefined) {
          console.log(sel$$1.nodes(), objectSize);
        }

        defaultExit(sel$$1);
        sel$$1.selectAll('g').classed("to-remove", true); // shrink rectangles in addition

        sel$$1.selectAll('* > rect').transition().duration(transitionDuration).attr('transform', function (d, i) {
          var x = horizontalQ ? 0 : 0,
              y = verticalQ ? 0 : scale(extent[1]),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }).attr('width', horizontalQ ? objectSize : 0).attr('height', verticalQ ? objectSize : 0).remove();
      }); // move stuff

      spacerFunction(container, ordered, 0);
      var parentIndexArray = [];
      container.selectAll('g:not(.to-remove).' + objectClass).each(function (d, i) {
        parentIndexArray.push(Number(d3$1.select(this).attr('parent-index')));
      });
      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, Math.max.apply(Math, parentIndexArray)]) : colorFunction$$1.dataExtent(extent);
      container.selectAll('g.' + objectClass + ':not(.to-remove)').each(function (key, i) {
        // console.log(key, scale(extent[1]) - scale(valueExtractor(key, i)))
        var t = d3$1.select(this),
            currentData = data[key],
            value = valueExtractor(key, i),
            i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
            fillColor = colorFunction$$1(key, value, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(key, value, i, 'stroke');
        var bar = utils.sel.safeSelect(t, 'rect', 'bar-rect');

        if (bar.attr('transform') == undefined) {
          bar.attr('transform', function (d, i) {
            var x = horizontalQ ? 0 : 0,
                y = verticalQ ? 0 : scale(extent[1]),
                t = 'translate(' + x + ',' + y + ')';
            return t;
          }).attr('width', horizontalQ ? objectSize : 0).attr('height', verticalQ ? objectSize : 0);
        }

        bar.transition().duration(transitionDuration).ease(easeFunc).attr('transform', function (d, i) {
          var x = horizontalQ ? objectSize - objectSize * barPercent : orient == 'right' ? scale(extent[1]) - scale(value) : objectSize - objectSize * barPercent,
              y = verticalQ ? objectSize - objectSize * barPercent : scale(extent[1]) - scale(value),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }).attr('width', horizontalQ ? objectSize * barPercent : scale(value)).attr('height', verticalQ ? objectSize * barPercent : scale(value)).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', barStrokeWidth);
        t.on('mouseover', function (d, i) {
          container.selectAll('g.' + objectClass).style('opacity', 0.2);
          t.style('opacity', 1);
          bar.attr('stroke-width', barStrokeWidth * 2);
        });
        t.on('mouseout', function () {
          container.selectAll('g.' + objectClass).style('opacity', 1);
          bar.attr('stroke-width', barStrokeWidth);
        });
      });
      tooltip$$1.selection(container.selectAll('.bar-rect')).data(data);
      tooltip$$1();
    }

    return bar;
  }

  function _toConsumableArray$3(arr$$1) { return _arrayWithoutHoles$3(arr$$1) || _iterableToArray$3(arr$$1) || _nonIterableSpread$3(); }

  function _nonIterableSpread$3() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$3(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$3(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /**
   * Creates a bubbleHeatmap
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/bubble-heatmap/index.html Demo}
   * @constructor bubbleHeatmap
   * @param {d3.selection} selection
   * @namespace bubbleHeatmap
   * @returns {function} bubbleHeatmap
   */

  function bubbleHeatmap(selection) {
    var 
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a cell
    * (see {@link bubbleHeatmap#data})
    * @param {Object} [data=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    data,

    /**
    * Amount of horizontal space (in pixels) avaible to render the bubbleHeatmap in
    * (see {@link bubbleHeatmap#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the bubbleHeatmap in
    * (see {@link bubbleHeatmap.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    spaceY,

    /**
    * The internal key of the cell specifiying to which x axis key it belongs
    * (see {@link bubbleHeatmap.xKey})
    * @param {string} [xKey='x']
    * @memberof bubbleHeatmap#
    * @property
    */
    xKey = 'x',

    /**
    * The internal key of the cell specifiying to which y axis key it belongs
    * (see {@link bubbleHeatmap.yKey})
    * @param {string} [yKey='y']
    * @memberof bubbleHeatmap#
    * @property
    */
    yKey = 'y',

    /**
    * The internal key of the cell specifiying what value to use to determine the radius
    * (see {@link bubbleHeatmap.rKey})
    * @param {string} [rKey='r']
    * @memberof bubbleHeatmap#
    * @property
    */
    rKey = 'r',

    /**
    * The internal key of the cell specifiying what value to use to determine the color
    * (see {@link bubbleHeatmap.vKey})
    * @param {string} [vKey='v']
    * @memberof bubbleHeatmap#
    * @property
    */
    vKey = 'v',

    /**
    * Function for extracting the the value from xKey.
    * (see {@link bubbleHeatmap.xExtractor})
    * @param {function} [xExtractor=function(key, i) { return data[key][xKey] }]
    * @returns {string}
    * @memberof bubbleHeatmap#
    * @property
    */
    xExtractor = function xExtractor(key, i) {
      return data[key][xKey];
    },

    /**
    * Function for extracting the the value from yKey.
    * (see {@link bubbleHeatmap.yExtractor})
    * @param {function} [yExtractor=function(key, i) { return data[key][yKey] }]
    * @returns {string}
    * @memberof bubbleHeatmap#
    * @property
    */
    yExtractor = function yExtractor(key, i) {
      return data[key][yKey];
    },

    /**
    * Function for extracting the the value from rKey.
    * (see {@link bubbleHeatmap.rExtractor})
    * @param {function} [rExtractor=function(key, i) { return data[key][rKey] }]
    * @returns {number}
    * @memberof bubbleHeatmap#
    * @property
    */
    rExtractor = function rExtractor(key, i) {
      return data[key][rKey];
    },

    /**
    * Function for extracting the the value from vKey.
    * (see {@link bubbleHeatmap.vExtractor})
    * @param {function} [vExtractor=function(key, i) { return data[key][vKey] }]
    * @returns {number}
    * @memberof bubbleHeatmap#
    * @property
    */
    vExtractor = function vExtractor(key, i) {
      return data[key][vKey];
    },

    /**
    * Whether or not to allow bubbleHeatmap to render elements pass the main spatial dimension
    * given the orientation (see {@link bubbleHeatmap#orient}), where {@link bubbleHeatmap#orient}="bottom" or {@link bubbleHeatmap#orient}="top"
    * the main dimension is {@link bubbleHeatmap#spaceX} and where {@link bubbleHeatmap#orient}="left" or {@link bubbleHeatmap#orient}="right"
    * the main dimension is {@link bubbleHeatmap#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof bubbleHeatmap#
    * @property
    */
    overflowQ = false,

    /**
    * The scale for which the radius values should be transformed by
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof bubbleHeatmap#
    * @property
    */
    scale = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scale (see {@link bubbleHeatmap#scale})
    * @param {number} [domainPadding=0.5]
    * @memberof bubbleHeatmap#
    * @property
    */
    domainPadding = 0.5,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link bubbleHeatmap#orient}), where {@link bubbleHeatmap#orient}="horizontal"
    * the main dimension is {@link bubbleHeatmap#spaceX} and where {@link bubbleHeatmap#orient}="vertical"
    * the main dimension is {@link bubbleHeatmap#spaceY} between bubbles
    * @param {number} [objectSpacer=0.0]
    * @memberof bubbleHeatmap#
    * @property
    */
    objectSpacer = 0.0,

    /**
    * The minimum size that an object can be
    * @param {number} [minObjectSize=50]
    * @memberof bubbleHeatmap#
    * @property
    */
    minObjectSize = 50,

    /**
    * The maximum size that an object can be
    * @param {number} [maxObjectSize=100]
    * @memberof bubbleHeatmap#
    * @property
    */
    maxObjectSize = 100,

    /**
    * The stroke width of the bubbles
    * @param {number} [bubbleStrokeWidth=2]
    * @memberof bubbleHeatmap#
    * @property
    */
    bubbleStrokeWidth = 2,
        // colorFunc = colorFunction(),

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof bubbleHeatmap#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of bubbleHeatmap
    * @param {string} [namespace="d3sm-bubble"]
    * @memberof bubbleHeatmap#
    * @property
    */
    namespace = 'd3sm-bubble',

    /**
    * Class name for bubble container (<g> element)
    * @param {string} [objectClass="bubble"]
    * @memberof bubbleHeatmap#
    * @property
    */
    objectClass = 'bubble',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof bubbleHeatmap#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof bubbleHeatmap#
    * @property
    */
    easeFunc = d3$1.easeExp,

    /**
    * Stores the keys of all the cells
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [cellKeys=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    cellKeys,

    /**
    * Stores the list of utils.arr.unique xValues
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [xValues=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    xValues,

    /**
    * Stores the list of utils.arr.unique yValues
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [yValues=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    yValues,

    /**
    * Stores the list of utils.arr.unique rValues
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [rValues=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    rValues,

    /**
    * Stores the list of utils.arr.unique vValues
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [vValues=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    vValues,
        xKeySortingFunction = function xKeySortingFunction(a, b) {
      return xExtractor(a) - xExtractor(b);
    },
        yKeySortingFunction = function yKeySortingFunction(a, b) {
      return yExtractor(a) - yExtractor(b);
    },
        /**
    * Instance of ColorFunction with .colorBy set to 'category'
    * @function colorFunction
    * @memberof bubbleHeatmap#
    * @property
    */
    colorFunction$$1 = colorFunction().colorBy('value'),

    /**
    * Instance of Tooltip
    * @function tooltip
    * @memberof bubbleHeatmap#
    * @property
    */
    tooltip$$1 = tooltip(),

    /**
    * store the size the bubble could be in the x dimension
    * the actuall size of the bubble will be the min of xSize and {@link bubbleHeatmap#ySize}
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [xSize=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    xSize,

    /**
    * store the size of the spacer in the x dimension
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [xSpacerSize=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    xSpacerSize,

    /**
    * store the size the bubble could be in the y dimension
    * the actuall size of the bubble will be the min of xSize and {@link bubbleHeatmap#xSize}
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [ySize=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    ySize,

    /**
    * store the size of the spacer in the y dimension.
    * Calculated after bubbleHeatmap called.
    * @param {string[]} [xSpacerSize=undefined]
    * @memberof bubbleHeatmap#
    * @property
    */
    ySpacerSize;
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {bubbleHeatmap | d3.selection}
     * @memberof bubbleHeatmap
     * @property
     * by default selection = selection
     */


    bhm.selection = function (_) {
      return arguments.length ? (selection = _, bhm) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link bubbleHeatmap#data})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | object}
     * @memberof bubbleHeatmap
     * @property
     */


    bhm.data = function (_) {
      return arguments.length ? (data = _, bhm) : data;
    }; // bhm.orient = function(_) { return arguments.length ? (orient = _, bhm) : orient; }

    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link bubbleHeatmap#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default spaceX = undefined
     */


    bhm.spaceX = function (_) {
      return arguments.length ? (spaceX = _, bhm) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link bubbleHeatmap#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default spaceY = undefined
     */


    bhm.spaceY = function (_) {
      return arguments.length ? (spaceY = _, bhm) : spaceY;
    };
    /**
     * Gets or sets the xKey
     * (see {@link bubbleHeatmap#xKey})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default xKey = 'x'
     */


    bhm.xKey = function (_) {
      return arguments.length ? (xKey = _, bhm) : xKey;
    };
    /**
     * Gets or sets the yKey
     * (see {@link bubbleHeatmap#yKey})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default yKey = 'y'
     */


    bhm.yKey = function (_) {
      return arguments.length ? (yKey = _, bhm) : yKey;
    };
    /**
     * Gets or sets the rKey
     * (see {@link bubbleHeatmap#rKey})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default rKey = 'r'
     */


    bhm.rKey = function (_) {
      return arguments.length ? (rKey = _, bhm) : rKey;
    };
    /**
     * Gets or sets the vKey
     * (see {@link bubbleHeatmap#vKey})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default vKey = 'y'
     */


    bhm.vKey = function (_) {
      return arguments.length ? (vKey = _, bhm) : vKey;
    };
    /**
     * Gets or sets the cellKeys
     * (see {@link bubbleHeatmap#cellKeys})
     * @param {string[]} [_=none]
     * @returns {bubbleHeatmap | string[]}
     * @memberof bubbleHeatmap
     * @property
     * by default cellKeys = undefined
     */


    bhm.cellKeys = function (_) {
      return arguments.length ? (cellKeys = _, bhm) : cellKeys;
    };
    /**
     * Gets or sets the xValues
     * (see {@link bubbleHeatmap#xValues})
     * @param {string[]} [_=none]
     * @returns {bubbleHeatmap | string[]}
     * @memberof bubbleHeatmap
     * @property
     * by default xValues = undefined
     */


    bhm.xValues = function (_) {
      return arguments.length ? (xValues = _, bhm) : xValues;
    };
    /**
     * Gets or sets the yValues
     * (see {@link bubbleHeatmap#yValues})
     * @param {string[]} [_=none]
     * @returns {bubbleHeatmap | string[]}
     * @memberof bubbleHeatmap
     * @property
     * by default yValues = undefined
     */


    bhm.yValues = function (_) {
      return arguments.length ? (yValues = _, bhm) : yValues;
    };
    /**
     * Gets or sets the rValues
     * (see {@link bubbleHeatmap#rValues})
     * @param {number[]} [_=none]
     * @returns {bubbleHeatmap | number[]}
     * @memberof bubbleHeatmap
     * @property
     * by default rValues = undefined
     */


    bhm.rValues = function (_) {
      return arguments.length ? (rValues = _, bhm) : rValues;
    };
    /**
     * Gets or sets the vValues
     * (see {@link bubbleHeatmap#vValues})
     * @param {number[]} [_=none]
     * @returns {bubbleHeatmap | number[]}
     * @memberof bubbleHeatmap
     * @property
     * by default vValues = undefined
     */


    bhm.vValues = function (_) {
      return arguments.length ? (vValues = _, bhm) : vValues;
    };
    /**
     * Gets or sets the xExtractor
     * (see {@link bubbleHeatmap#xExtractor})
     * @param {function} [_=none]
     * @returns {bubbleHeatmap | function}
     * @memberof bubbleHeatmap
     * @property
     * by default xExtractor = undefined
     */


    bhm.xExtractor = function (_) {
      return arguments.length ? (xExtractor = _, bhm) : xExtractor;
    };
    /**
     * Gets or sets the yExtractor
     * (see {@link bubbleHeatmap#yExtractor})
     * @param {function} [_=none]
     * @returns {bubbleHeatmap | function}
     * @memberof bubbleHeatmap
     * @property
     * by default yExtractor = undefined
     */


    bhm.yExtractor = function (_) {
      return arguments.length ? (yExtractor = _, bhm) : yExtractor;
    };
    /**
     * Gets or sets the rExtractor
     * (see {@link bubbleHeatmap#rExtractor})
     * @param {function} [_=none]
     * @returns {bubbleHeatmap | function}
     * @memberof bubbleHeatmap
     * @property
     * by default rExtractor = undefined
     */


    bhm.rExtractor = function (_) {
      return arguments.length ? (rExtractor = _, bhm) : rExtractor;
    };
    /**
     * Gets or sets the vExtractor
     * (see {@link bubbleHeatmap#vExtractor})
     * @param {function} [_=none]
     * @returns {bubbleHeatmap | function}
     * @memberof bubbleHeatmap
     * @property
     * by default vExtractor = undefined
     */


    bhm.vExtractor = function (_) {
      return arguments.length ? (vExtractor = _, bhm) : vExtractor;
    };
    /**
     * Gets / sets whether or not bubbleHeatmap is allowed to go beyond specified dimensions
     * (see {@link bubbleHeatmap#spaceX})
     * @param {boolean} [_=none]
     * @returns {bubbleHeatmap | boolean}
     * @memberof bubbleHeatmap
     * @property
     * by default overflowQ = false
     */


    bhm.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, bhm) : overflowQ;
    };
    /**
     * Gets / sets the scale for which the radius of bubbles should be transformed by
     * (see {@link bubbleHeatmap#scale})
     * @param {d3.scale} [_=none]
     * @returns {bubbleHeatmap | d3.scale}
     * @memberof bubbleHeatmap
     * @property
     * by default scale = d3.scaleLinear()
     */


    bhm.scale = function (_) {
      return arguments.length ? (scale = _, bhm) : scale;
    };
    /**
     * Gets / sets the padding for the domain of the scale
     * (see {@link bubbleHeatmap#domainPadding})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default domainPadding = 0.5
     */


    bhm.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, bhm) : domainPadding;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link bubbleHeatmap#objectSpacer})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default objectSpacer = 0.0
     */


    bhm.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, objectSpacer) : data;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link bubbleHeatmap#minObjectSize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default minObjectSize = 50
     */


    bhm.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, bhm) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link bubbleHeatmap#maxObjectSize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default maxObjectSize = 100
     */


    bhm.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, bhm) : maxObjectSize;
    };
    /**
     * Gets / sets the bubbleStrokeWidth
     * (see {@link bubbleHeatmap#bubbleStrokeWidth})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default bubbleStrokeWidth = 2
     */


    bhm.bubbleStrokeWidth = function (_) {
      return arguments.length ? (bubbleStrokeWidth = _, bhm) : bubbleStrokeWidth;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link bubbleHeatmap#backgroundFill})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default backgroundFill = 'transparent'
     */


    bhm.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, bhm) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link bubbleHeatmap#namespace})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default namespace = 'd3sm-bubbleHeatmap'
     */


    bhm.namespace = function (_) {
      return arguments.length ? (namespace = _, bhm) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link bubbleHeatmap#objectClass})
     * @param {string} [_=none]
     * @returns {bubbleHeatmap | string}
     * @memberof bubbleHeatmap
     * @property
     * by default objectClass = 'tick-group'
     */


    bhm.objectClass = function (_) {
      return arguments.length ? (objectClass = _, bhm) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link bubbleHeatmap#transitionDuration})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default transitionDuration = 1000
     */


    bhm.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, bhm) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link bubbleHeatmap#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {bubbleHeatmap | d3.ease}
     * @memberof bubbleHeatmap
     * @property
     * by default easeFunc = d3.easeExp
     */


    bhm.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, bhm) : easeFunc;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link bubbleHeatmap#tooltip})
     * @param {tooltip} [_=none]
     * @returns {bubbleHeatmap | tooltip}
     * @memberof bubbleHeatmap
     * @property
     * by default tooltip = tooltip()
     */


    bhm.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, bhm) : tooltip$$1;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link bubbleHeatmap#colorFunction})
     * @param {colorFunction} [_=none]
     * @returns {bubbleHeatmap | colorFunction}
     * @memberof bubbleHeatmap
     * @property
     * by default colorFunction = colorFunction()
     */


    bhm.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, bhm) : colorFunction$$1;
    };
    /**
     * Gets / sets the xSize
     * (see {@link bubbleHeatmap#xSize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default xSize = undefined
     */


    bhm.xSize = function (_) {
      return arguments.length ? (xSize = _, bhm) : xSize;
    };
    /**
     * Gets / sets the xSpacerSize
     * (see {@link bubbleHeatmap#xSpacerSize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default xSpacerSize = undefined
     */


    bhm.xSpacerSize = function (_) {
      return arguments.length ? (xSpacerSize = _, bhm) : xSpacerSize;
    };
    /**
     * Gets / sets the ySize
     * (see {@link bubbleHeatmap#ySize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default ySize = undefined
     */


    bhm.ySize = function (_) {
      return arguments.length ? (ySize = _, bhm) : ySize;
    };
    /**
     * Gets / sets the ySpacerSize
     * (see {@link bubbleHeatmap#ySpacerSize})
     * @param {number} [_=none]
     * @returns {bubbleHeatmap | number}
     * @memberof bubbleHeatmap
     * @property
     * by default ySpacerSize = undefined
     */


    bhm.ySpacerSize = function (_) {
      return arguments.length ? (ySpacerSize = _, bhm) : ySpacerSize;
    }; // bhm.yKeySortingFunction = function(_) { return arguments.length ? (yKeySortingFunction = _, bhm) : yKeySortingFunction; }
    // bhm.xKeySortingFunction = function(_) { return arguments.length ? (xKeySortingFunction = _, bhm) : xKeySortingFunction; }


    function bhm() {

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      cellKeys = d3$1.keys(data);
      cellKeys.sort(function (a, b) {
        return xKeySortingFunction(a, b) || yKeySortingFunction(a, b);
      });
      utils.con.log('bubbleHeatmap', 'cells are sorted by', cellKeys);
      xValues = utils.arr.unique(cellKeys.map(xExtractor));
      yValues = utils.arr.unique(cellKeys.map(yExtractor));
      rValues = utils.arr.unique(cellKeys.map(rExtractor));
      vValues = utils.arr.unique(cellKeys.map(vExtractor));
      utils.con.log('bubbleHeatmap', 'x and y keys are', {
        x: xValues,
        y: yValues
      });
      var xDim = xValues.length,
          yDim = yValues.length;
      var extent = [Math.min.apply(Math, _toConsumableArray$3(rValues)) - domainPadding, Math.max.apply(Math, _toConsumableArray$3(rValues)) + domainPadding];
      ySize = utils.math.calculateWidthOfObject(spaceY, yDim, minObjectSize, maxObjectSize, objectSpacer, overflowQ);
      xSize = utils.math.calculateWidthOfObject(spaceX, xDim, minObjectSize, maxObjectSize, objectSpacer, overflowQ);
      ySpacerSize = utils.math.calculateWidthOfSpacer(yValues, spaceY, ySize, yDim, objectSpacer, overflowQ);
      xSpacerSize = utils.math.calculateWidthOfSpacer(xValues, spaceX, xSize, xDim, objectSpacer, overflowQ);
      utils.con.log('bubbleHeatmap', 'size of', {
        x: xSize,
        y: ySize
      });
      scale.domain(extent).range([Math.min(minObjectSize / 2, Math.min(ySize, xSize) / 2), Math.min(ySize, xSize) / 2]);
      var ySpacer = groupingSpacer().horizontalQ(false).moveby('category').numberOfObjects(yDim).objectClass(utils.str.hypenate(objectClass, 'row')).objectSize(ySize).spacerSize(ySpacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace('row');
      var xSpacer = groupingSpacer().horizontalQ(true).moveby('category').numberOfObjects(xDim).objectClass(objectClass).objectSize(xSize).spacerSize(xSpacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc);
      ySpacer(container, yValues, 0);
      container.selectAll('g.' + utils.str.hypenate(objectClass, 'row')).each(function (d, i) {
        xSpacer(d3$1.select(this), xValues, 0);
      });
      var cells = container.selectAll('g:not(.to-remove).' + objectClass).data(cellKeys);
      var parentIndexArray = [];
      cells.each(function (d, i) {
        parentIndexArray.push(Number(d3$1.select(this).attr('parent-index')));
      });
      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, Math.max.apply(Math, parentIndexArray)]) : colorFunction$$1.dataExtent([0, Math.max.apply(Math, _toConsumableArray$3(vValues))]);
      cells.each(function (key, i) {
        utils.con.log('bubbleHeatmap', 'each cell', {
          key: key,
          index: i,
          node: d3$1.select(this).node()
        });
        var t = d3$1.select(this),
            currentData = data[key],
            value = vExtractor(key, i),
            radius = rExtractor(key, i),
            i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
            fillColor = colorFunction$$1(key, value, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(key, value, i, 'stroke');
        utils.con.log('bubbleHeatmap', 'radius', {
          radius: radius,
          scaled: scale(radius),
          extent: extent,
          range: scale.range()
        });
        var c = utils.sel.safeSelect(t, 'circle', utils.str.hypenate(objectClass, 'circle'));
        c.attr('cx', xSize / 2).attr('cy', ySize / 2).attr('r', scale(radius)).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', bubbleStrokeWidth);
      });
      tooltip$$1.selection(cells.selectAll('circle.' + utils.str.hypenate(objectClass, 'circle'))).data(data); // .keys(['r', 'v'])
      // .header(function(d, i){return utils.str.hypenate(data[d][xKey], data[d][yKey]) })

      tooltip$$1();
    }

    return bhm;
  }

  function _toConsumableArray$4(arr$$1) { return _arrayWithoutHoles$4(arr$$1) || _iterableToArray$4(arr$$1) || _nonIterableSpread$4(); }

  function _nonIterableSpread$4() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$4(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$4(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                             BOX AND WHISKER                                **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates a boxwhisker
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/box-whiskers/index.html Demo}
   * @constructor boxwhisker
   * @param {d3.selection} selection
   * @namespace boxwhisker
   * @returns {function} boxwhisker
   */

  function boxwhisker(selection) {
    var
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a box
    * (see {@link boxwhisker#data})
    * @param {Object} [data=undefined]
    * @memberof boxwhisker#
    * @property
    */
    data,

    /**
    * Which direction to render the boxes in
    * (see {@link boxwhisker#orient})
    * @param {number} [orient='horizontal']
    * @memberof boxwhisker#
    * @property
    */
    orient = 'horizontal',

    /**
    * Amount of horizontal space (in pixels) avaible to render the boxwhisker in
    * (see {@link boxwhisker#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof boxwhisker#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the boxwhisker in
    * (see {@link boxwhisker.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof boxwhisker#
    * @property
    */
    spaceY,

    /**
    * Whether or not to allow boxwhisker to render elements pass the main spatial dimension
    * given the orientation (see {@link boxwhisker#orient}), where {@link boxwhisker#orient}="horizontal"
    * the main dimension is {@link boxwhisker#spaceX} and where {@link boxwhisker#orient}="vertical"
    * the main dimension is {@link boxwhisker#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof boxwhisker#
    * @property
    */
    overflowQ = true,

    /**
    * An array - putatively of other arrays - depicting how boxes should be arranged
    * @param {Array[]} [grouping=undefined]
    * @memberof boxwhisker#
    * @property
    */
    grouping,
        quartilesKey = 'quartiles',
        // key in object where quartiles are stored
    quartilesKeys = ["0.00", "0.25", "0.50", "0.75", "1.00"],
        // keys in quartiles object mapping values to min, q1, q2, q3 and max

    /**
    * How to get the value of the boxwhisker
    * @param {function} [valueExtractor=function(key, index) { return data[key][quartilesKey] }]
    * @memberof boxwhisker#
    * @property
    */
    valueExtractor = function valueExtractor(key, index) {
      return data[key][quartilesKey];
    },

    /**
    * How to sort the boxes - if {@link boxwhisker#grouping} is not provided.
    * @param {function} [sortingFunction=descending]
    * @memberof boxwhisker#
    * @property
    * default
    * function(keyA, keyB) {return d3.descending(
    *   valueExtractor(keyA)[quartilesKeys[4]],
    *   valueExtractor(keyB)[quartilesKeys[4]]
    * )}
    */
    sortingFunction = function sortingFunction(keyA, keyB) {
      return d3$1.descending(valueExtractor(keyA)[quartilesKeys[4]], valueExtractor(keyB)[quartilesKeys[4]]);
    },

    /**
    * The scale for which boxwhisker values should be transformed by
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof boxwhisker#
    * @property
    */
    scale = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scale (see {@link boxwhisker#scale})
    * @param {number} [domainPadding=0.5]
    * @memberof boxwhisker#
    * @property
    */
    domainPadding = 0.5,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link boxwhisker#orient}), where {@link boxwhisker#orient}="horizontal"
    * the main dimension is {@link boxwhisker#spaceX} and where {@link boxwhisker#orient}="vertical"
    * the main dimension is {@link boxwhisker#spaceY} between boxes
    * @param {number} [objectSpacer=0.05]
    * @memberof boxwhisker#
    * @property
    */
    objectSpacer = 0.05,

    /**
    * The minimum size that an object can be
    * @param {number} [minObjectSize=15]
    * @memberof boxwhisker#
    * @property
    */
    minObjectSize = 15,

    /**
    * The maximum size that an object can be
    * @param {number} [maxObjectSize=50]
    * @memberof boxwhisker#
    * @property
    */
    maxObjectSize = 50,

    /**
    * Percent of box and whisker size that whiskers will be rendered
    * see {@link boxwhisker#objectSize}
    * @param {number} [whiskerWidthPercent=0.6]
    * @memberof boxwhisker#
    * @property
    */
    whiskerWidthPercent = .6,

    /**
    * Instance of ColorFunction
    * @param {function} [colorFunction = colorFunction()]
    * @memberof boxwhisker#
    * @property
    */
    colorFunction$$1 = colorFunction(),

    /**
    * The stroke width of the boxes
    * @param {number} [boxStrokeWidth=2]
    * @memberof boxwhisker#
    * @property
    */
    boxStrokeWidth = 2,

    /**
    * The stroke width of the whiskers
    * @param {number} [whiskerStrokeWidth=2]
    * @memberof boxwhisker#
    * @property
    */
    whiskerStrokeWidth = 2,

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof boxwhisker#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of boxwhisker
    * @param {string} [namespace="d3sm-box-whisker"]
    * @memberof boxwhisker#
    * @property
    */
    namespace = 'd3sm-box-whisker',

    /**
    * Class name for boxwhisker container (<g> element)
    * @param {string} [objectClass="box-whisk"]
    * @memberof boxwhisker#
    * @property
    */
    objectClass = 'box-whisk',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof boxwhisker#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof boxwhisker#
    * @property
    */
    easeFunc = d3$1.easeExp,

    /**
    * The keys of the boxes
    * @param {string[]} [boxKeys=undefined]
    * @memberof boxwhisker#
    * @property
    */
    boxKeys,

    /**
    * The values of the boxes
    * @param {string[]} [boxValues=undefined]
    * @memberof boxwhisker#
    * @property
    */
    boxValues,

    /**
    * The objectSize (actual width) used by the boxes
    * @param {number} [objectSize=undefined]
    * @memberof boxwhisker#
    * @property
    */
    objectSize,

    /**
    * The spacerSize (actual width) used by the spacers between the boxes
    * @param {number} [spacerSize=undefined]
    * @memberof boxwhisker#
    * @property
    */
    spacerSize,

    /**
    * Instance of Tooltip
    * @param {function} [tooltip=tooltip()]
    * @memberof boxwhisker#
    * @property
    */
    tooltip$$1 = tooltip();
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {boxwhisker | d3.selection}
     * @memberof boxwhisker
     * @property
     * by default selection = selection
     */


    boxwhisker.selection = function (_) {
      return arguments.length ? (selection = _, boxwhisker) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link boxwhisker#data})
     * @param {number} [_=none]
     * @returns {boxwhisker | object}
     * @memberof boxwhisker
     * @property
     */


    boxwhisker.data = function (_) {
      return arguments.length ? (data = _, boxwhisker) : data;
    };
    /**
     * Gets or sets the orient of the boxes
     * (see {@link boxwhisker#orient})
     * @param {number} [_=none]
     * @returns {boxwhisker | object}
     * @memberof boxwhisker
     * @property
     */


    boxwhisker.orient = function (_) {
      return arguments.length ? (orient = _, boxwhisker) : orient;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link boxwhisker#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default spaceX = undefined
     */


    boxwhisker.spaceX = function (_) {
      return arguments.length ? (spaceX = _, boxwhisker) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link boxwhisker#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default spaceY = undefined
     */


    boxwhisker.spaceY = function (_) {
      return arguments.length ? (spaceY = _, boxwhisker) : spaceY;
    };
    /**
     * Gets / sets whether or not boxwhisker is allowed to go beyond specified dimensions
     * (see {@link boxwhisker#overflowQ})
     * @param {boolean} [_=none]
     * @returns {boxwhisker | boolean}
     * @memberof boxwhisker
     * @property
     * by default overflowQ = false
     */


    boxwhisker.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, boxwhisker) : overflowQ;
    };
    /**
     * Gets / sets the grouping of the boxes
     * (see {@link boxwhisker#grouping})
     * @param {Array[]} [_=none]
     * @returns {boxwhisker | Array[]}
     * @memberof boxwhisker
     * @property
     * by default grouping = undefined
     */


    boxwhisker.grouping = function (_) {
      return arguments.length ? (grouping = _, boxwhisker) : grouping;
    };
    /**
     * Gets / sets the quartilesKey
     * (see {@link boxwhisker#quartilesKey})
     * @param {string} [_=none]
     * @returns {boxwhisker | string}
     * @memberof boxwhisker
     * @property
     * by default quartilesKey = quartiles
     */


    boxwhisker.quartilesKey = function (_) {
      return arguments.length ? (quartilesKey = _, boxwhisker) : quartilesKey;
    };
    /**
     * Gets / sets the quartilesKeys
     * (see {@link boxwhisker#quartilesKeys})
     * @param {string[]} [_=none]
     * @returns {boxwhisker | string[]}
     * @memberof boxwhisker
     * @property
     * by default quartilesKeys = [Q0, Q1, Q2, Q3, Q4]
     */


    boxwhisker.quartilesKeys = function (_) {
      return arguments.length ? (quartilesKeys = _, boxwhisker) : quartilesKeys;
    };
    /**
     * Gets / sets the valueExtractor
     * (see {@link boxwhisker#valueExtractor})
     * @param {function} [_=none]
     * @returns {boxwhisker | function}
     * @memberof boxwhisker
     * @property
     * by default valueExtractor = function(key, index) { return data[key][quartilesKey] },
     */


    boxwhisker.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, boxwhisker) : valueExtractor;
    };
    /**
     * Gets / sets the sortingFunction
     * (see {@link boxwhisker#sortingFunction})
     * @param {function} [_=none]
     * @returns {boxwhisker | function}
     * @memberof boxwhisker
     * @property
     * by default sortingFunction = function(keyA, keyB) {return d3.descending(
     *   valueExtractor(keyA)[quartilesKeys[4]],
     *   valueExtractor(keyB)[quartilesKeys[4]]
     * )},
     */


    boxwhisker.sortingFunction = function (_) {
      return arguments.length ? (sortingFunction = _, boxwhisker) : sortingFunction;
    };
    /**
     * Gets / sets the scale for which the boxwhisker values should be transformed by
     * (see {@link boxwhisker#scale})
     * @param {d3.scale} [_=none]
     * @returns {boxwhisker | d3.scale}
     * @memberof boxwhisker
     * @property
     * by default scale = d3.scaleLinear()
     */


    boxwhisker.scale = function (_) {
      return arguments.length ? (scale = _, boxwhisker) : scale;
    };
    /**
     * Gets / sets the padding for the domain of the scale
     * (see {@link boxwhisker#domainPadding})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default domainPadding = 0.5
     */


    boxwhisker.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, boxwhisker) : domainPadding;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link boxwhisker#objectSpacer})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default objectSpacer = 0.05
     */


    boxwhisker.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, boxwhisker) : objectSpacer;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link boxwhisker#minObjectSize})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default minObjectSize = 15
     */


    boxwhisker.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, boxwhisker) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link boxwhisker#maxObjectSize})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default maxObjectSize = 50
     */


    boxwhisker.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, boxwhisker) : maxObjectSize;
    };
    /**
     * Gets / sets the whiskerWidthPercent
     * (see {@link boxwhisker#whiskerWidthPercent})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default maxObjectSize = 0.6
     */


    boxwhisker.whiskerWidthPercent = function (_) {
      return arguments.length ? (whiskerWidthPercent = _, boxwhisker) : whiskerWidthPercent;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link boxwhisker#colorFunction})
     * @param {colorFunction} [_=none]
     * @returns {boxwhisker | colorFunction}
     * @memberof boxwhisker
     * @property
     * by default colorFunction = colorFunction()
     */


    boxwhisker.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, boxwhisker) : colorFunction$$1;
    };
    /**
     * Gets / sets the boxStrokeWidth
     * (see {@link boxwhisker#boxStrokeWidth})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default boxStrokeWidth = 2
     */


    boxwhisker.boxStrokeWidth = function (_) {
      return arguments.length ? (boxStrokeWidth = _, boxwhisker) : boxStrokeWidth;
    };
    /**
     * Gets / sets the whiskerStrokeWidth
     * (see {@link boxwhisker#whiskerStrokeWidth})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default whiskerStrokeWidth = 2
     */


    boxwhisker.whiskerStrokeWidth = function (_) {
      return arguments.length ? (whiskerStrokeWidth = _, boxwhisker) : whiskerStrokeWidth;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link boxwhisker#backgroundFill})
     * @param {string} [_=none]
     * @returns {boxwhisker | string}
     * @memberof boxwhisker
     * @property
     * by default backgroundFill = 'transparent'
     */


    boxwhisker.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, boxwhisker) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link boxwhisker#namespace})
     * @param {string} [_=none]
     * @returns {boxwhisker | string}
     * @memberof boxwhisker
     * @property
     * by default namespace = 'd3sm-boxwhisker'
     */


    boxwhisker.namespace = function (_) {
      return arguments.length ? (namespace = _, boxwhisker) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link boxwhisker#objectClass})
     * @param {string} [_=none]
     * @returns {boxwhisker | string}
     * @memberof boxwhisker
     * @property
     * by default objectClass = 'tick-group'
     */


    boxwhisker.objectClass = function (_) {
      return arguments.length ? (objectClass = _, boxwhisker) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link boxwhisker#transitionDuration})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default transitionDuration = 1000
     */


    boxwhisker.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, boxwhisker) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link boxwhisker#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {boxwhisker | d3.ease}
     * @memberof boxwhisker
     * @property
     * by default easeFunc = d3.easeExp
     */


    boxwhisker.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, boxwhisker) : easeFunc;
    };
    /**
     * Gets / sets the barKeys
     * (see {@link boxwhisker#boxKeys})
     * @param {string[]} [_=none]
     * @returns {boxwhisker | string[]}
     * @memberof boxwhisker
     * @property
     * by default boxKeys = undefined
     */


    boxwhisker.boxKeys = function (_) {
      return arguments.length ? (boxKeys = _, boxwhisker) : boxKeys;
    };
    /**
     * Gets / sets the boxValues
     * (see {@link boxwhisker#boxValues})
     * @param {number[]} [_=none]
     * @returns {boxwhisker | number[]}
     * @memberof boxwhisker
     * @property
     * by default boxValues = undefined
     */


    boxwhisker.boxValues = function (_) {
      return arguments.length ? (boxValues = _, boxwhisker) : boxValues;
    };
    /**
     * Gets / sets the objectSize
     * (see {@link boxwhisker#objectSize})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default objectSize = undefined
     */


    boxwhisker.objectSize = function (_) {
      return arguments.length ? (objectSize = _, boxwhisker) : objectSize;
    };
    /**
     * Gets / sets the spacerSize
     * (see {@link boxwhisker#spacerSize})
     * @param {number} [_=none]
     * @returns {boxwhisker | number}
     * @memberof boxwhisker
     * @property
     * by default spacerSize = undefined
     */


    boxwhisker.spacerSize = function (_) {
      return arguments.length ? (spacerSize = _, boxwhisker) : spacerSize;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link boxwhisker#tooltip})
     * @param {tooltip} [_=none]
     * @returns {boxwhisker | tooltip}
     * @memberof boxwhisker
     * @property
     * by default tooltip = tooltip()
     */


    boxwhisker.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, boxwhisker) : tooltip$$1;
    };

    function boxwhisker() {
      // for convenience in handling orientation specific values
      var horizontalQ = orient == 'horizontal' ? true : false;
      var verticalQ = !horizontalQ; // background cliping rectangle

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill); // if grouping is undefined sort keys by sorting funct

      var ordered = grouping == undefined ? d3$1.keys(data).sort(sortingFunction) : grouping; // to prevent re-calculation and getters to be passed to axes

      boxKeys = utils.arr.flatten(ordered);
      boxValues = boxKeys.map(valueExtractor);
      var numberOfObjects = boxKeys.length;
      var extent = [Math.min.apply(Math, _toConsumableArray$4(boxValues.map(function (d, i) {
        return d[quartilesKeys[0]];
      }))) - domainPadding, Math.max.apply(Math, _toConsumableArray$4(boxValues.map(function (d, i) {
        return d[quartilesKeys[4]];
      }))) + domainPadding]; // set the scale

      scale.domain(extent).range(horizontalQ ? [0, spaceY] : [spaceX, 0]);
      var space = horizontalQ ? spaceX : spaceY; // calculate object size

      objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ); // calculate spacer size if needed

      spacerSize = utils.math.calculateWidthOfSpacer(boxKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ); // make the nested groups

      var spacerFunction = groupingSpacer().horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects).objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace(namespace); // move stuff

      spacerFunction(container, ordered, 0);
      var parentIndexArray = [];
      container.selectAll('g:not(.to-remove).' + objectClass).each(function (d, i) {
        if (utils.arr.hasQ(boxKeys, d)) {
          parentIndexArray.push(Number(d3$1.select(this).attr('parent-index')));
        }
      });
      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, Math.max.apply(Math, parentIndexArray)]) : colorFunction$$1.dataExtent(extent); // set attributes for box and whiskers

      container.selectAll('g:not(.to-remove).' + objectClass).each(function (key, i) {
        var t = d3$1.select(this),
            currentData = data[key],
            quartiles$$1 = valueExtractor(key, i),
            q0 = quartiles$$1[quartilesKeys[0]],
            q1 = quartiles$$1[quartilesKeys[1]],
            q2 = quartiles$$1[quartilesKeys[2]],
            q3 = quartiles$$1[quartilesKeys[3]],
            q4 = quartiles$$1[quartilesKeys[4]];
        var i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
            fillColor = colorFunction$$1(key, q2, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(key, q2, i, 'stroke');
        var whisk = utils.sel.safeSelect(t, 'g', 'whisker'),
            uWhisk = utils.sel.safeSelect(whisk, 'path', 'upper'),
            lWhisk = utils.sel.safeSelect(whisk, 'path', 'lower'),
            quart = utils.sel.safeSelect(t, 'g', 'quartile'),
            uQuart = utils.sel.safeSelect(quart, 'rect', 'upper'),
            lQuart = utils.sel.safeSelect(quart, 'rect', 'lower'),
            mQuart = utils.sel.safeSelect(quart, 'circle', 'median'); // set upper quartile (q3)

        uQuart.transition().duration(transitionDuration).ease(easeFunc).attr('width', horizontalQ ? objectSize : scale(q3) - scale(q2)).attr('height', verticalQ ? objectSize : scale(q3) - scale(q2)).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', boxStrokeWidth).attr('transform', function (d, i) {
          var x = horizontalQ ? 0 : scale(q2),
              y = verticalQ ? 0 : scale(extent[1]) - scale(q3),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }); // set lower quartile (q1)

        lQuart.transition().duration(transitionDuration).ease(easeFunc).attr('width', horizontalQ ? objectSize : scale(q2) - scale(q1)).attr('height', verticalQ ? objectSize : scale(q2) - scale(q1)).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', boxStrokeWidth).attr('transform', function (d, i) {
          var x = horizontalQ ? 0 : scale(q1),
              y = verticalQ ? 0 : scale(extent[1]) - scale(q2),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }); // set median (q2)

        mQuart.transition().duration(transitionDuration).ease(easeFunc).attr('r', function (d, i) {
          var r = objectSize / 2;
          var dif = (scale(q3) - scale(q1)) / 2;
          return r > dif ? dif : r;
        }).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', boxStrokeWidth).attr('transform', function (d, i) {
          var x = horizontalQ ? objectSize / 2 : scale(q2),
              y = verticalQ ? objectSize / 2 : scale(extent[1]) - scale(q2),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }); // set lower whisker (min)

        lWhisk.transition().duration(transitionDuration).ease(easeFunc).attr('d', function (dd, ii) {
          var dir = false,
              x = 0,
              y = 0,
              h = horizontalQ ? scale(q1) - scale(q0) : objectSize,
              w = verticalQ ? scale(q1) - scale(q0) : objectSize;
          return utils.paths.whiskerPath(dir, x, y, w, h, whiskerWidthPercent, orient);
        }).attr('transform', function (d, i) {
          var x = horizontalQ ? 0 : scale(q1),
              y = verticalQ ? 0 : scale(extent[1]) - scale(q1),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }).attr('stroke', 'black').attr('stroke-width', whiskerStrokeWidth).attr('fill', 'none'); // set upper whisker (max)

        uWhisk.transition().duration(transitionDuration).ease(easeFunc).attr('d', function (dd, ii) {
          var dir = true,
              x = 0,
              y = 0,
              h = horizontalQ ? scale(q4) - scale(q3) : objectSize,
              w = verticalQ ? scale(q4) - scale(q3) : objectSize;
          return utils.paths.whiskerPath(dir, x, y, w, h, whiskerWidthPercent, orient);
        }).attr('transform', function (d, i) {
          var x = horizontalQ ? 0 : scale(q3),
              y = verticalQ ? 0 : scale(extent[1]) - scale(q4),
              t = 'translate(' + x + ',' + y + ')';
          return t;
        }).attr('stroke', 'black').attr('stroke-width', whiskerStrokeWidth).attr('fill', 'none');
      });
      tooltip$$1.selection(container.selectAll('g:not(.to-remove).' + objectClass)).data(data);
      tooltip$$1();
    }

    return boxwhisker;
  }

  function _toConsumableArray$5(arr$$1) { return _arrayWithoutHoles$5(arr$$1) || _iterableToArray$5(arr$$1) || _nonIterableSpread$5(); }

  function _nonIterableSpread$5() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$5(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$5(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /**
   * Creates a heatmap
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/heatmap-heatmap/index.html Demo}
   * @constructor heatmap
   * @param {d3.selection} selection
   * @namespace heatmap
   * @returns {function} heatmap
   */

  function heatmap(selection) {
    var 
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a cell
    * (see {@link heatmap#data})
    * @param {Object} [data=undefined]
    * @memberof heatmap#
    * @property
    */
    data,

    /**
    * Amount of horizontal space (in pixels) avaible to render the heatmap in
    * (see {@link heatmap#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof heatmap#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the heatmap in
    * (see {@link heatmap.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof heatmap#
    * @property
    */
    spaceY,

    /**
    * The internal key of the cell specifiying to which x axis key it belongs
    * (see {@link heatmap.xKey})
    * @param {string} [xKey='x']
    * @memberof heatmap#
    * @property
    */
    xKey = 'x',

    /**
    * The internal key of the cell specifiying to which y axis key it belongs
    * (see {@link heatmap.yKey})
    * @param {string} [yKey='y']
    * @memberof heatmap#
    * @property
    */
    yKey = 'y',

    /**
    * The internal key of the cell specifiying what value to use to determine the color
    * (see {@link heatmap.vKey})
    * @param {string} [vKey='v']
    * @memberof heatmap#
    * @property
    */
    vKey = 'v',

    /**
    * Function for extracting the the value from xKey.
    * (see {@link heatmap.xExtractor})
    * @param {function} [xExtractor=function(key, i) { return data[key][xKey] }]
    * @returns {string}
    * @memberof heatmap#
    * @property
    */
    xExtractor = function xExtractor(key, i) {
      return data[key][xKey];
    },

    /**
    * Function for extracting the the value from yKey.
    * (see {@link heatmap.yExtractor})
    * @param {function} [yExtractor=function(key, i) { return data[key][yKey] }]
    * @returns {string}
    * @memberof heatmap#
    * @property
    */
    yExtractor = function yExtractor(key, i) {
      return data[key][yKey];
    },

    /**
    * Function for extracting the the value from vKey.
    * (see {@link heatmap.vExtractor})
    * @param {function} [vExtractor=function(key, i) { return data[key][vKey] }]
    * @returns {number}
    * @memberof heatmap#
    * @property
    */
    vExtractor = function vExtractor(key, i) {
      return data[key][vKey];
    },

    /**
    * Whether or not to allow heatmap to render elements pass the main spatial dimension
    * given the orientation (see {@link heatmap#orient}), where {@link heatmap#orient}="bottom" or {@link heatmap#orient}="top"
    * the main dimension is {@link heatmap#spaceX} and where {@link heatmap#orient}="left" or {@link heatmap#orient}="right"
    * the main dimension is {@link heatmap#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof heatmap#
    * @property
    */
    overflowQ = false,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link heatmap#orient}), where {@link heatmap#orient}="horizontal"
    * the main dimension is {@link heatmap#spaceX} and where {@link heatmap#orient}="vertical"
    * the main dimension is {@link heatmap#spaceY} between bubbles
    * @param {number} [objectSpacer=0.0]
    * @memberof heatmap#
    * @property
    */
    objectSpacer = 0.0,

    /**
    * The minimum size that an object can be in the y dimension
    * @param {number} [minObjectSize=50]
    * @memberof heatmap#
    * @property
    */
    yMinObjectSize = 50,

    /**
    * The minimum size that an object can be in the x dimension
    * @param {number} [minObjectSize=50]
    * @memberof heatmap#
    * @property
    */
    xMinObjectSize = 50,

    /**
    * The maximum size that an object can be in the x dimension
    * @param {number} [maxObjectSize=100]
    * @memberof heatmap#
    * @property
    */
    xMaxObjectSize = 100,

    /**
    * The maximum size that an object can be in the y dimension
    * @param {number} [maxObjectSize=100]
    * @memberof heatmap#
    * @property
    */
    yMaxObjectSize = 100,

    /**
    * The stroke width of the bubbles
    * @param {number} [objectStrokeWidth=2]
    * @memberof heatmap#
    * @property
    */
    objectStrokeWidth = 2,
        // colorFunc = colorFunction(),

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof heatmap#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of heatmap
    * @param {string} [namespace="d3sm-heatmap"]
    * @memberof heatmap#
    * @property
    */
    namespace = 'd3sm-heatmap',

    /**
    * Class name for heatmap container (<g> element)
    * @param {string} [objectClass="heatmap"]
    * @memberof heatmap#
    * @property
    */
    objectClass = 'heatmap',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof heatmap#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof heatmap#
    * @property
    */
    easeFunc = d3$1.easeExp,

    /**
    * Stores the keys of all the cells
    * Calculated after heatmap called.
    * @param {string[]} [cellKeys=undefined]
    * @memberof heatmap#
    * @property
    */
    cellKeys,

    /**
    * Stores the list of utils.arr.unique xValues
    * Calculated after heatmap called.
    * @param {string[]} [xValues=undefined]
    * @memberof heatmap#
    * @property
    */
    xValues,

    /**
    * Stores the list of utils.arr.unique yValues
    * Calculated after heatmap called.
    * @param {string[]} [yValues=undefined]
    * @memberof heatmap#
    * @property
    */
    yValues,

    /**
    * Stores the list of utils.arr.unique vValues
    * Calculated after heatmap called.
    * @param {string[]} [vValues=undefined]
    * @memberof heatmap#
    * @property
    */
    vValues,
        xKeySortingFunction = function xKeySortingFunction(a, b) {
      return xValues.indexOf(xExtractor(a)) - xValues.indexOf(xExtractor(b));
    },
        yKeySortingFunction = function yKeySortingFunction(a, b) {
      return yValues.indexOf(yExtractor(a)) - yValues.indexOf(yExtractor(b));
    },
        /**
    * Instance of ColorFunction with .colorBy set to 'category'
    * @function colorFunction
    * @memberof heatmap#
    * @property
    */
    colorFunction$$1 = colorFunction().colorBy('category'),

    /**
    * Instance of Tooltip
    * @function tooltip
    * @memberof heatmap#
    * @property
    */
    tooltip$$1 = tooltip(),

    /**
    * store the size the heatmap could be in the x dimension
    * the actuall size of the heatmap will be the min of xSize and {@link heatmap#ySize}
    * Calculated after heatmap called.
    * @param {string[]} [xSize=undefined]
    * @memberof heatmap#
    * @property
    */
    xSize,

    /**
    * store the size of the spacer in the x dimension
    * Calculated after heatmap called.
    * @param {string[]} [xSpacerSize=undefined]
    * @memberof heatmap#
    * @property
    */
    xSpacerSize,

    /**
    * store the size the heatmap could be in the y dimension
    * the actuall size of the heatmap will be the min of xSize and {@link heatmap#xSize}
    * Calculated after heatmap called.
    * @param {string[]} [ySize=undefined]
    * @memberof heatmap#
    * @property
    */
    ySize,

    /**
    * store the size of the spacer in the y dimension.
    * Calculated after heatmap called.
    * @param {string[]} [xSpacerSize=undefined]
    * @memberof heatmap#
    * @property
    */
    ySpacerSize;
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {heatmap | d3.selection}
     * @memberof heatmap
     * @property
     * by default selection = selection
     */


    hm.selection = function (_) {
      return arguments.length ? (selection = _, hm) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link heatmap#data})
     * @param {number} [_=none]
     * @returns {heatmap | object}
     * @memberof heatmap
     * @property
     */


    hm.data = function (_) {
      return arguments.length ? (data = _, hm) : data;
    }; // hm.orient = function(_) { return arguments.length ? (orient = _, hm) : orient; }

    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link heatmap#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default spaceX = undefined
     */


    hm.spaceX = function (_) {
      return arguments.length ? (spaceX = _, hm) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link heatmap#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default spaceY = undefined
     */


    hm.spaceY = function (_) {
      return arguments.length ? (spaceY = _, hm) : spaceY;
    };
    /**
     * Gets or sets the xKey
     * (see {@link heatmap#xKey})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default xKey = 'x'
     */


    hm.xKey = function (_) {
      return arguments.length ? (xKey = _, hm) : xKey;
    };
    /**
     * Gets or sets the yKey
     * (see {@link heatmap#yKey})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default yKey = 'y'
     */


    hm.yKey = function (_) {
      return arguments.length ? (yKey = _, hm) : yKey;
    };
    /**
     * Gets or sets the vKey
     * (see {@link heatmap#vKey})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default vKey = 'y'
     */


    hm.vKey = function (_) {
      return arguments.length ? (vKey = _, hm) : vKey;
    };
    /**
     * Gets or sets the cellKeys
     * (see {@link heatmap#cellKeys})
     * @param {string[]} [_=none]
     * @returns {heatmap | string[]}
     * @memberof heatmap
     * @property
     * by default cellKeys = undefined
     */


    hm.cellKeys = function (_) {
      return arguments.length ? (cellKeys = _, hm) : cellKeys;
    };
    /**
     * Gets or sets the xValues
     * (see {@link heatmap#xValues})
     * @param {string[]} [_=none]
     * @returns {heatmap | string[]}
     * @memberof heatmap
     * @property
     * by default xValues = undefined
     */


    hm.xValues = function (_) {
      return arguments.length ? (xValues = _, hm) : xValues;
    };
    /**
     * Gets or sets the yValues
     * (see {@link heatmap#yValues})
     * @param {string[]} [_=none]
     * @returns {heatmap | string[]}
     * @memberof heatmap
     * @property
     * by default yValues = undefined
     */


    hm.yValues = function (_) {
      return arguments.length ? (yValues = _, hm) : yValues;
    };
    /**
     * Gets or sets the vValues
     * (see {@link heatmap#vValues})
     * @param {number[]} [_=none]
     * @returns {heatmap | number[]}
     * @memberof heatmap
     * @property
     * by default vValues = undefined
     */


    hm.vValues = function (_) {
      return arguments.length ? (vValues = _, hm) : vValues;
    };
    /**
     * Gets or sets the xExtractor
     * (see {@link heatmap#xExtractor})
     * @param {function} [_=none]
     * @returns {heatmap | function}
     * @memberof heatmap
     * @property
     * by default xExtractor = undefined
     */


    hm.xExtractor = function (_) {
      return arguments.length ? (xExtractor = _, hm) : xExtractor;
    };
    /**
     * Gets or sets the yExtractor
     * (see {@link heatmap#yExtractor})
     * @param {function} [_=none]
     * @returns {heatmap | function}
     * @memberof heatmap
     * @property
     * by default yExtractor = undefined
     */


    hm.yExtractor = function (_) {
      return arguments.length ? (yExtractor = _, hm) : yExtractor;
    };
    /**
     * Gets or sets the vExtractor
     * (see {@link heatmap#vExtractor})
     * @param {function} [_=none]
     * @returns {heatmap | function}
     * @memberof heatmap
     * @property
     * by default vExtractor = undefined
     */


    hm.vExtractor = function (_) {
      return arguments.length ? (vExtractor = _, hm) : vExtractor;
    };
    /**
     * Gets / sets whether or not heatmap is allowed to go beyond specified dimensions
     * (see {@link heatmap#spaceX})
     * @param {boolean} [_=none]
     * @returns {heatmap | boolean}
     * @memberof heatmap
     * @property
     * by default overflowQ = false
     */


    hm.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, hm) : overflowQ;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link heatmap#objectSpacer})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default objectSpacer = 0.0
     */


    hm.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, objectSpacer) : data;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link heatmap#minObjectSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default minObjectSize = 50
     */


    hm.yMinObjectSize = function (_) {
      return arguments.length ? (yMinObjectSize = _, hm) : yMinObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link heatmap#maxObjectSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default maxObjectSize = 100
     */


    hm.yMaxObjectSize = function (_) {
      return arguments.length ? (yMaxObjectSize = _, hm) : yMaxObjectSize;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link heatmap#minObjectSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default minObjectSize = 50
     */


    hm.xMinObjectSize = function (_) {
      return arguments.length ? (xMinObjectSize = _, hm) : xMinObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link heatmap#maxObjectSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default maxObjectSize = 100
     */


    hm.xMaxObjectSize = function (_) {
      return arguments.length ? (xMaxObjectSize = _, hm) : xMaxObjectSize;
    };
    /**
     * Gets / sets the objectStrokeWidth
     * (see {@link heatmap#objectStrokeWidth})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default objectStrokeWidth = 2
     */


    hm.objectStrokeWidth = function (_) {
      return arguments.length ? (objectStrokeWidth = _, hm) : objectStrokeWidth;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link heatmap#backgroundFill})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default backgroundFill = 'transparent'
     */


    hm.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, hm) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link heatmap#namespace})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default namespace = 'd3sm-heatmap'
     */


    hm.namespace = function (_) {
      return arguments.length ? (namespace = _, hm) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link heatmap#objectClass})
     * @param {string} [_=none]
     * @returns {heatmap | string}
     * @memberof heatmap
     * @property
     * by default objectClass = 'tick-group'
     */


    hm.objectClass = function (_) {
      return arguments.length ? (objectClass = _, hm) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link heatmap#transitionDuration})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default transitionDuration = 1000
     */


    hm.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, hm) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link heatmap#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {heatmap | d3.ease}
     * @memberof heatmap
     * @property
     * by default easeFunc = d3.easeExp
     */


    hm.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, hm) : easeFunc;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link heatmap#tooltip})
     * @param {tooltip} [_=none]
     * @returns {heatmap | tooltip}
     * @memberof heatmap
     * @property
     * by default tooltip = tooltip()
     */


    hm.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, hm) : tooltip$$1;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link heatmap#colorFunction})
     * @param {colorFunction} [_=none]
     * @returns {heatmap | colorFunction}
     * @memberof heatmap
     * @property
     * by default colorFunction = colorFunction()
     */


    hm.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, hm) : colorFunction$$1;
    };
    /**
     * Gets / sets the xSize
     * (see {@link heatmap#xSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default xSize = undefined
     */


    hm.xSize = function (_) {
      return arguments.length ? (xSize = _, hm) : xSize;
    };
    /**
     * Gets / sets the xSpacerSize
     * (see {@link heatmap#xSpacerSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default xSpacerSize = undefined
     */


    hm.xSpacerSize = function (_) {
      return arguments.length ? (xSpacerSize = _, hm) : xSpacerSize;
    };
    /**
     * Gets / sets the ySize
     * (see {@link heatmap#ySize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default ySize = undefined
     */


    hm.ySize = function (_) {
      return arguments.length ? (ySize = _, hm) : ySize;
    };
    /**
     * Gets / sets the ySpacerSize
     * (see {@link heatmap#ySpacerSize})
     * @param {number} [_=none]
     * @returns {heatmap | number}
     * @memberof heatmap
     * @property
     * by default ySpacerSize = undefined
     */


    hm.ySpacerSize = function (_) {
      return arguments.length ? (ySpacerSize = _, hm) : ySpacerSize;
    }; // hm.yKeySortingFunction = function(_) { return arguments.length ? (yKeySortingFunction = _, hm) : yKeySortingFunction; }
    // hm.xKeySortingFunction = function(_) { return arguments.length ? (xKeySortingFunction = _, hm) : xKeySortingFunction; }


    function hm() {

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      cellKeys = d3$1.keys(data);
      xValues = utils.arr.unique(cellKeys.map(xExtractor));
      yValues = utils.arr.unique(cellKeys.map(yExtractor));
      vValues = utils.arr.unique(cellKeys.map(vExtractor));
      cellKeys.sort(function (a, b) {
        return xKeySortingFunction(a, b) || yKeySortingFunction(a, b);
      });
      utils.con.log('heatmap', 'cells are sorted by', cellKeys);
      utils.con.log('heatmap', 'x and y keys are', {
        x: xValues,
        y: yValues
      });
      var xDim = xValues.length,
          yDim = yValues.length;
      ySize = utils.math.calculateWidthOfObject(spaceY, yDim, yMinObjectSize, yMaxObjectSize, objectSpacer, overflowQ);
      xSize = utils.math.calculateWidthOfObject(spaceX, xDim, xMinObjectSize, xMaxObjectSize, objectSpacer, overflowQ);
      ySpacerSize = utils.math.calculateWidthOfSpacer(yValues, spaceY, ySize, yDim, objectSpacer, overflowQ);
      xSpacerSize = utils.math.calculateWidthOfSpacer(xValues, spaceX, xSize, xDim, objectSpacer, overflowQ); // console.table({
      //     x:{
      //       object: xSize,
      //       spacer: xSpacerSize,
      //       dim: xDim
      //     },
      //     y:{
      //       object: ySize,
      //       spacer: ySpacerSize,
      //       dim: yDim
      //     }
      //
      // })

      utils.con.log('heatmap', 'size of', {
        x: xSize,
        y: ySize
      });
      var ySpacer = groupingSpacer().horizontalQ(false).moveby('category').numberOfObjects(yDim).objectClass(utils.str.hypenate(objectClass, 'row')).objectSize(ySize + ySpacerSize).spacerSize(0).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace('row');
      var xSpacer = groupingSpacer().horizontalQ(true).moveby('category').numberOfObjects(xDim).objectClass(objectClass).objectSize(xSize + xSpacerSize).spacerSize(0).transitionDuration(transitionDuration).easeFunc(easeFunc);
      ySpacer(container, yValues, 0);
      container.selectAll('g.' + utils.str.hypenate(objectClass, 'row')).each(function (d, i) {
        xSpacer(d3$1.select(this), xValues, 0);
      });
      var cells = container.selectAll('g:not(.to-remove).' + objectClass);

      if (cellKeys.length != yValues.length * xValues.length) {
        var lookup = {};
        cellKeys.map(function (k, i) {
          lookup[xExtractor(k) + '::' + yExtractor(k)] = k;
        });
        var positionedCellKeys = [];

        for (var i = 0; i < yValues.length; i++) {
          for (var j = 0; j < xValues.length; j++) {
            var lookupValue = lookup[xValues[j] + "::" + yValues[i]];

            if (lookupValue == undefined) {
              positionedCellKeys.push(undefined);
            } else {
              positionedCellKeys.push(lookupValue);
            }
          }
        }

        cells.data(positionedCellKeys); // maybe breaks this
        // !!!!!! IMPORTANT NOTE TODO LOOK HERE BUG

        cellKeys = positionedCellKeys;
      } else {
        cells.data(cellKeys);
      }

      var parentIndexArray = [];
      cells.each(function (d, i) {
        parentIndexArray.push(Number(d3$1.select(this).attr('parent-index')));
      });
      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, Math.max.apply(Math, parentIndexArray)]) : colorFunction$$1.dataExtent([0, Math.max.apply(Math, _toConsumableArray$5(vValues))]);
      cells.each(function (key, i) {
        utils.con.log('heatmap', 'each cell', {
          key: key,
          index: i,
          node: d3$1.select(this).node()
        });
        var t = d3$1.select(this);

        if (key == undefined) {
          return;
        }

        var currentData = data[key],
            value = vExtractor(key, i),
            i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
            fillColor = colorFunction$$1(key, value, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(key, value, i, 'stroke');
        var c = utils.sel.safeSelect(t, 'rect', utils.str.hypenate(objectClass, 'rect'));
        c.attr('width', xSize + xSpacerSize - objectStrokeWidth).attr('height', ySize + ySpacerSize - objectStrokeWidth).attr('fill', fillColor).attr('x', objectStrokeWidth / 2).attr('y', objectStrokeWidth / 2).attr('stroke', "#000").attr('stroke-width', objectStrokeWidth);
      });
      tooltip$$1.selection(cells.selectAll('rect.' + utils.str.hypenate(objectClass, 'rect'))).data(data); // .keys(['r', 'v'])
      // .header(function(d, i){return utils.str.hypenate(data[d][xKey], data[d][yKey]) })

      tooltip$$1();
    }

    return hm;
  }

  function _toConsumableArray$6(arr$$1) { return _arrayWithoutHoles$6(arr$$1) || _iterableToArray$6(arr$$1) || _nonIterableSpread$6(); }

  function _nonIterableSpread$6() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$6(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$6(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                 VIOLIN                                     **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates a violin
   *
   * {@link https://sumneuron.gitlab.io/d3sm/demos/basic-violins/index.html Demo}
   * @constructor violin
   * @param {d3.selection} selection
   * @namespace violin
   * @returns {function} violin
   */

  function violin(selection) {
    var 
    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a violin
    * (see {@link violin#data})
    * @param {Object} [data=undefined]
    * @memberof violin#
    * @property
    */
    data,

    /**
    * Which direction to render the bars in
    * (see {@link violin#orient})
    * @param {number} [orient='horizontal']
    * @memberof violin#
    * @property
    */
    orient = 'horizontal',

    /**
    * Amount of horizontal space (in pixels) avaible to render the violin in
    * (see {@link violin#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof violin#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the violin in
    * (see {@link violin.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof violin#
    * @property
    */
    spaceY,

    /**
    * Whether or not to allow violin to render elements pass the main spatial dimension
    * given the orientation (see {@link violin#orient}), where {@link violin#orient}="horizontal"
    * the main dimension is {@link violin#spaceX} and where {@link violin#orient}="vertical"
    * the main dimension is {@link violin#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof violin#
    * @property
    */
    overflowQ = true,

    /**
    * Whether or not to display points inside the points
    * @param {boolean} [pointsQ=false]
    * @memberof violin#
    * @property
    */
    pointsQ = true,

    /**
    * An array - putatively of other arrays - depicting how bars should be arranged
    * @param {Array[]} [grouping=undefined]
    * @memberof violin#
    * @property
    */
    grouping,

    /**
    * How to get the value of the violin
    * @param {function} [valueExtractor=function(key, index) { return data[key] }]
    * @memberof violin#
    * @property
    */
    valueExtractor = function valueExtractor(key, index) {
      return data[key];
    },

    /**
    * How to sort the bars - if {@link violin#grouping} is not provided.
    * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
    * @memberof violin#
    * @property
    */
    sortingFunction = function sortingFunction(keyA, keyB) {
      return d3$1.descending(data[keyA], data[keyB]);
    },

    /**
    * The scale for which violin values should be transformed by
    * @param {d3.scale} [scale=d3.scaleLinear]
    * @memberof violin#
    * @property
    */
    scale = d3$1.scaleLinear(),

    /**
    * The padding for the domain of the scale (see {@link violin#scale})
    * @param {number} [domainPadding=0.5]
    * @memberof violin#
    * @property
    */
    domainPadding = 0.5,

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link violin#orient}), where {@link violin#orient}="horizontal"
    * the main dimension is {@link violin#spaceX} and where {@link violin#orient}="vertical"
    * the main dimension is {@link violin#spaceY} between bars
    * @param {number} [objectSpacer=0.05]
    * @memberof violin#
    * @property
    */
    objectSpacer = 0.05,

    /**
    * The minimum size that an object can be
    * @param {number} [minObjectSize=50]
    * @memberof violin#
    * @property
    */
    minObjectSize = 50,

    /**
    * The maximum size that an object can be
    * @param {number} [maxObjectSize=100]
    * @memberof violin#
    * @property
    */
    maxObjectSize = 100,

    /**
    * The stroke width of the bars
    * @param {number} [barStrokeWidth=2]
    * @memberof violin#
    * @property
    */
    objectStrokeWidth = 2,

    /**
    * Instance of ColorFunction
    * @param {function} [colorFunction = colorFunction()]
    * @memberof violin#
    * @property
    */
    colorFunction$$1 = colorFunction(),

    /**
    * Instance of ColorFunction modified by a scale for the points
    * @param {function} [pointColorFunc = colorFunction()]
    * @memberof violin#
    * @property
    */
    pointColorFunc = function pointColorFunc(d, type, base, min, max) {
      var minMaxHexScale = d3$1.scaleLinear().domain([min, max]).range([-0.25, 0.05]);
      var scaledColor = utils.color.modifyHexidecimalColorLuminance(base.replace('#', ''), minMaxHexScale(d));
      var mod = type == "stroke" ? 0 : 0.25;
      return utils.color.modifyHexidecimalColorLuminance(scaledColor.replace('#', ''), mod);
    },

    /**
    * The radius of a point
    * @param {number} [pointRadius=3]
    * @memberof violin#
    * @property
    */
    pointRadius = 3,

    /**
    * The stroke width of the oints
    * @param {number} [pointStrokeWidth=2]
    * @memberof violin#
    * @property
    */
    pointStrokeWidth = 2,

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof violin#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of violin
    * @param {string} [namespace="d3sm-violin"]
    * @memberof violin#
    * @property
    */
    namespace = 'd3sm-violin',

    /**
    * Class name for violin container (<g> element)
    * @param {string} [objectClass="violin"]
    * @memberof violin#
    * @property
    */
    objectClass = 'violin',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof violin#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof violin#
    * @property
    */
    easeFunc = d3$1.easeExp,

    /**
    * The keys corresponding to each quartile
    * @param {string[]} [quartileKeys=["Q0", "Q1", "Q2", "Q3", "Q4"]]
    * @memberof violin#
    * @property
    */
    quartileKeys = ["Q0", "Q1", "Q2", "Q3", "Q4"],

    /**
    * The keys of the bars
    * @param {string[]} [violinKeys=undefined]
    * @memberof violin#
    * @property
    */
    violinKeys,

    /**
    * The values of the bars
    * @param {number[]} [violinValues=undefined]
    * @memberof violin#
    * @property
    */
    violinValues,

    /**
    * The objectSize (actual width) used by the bars
    * @param {number} [objectSize=undefined]
    * @memberof violin#
    * @property
    */
    objectSize,

    /**
    * The spacerSize (actual width) used by the spacers between the bars
    * @param {number} [spacerSize=undefined]
    * @memberof violin#
    * @property
    */
    spacerSize,

    /**
    * Instance of Tooltip
    * @param {function} [tooltip=tooltip()]
    * @memberof violin#
    * @property
    */
    tooltip$$1 = tooltip().keys([quartileKeys[4], quartileKeys[3], quartileKeys[2], quartileKeys[1], quartileKeys[0]]),
        pointsTooltip = tooltip(),
        // pointKeyExtractor = function(violinKey, violinData, violinValues) {return d3.keys(violinValues[violinKey].values)},
    // pointValueExtractor = function(pointKey, violinKey, violinData, violinValues) {return violinValues[violinKey].values[pointKey]},

    /**
    * Function which given the key of the violin and that key's associated value
    * returns the object consiting of the points of the violin
    * (see {@link violin#violinPointsExtractor})
    * @param {Object} [violinPointsExtractor=function(violinKey, violinData) { return violinData.points }]
    * @memberof violin#
    * @property
    */
    violinPointsExtractor = function violinPointsExtractor(violinKey, violinData) {
      return violinData.points;
    },

    /**
    * Function which given the key of the current point and the object of points for the
    * violin, returns the numerical value of the point
    * (see {@link violin#violinPointValueExtractor})
    * @param {Object} [violinPointValueExtractor=function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }]
    * @memberof violin#
    * @property
    */
    violinPointValueExtractor = function violinPointValueExtractor(violinPointKey, violinPointData) {
      return violinPointData[violinPointKey].value;
    };
    /**
     * Gets or sets the violinPointsExtractor
     * @param {function} [_=none]
     * @returns {violin | function}
     * @memberof violin
     * @property
     * by default violinPointsExtractor = function(violinKey, violinData) { return violinData.points }
     */


    violin.violinPointsExtractor = function (_) {
      return arguments.length ? (violinPointsExtractor = _, violin) : violinPointsExtractor;
    };
    /**
     * Gets or sets the violinPointValueExtractor
     * @param {function} [_=none]
     * @returns {violin | function}
     * @memberof violin
     * @property
     * by default violinPointsExtractor = function(pointKey, violinKey, violinData, violinValues) {return violinValues[violinKey].values[pointKey]}
     */


    violin.violinPointValueExtractor = function (_) {
      return arguments.length ? (violinPointValueExtractor = _, violin) : violinPointValueExtractor;
    };
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {violin | d3.selection}
     * @memberof violin
     * @property
     * by default selection = selection
     */


    violin.selection = function (_) {
      return arguments.length ? (selection = _, violin) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link violin#data})
     * @param {number} [_=none]
     * @returns {violin | object}
     * @memberof violin
     * @property
     */


    violin.data = function (_) {
      return arguments.length ? (data = _, violin) : data;
    };
    /**
     * Gets or sets the orient of the boxes
     * (see {@link violin#orient})
     * @param {number} [_=none]
     * @returns {violin | object}
     * @memberof violin
     * @property
     */


    violin.orient = function (_) {
      return arguments.length ? (orient = _, violin) : orient;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link violin#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default spaceX = undefined
     */


    violin.spaceX = function (_) {
      return arguments.length ? (spaceX = _, violin) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link violin#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default spaceY = undefined
     */


    violin.spaceY = function (_) {
      return arguments.length ? (spaceY = _, violin) : spaceY;
    };
    /**
     * Gets / sets whether or not violin is allowed to go beyond specified dimensions
     * (see {@link violin#overflowQ})
     * @param {boolean} [_=none]
     * @returns {violin | boolean}
     * @memberof violin
     * @property
     * by default overflowQ = false
     */


    violin.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, violin) : overflowQ;
    };
    /**
     * Gets / sets whether or not to plot points with the violins
     * (see {@link violin#pointsQ})
     * @param {boolean} [_=none]
     * @returns {violin | boolean}
     * @memberof violin
     * @property
     * by default pointsQ = false
     */


    violin.pointsQ = function (_) {
      return arguments.length ? (pointsQ = _, violin) : pointsQ;
    };
    /**
     * Gets / sets the grouping of the boxes
     * (see {@link violin#grouping})
     * @param {Array[]} [_=none]
     * @returns {violin | Array[]}
     * @memberof violin
     * @property
     * by default grouping = undefined
     */


    violin.grouping = function (_) {
      return arguments.length ? (grouping = _, violin) : grouping;
    };
    /**
     * Gets / sets the valueExtractor
     * (see {@link violin#valueExtractor})
     * @param {function} [_=none]
     * @returns {violin | function}
     * @memberof violin
     * @property
     */


    violin.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, violin) : valueExtractor;
    };
    /**
     * Gets / sets the sortingFunction
     * (see {@link violin#sortingFunction})
     * @param {function} [_=none]
     * @returns {violin | function}
     * @memberof violin
     * @property
     */


    violin.sortingFunction = function (_) {
      return arguments.length ? (sortingFunction = _, violin) : sortingFunction;
    };
    /**
     * Gets / sets the scale for which the violin values should be transformed by
     * (see {@link violin#scale})
     * @param {d3.scale} [_=none]
     * @returns {violin | d3.scale}
     * @memberof violin
     * @property
     * by default scale = d3.scaleLinear()
     */


    violin.scale = function (_) {
      return arguments.length ? (scale = _, violin) : scale;
    };
    /**
     * Gets / sets the padding for the domain of the scale
     * (see {@link violin#domainPadding})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default domainPadding = 0.5
     */


    violin.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, violin) : domainPadding;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link violin#objectSpacer})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default objectSpacer = 0.05
     */


    violin.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, violin) : objectSpacer;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link violin#minObjectSize})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default minObjectSize = 15
     */


    violin.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, violin) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link violin#maxObjectSize})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default maxObjectSize = 50
     */


    violin.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, violin) : maxObjectSize;
    };
    /**
     * Gets / sets the objectStrokeWidth
     * (see {@link violin#objectStrokeWidth})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default objectStrokeWidth = 2
     */


    violin.objectStrokeWidth = function (_) {
      return arguments.length ? (objectStrokeWidth = _, violin) : objectStrokeWidth;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link violin#colorFunction})
     * @param {colorFunction} [_=none]
     * @returns {violin | colorFunction}
     * @memberof violin
     * @property
     * by default colorFunction = colorFunction()
     */


    violin.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, violin) : colorFunction$$1;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link violin#colorFunction})
     * @param {colorFunction} [_=none]
     * @returns {violin | colorFunction}
     * @memberof violin
     * @property
     * by default colorFunction = colorFunction()
     */


    violin.pointColorFunc = function (_) {
      return arguments.length ? (pointColorFunc = _, violin) : pointColorFunc;
    };
    /**
     * Gets / sets the pointRadius
     * (see {@link violin#pointRadius})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default pointRadius = 2
     */


    violin.pointRadius = function (_) {
      return arguments.length ? (pointRadius = _, violin) : pointRadius;
    };
    /**
     * Gets / sets the pointStrokeWidth
     * (see {@link violin#pointStrokeWidth})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default pointStrokeWidth = 2
     */


    violin.pointStrokeWidth = function (_) {
      return arguments.length ? (pointStrokeWidth = _, violin) : pointStrokeWidth;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link violin#backgroundFill})
     * @param {string} [_=none]
     * @returns {violin | string}
     * @memberof violin
     * @property
     * by default backgroundFill = 'transparent'
     */


    violin.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, violin) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link violin#namespace})
     * @param {string} [_=none]
     * @returns {violin | string}
     * @memberof violin
     * @property
     * by default namespace = 'd3sm-violin'
     */


    violin.namespace = function (_) {
      return arguments.length ? (namespace = _, violin) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link violin#objectClass})
     * @param {string} [_=none]
     * @returns {violin | string}
     * @memberof violin
     * @property
     * by default objectClass = 'tick-group'
     */


    violin.objectClass = function (_) {
      return arguments.length ? (objectClass = _, violin) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link violin#transitionDuration})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default transitionDuration = 1000
     */


    violin.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, violin) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link violin#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {violin | d3.ease}
     * @memberof violin
     * @property
     * by default easeFunc = d3.easeExp
     */


    violin.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, violin) : easeFunc;
    };
    /**
     * Gets / sets the quartileKey
     * (see {@link violin#quartileKey})
     * @param {string} [_=none]
     * @returns {violin | string}
     * @memberof violin
     * @property
     * by default quartileKey = "utils.math.quartiles"
     */


    violin.quartileKey = function (_) {
      return arguments.length ? (quartileKey = _, violin) : quartileKey;
    };
    /**
     * Gets / sets the quartileKeys
     * (see {@link violin#quartileKeys})
     * @param {string[]} [_=none]
     * @returns {violin | string[]}
     * @memberof violin
     * @property
     * by default quartileKeys = ["Q0","Q1","Q2","Q3","Q4"]
     */


    violin.quartileKeys = function (_) {
      return arguments.length ? (quartileKeys = _, violin) : quartileKeys;
    };
    /**
     * Gets / sets the violinKeys
     * (see {@link violin#violinKeys})
     * @param {string[]} [_=none]
     * @returns {violin | string[]}
     * @memberof violin
     * @property
     * by default violinKeys = undefined
     */


    violin.violinKeys = function (_) {
      return arguments.length ? (violinKeys = _, violin) : violinKeys;
    };
    /**
     * Gets / sets the violinValues
     * (see {@link violin#violinValues})
     * @param {Object[]} [_=none]
     * @returns {violin | Object[]}
     * @memberof violin
     * @property
     * by default violinValues = undefined
     */


    violin.violinValues = function (_) {
      return arguments.length ? (violinValues = _, violin) : violinValues;
    };
    /**
     * Gets / sets the objectSize
     * (see {@link violin#objectSize})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default objectSize = undefined
     */


    violin.objectSize = function (_) {
      return arguments.length ? (objectSize = _, violin) : objectSize;
    };
    /**
     * Gets / sets the spacerSize
     * (see {@link violin#spacerSize})
     * @param {number} [_=none]
     * @returns {violin | number}
     * @memberof violin
     * @property
     * by default spacerSize = undefined
     */


    violin.spacerSize = function (_) {
      return arguments.length ? (spacerSize = _, violin) : spacerSize;
    };
    /**
     * Gets / sets the tooltip
     * (see {@link violin#tooltip})
     * @param {tooltip} [_=none]
     * @returns {violin | tooltip}
     * @memberof violin
     * @property
     * by default tooltip = tooltip()
     */


    violin.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, violin) : tooltip$$1;
    };
    /**
     * Gets / sets the pointsTooltip
     * (see {@link violin#pointsTooltip})
     * @param {tooltip} [_=none]
     * @returns {violin | tooltip}
     * @memberof violin
     * @property
     * by default pointsTooltip = tooltip()
     */


    violin.pointsTooltip = function (_) {
      return arguments.length ? (pointsTooltip = _, violin) : pointsTooltip;
    };

    function violin() {
      var _ref, _ref2, _ref3;

      // for convenience in handling orientation specific values
      var horizontalQ = orient == 'horizontal' ? true : false;

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill); // if grouping is undefined sort violinKeys by sortingFunction

      var ordered = grouping == undefined ? d3$1.keys(data).sort(sortingFunction) : grouping; // console.log(ordered)

      violinKeys = utils.arr.flatten(ordered);
      var calcValues = neededViolinValues().horizontalQ(horizontalQ).quartileKeys(quartileKeys).violinPointsExtractor(violinPointsExtractor).violinPointValueExtractor(violinPointValueExtractor); // augment valus

      violinValues = {};
      violinKeys.forEach(function (vk, i) {
        violinValues[vk] = calcValues(vk, data); // console.log(data, violinValues[vk] )
      });
      var numberOfObjects = violinKeys.length;

      var min = (_ref = []).concat.apply(_ref, _toConsumableArray$6(violinKeys.map(function (k, i) {
        return violinValues[k].quartiles[quartileKeys[0]];
      }))).filter(function (x) {
        return x !== undefined;
      });

      var max = (_ref2 = []).concat.apply(_ref2, _toConsumableArray$6(violinKeys.map(function (k, i) {
        return violinValues[k].quartiles[quartileKeys[quartileKeys.length - 1]];
      }))).filter(function (x) {
        return x !== undefined;
      });

      var extent = [Math.min.apply(Math, _toConsumableArray$6(min)) - domainPadding, Math.max.apply(Math, _toConsumableArray$6(max)) + domainPadding]; // console.log(min, max, extent, violinValues, ordered)
      // set the scale

      scale.domain(extent).range(horizontalQ ? [0, spaceY] : [0, spaceX]);
      var space = horizontalQ ? spaceX : spaceY; // calculate object size

      objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ); // calculate spacer size if needed

      spacerSize = utils.math.calculateWidthOfSpacer(ordered, space, objectSize, numberOfObjects, objectSpacer, overflowQ); // make the nested groups

      var spacerFunction = groupingSpacer().horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects).objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace(namespace); // move stuff

      spacerFunction(container, ordered, 0); // console.log(violinKeys, ordered, container.selectAll('g:not(.to-remove).'+objectClass).nodes())
      // for color function

      var parentIndexArray = [];
      container.selectAll('g:not(.to-remove).' + objectClass).each(function (d, i) {
        if (utils.arr.hasQ(violinKeys, d)) {
          parentIndexArray.push(Number(d3$1.select(this).attr('parent-index')));
        }
      }); // update color function

      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, Math.max.apply(Math, parentIndexArray)]) : colorFunction$$1.dataExtent(extent);
      /* violiin specific needs */

      var frequencyMax = Math.max.apply(Math, _toConsumableArray$6((_ref3 = []).concat.apply(_ref3, _toConsumableArray$6(violinKeys.map(function (k, i) {
        return d3$1.max(violinValues[k].frequencies);
      })))));
      var vScale = d3$1.scaleLinear().domain([0, frequencyMax]).range([0, objectSize / 2]);
      var lArea = d3$1.line().x(function (d, i) {
        return horizontalQ ? -vScale(d.x) : scale(d.x);
      }).y(function (d, i) {
        return horizontalQ ? scale(extent[1]) - scale(d.y) : -vScale(d.y);
      }).curve(d3$1.curveBasis);
      var rArea = d3$1.line().x(function (d, i) {
        return horizontalQ ? vScale(d.x) : scale(d.x);
      }).y(function (d, i) {
        return horizontalQ ? scale(extent[1]) - scale(d.y) : vScale(d.y);
      }).curve(d3$1.curveBasis);
      container.selectAll('g:not(.to-remove).' + objectClass).each(function (key, i) {
        var t = d3$1.select(this),
            currentData = data[key],
            currentViolinData = violinValues[key]; // needed because bug in selecting .to-remove

        if (!utils.arr.hasQ(violinKeys, key)) {
          return;
        }

        var i = t.attr('parent-index') == undefined ? i : t.attr('parent-index'),
            fillColor = colorFunction$$1(key, currentData, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(key, currentData, i, 'stroke'),
            area = utils.sel.safeSelect(t, 'g', 'area'),
            la = utils.sel.safeSelect(area, 'path', 'left'),
            ra = utils.sel.safeSelect(area, 'path', 'right'),
            quarts = utils.sel.safeSelect(t, 'g', 'quarts'),
            lq3 = utils.sel.safeSelect(quarts, 'line', 'q3'),
            lq1 = utils.sel.safeSelect(quarts, 'line', 'q1'),
            q3 = currentViolinData.quartiles[quartileKeys[3]],
            q2 = currentViolinData.quartiles[quartileKeys[2]],
            q1 = currentViolinData.quartiles[quartileKeys[1]];
        t.attr('transform', horizontalQ ? 'translate(' + objectSize / 2 + ',0)' : 'translate(0,' + objectSize / 2 + ')'); // draw curve

        la.transition().duration(transitionDuration).attr('d', function (dd, ii) {
          return lArea(currentViolinData.contour);
        }).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', objectStrokeWidth);
        ra.transition().duration(transitionDuration).attr('d', function (dd, ii) {
          return rArea(currentViolinData.contour);
        }).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', objectStrokeWidth);
        area.node().addEventListener('mouseover', function (dd, ii) {
          container.selectAll('g.' + objectClass).style('opacity', 0.2);
          t.style('opacity', 1);
          la.attr('stroke-width', objectStrokeWidth * 2);
          ra.attr('stroke-width', objectStrokeWidth * 2);
        });
        area.node().addEventListener('mouseout', function (dd, ii) {
          container.selectAll('g.' + objectClass).style('opacity', 1);
          la.attr('stroke-width', objectStrokeWidth);
          ra.attr('stroke-width', objectStrokeWidth);
        });

        if (pointsQ) {
          var ptsContainer = utils.sel.safeSelect(t, 'g', 'points');
          var pts = ptsContainer.selectAll('.point').data(currentViolinData.pointKeys);
          pts.on('mouseover', null);
          var ptsExit = pts.exit().transition().ease(easeFunc).duration(transitionDuration).attr('r', 0).attr('cy', horizontalQ ? scale(extent[1]) - scale(q2) : vScale(0)).attr('cx', horizontalQ ? vScale(0) : scale(q2)).remove();
          var ptsEnter = pts.enter().append('circle').attr('class', 'point').attr('r', 0).attr('cx', horizontalQ ? 0 : scale(q2)).attr('cy', horizontalQ ? scale(q2) : 0);
          pts = pts.merge(ptsEnter); // console.log(pointsTooltip.header())

          var pTTips = pointsTooltip.selection(pts).data(violinPointsExtractor(key, currentData));
          pTTips();
          var pMin = d3$1.min(currentViolinData.pointValues),
              pMax = d3$1.max(currentViolinData.pointValues);
          pts.transition().duration(transitionDuration).ease(easeFunc).attr('r', pointRadius).attr('cy', function (pointKey, ii) {
            var dd = currentViolinData.pointValues[ii];

            if (horizontalQ) {
              return scale(extent[1]) - scale(dd);
            }

            var j = utils.arr.whichBin(currentViolinData.binned, dd);
            var r = Math.random();
            var n = vScale(r * currentViolinData.frequencies[j] * 0.5);
            var k = Math.random() > 0.5 ? n : -n;
            return k;
          }).attr('cx', function (pointKey, ii) {
            var dd = currentViolinData.pointValues[ii];

            if (horizontalQ) {
              var j = utils.arr.whichBin(currentViolinData.binned, dd);
              var r = Math.random();
              var n = vScale(r * currentViolinData.frequencies[j] * 0.5);
              var k = Math.random() > 0.5 ? n : -n;
              return k;
            }

            return scale(dd);
          }).attr('stroke', function (dd, ii) {
            var dd = currentViolinData.pointValues[ii];
            return pointColorFunc(dd, 'stroke', strokeColor, pMin, pMax);
          }).attr('fill', function (dd, ii) {
            var dd = currentViolinData.pointValues[ii];
            return pointColorFunc(dd, 'fill', strokeColor, pMin, pMax);
          }).attr('stroke-width', pointStrokeWidth);
          ptsContainer.selectAll('circle.point').on('mouseover', function (dd, ii) {
            container.selectAll('g.' + objectClass).style('opacity', 0.2);
            t.style('opacity', 1);
            la.attr('stroke-width', objectStrokeWidth * 2);
            ra.attr('stroke-width', objectStrokeWidth * 2);
            container.selectAll('.point').style('opacity', 0.2);
            d3$1.select(this).style('opacity', 1).attr('r', pointRadius * 2).attr('stroke-width', pointStrokeWidth * 2);
          });
          ptsContainer.selectAll('circle.point').on('mouseout', function (dd, ii) {
            var e = document.createEvent('SVGEvents');
            e.initEvent('mouseout', true, true);
            area.node().dispatchEvent(e);
            container.selectAll('.point').style('opacity', 1);
            d3$1.select(this).attr('stroke-width', pointStrokeWidth).attr('r', pointRadius);
          });
        } else {
          cV.selectAll('.point').transition().duration(transitionDuration).ease(easeFunc).attr('r', 0).attr('cy', horizontalQ ? scale(extent[1]) - scale(q2) : vScale(0)).attr('cx', horizontalQ ? vScale(0) : scale(q2)).remove();
        }
      });
      tooltip$$1.selection(container.selectAll('g:not(.to-remove).' + objectClass + ' .area'));

      if (tooltip$$1.data() == undefined) {
        tooltip$$1.data(data);
      }

      tooltip$$1();

      if (tooltip$$1.values() == undefined) {
        tooltip$$1.values([function (currentKey, currentData, tooltipKey) {
          return violinValues[currentKey].quartiles[tooltipKey];
        }, function (currentKey, currentData, tooltipKey) {
          return violinValues[currentKey].quartiles[tooltipKey];
        }, function (currentKey, currentData, tooltipKey) {
          return violinValues[currentKey].quartiles[tooltipKey];
        }, function (currentKey, currentData, tooltipKey) {
          return violinValues[currentKey].quartiles[tooltipKey];
        }, function (currentKey, currentData, tooltipKey) {
          return violinValues[currentKey].quartiles[tooltipKey];
        }]);
      }
    }

    return violin;
  }
  /**
  * Produces the function which manipulates the violin data to have values needed
  * for rendering the violins as svg.
  * @returns {function} calculateViolinValues
  * @namespace neededViolinValues
  */

  function neededViolinValues() {
    var
    /**
    * Whether or not the orientation of the violins are horizontal
    * (see {@link violin#orient})
    * @param {Object} [horizontalQ=true]
    * @memberof neededViolinValues#
    * @property
    */
    horizontalQ = true,

    /**
    * Keys to be put into the utils.math.quartiles if they need to be calculated.
    * (see {@link violin#quartileKeys})
    * @param {Object} [quartileKeys=['Q0', 'Q1', 'Q2', 'Q3', 'Q4']]
    * @memberof neededViolinValues#
    * @property
    */
    quartileKeys = ['Q0', 'Q1', 'Q2', 'Q3', 'Q4'],

    /**
    * Function which given the key of the violin and that key's associated value
    * returns the object consiting of the points of the violin
    * (see {@link violin#violinPointsExtractor})
    * @param {Object} [violinPointsExtractor=function(violinKey, violinData) { return violinData.points }]
    * @memberof neededViolinValues#
    * @property
    */
    violinPointsExtractor,

    /**
    * Function which given the key of the current point and the object of points for the
    * violin, returns the numerical value of the point
    * (see {@link violin#violinPointValueExtractor})
    * @param {Object} [violinPointValueExtractor=function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }]
    * @memberof neededViolinValues#
    * @property
    */
    violinPointValueExtractor;
    /**
     * Gets / sets the horizontalQ
     * (see {@link violin#orient})
     * @param {boolean} [_=none]
     * @returns {calculateViolinValues | boolean}
     * @memberof calculateViolinValues
     * @property
     *
     * by default horizontalQ = true
     */

    calculateViolinValues.horizontalQ = function (_) {
      return arguments.length ? (horizontalQ = _, calculateViolinValues) : horizontalQ;
    };
    /**
     * Gets / sets the quartileKeys
     * (see {@link violin#quartileKeys})
     * @param {string[]} [_=none]
     * @returns {calculateViolinValues | string[]}
     * @memberof calculateViolinValues
     * @property
     *
     * by default quartileKeys = ["Q0","Q1","Q2","Q3","Q4"]
     */


    calculateViolinValues.quartileKeys = function (_) {
      return arguments.length ? (quartileKeys = _, calculateViolinValues) : quartileKeys;
    };
    /**
     * Gets / sets the violinPointsExtractor
     * (see {@link violin#violinPointsExtractor})
     * @param {function} [_=none]
     * @returns {calculateViolinValues | function}
     * @memberof calculateViolinValues
     * @property
     *
     * by default violinPointsExtractor = function(violinKey, violinData) { return violinData.points }
     */


    calculateViolinValues.violinPointsExtractor = function (_) {
      return arguments.length ? (violinPointsExtractor = _, calculateViolinValues) : violinPointsExtractor;
    };
    /**
     * Gets / sets the violinPointValueExtractor
     * (see {@link violin#violinPointValueExtractor})
     * @param {function} [_=none]
     * @returns {calculateViolinValues | function}
     * @memberof calculateViolinValues
     * @property
     *
     * by default violinPointValueExtractor = function(violinPointKey, violinPointData) { return violinPointData[violinPointKey].value }
     */


    calculateViolinValues.violinPointValueExtractor = function (_) {
      return arguments.length ? (violinPointValueExtractor = _, calculateViolinValues) : violinPointValueExtractor;
    };
    /**
    * The function produced by neededViolinValues.
    *
    * Adds the data need to render the violin as an svg
    * @param {string} violinKey the key of the current violin
    * @param {object} data the object consisting of violinKey - values (violin data) pairs
    * @returns {none} this function manipulates the passed data object adding the following keys
    *
    * data[violinKey].binned // the binned values of the points
    *
    * data[violinKey].frequencies // the list consisting of the length of each bin
    *
    * data[violinKey].contour // the points depicting the contour of 1/2 of the violin
    *
    * data[violinKey].utils.math.quartiles // the utils.math.quartiles of the points' values
    *
    * data[violinKey].pointKeys // the keys associated with the points
    *
    * data[violinKey].pointValues // the numerical values of the points
    *
    * @memberof neededViolinValues#
    * @property
    */


    function calculateViolinValues(violinKey, data) {
      // data for the current violin
      var violinData = {}; // Object.assign(violinData, data[violinKey])
      // console.log(data[violinKey], violinData)
      // the object of points

      var violinPoints = violinPointsExtractor(violinKey, data[violinKey]); //

      var violinPointsKeys = d3$1.keys(violinPoints); // the numerical values of those points

      var violinPointsValues = violinPointsKeys.map(function (pk, i) {
        return violinPointValueExtractor(pk, violinPoints);
      }); // utils.math.quartiles of those points

      var pointQuartiles = utils.math.quartiles(violinPointsValues, quartileKeys); // binned points

      var binned = d3$1.histogram()(violinPointsValues); // length of bins

      var frequencies = binned.map(function (bin) {
        return bin.length;
      }); // min and max countour points for nice drawings

      var minContourPoint = horizontalQ ? {
        x: 0,
        y: d3$1.min(violinPointsValues)
      } : {
        x: d3$1.min(violinPointsValues),
        y: 0
      };
      var maxContourPoint = horizontalQ ? {
        x: 0,
        y: d3$1.max(violinPointsValues)
      } : {
        x: d3$1.max(violinPointsValues),
        y: 0
      };
      var violinContourPoints = binned.map(function (bin, i) {
        return horizontalQ ? {
          y: bin.length ? d3$1.median(bin) : d3$1.median([bin.x0, bin.x1]),
          x: frequencies[i]
        } : {
          x: bin.length ? d3$1.median(bin) : d3$1.median([bin.x0, bin.x1]),
          y: frequencies[i]
        };
      }); // points along which to draw the violin shpe

      violinContourPoints = [minContourPoint].concat(violinContourPoints).concat([maxContourPoint]); // set data

      violinData.binned = binned;
      violinData.frequencies = frequencies;
      violinData.contour = violinContourPoints;
      violinData.quartiles = pointQuartiles;
      violinData.pointKeys = violinPointsKeys;
      violinData.pointValues = violinPointsValues;
      return violinData;
    }

    return calculateViolinValues;
  }

  function upset(selection) {
    var data,
        orient = 'horizontal',
        spaceX,
        spaceY,
        overflowQ = false,
        minObjectSize = 20,
        maxObjectSize = 50,
        circleStrokeWidth = 2,
        // colorFunction=
    backgroundFill = 'transparent',
        namespace = 'd3sm-upset',
        objectClass = 'upset',
        transitionDuration = 1000,
        easeFunc = d3$1.easeExp,
        setKey = "set",
        intersectionKey = "intersection",
        elementsKey = "elements",
        setExtractor = function setExtractor(key, i) {
      return data[key][setKey];
    },
        intersectionExtractor = function intersectionExtractor(key, i) {
      return data[key][intersectionKey];
    },
        elementExtractor = function elementExtractor(key, i) {
      return data[key][elementsKey];
    },
        cellKeys,
        setValues,
        intersectionValues,
        xObjectSpacer = 0.05,
        yObjectSpacer = 0.05,
        radius,
        // listDelim = ';'
    yObjectSize,
        ySpacerSize,
        xObjectSize,
        xSpacerSize,
        setKeySortingFunction = function setKeySortingFunction(a, b) {
      return setValues.indexOf(setExtractor(a)) - setValues.indexOf(setExtractor(b));
    },
        intersectionKeySortingFunction = function intersectionKeySortingFunction(a, b) {
      return intersectionValues.indexOf(intersectionExtractor(a)) - intersectionValues.indexOf(intersectionExtractor(b));
    };

    upset.selection = function (_) {
      return arguments.length ? (selection = _, upset) : selection;
    };

    upset.data = function (_) {
      return arguments.length ? (data = _, upset) : data;
    };

    upset.orient = function (_) {
      return arguments.length ? (orient = _, upset) : orient;
    };

    upset.spaceX = function (_) {
      return arguments.length ? (spaceX = _, upset) : spaceX;
    };

    upset.spaceY = function (_) {
      return arguments.length ? (spaceY = _, upset) : spaceY;
    };

    upset.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, upset) : overflowQ;
    };

    upset.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, upset) : minObjectSize;
    };

    upset.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, upset) : maxObjectSize;
    };

    upset.circleStrokeWidth = function (_) {
      return arguments.length ? (circleStrokeWidth = _, upset) : circleStrokeWidth;
    };

    upset.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, upset) : backgroundFill;
    };

    upset.namespace = function (_) {
      return arguments.length ? (namespace = _, upset) : namespace;
    };

    upset.objectClass = function (_) {
      return arguments.length ? (objectClass = _, upset) : objectClass;
    };

    upset.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, upset) : transitionDuration;
    };

    upset.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, upset) : easeFunc;
    };

    upset.cellKeys = function (_) {
      return arguments.length ? (cellKeys = _, upset) : cellKeys;
    };

    upset.setValues = function (_) {
      return arguments.length ? (setValues = _, upset) : setValues;
    };

    upset.intersectionValues = function (_) {
      return arguments.length ? (intersectionValues = _, upset) : intersectionValues;
    };

    upset.xObjectSpacer = function (_) {
      return arguments.length ? (xObjectSpacer = _, upset) : xObjectSpacer;
    };

    upset.yObjectSpacer = function (_) {
      return arguments.length ? (yObjectSpacer = _, upset) : yObjectSpacer;
    };

    upset.radius = function (_) {
      return arguments.length ? (radius = _, upset) : radius;
    };

    upset.setExtractor = function (_) {
      return arguments.length ? (setExtractor = _, upset) : setExtractor;
    };

    upset.intersectionExtractor = function (_) {
      return arguments.length ? (intersectionExtractor = _, upset) : intersectionExtractor;
    };

    upset.elementExtractor = function (_) {
      return arguments.length ? (elementExtractor = _, upset) : elementExtractor;
    };

    upset.setKeySortingFunction = function (_) {
      return arguments.length ? (setKeySortingFunction = _, upset) : setKeySortingFunction;
    };

    upset.intersectionKeySortingFunction = function (_) {
      return arguments.length ? (intersectionKeySortingFunction = _, upset) : intersectionKeySortingFunction;
    };

    upset.yObjectSize = function (_) {
      return arguments.length ? (yObjectSize = _, upset) : yObjectSize;
    };

    upset.ySpacerSize = function (_) {
      return arguments.length ? (ySpacerSize = _, upset) : ySpacerSize;
    };

    upset.xObjectSize = function (_) {
      return arguments.length ? (xObjectSize = _, upset) : xObjectSize;
    };

    upset.xSpacerSize = function (_) {
      return arguments.length ? (xSpacerSize = _, upset) : xSpacerSize;
    };

    function upset() {
      // for convenience in handling orientation specific values
      var horizontalQ = orient == 'horizontal' ? true : false;
      var verticalQ = !horizontalQ; // background cliping rectangle

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      cellKeys = d3$1.keys(data);
      setValues = utils.arr.unique(cellKeys.map(setExtractor)).sort();
      intersectionValues = utils.arr.unique(cellKeys.map(intersectionExtractor)).sort().sort(function (a, b) {
        return a.split(';').length - b.split(';').length;
      });

      if (!horizontalQ) {
        cellKeys.sort(function (a, b) {
          return setKeySortingFunction(a, b) || intersectionKeySortingFunction(a, b);
        });
      } else {
        cellKeys.sort(function (a, b) {
          return intersectionKeySortingFunction(a, b) || setKeySortingFunction(a, b);
        });
      }

      var xValues = horizontalQ ? intersectionValues : setValues,
          yValues = horizontalQ ? setValues : intersectionValues,
          xDim = horizontalQ ? xValues.length : yValues.length,
          yDim = horizontalQ ? yValues.length : xValues.length; // console.utils.con.log(xValues, yValues)

      xObjectSize = utils.math.calculateWidthOfObject(spaceX, xDim, minObjectSize, maxObjectSize, xObjectSpacer, overflowQ);
      yObjectSize = utils.math.calculateWidthOfObject(spaceY, yDim, minObjectSize, maxObjectSize, yObjectSpacer, overflowQ);
      xSpacerSize = utils.math.calculateWidthOfSpacer(xValues, spaceX, xObjectSize, xDim, xObjectSpacer, overflowQ);
      ySpacerSize = utils.math.calculateWidthOfSpacer(yValues, spaceY, yObjectSize, yDim, yObjectSpacer, overflowQ);
      var ySpacer = groupingSpacer().horizontalQ(false).moveby('category').numberOfObjects(yDim).objectSize(yObjectSize).spacerSize(ySpacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc);
      var xSpacer = groupingSpacer().horizontalQ(true).moveby('category').numberOfObjects(xDim).objectClass(objectClass).objectSize(xObjectSize).spacerSize(xSpacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc);

      if (verticalQ) {
        xSpacer.objectClass(objectClass);
        ySpacer.namespace('across').objectClass(utils.str.hypenate(objectClass, 'across'));
        ySpacer(container, yValues, 0);
        container.selectAll('g.' + utils.str.hypenate(objectClass, 'across')).each(function (d, i) {
          xSpacer(d3$1.select(this), xValues, 0);
        });
      } else {
        xSpacer.namespace('across').objectClass(utils.str.hypenate(objectClass, 'across'));
        ySpacer.objectClass(objectClass);
        xSpacer(container, xValues, 0);
        container.selectAll('g.' + utils.str.hypenate(objectClass, 'across')).each(function (d, i) {
          ySpacer(d3$1.select(this), yValues, 0);
        });
      }

      var cells = container.selectAll('g:not(.to-remove).' + objectClass);
      var lookup = {};
      cellKeys.map(function (k, i) {
        lookup[setExtractor(k) + '::' + intersectionExtractor(k)] = k;
      }); // var positionedCellKeys = []
      // for (var i = 0; i < setValues.length; i++) {
      //   for (var j = 0; j < intersectionValues.length; j++) {
      //     var lookupValue = lookup[setValues[j]+"::"+intersectionValues[i]]
      //     if (lookupValue == undefined) {
      //       positionedCellKeys.push(undefined)
      //     } else {
      //       positionedCellKeys.push(lookupValue)
      //     }
      //     console.utils.con.log(i, j, lookupValue)
      //   }
      // }
      //
      // // console.utils.con.log(positionedCellKeys)
      //
      // cells.data(positionedCellKeys);

      cells.data(cellKeys);
      cells.each(function (key, i) {
        var t = d3$1.select(this);

        if (key == undefined) {
          return;
        }

        var currentData = data[key]; // console.utils.con.log(key, currentData)

        var set = setExtractor(key, i),
            intersection = intersectionExtractor(key, i); // console.utils.con.log(set, intersection)

        t.classed(intersection, true);
        t.classed(set, true);
        var c = utils.sel.safeSelect(t, 'circle', utils.str.hypenate(objectClass, 'circle'));
        c.attr('cx', xObjectSize / 2).attr('cy', yObjectSize / 2).attr('r', radius == undefined ? Math.min(xObjectSize, yObjectSize) / 2 : radius).attr('fill', intersection.includes(set) ? "black" : 'rgb(233,233,233)').attr('stroke', "black").attr("in-intersection", intersection.includes(set));
      });
    }

    function intersectionTotals() {
      var totals = {}; // intersectionValues.sort(function(a, b){ return intersectionKeySortingFunction(a, b) })

      intersectionValues.map(function (k, i) {
        totals[k] = {
          'total': 0
        };
      });
      cellKeys.map(function (k, i) {
        var e = elementExtractor(k, i);

        if (totals[intersectionExtractor(k, i)]['total'] == 0) {
          if (Array.isArray(e)) {
            totals[intersectionExtractor(k, i)]['total'] += e.length;
            totals[intersectionExtractor(k, i)]['values'] = e;
          } else {
            totals[intersectionExtractor(k, i)]['total'] += e;
          }
        }
      });
      return totals;
    }

    function setTotals() {
      var totals = {}; // intersectionValues.sort(function(a, b){ return intersectionKeySortingFunction(a, b) })

      setValues.map(function (k, i) {
        totals[k] = {
          'total': 0
        };
      });
      cellKeys.map(function (k, i) {
        var e = elementExtractor(k, i);

        if (Array.isArray(e)) {
          totals[setExtractor(k, i)]['total'] += e.length;
        } else {
          totals[setExtractor(k, i)]['total'] += e;
        }
      });
      return totals;
    }

    upset.intersectionTotals = intersectionTotals;
    upset.setTotals = setTotals;
    return upset;
  }

  function _toConsumableArray$7(arr$$1) { return _arrayWithoutHoles$7(arr$$1) || _iterableToArray$7(arr$$1) || _nonIterableSpread$7(); }

  function _nonIterableSpread$7() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

  function _iterableToArray$7(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

  function _arrayWithoutHoles$7(arr$$1) { if (Array.isArray(arr$$1)) { for (var i = 0, arr2 = new Array(arr$$1.length); i < arr$$1.length; i++) { arr2[i] = arr$$1[i]; } return arr2; } }
  function starmap(selection) {
    var
    /*
    {
      dataset: {
        element: {
          tooltip: elementName,
          value: ###
        }
      }
    }
    */
    data,
        spaceX,
        spaceY,
        // [datasetNames,...]
    dataKeys,
        // [[elementName,...],...]
    dataValues,
        elementValues,
        elementExtractor = function elementExtractor(dataset, index) {
      return Object.keys(dataset);
    },
        valueExtractor = function valueExtractor(element, index) {
      return element;
    },
        namespace = 'd3sm-starmap',
        backgroundFill = 'transparent',
        domainPadding = 0.5,
        objectClass = 'star-polygon',
        radiusPercent = 0.8,
        radius,
        scale = d3$1.scaleLinear(),
        transitionDuration = 1000,
        numberOfCircles = 5,
        colorFunction$$1 = colorFunction(),
        tooltip$$1 = tooltip(),
        easeFunc = d3$1.easeSinIn,
        shapeStrokeWidth = 1,
        shapeOpacity = 0.8,
        labelPadding = 10;

    starmap.data = function (_) {
      return arguments.length ? (data = _, starmap) : data;
    };

    starmap.spaceX = function (_) {
      return arguments.length ? (spaceX = _, starmap) : spaceX;
    };

    starmap.spaceY = function (_) {
      return arguments.length ? (spaceY = _, starmap) : spaceY;
    };

    starmap.dataKeys = function (_) {
      return arguments.length ? (dataKeys = _, starmap) : dataKeys;
    };

    starmap.dataValues = function (_) {
      return arguments.length ? (dataValues = _, starmap) : dataValues;
    };

    starmap.elementValues = function (_) {
      return arguments.length ? (elementValues = _, starmap) : elementValues;
    };

    starmap.elementExtractor = function (_) {
      return arguments.length ? (elementExtractor = _, starmap) : elementExtractor;
    };

    starmap.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, starmap) : valueExtractor;
    };

    starmap.namespace = function (_) {
      return arguments.length ? (namespace = _, starmap) : namespace;
    };

    starmap.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, starmap) : backgroundFill;
    };

    starmap.domainPadding = function (_) {
      return arguments.length ? (domainPadding = _, starmap) : domainPadding;
    };

    starmap.objectClass = function (_) {
      return arguments.length ? (objectClass = _, starmap) : objectClass;
    };

    starmap.radiusPercent = function (_) {
      return arguments.length ? (radiusPercent = _, starmap) : radiusPercent;
    };

    starmap.radius = function (_) {
      return arguments.length ? (radius = _, starmap) : radius;
    };

    starmap.scale = function (_) {
      return arguments.length ? (scale = _, starmap) : scale;
    };

    starmap.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, starmap) : transitionDuration;
    };

    starmap.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, starmap) : colorFunction$$1;
    };

    starmap.tooltip = function (_) {
      return arguments.length ? (tooltip$$1 = _, starmap) : tooltip$$1;
    };

    starmap.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, starmap) : easeFunc;
    };

    function starmap() {
      // set up container
      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill); // circles fit in a square

      var minDim = Math.min.apply(Math, [spaceX, spaceY]);
      var dimDif = {
        x: spaceX - minDim,
        y: spaceY - minDim
      };
      radius = radiusPercent * minDim / 2;
      var center = {
        x: minDim / 2,
        y: minDim / 2 // shift container to compensate for uneven spacing

      };
      container.attr('transform', "translate(".concat(dimDif.x / 2, ",").concat(dimDif.y / 2, ")")); // add background container for circles, spokes and text

      var radialBackgroundContainer = container.select('g.radial-background-container');

      if (radialBackgroundContainer.empty()) {
        radialBackgroundContainer = container.append('g').attr('class', 'radial-background-container');
      } // these are the background circles


      radialBackgroundContainer.selectAll('circle').data(Array(numberOfCircles)).enter().append('circle').attr('r', 0).merge(radialBackgroundContainer.selectAll('circle')).attr('fill', 'transparent').attr('stroke', 'black').attr('opacity', '1').attr("cx", center.x).attr("cy", center.y).transition().duration(transitionDuration).ease(easeFunc).attr('r', function (d, i) {
        return radius * (i + 1) / numberOfCircles;
      });
      radialBackgroundContainer.selectAll('circle').exit().remove(); // keys of datasets

      dataKeys = d3$1.keys(data); // names of all the elements that appear in the datasets

      elementValues = []; // the values for each

      dataValues = dataKeys.map(function (ds, i) {
        var elementKeys = elementExtractor(data[ds], i);
        return elementKeys.map(function (e, j) {
          if (elementValues.indexOf(e) < 0) elementValues.push(e);
          return valueExtractor(data[ds][e], j);
        });
      });
      var numberOfKeys = dataKeys.length;
      var extent = [Math.min.apply(Math, _toConsumableArray$7(utils.arr.flatten(dataValues))) - domainPadding, Math.max.apply(Math, _toConsumableArray$7(utils.arr.flatten(dataValues))) + domainPadding]; // radius scale

      scale.domain(extent); //.range([0, radius])
      // now can make spokes since we how many we need

      radialBackgroundContainer.selectAll("line.".concat(namespace, "-spoke")).data(elementValues).enter().append('line').attr('class', "".concat(namespace, "-spoke")).merge(radialBackgroundContainer.selectAll("line.".concat(namespace, "-spoke"))).attr("x1", 0).attr("y1", 0).attr("x2", 0).attr("y2", 0).attr('stroke', 'black').attr('stroke-width', 1).attr('transform', "translate(".concat(center.x, ",").concat(center.y, ")")).transition().duration(transitionDuration).ease(easeFunc).attr("x2", function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1);
        return polarToCartesian(0, 0, radius, degrees).x;
      }).attr("y2", function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1);
        return polarToCartesian(0, 0, radius, degrees).y;
      }); // .attr('transform',(d,i)=>{
      //   let degrees = (360 / elementValues.length ) * (i + 1)
      //   let ptc = polarToCartesian(0, 0, 0, degrees)
      //   return `translate(${center.x},${center.y})`
      // })

      radialBackgroundContainer.selectAll("line.".concat(namespace, "-spoke")).exit().remove(); // add labels to spokes

      radialBackgroundContainer.selectAll("text.".concat(namespace, "-spoke-label")).data(elementValues).enter().append('text').attr('class', "".concat(namespace, "-spoke-label")).attr('opacity', 0).attr('x', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1); // r = radius + config.background["accent-bubbles"].spacing

        return polarToCartesian(0, 0, 0, degrees).x;
      }).attr('y', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1); // r = radius + config.background["accent-bubbles"].spacing

        return polarToCartesian(0, 0, 0, degrees).y;
      }).attr('transform', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1);
        var ptc = polarToCartesian(0, 0, 0, degrees);
        var j = -1;
        if (i > elementValues.length / 2 - 1) j *= -1;
        return "translate(".concat(center.x, ",").concat(center.y, ") rotate(").concat(j * 90 + degrees, ", ").concat(ptc.x, ", ").concat(ptc.y, ")");
      }).merge(radialBackgroundContainer.selectAll("text.".concat(namespace, "-spoke-label"))).text(function (d) {
        return d;
      }).transition().duration(transitionDuration).ease(easeFunc).attr('x', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1); // r = radius + config.background["accent-bubbles"].spacing

        return polarToCartesian(0, 0, radius + labelPadding, degrees).x;
      }).attr('y', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1); // r = radius + config.background["accent-bubbles"].spacing

        return polarToCartesian(0, 0, radius + labelPadding, degrees).y;
      }).attr('transform', function (d, i) {
        var degrees = 360 / elementValues.length * (i + 1);
        var ptc = polarToCartesian(0, 0, radius + labelPadding, degrees);
        var j = -1;
        if (i > elementValues.length / 2 - 1) j *= -1;
        return "translate(".concat(center.x, ",").concat(center.y, ") rotate(").concat(j * 90 + degrees, ", ").concat(ptc.x, ", ").concat(ptc.y, ")");
      }).attr('text-anchor', function (d, i) {
        return i > elementValues.length / 2 ? 'end' : 'start';
      }).attr('opacity', 1);
      radialBackgroundContainer.selectAll("".concat(namespace, "-spoke-label")).exit().remove(); // background set up, now onto shapes

      colorFunction$$1 = colorFunction$$1.colorBy() == 'index' ? colorFunction$$1.dataExtent([0, elementValues.length]) : colorFunction$$1.dataExtent(extent);
      var selc = container.selectAll("g.".concat(namespace)).data(dataKeys).enter().append('g').attr('class', namespace).merge(container.selectAll("g.".concat(namespace)));
      var exit = selc.exit().remove();
      selc.each(function (k, i) {
        var dthis = d3$1.select(this);
        var polygonPoints = [];
        var currentData = data[k];
        var currentValues = dataValues[dataKeys.indexOf(k)];
        var currentElementKeys = elementExtractor(currentData, i);
        var fillColor = colorFunction$$1(k, currentValues, i, 'fill');
        var strokeColor = colorFunction$$1(k, currentValues, i, 'stroke');
        elementValues.forEach(function (e, j) {
          var r = scale(radius * valueExtractor(currentData[e], currentElementKeys.indexOf(j)));
          var point = currentElementKeys.indexOf(e) === -1 ? center : polarToCartesian(center.x, center.y, r, 360 / elementValues.length * (j + 1));
          polygonPoints.push(point);
        });
        var line = d3$1.line().curve(d3$1.curveCardinal).x(function (d) {
          return d.x;
        }).y(function (d) {
          return d.y;
        });
        var poly = dthis.selectAll("path.".concat(objectClass)).data([polygonPoints]);
        var pentr = poly.enter().append('path').attr('class', objectClass).attr('d', function (d) {
          return line(d.map(function (dd) {
            return center;
          }));
        });
        var pexit = poly.exit().remove();
        poly = poly.merge(pentr).transition().ease(easeFunc).duration(transitionDuration).attr('d', function (d) {
          return line(d);
        }).attr("stroke", strokeColor).attr("stroke-width", shapeStrokeWidth).attr("fill", fillColor).attr('opacity', shapeOpacity);
      });
      d3$1.selectAll("path.".concat(objectClass)).on('mouseover', function (d, j) {
        d3$1.select(this).transition() // .ease(easeFunc)
        // .duration(transitionDuration)
        .attr('opacity', 1).attr("stroke-width", shapeStrokeWidth * 2);
      }).on('mouseleave', function (d, j) {
        d3$1.select(this).transition() // .ease(easeFunc)
        // .duration(transitionDuration)
        .attr('opacity', shapeOpacity).attr("stroke-width", shapeStrokeWidth);
      });
      tooltip$$1.selection(container.selectAll("g.".concat(namespace))).data(data);
      tooltip$$1();
    }

    return starmap;
  } // Short cut for converting

  function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
    return {
      x: centerX + radius * Math.cos(angleInRadians),
      y: centerY + radius * Math.sin(angleInRadians)
    };
  }

  var charts = {
    scatter: scatter,
    bar: bar,
    bubble: bubbleHeatmap,
    boxwhisker: boxwhisker,
    heatmap: heatmap,
    violin: violin,
    neededViolinValues: neededViolinValues,
    upset: upset,
    starmap: starmap,
    polarToCartesian: polarToCartesian
  };

  function categoricLegend(selection) {
    var categories,

    /**
    * Data to plot. Assumed to be a object, where each key corresponds to a legend
    * (see {@link legend#data})
    * @param {Object} [data=undefined]
    * @memberof legend#
    * @property
    */
    data,

    /**
    * Which direction to render the bars in
    * (see {@link legend#orient})
    * @param {number} [orient='horizontal']
    * @memberof legend#
    * @property
    */
    orient = 'horizontal',

    /**
    * Amount of horizontal space (in pixels) avaible to render the legend in
    * (see {@link legend#spaceX})
    * @param {number} [spaceX=undefined]
    * @memberof legend#
    * @property
    */
    spaceX,

    /**
    * Amount of vertical space (in pixels) avaible to render the legend in
    * (see {@link legend.spaceY})
    * @param {number} [spaceY=undefined]
    * @memberof legend#
    * @property
    */
    spaceY,

    /**
    * Whether or not to allow legend to render elements pass the main spatial dimension
    * given the orientation (see {@link legend#orient}), where {@link legend#orient}="horizontal"
    * the main dimension is {@link legend#spaceX} and where {@link legend#orient}="vertical"
    * the main dimension is {@link legend#spaceY}
    * @param {boolean} [overflowQ=false]
    * @memberof legend#
    * @property
    */
    overflowQ = false,

    /**
    * An array - putatively of other arrays - depicting how bars should be arranged
    * @param {Array[]} [grouping=undefined]
    * @memberof legend#
    * @property
    */
    grouping,

    /**
    * How to get the value of the legend
    * @param {function} [valueExtractor=function(key, index) { return data[key] }]
    * @memberof legend#
    * @property
    */
    valueExtractor = function valueExtractor(key, index) {
      return data[key];
    },

    /**
    * How to sort the bars - if {@link bar#grouping} is not provided.
    * @param {function} [sortingFunction=function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])}]
    * @memberof bar#
    * @property
    */
    sortingFunction = function sortingFunction(keyA, keyB) {
      return d3$1.ascending(keyA, keyB);
    },

    /**
    * Default space for the spacer (percentage) of main dimension given the orientation
    * (see {@link legend#orient}), where {@link legend#orient}="horizontal"
    * the main dimension is {@link legend#spaceX} and where {@link legend#orient}="vertical"
    * the main dimension is {@link legend#spaceY} between bars
    * @param {number} [objectSpacer=0.05]
    * @memberof legend#
    * @property
    */
    objectSpacer = 0.05,

    /**
    * The minimum size that an object can be
    * @param {number} [minObjectSize=50]
    * @memberof legend#
    * @property
    */
    minObjectSize = 10,

    /**
    * The maximum size that an object can be
    * @param {number} [maxObjectSize=100]
    * @memberof legend#
    * @property
    */
    maxObjectSize = 100,

    /**
    * The stroke width of the bars
    * @param {number} [barStrokeWidth=2]
    * @memberof legend#
    * @property
    */
    bubbleStrokeWidth = 2,

    /**
    * Instance of ColorFunction
    * @param {function} [colorFunction = colorFunction()]
    * @memberof legend#
    * @property
    */
    colorFunction$$1 = colorFunction(),

    /**
    * Color of the background
    * @param {string} [backgroundFill="transparent"]
    * @memberof legend#
    * @property
    */
    backgroundFill = 'transparent',

    /**
    * Namespace for all items made by this instance of legend
    * @param {string} [namespace="d3sm-legend"]
    * @memberof legend#
    * @property
    */
    namespace = 'd3sm-legend',

    /**
    * Class name for legend container (<g> element)
    * @param {string} [objectClass="legend"]
    * @memberof legend#
    * @property
    */
    objectClass = 'legend',

    /**
    * Duration of all transitions of this element
    * @param {number} [transitionDuration=1000]
    * @memberof legend#
    * @property
    */
    transitionDuration = 1000,

    /**
    * Easing function for transitions
    * @param {d3.ease} [easeFunc=d3.easeExp]
    * @memberof legend#
    * @property
    */
    easeFunc = d3$1.easeExp;

    legend.categories = function (_) {
      return arguments.length ? (categories = _, legend) : categories;
    };
    /**
     * Gets or sets the selection in which items are manipulated
     * @param {d3.selection} [_=none]
     * @returns {legend | d3.selection}
     * @memberof legend
     * @property
     * by default selection = selection
     */


    legend.selection = function (_) {
      return arguments.length ? (selection = _, legend) : selection;
    };
    /**
     * Gets or sets the data
     * (see {@link legend#data})
     * @param {number} [_=none]
     * @returns {legend | object}
     * @memberof legend
     * @property
     */


    legend.data = function (_) {
      return arguments.length ? (data = _, legend) : data;
    };
    /**
     * Gets or sets the orient of the bars
     * (see {@link legend#orient})
     * @param {number} [_=none]
     * @returns {legend | object}
     * @memberof legend
     * @property
     */


    legend.orient = function (_) {
      return arguments.length ? (orient = _, legend) : orient;
    };
    /**
     * Gets or sets the amount of horizontal space in which items are manipulated
     * (see {@link legend#spaceX})
     * @param {number} [_=none] should be a number > 0
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default spaceX = undefined
     */


    legend.spaceX = function (_) {
      return arguments.length ? (spaceX = _, legend) : spaceX;
    };
    /**
     * Gets or sets the amount of vertical space in which items are manipulated
     * (see {@link legend#spaceY})
     * @param {number} [_=none] should be a number > 0
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default spaceY = undefined
     */


    legend.spaceY = function (_) {
      return arguments.length ? (spaceY = _, legend) : spaceY;
    };
    /**
     * Gets / sets whether or not legend is allowed to go beyond specified dimensions
     * (see {@link legend#spaceX})
     * @param {boolean} [_=none]
     * @returns {legend | boolean}
     * @memberof legend
     * @property
     * by default overflowQ = false
     */


    legend.overflowQ = function (_) {
      return arguments.length ? (overflowQ = _, legend) : overflowQ;
    };
    /**
     * Gets / sets the grouping of the bars
     * (see {@link legend#grouping})
     * @param {Array[]} [_=none]
     * @returns {legend | Array[]}
     * @memberof legend
     * @property
     * by default grouping = undefined
     */


    legend.grouping = function (_) {
      return arguments.length ? (grouping = _, legend) : grouping;
    };
    /**
     * Gets / sets the valueExtractor
     * (see {@link legend#valueExtractor})
     * @param {function} [_=none]
     * @returns {legend | function}
     * @memberof legend
     * @property
     * by default valueExtractor = function(key, index) { return data[key] },
     */


    legend.valueExtractor = function (_) {
      return arguments.length ? (valueExtractor = _, legend) : valueExtractor;
    };
    /**
     * Gets / sets the sortingFunction
     * (see {@link bar#sortingFunction})
     * @param {function} [_=none]
     * @returns {bar | function}
     * @memberof bar
     * @property
     * by default sortingFunction = function(keyA, keyB) {return d3.descending(data[keyA], data[keyB])},
     */


    legend.sortingFunction = function (_) {
      return arguments.length ? (sortingFunction = _, legend) : sortingFunction;
    };
    /**
     * Gets / sets objectSpacer
     * (see {@link legend#objectSpacer})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default objectSpacer = 0.05
     */


    legend.objectSpacer = function (_) {
      return arguments.length ? (objectSpacer = _, legend) : objectSpacer;
    };
    /**
     * Gets / sets the minObjectSize
     * (see {@link legend#minObjectSize})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default minObjectSize = 50
     */


    legend.minObjectSize = function (_) {
      return arguments.length ? (minObjectSize = _, legend) : minObjectSize;
    };
    /**
     * Gets / sets the maxObjectSize
     * (see {@link legend#maxObjectSize})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default maxObjectSize = 100
     */


    legend.maxObjectSize = function (_) {
      return arguments.length ? (maxObjectSize = _, legend) : maxObjectSize;
    };
    /**
     * Gets / sets the barStrokeWidth
     * (see {@link legend#barStrokeWidth})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default barStrokeWidth = 2
     */


    legend.bubbleStrokeWidth = function (_) {
      return arguments.length ? (bubbleStrokeWidth = _, legend) : bubbleStrokeWidth;
    };
    /**
     * Gets / sets the colorFunction
     * (see {@link legend#colorFunction})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default colorFunction = colorFunction()
     */


    legend.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, legend) : colorFunction$$1;
    };
    /**
     * Gets / sets the backgroundFill
     * (see {@link legend#backgroundFill})
     * @param {string} [_=none]
     * @returns {legend | string}
     * @memberof legend
     * @property
     * by default backgroundFill = 'transparent'
     */


    legend.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, legend) : backgroundFill;
    };
    /**
     * Gets / sets the namespace
     * (see {@link legend#namespace})
     * @param {string} [_=none]
     * @returns {legend | string}
     * @memberof legend
     * @property
     * by default namespace = 'd3sm-legend'
     */


    legend.namespace = function (_) {
      return arguments.length ? (namespace = _, legend) : namespace;
    };
    /**
     * Gets / sets the objectClass
     * (see {@link legend#objectClass})
     * @param {string} [_=none]
     * @returns {legend | string}
     * @memberof legend
     * @property
     * by default objectClass = 'tick-group'
     */


    legend.objectClass = function (_) {
      return arguments.length ? (objectClass = _, legend) : objectClass;
    };
    /**
     * Gets / sets the transitionDuration
     * (see {@link legend#transitionDuration})
     * @param {number} [_=none]
     * @returns {legend | number}
     * @memberof legend
     * @property
     * by default transitionDuration = 1000
     */


    legend.transitionDuration = function (_) {
      return arguments.length ? (transitionDuration = _, legend) : transitionDuration;
    };
    /**
     * Gets / sets the easeFunc
     * (see {@link legend#easeFunc})
     * @param {d3.ease} [_=none]
     * @returns {legend | d3.ease}
     * @memberof legend
     * @property
     * by default easeFunc = d3.easeExp
     */


    legend.easeFunc = function (_) {
      return arguments.length ? (easeFunc = _, legend) : easeFunc;
    };

    function legend() {
      var horizontalQ = orient == 'horizontal' ? true : false;
      var verticalQ = !horizontalQ; // background cliping rectangle

      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      colorFunction$$1.dataExtent([0, categories.length - 1]).colorBy('categories').categoryExtractor(function (k, v, i) {
        return v;
      });
      var r = Math.min(spaceX, spaceY) / 2;
      var numberOfObjects = categories.length; // if grouping is undefined sort barKeys by sortingFunction

      var ordered = grouping == undefined ? categories.sort(sortingFunction) : grouping; // ordered might be nested depending on grouping

      var catKeys = utils.arr.flatten(ordered);
      var space = horizontalQ ? spaceX : spaceY; // calculate object size

      var objectSize = utils.math.calculateWidthOfObject(space, numberOfObjects, minObjectSize, maxObjectSize, objectSpacer, overflowQ); // calculate spacer size if needed

      var spacerSize = utils.math.calculateWidthOfSpacer(catKeys, space, objectSize, numberOfObjects, objectSpacer, overflowQ); // make the nested groups

      var spacerFunction = groupingSpacer().horizontalQ(horizontalQ).scale(scale).moveby('category').numberOfObjects(numberOfObjects).objectClass(objectClass).objectSize(objectSize).spacerSize(spacerSize).transitionDuration(transitionDuration).easeFunc(easeFunc).namespace(namespace);
      spacerFunction(container, ordered, 0);
      var r = Math.min(objectSize, spaceX, spaceY) / 2 - bubbleStrokeWidth;
      container.selectAll('g:not(.to-remove).' + objectClass).each(function (cat, i) {
        var t = d3$1.select(this);
        var c = utils.sel.safeSelect(t, 'circle');
        var fillColor = colorFunction$$1(undefined, cat, i, 'fill'),
            // prevent duplicate computation
        strokeColor = colorFunction$$1(undefined, cat, i, 'stroke');
        var cx = horizontalQ ? r + bubbleStrokeWidth : (spaceX - r * 2) / 2 + r;
        var cy = verticalQ ? r + bubbleStrokeWidth : (spaceX - r * 2) / 2 + r;
        c.attr("r", r).attr('cx', cx).attr('cy', cy).attr('fill', fillColor).attr('stroke', strokeColor).attr('stroke-width', bubbleStrokeWidth);
        var text = utils.sel.safeSelect(t, 'text');
        text.text(cat).attr('text-anchor', 'middle').attr("transform", function (d, i) {
          var x = cx,
              y = cy + text.node().getBoundingClientRect().height / 4,
              t = 'translate(' + x + ',' + y + ')';
          return t;
        });
      });
    }

    return legend;
  }

  function numericLegend(selection) {
    var min = 0,
        max = 1,
        spaceX,
        spaceY,
        colorFunction$$1 = colorFunction(),
        namespace = 'd3sm-linear-vertical-gradient',
        fontSize = 12,
        backgroundFill = 'transparent',
        textColor = 'black',
        roundTo = 2;

    legend.min = function (_) {
      return arguments.length ? (min = _, legend) : min;
    };

    legend.max = function (_) {
      return arguments.length ? (max = _, legend) : max;
    };

    legend.spaceX = function (_) {
      return arguments.length ? (spaceX = _, legend) : spaceX;
    };

    legend.spaceY = function (_) {
      return arguments.length ? (spaceY = _, legend) : spaceY;
    };

    legend.namespace = function (_) {
      return arguments.length ? (namespace = _, legend) : namespace;
    };

    legend.fontSize = function (_) {
      return arguments.length ? (fontSize = _, legend) : fontSize;
    };

    legend.backgroundFill = function (_) {
      return arguments.length ? (backgroundFill = _, legend) : backgroundFill;
    };

    legend.colorFunction = function (_) {
      return arguments.length ? (colorFunction$$1 = _, legend) : colorFunction$$1;
    };

    legend.textColor = function (_) {
      return arguments.length ? (textColor = _, legend) : textColor;
    };

    legend.roundTo = function (_) {
      return arguments.length ? (roundTo = _, legend) : roundTo;
    };

    function legend() {
      // background cliping rectangle
      var bgcpRect = {
        x: 0,
        y: 0,
        width: spaceX,
        height: spaceY
      };
      var container = utils.sel.setupContainer(selection, namespace, bgcpRect, backgroundFill);
      var defs = utils.sel.safeSelect(selection, 'defs');
      var linearGradient = utils.sel.safeSelect(defs, 'linearGradient').attr("x1", "0%").attr("y1", "100%").attr("x2", "0%").attr("y2", "0%").attr('id', utils.str.hypenate(namespace, 'numerical-legend-gradient'));
      colorFunction$$1.dataExtent([min, max]).colorBy('value').valueExtractor(function (k, v, i) {
        return v;
      });
      linearGradient.selectAll('stop').data(colorFunction$$1.colors()).enter().append('stop').attr("offset", function (d, i) {
        return i / (colorFunction$$1.colors().length - 1);
      }).attr('stop-color', function (d) {
        return d;
      });
      var rect = utils.sel.safeSelect(container, 'rect', 'legend').attr('transform', 'translate(0,' + fontSize + ')').style("fill", "url(#" + utils.str.hypenate(namespace, 'numerical-legend-gradient') + ")").attr('x', 0).attr('y', 0).attr('width', spaceX).attr('height', spaceY - fontSize * 2).on('mousemove', function (d, i) {
        legendMousemove(d, i, rect, d3$1.select(this));
      }).on('mouseout', function (d, i) {
        d3$1.select("#" + utils.str.hypenate(namespace, 'legend-tooltip')).remove();
      });
      var minText = utils.sel.safeSelect(container, 'text', 'min').text(utils.math.round(min, 2)).attr('text-anchor', 'middle').attr("font-size", fontSize + 'px').attr('transform', function (d, i) {
        var x = spaceX / 2,
            y = spaceY - fontSize / 4,
            t = 'translate(' + x + ',' + y + ')';
        return t;
      });
      var maxText = utils.sel.safeSelect(container, 'text', 'max').text(utils.math.round(max, 2)).attr('text-anchor', 'middle').attr("font-size", fontSize + 'px').attr('transform', function (d, i) {
        var x = spaceX / 2,
            y = fontSize,
            t = 'translate(' + x + ',' + y + ')';
        return t;
      });
    }

    function legendMousemove(d, i, rect, t) {
      var s = d3$1.scaleLinear().domain([0, rect.attr('height')]).range([max, min]);
      var m = d3$1.mouse(rect.node());
      var v = utils.math.round(s(m[1]), roundTo);
      var strokeColor = colorFunction$$1(undefined, v, undefined, 'stroke');
      var fillColor = colorFunction$$1(undefined, v, undefined, 'fill');
      var div = utils.sel.safeSelect(d3$1.select('body'), 'div', utils.str.hypenate(namespace, 'legend-tooltip')).attr('id', utils.str.hypenate(namespace, 'legend-tooltip')).style('position', 'absolute').style('left', d3$1.event.pageX + 15 + 'px').style('top', d3$1.event.pageY + 15 + 'px').style('background-color', fillColor).style('border-color', strokeColor).style('min-width', fontSize * (String(max).split('.')[0].length + 3) + 'px').style('min-height', fontSize * (String(max).split('.')[0].length + 3) + 'px').style('border-radius', '50%').style('border-radius', '5000px').style('display', 'flex').style('justify-content', 'center').style('text-align', 'middle').style('padding', 2 + "px").style('border-style', 'solid').style('border-width', 2);
      var text = utils.sel.safeSelect(div, 'div').text(v).style('color', textColor).style('align-self', 'center');
    }

    return legend;
  }

  var legends = {
    categorical: categoricLegend,
    numeric: numericLegend
  };

  function _slicedToArray$1(arr$$1, i) { return _arrayWithHoles$1(arr$$1) || _iterableToArrayLimit$1(arr$$1, i) || _nonIterableRest$1(); }

  function _nonIterableRest$1() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

  function _iterableToArrayLimit$1(arr$$1, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr$$1[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles$1(arr$$1) { if (Array.isArray(arr$$1)) return arr$$1; }
  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                             D3 EXTENSIONS                                  **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
  * Recursively ascends parents of selection until it finds an svg tag
  * @function d3.selection.thisSVG
  * @augments d3.selection
  * @returns {Element} which is the svg tag, not the d3 selection of that tag
  */

  d3$1.selection.prototype.thisSVG = function () {
    return utils.sel.getContainingSVG(this.node());
  };
  /**
  * Helper for getting absolute position of the mouse
  * @function d3.mouse.absolute
  * @augments d3.mouse
  * @returns {number[]} [x, y] as they relate to `html` not to local scope.
  */


  d3$1.mouse.absolute = function () {
    var html = d3$1.select('html').node();

    var _this = this(html),
        _this2 = _slicedToArray$1(_this, 2),
        x = _this2[0],
        y = _this2[1];

    return [x, y];
  };
  /**
  * Gets position of the selection in relation to the containing svg
  * @see{@link getContainingSVG}
  * @function d3.selection.absolutePosition
  * @augments d3.selection
  * @returns {Object} with structure similar to getBoundingClientRect, e.g.
  * top, left, bottom, right, height, width
  */


  d3$1.selection.prototype.absolutePosition = function () {
    var element = this.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = utils.sel.getContainingSVG(element);
    var svgPosition = containerSVG.getBoundingClientRect();
    return {
      top: elementPosition.top - svgPosition.top,
      left: elementPosition.left - svgPosition.left,
      bottom: elementPosition.bottom - svgPosition.top,
      right: elementPosition.right - svgPosition.left,
      height: elementPosition.height,
      width: elementPosition.width
    };
  };

  d3$1.selection.prototype.relativePositionTo = function (container) {
    var element = this.node();
    var elementPosition = element.getBoundingClientRect();
    var containerSVG = container;
    var svgPosition = containerSVG.getBoundingClientRect();
    return {
      top: elementPosition.top - svgPosition.top,
      left: elementPosition.left - svgPosition.left,
      bottom: elementPosition.bottom - svgPosition.top,
      right: elementPosition.right - svgPosition.left,
      height: elementPosition.height,
      width: elementPosition.width
    };
  };

  function _slicedToArray$2(arr$$1, i) { return _arrayWithHoles$2(arr$$1) || _iterableToArrayLimit$2(arr$$1, i) || _nonIterableRest$2(); }

  function _nonIterableRest$2() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

  function _iterableToArrayLimit$2(arr$$1, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr$$1[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles$2(arr$$1) { if (Array.isArray(arr$$1)) return arr$$1; }

  function getTranslation$1(selection) {
    var transform = selection.attr('transform');

    var _transform$split = transform.split('translate('),
        _transform$split2 = _slicedToArray$2(_transform$split, 2),
        junk = _transform$split2[0],
        xy = _transform$split2[1];

    var _xy$split = xy.split(','),
        _xy$split2 = _slicedToArray$2(_xy$split, 2),
        x = _xy$split2[0],
        y = _xy$split2[1];

    junk = y.split(')');
    return [parseFloat(x), parseFloat(y)];
  }

  function lasso(selection) {
    var svg,
        // svg that is target of events
    objectContainer,
        // container which houses objects we are selecting (allows for transform to be applied to lasso)
    objectClass,
        // class of object we are selecting
    namespace = "d3sm-lasso",
        chartContainer,
        chartOffset,
        objectsOffset,
        eventCatcher,
        xScale,
        // optional scale for the lasso currentPoints
    yScale,
        // optional scale for the lasso currentPoints
    activeQ = false,
        // whether or not lasso is active
    currentPoints = [],
        // mouse points for current lasso
    allPoints = [],
        // list of lists for all points of lassos
    line = d3.line().x(function (d, i) {
      var x;

      if (xScale != undefined) {
        x = xScale(d[0]);
      } else {
        x = d[0];
      }

      return x; //- chartOffset[0]// - objectsOffset[0]
    }).y(function (d, i) {
      var y;

      if (yScale != undefined) {
        y = yScale(d[1]);
      } else {
        y = d[1];
      }

      return y; // - chartOffset[1]// - objectsOffset[1]
    }).curve(d3.curveLinearClosed),
        instance = 0,
        // an indentifier for which instance this lasso is under the current svg
    tickDistance = 10,
        // styles for lasso path
    color$$1 = '#17a2b8',
        animationRate = '10s',
        opacity = 0.3,
        dashArray = '5, 10',
        stroke = 'black',
        strokeWidth = 2,
        // styles for lassoed objects
    lassoedFill = "white",
        lassoedStroke = 'black',
        lassoedStrokeWidth = 3,
        transitionDuration = 1000,
        easeFunc = d3.easeExp;
    var path;

    lasso.svg = function (_) {
      return arguments.length ? (svg = _, lasso) : svg;
    };

    lasso.chartContainer = function (_) {
      return arguments.length ? (chartContainer = _, lasso) : chartContainer;
    };

    lasso.objectContainer = function (_) {
      return arguments.length ? (objectContainer = _, lasso) : objectContainer;
    };

    lasso.objectClass = function (_) {
      return arguments.length ? (objectClass = _, lasso) : objectClass;
    };

    lasso.namespace = function (_) {
      return arguments.length ? (namespace = _, lasso) : namespace;
    };

    lasso.xScale = function (_) {
      return arguments.length ? (xScale = _, lasso) : xScale;
    };

    lasso.yScale = function (_) {
      return arguments.length ? (yScale = _, lasso) : yScale;
    };

    lasso.activeQ = function (_) {
      return arguments.length ? (activeQ = _, lasso) : activeQ;
    };

    lasso.currentPoints = function (_) {
      return arguments.length ? (currentPoints = _, lasso) : currentPoints;
    };

    lasso.allPoints = function (_) {
      return arguments.length ? (allPoints = _, lasso) : allPoints;
    };

    lasso.instance = function (_) {
      return arguments.length ? (instance = _, lasso) : instance;
    };

    lasso.tickDistance = function (_) {
      return arguments.length ? (tickDistance = _, lasso) : tickDistance;
    };

    lasso.color = function (_) {
      return arguments.length ? (color$$1 = _, lasso) : color$$1;
    };

    lasso.animationRate = function (_) {
      return arguments.length ? (animationRate = _, lasso) : animationRate;
    };

    lasso.opacity = function (_) {
      return arguments.length ? (opacity = _, lasso) : opacity;
    };

    lasso.dashArray = function (_) {
      return arguments.length ? (dashArray = _, lasso) : dashArray;
    };

    lasso.stroke = function (_) {
      return arguments.length ? (stroke = _, lasso) : stroke;
    };

    lasso.lassoedFill = function (_) {
      return arguments.length ? (lassoedFill = _, lasso) : lassoedFill;
    };

    lasso.lassoedStroke = function (_) {
      return arguments.length ? (lassoedStroke = _, lasso) : lassoedStroke;
    };

    lasso.lassoedStrokeWidth = function (_) {
      return arguments.length ? (lassoedStrokeWidth = _, lasso) : lassoedStrokeWidth;
    };

    lasso.eventCatcher = function (_) {
      return arguments.length ? (eventCatcher = _, lasso) : eventCatcher;
    };

    lasso.drag = drag;
    lasso.draw = draw;
    lasso.tick = tick;
    lasso.detect = detect;
    lasso.toggle = toggle;
    lasso.remove = remove;
    lasso.render = render;
    lasso.keyFrames = keyFrames;
    lasso.updateObjects = updateObjects;
    lasso.applyPathAttributes = applyPathAttributes;
    lasso.applyObjectAttributes = applyObjectAttributes;
    keyFrames();

    function lasso() {
      // add a dash animation if needed
      if (activeQ) {
        transitionDraw();
      }
    }

    function toggle(state) {
      // use optional param to set state, otherwise toggle state
      activeQ = state != undefined ? state : !activeQ;
      chartOffset = getTranslation$1(chartContainer); //utils.sel.getTranslation(chartContainer)

      objectsOffset = getTranslation$1(objectContainer); //utils.sel.getTranslation(objectContainer)

      if (activeQ) {
        svg.node().addEventListener('mousedown', render, true);
      } else {
        svg.node().removeEventListener('mousedown', render, true);
        remove();
      }
    }

    function draw() {
      chartOffset = getTranslation$1(chartContainer); //utils.sel.getTranslation(chartContainer)

      objectsOffset = getTranslation$1(objectContainer); //utils.sel.getTranslation(objectContainer)

      var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container');
      var paths$$1 = container.selectAll('path[instance="' + instance + '"]'); // update

      paths$$1 = paths$$1.data(allPoints); // remove excess

      var pExit = paths$$1.exit().remove(); // add needed paths

      var pEnter = paths$$1.enter().append('path'); // merge

      paths$$1 = paths$$1.merge(pEnter); // apply

      applyPathAttributes(paths$$1);
    }

    function remove() {
      var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container');
      var paths$$1 = container.selectAll('path[instance="' + instance + '"]').remove();
      container.remove();
      objectContainer.selectAll(objectClass).classed("in-lasso", false);
      updateObjects();
    }

    function render(event) {
      // nothing can interefer with drawing the lasso
      event.preventDefault();
      event.stopPropagation();
      var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container');
      /*
      each time the user presses down, while the state is active, the lasso
      the lasso should make a seperate segment.
      */

      currentPoints = [];
      svg.node().addEventListener('mousemove', drag);
      svg.node().addEventListener('mouseup', function (event) {
        svg.node().removeEventListener('mousemove', drag);
        allPoints.push(currentPoints); // BUG:  somehow this is pushing currentPoints n times where n is the nth lasso path for the current instance
        // NOTE: allPoints = utils.arr.unique(allPoints) is a temporary and inefficient fix

        allPoints = utils.arr.unique(allPoints);
      });
      path = container.append('path').data([currentPoints]);
      applyPathAttributes(path);
    }

    function transitionDraw() {
      var container = utils.sel.safeSelect(objectContainer, 'g', 'lasso-container');
      var paths$$1 = container.selectAll('path[instance="' + instance + '"]'); // update

      paths$$1 = paths$$1.data(allPoints); // remove excess

      var pExit = paths$$1.exit().remove(); // add needed paths

      var pEnter = paths$$1.enter().append('path'); // merge

      paths$$1 = paths$$1.merge(pEnter).transition().duration(transitionDuration).ease(easeFunc);
      applyPathAttributes(paths$$1);
    }

    function applyPathAttributes(path) {
      path.attr("class", utils.str.hypenate(namespace, "lasso-path")).style('opacity', opacity).attr('fill', color$$1).attr("d", line).attr('instance', instance).style("stroke-dasharray", dashArray).attr("stroke", stroke).attr("stroke-width", strokeWidth).style('animation', 'lassoDash ' + animationRate + ' linear').style("animation-iteration-count", "infinite");
    }

    function drag(event) {
      /*
      effectively create a mouse down and move event (which normally is inteperated
      as 'drag' by the browser) by dynamically adding / removing this event on
      mouse down / mouse up.
      */
      if (eventCatcher != undefined) {
        eventCatcher.dispatch(utils.str.hypenate(namespace, "drag"));
      } // d3.dispatch(utils.str.hypenate(namespace,"drag"))


      if (event.which != 1) {
        return;
      } // ensures left mouse button set


      d3.event = event;
      var pt = d3.mouse(objectContainer.node());
      var pt = d3.mouse(svg.node());

      if (xScale != undefined) {
        pt[0] = xScale.invert(pt[0]);
      }

      if (yScale != undefined) {
        pt[1] = yScale.invert(pt[1]);
      }

      pt[0] = pt[0] - chartOffset[0] - objectsOffset[0];
      pt[1] = pt[1] - chartOffset[1] - objectsOffset[1];
      /* if we have a point already, test if it passes a minimum distance to prevent overwhelming with too many tick functions */

      if (currentPoints.length) {
        var lastPt = currentPoints[currentPoints.length - 1];
        var a = [pt[0], pt[1]],
            b = [lastPt[0], lastPt[1]];

        if (xScale) {
          b[0] = xScale(b[0]);
          a[0] = xScale(a[0]);
        }

        if (yScale) {
          b[1] = yScale(b[1]);
          a[1] = yScale(a[1]);
        }

        var dist = utils.math.euclideanDistance(b, a);

        if (dist > tickDistance) {
          tick(pt);
        }
      } else {
        tick(pt);
      }
    }

    function tick(pt) {
      /*
      If a point is provided update data and objects.
      Otherwise just call on data we already have.
       Why like this?:
      1. currentPoints is current points to allow disjunct lassos, currentPoints is only pushed to
      allPoints after mouseup.
      2. to allow render of objects in the lasso class / updating the data list
      just by toggling the button
      */
      if (pt != undefined) {
        currentPoints.push(pt);
        path.attr("d", line);

        if (currentPoints.length < 3) {
          return;
        } // need at least 3 points to detect anything.


        detect(allPoints.concat([currentPoints]));
      } else {
        detect(allPoints);
      }
    }

    function detect(lassos) {
      if (lassos == undefined) {
        lassos = allPoints;
      }

      objectContainer.selectAll(objectClass).each(function (d, i) {
        var current = d3.select(this),
            box = utils.sel.absolutePosition(current); //.absolutePosition(),
        // box = current.relativePositionTo(objectContainer.node()),

        boxPts = [[box.left - chartOffset[0] - objectsOffset[0], box.top - chartOffset[1] - objectsOffset[1]], [box.right - chartOffset[0] - objectsOffset[0], box.top - chartOffset[1] - objectsOffset[1]], [box.left - chartOffset[0] - objectsOffset[0], box.bottom - chartOffset[1] - objectsOffset[1]], [box.right - chartOffset[0] - objectsOffset[0], box.bottom - chartOffset[1] - objectsOffset[1]]];

        if (xScale != undefined) {
          boxPts[0][0] = xScale.invert(boxPts[0][0]);
          boxPts[1][0] = xScale.invert(boxPts[1][0]);
          boxPts[2][0] = xScale.invert(boxPts[2][0]);
          boxPts[3][0] = xScale.invert(boxPts[3][0]);
        }

        if (yScale != undefined) {
          boxPts[0][1] = yScale.invert(boxPts[0][1]);
          boxPts[1][1] = yScale.invert(boxPts[1][1]);
          boxPts[2][1] = yScale.invert(boxPts[2][1]);
          boxPts[3][1] = yScale.invert(boxPts[3][1]);
        }
        /*
        flag needed as we have to test multiple lasso segments, and if the point
        is not in one segment, it does not mean it is not in any
        */


        var inAnyLassoQ = false;

        for (var i = 0; i < lassos.length; i++) {
          var lassoPoints = lassos[i]; // .map(function(pt){
          //   var x, y = pt
          //   if (xScale!=undefined) {x = xScale(x)}
          //   if (yScale!=undefined) {y = yScale(y)}
          //   return [x, y]
          // })

          var boxInLassoQ = boxPts.every(function (coord) {
            return d3.polygonContains(lassoPoints, coord);
          });

          if (boxInLassoQ) {
            inAnyLassoQ = true;
          } // only update flag in the positive case.

        }

        current.classed('in-lasso', inAnyLassoQ);
        current.classed('in-lasso-' + instance, inAnyLassoQ);
      });
      updateObjects();
      return objectContainer.selectAll('.in-lasso-' + instance);
    }

    function updateObjects() {
      objectContainer.selectAll(objectClass).each(function (d, i) {
        var t = d3.select(this);
        applyObjectAttributes(t, t.classed('in-lasso'));
      });
    }

    function applyObjectAttributes(obj, setQ) {
      var preLassoFill = obj.attr('_pre_lasso_fill'),
          preLassoStroke = obj.attr('_pre_lasso_stroke'),
          preLassoStrokeWidth = obj.attr('_pre_lasso_stroke-width');

      if (setQ) {
        obj.classed("in-lasso", true);
        obj.classed('in-lasso-' + instance, true);

        if (preLassoFill == undefined) {
          obj.attr('_pre_lasso_fill', obj.attr('fill'));
        }

        if (preLassoStroke == undefined) {
          obj.attr('_pre_lasso_stroke', obj.attr('stroke'));
        }

        if (preLassoStrokeWidth == undefined) {
          obj.attr('_pre_lasso_stroke-width', obj.attr('stroke-width'));
        }

        obj //BUG: when .raise()
        .attr('fill', lassoedFill).attr('stroke', lassoedStroke).attr('stoke-width', lassoedStrokeWidth);
      } else {
        obj.classed("in-lasso", false);
        obj.classed('in-lasso-' + instance, false);

        if (preLassoFill != undefined) {
          obj.attr('fill', preLassoFill);
        }

        if (preLassoStroke != undefined) {
          obj.attr('stroke', preLassoStroke);
        }

        if (preLassoStrokeWidth != undefined) {
          obj.attr('stroke-width', preLassoStrokeWidth);
        }
      }
    }

    function keyFrames() {
      var style = d3.select("html").select('style.' + utils.str.hypenate(namespace, "lasso-dash"));

      if (style.empty()) {
        d3.select("html").append('style').classed(utils.str.hypenate(namespace, "lasso-dash"), true).html("@keyframes lassoDash {to { stroke-dashoffset: 1000;}}");
      }
    }

    return lasso;
  }

  function lassoWidget(selection) {
    var namespace = 'd3sm-lasso',
        selection = selection,
        svg,
        chartContainer,
        objectContainer,
        objectClass,
        lassoLine = d3.line().x(function (d, i) {
      return d[0];
    }).y(function (d, i) {
      return d[1];
    }).curve(d3.curveLinearClosed),
        colorFunction$$1 = colorFunction(),
        maxNumberOfGroups,
        dataExtractor,
        xScale,
        yScale;

    function onError(msg, style) {
      console.log(msg, style);
    } // setup the container


    setupDataselectContainer();

    lassoWidget.objectClass = function (_) {
      return arguments.length ? (objectClass = '.' + _.replace('.', ''), lassoWidget) : objectClass;
    };

    lassoWidget.svg = function (_) {
      return arguments.length ? (svg = _, lassoWidget) : svg;
    };

    lassoWidget.submit = function (_) {
      return arguments.length ? (submit = _, lassoWidget) : submit;
    };

    lassoWidget.maxNumberOfGroups = function (_) {
      return arguments.length ? (maxNumberOfGroups = _, lassoWidget) : maxNumberOfGroups;
    };

    lassoWidget.onError = function (_) {
      return arguments.length ? (onError = _, lassoWidget) : onError;
    };

    lassoWidget.objectContainer = function (_) {
      return arguments.length ? (objectContainer = _, lassoWidget) : objectContainer;
    };

    lassoWidget.chartContainer = function (_) {
      return arguments.length ? (chartContainer = _, lassoWidget) : chartContainer;
    };

    lassoWidget.dataExtractor = function (_) {
      return arguments.length ? (dataExtractor = _, lassoWidget) : dataExtractor;
    };

    lassoWidget.xScale = function (_) {
      return arguments.length ? (xScale = _, lassoWidget) : xScale;
    };

    lassoWidget.yScale = function (_) {
      return arguments.length ? (yScale = _, lassoWidget) : yScale;
    };

    function styles() {
      var s = d3.select("html").select("style." + namespace + 'lasso-widget');

      if (s.empty()) {
        d3.select("html").append("style").classed(namespace + 'lasso-widget', true).html("." + utils.str.hypenate(namespace, "data-table") + "{\
          height:100px;\
          overflow:auto;\
        }");
      }
    }

    function lassoWidget() {
      styles();
    }

    function submit(data) {
      console.log(data);
    }

    function plusTab(tabList) {
      var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'plus-tab')).classed('ml-auto', true).classed('nav-item', true).on('click', makeNewGroup),
          anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
          icon = utils.sel.safeSelect(anchor, 'i', 'fa').classed('fa-plus fa-2x text-success', true);
    }

    function sendTab(tabList) {
      var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'send-tab')).classed('ml-auto', true).classed('nav-item', true).on('click', clickSend),
          anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
          icon = utils.sel.safeSelect(anchor, 'i', 'fa').classed('fa-paper-plane-o fa-2x text-primary', true);
    }

    function clickSend() {
      var data = gatherDataLists();
      submit(data);
    }

    function closeTab(tabList) {
      var tab = utils.sel.safeSelect(tabList, 'li', utils.str.hypenate(namespace, 'close-tab')).classed('ml-auto', true).classed('nav-item', true).on('click', function () {
        var m = d3.mouse(d3.select("html").node());
        selection.select('div.card').style("position", 'relative').transition().duration(1000).ease(d3.easeBack).style('left', window.outerWidth + 'px').remove();
      }),
          anchor = utils.sel.safeSelect(tab, 'a', 'nav-link'),
          icon = utils.sel.safeSelect(anchor, 'i', 'fa').classed('fa-window-close-o fa-2x text-danger', true);
    }

    function setupDataselectContainer() {
      var card = utils.sel.safeSelect(selection, 'div', 'card'),
          cardHeader = utils.sel.safeSelect(card, 'div', 'card-header'),
          tabList = utils.sel.safeSelect(cardHeader, 'ul', 'nav').classed('nav-tabs card-header-tabs', true).classed(utils.str.hypenate(namespace, 'tab-list'), true).attr('role', 'tablist'),
          cardBody = utils.sel.safeSelect(card, 'div', 'card-body'),
          tabContent = utils.sel.safeSelect(cardBody, 'div', 'tab-content').classed(utils.str.hypenate(namespace, 'tab-content'), true),
          // leftTabs = utils.sel.safeSelect(tabList, 'div', 'nav mr-auto left-aligned-tabs')
      rightTabs = utils.sel.safeSelect(tabList, 'div', 'right-aligned-tabs').classed('nav ml-auto ', true);
      plusTab(rightTabs);
      sendTab(rightTabs);
      closeTab(rightTabs);
      var defaultTab = utils.sel.safeSelect(tabContent, 'div', 'tab-pane').classed(utils.str.hypenate(namespace, 'default-tab'), true).classed("active", true).classed('text-left', true),
          p = utils.sel.safeSelect(defaultTab, 'div').html("Click <kbd><i class='fa fa-plus text-success'></i></kbd> to add a new group.<br>" + "Click <kbd><i class='fa fa-paper-plane-o text-primary'></i></kbd> to submit for re-analysis.<br>" + "Click <kbd><i class='fa fa-window-close-o text-danger'></i></kbd> to close the dataselect widget."); // .text('Click the green plus to add a new group.')
    }

    function makeRemoveButton(paneBtnList) {
      var btn = utils.sel.safeSelect(paneBtnList, 'button', 'remove-btn').classed('btn btn-danger', true).on('click', removeBtnClickToRemove),
          i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-trash-o', true),
          s = utils.sel.safeSelect(btn, 'span').text('Remove group');
      return btn;
    }

    function removeBtnClickToRemove(d, i) {
      var tab = selection.select('#' + utils.str.hypenate(namespace, 'tab', d)),
          pane = selection.select('#' + utils.str.hypenate(namespace, 'tab', 'pane', d));
      tab.remove();
      pane.remove();
      showRemainingPanes();
      updateTabAndPaneNumbers();
    }

    function togglePaneById(id) {
      var panes = selection.select('.' + utils.str.hypenate(namespace, 'tab-content'));
      panes.selectAll('div.tab-pane').each(function (d, i) {
        d3.select(this).classed('active show', d3.select(this).attr('id') == id);
      });
    }

    function insertTab(tabs, n) {
      // var li = tabs.insert("li", ':nth-child('+(n)+')')
      var li = tabs.insert("li", '.right-aligned-tabs').classed('nav-item', true).classed('alert', true).classed('alert-secondary', true).classed('alert-dismissable', true).classed('fade', true).classed('show', true) // .classed('mr-auto', true)
      .attr("role", 'alert').attr("id", utils.str.hypenate(namespace, 'tab', n)).classed(utils.str.hypenate(namespace, 'tab'), true);
      var a = utils.sel.safeSelect(li, 'a').attr('data-toggle', 'tab').text('Group ' + n).attr('href', '#' + utils.str.hypenate(namespace, 'tab', 'pane', n)).on('dblclick', function (d, i) {
        var t = d3.select(this);
        t.attr('contenteditable', true);
        d3.select(t.node().parentNode).classed('alert-secondary', false).classed('alert-warning', true); // d3.select("html").on("click", dispatchBlur)
        //
        // function dispatchBlur(d, i) {
        //   if (d3.event.target != d3.select(this)) {
        //     d3.select(this).dispatch("blur")
        //   }
        // }
      }).on('blur', function (d, i) {
        var t = d3.select(this);
        t.attr('contenteditable', false);
        d3.select(t.node().parentNode).classed('alert-secondary', true).classed('alert-warning', false);
      }).on('input', function (d, i) {
        var t = d3.select(this);
        var txt = t.text();
        d3.select(t.attr('href')).select("p.lead").text(txt);
      });
      return li;
    }

    function makeTabPane(panes, n) {
      var pane = panes.append('div').datum(n).attr('role', 'tabpanel').classed('tab-pane', true).classed('text-left', true).classed(utils.str.hypenate(namespace, 'tab', 'pane'), true).attr('id', utils.str.hypenate(namespace, 'tab', 'pane', n));
      return pane;
    }

    function populatePane(pane, n) {
      var lead = utils.sel.safeSelect(pane, 'p', 'lead').text('Group ' + n),
          tableContainer = utils.sel.safeSelect(pane, 'div', 'table-responsive').attr("class", utils.str.hypenate(namespace, "data-table")),
          table = utils.sel.safeSelect(tableContainer, "table", "table").classed("table-sm", true).classed("table-hover", true),
          caption = utils.sel.safeSelect(table, "caption", "caption").html("List of selected"),
          btns = utils.sel.safeSelect(pane, 'div', 'text-right'),
          cN = n % colorFunction$$1.colors().length;

      if (n % 2 != 0) {
        cN = colorFunction$$1.colors().length - 1 - cN;
      }

      var lassoBtn = makeLassoButton(btns);
      var currentLasso = makeLassoFunction(n, colorFunction$$1.colors()[cN]);
      var clearBtn = makeClearButton(btns);
      bindButtons(lassoBtn, clearBtn, currentLasso, table);
      var removeBtn = makeRemoveButton(btns);
      lead.on("mouseover", function () {
        currentLasso.draw();
      });
      lead.on("mouseout", function () {
        currentLasso.remove();
      });
    }

    function makeLassoButton(btns) {
      var btn = utils.sel.safeSelect(btns, 'button', 'lasso-btn').classed('btn btn-info', true).classed(namespace, true); // .datum([lasso])

      var i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-hand-pointer-o', true);
      var s = utils.sel.safeSelect(btn, 'span').text('Lasso select');
      return btn;
    }

    function makeClearButton(btns) {
      var btn = utils.sel.safeSelect(btns, 'button', 'clear-btn').classed('btn btn-light', true).classed(namespace, true);
      var i = utils.sel.safeSelect(btn, 'i', 'fa').classed('fa-eraser', true);
      var s = utils.sel.safeSelect(btn, 'span').text('Clear selection');
      return btn;
    }

    function makeLassoFunction(instance, color$$1) {
      var currentLasso = lasso().namespace(namespace).svg(svg).objectClass(objectClass).chartContainer(chartContainer).objectContainer(objectContainer).instance(instance).color(color$$1).yScale(yScale).xScale(xScale);
      return currentLasso;
    }

    function bindButtons(lassoBtn, clearBtn, currentLasso, table) {
      currentLasso.eventCatcher(lassoBtn);
      lassoBtn.node().addEventListener("click", lassoBtnToggle);
      lassoBtn.datum([currentLasso]).on(utils.str.hypenate(namespace, 'render'), function () {
        d3.select(this).datum()[0].draw();
      }).on(utils.str.hypenate(namespace, "drag"), function (las) {
        var nodes = chartContainer.selectAll(".in-lasso-" + las[0].instance()).nodes();
        var tableData = nodes.map(function (d, i) {
          var extracted = dataExtractor(d3.select(d).datum());
          extracted["__node"] = d;
          return extracted;
        });
        makeTable(table, tableData, currentLasso);
      });
      clearBtn.on('click', function () {
        currentLasso.allPoints([]);
        currentLasso.currentPoints([]);
        lassoBtn.dispatch(utils.str.hypenate(namespace, "drag"));
      });
    }

    function lassoBtnToggle() {
      var that = d3.select(this);
      var las = that.datum()[0];
      las.toggle();
      var activeQ = las.activeQ();

      if (las.activeQ()) {
        that.classed('btn-info', !activeQ);
        that.classed('btn-warning', activeQ);
        that.select("span").text("Lasso select (active)");
        selection.selectAll("." + namespace + ".lasso-btn").dispatch(utils.str.hypenate(namespace, 'render'));
        d3.select("html").node().addEventListener('mousedown', monitorLassoButtonState);
      } else {
        that.classed('btn-info', !activeQ);
        that.classed('btn-warning', activeQ);
        that.select("span").text("Lasso select");
        d3.select("html").node().removeEventListener('mousedown', monitorLassoButtonState);
      }

      function monitorLassoButtonState(event) {
        /*
        activeLasso stops event stopPropagation, so this event will not register in
        that case. Thus we only need to ensure that the usre is clicking on the lasso button
        otherwise, de-spawn lasso.
        */
        if (event.target != that.node() && event.target != that.select("span").node() && event.target != that.select("i").node()) {
          // event.preventDefault()
          // event.stopPropagation()
          las.toggle(false);
          that.classed('btn-info', true);
          that.classed('btn-warning', false);
          that.select("span").text("Lasso select"); // console.log(that, that.node())
          // that.dispatch("focusout")
        }
      }
    }

    function updateTableHeaderColumns(headR, tableData) {
      var headerKeys = d3.keys(tableData[0]).filter(function (k) {
        return k != "__node";
      });

      if (headerKeys.length > 0) {
        // headerKeys = ["remove"].concat(headerKeys)
        headerKeys.push("remove");
      }

      var headerCols = headR.selectAll("th");
      headerCols = headerCols.data(headerKeys);
      headerCols.exit().remove();
      headerCols = headerCols.merge(headerCols.enter().append("th").attr("scope", "col")).text(function (d, i) {
        return d;
      });
      return headerKeys;
    }

    function updateTableRows(body, tableData) {
      var bodyRows = body.selectAll("tr");
      bodyRows = bodyRows.data(tableData);
      bodyRows.exit().remove();
      bodyRows = bodyRows.merge(bodyRows.enter().append("tr"));
      return bodyRows;
    }

    function updateTableRowColumns(cols, headerKeys, rowData, tableData, lasso$$1, table) {
      cols = cols.data(headerKeys);
      cols.exit().remove();
      cols = cols.merge(cols.enter().append("td"));
      cols.html(function (d, i) {
        if (d != "remove") {
          return rowData[d];
        }

        return "<i class='fa fa-close'></i>";
      }).classed("text-left", function (d, i) {
        if (d != "remove") {
          return false;
        }

        return true;
      }); // .attr("scope",function(d, i){
      //   if (d != "remove") { return false }
      //   return true
      // })

      cols.select("i.fa-close").on("click", function (d, i) {
        var node = rowData["__node"],
            n = d3.select(node);
        n.classed("in-lasso", false);
        n.classed("in-lasso-" + lasso$$1.instance(), false);
        tableData.map(function (dd, j) {
          if (dd["__node"] == node) {
            tableData.splice(j, 1);
            makeTable(table, tableData, lasso$$1);
          }
        });
      });
    }

    function makeTable(table, tableData, lasso$$1) {
      var head = utils.sel.safeSelect(table, "thead"),
          headR = utils.sel.safeSelect(head, "tr"),
          body = utils.sel.safeSelect(table, "tbody"),
          headerKeys = updateTableHeaderColumns(headR, tableData),
          bodyRows = updateTableRows(body, tableData);
      bodyRows.each(function (rowData, i) {
        var t = d3.select(this);
        var cols = t.selectAll("td");
        updateTableRowColumns(cols, headerKeys, rowData, tableData, lasso$$1, table);
        t.on("mouseover", function (d, i) {
          lasso$$1.applyObjectAttributes(d3.select(d["__node"]), true);
        }).on("mouseout", function (d, i) {
          lasso$$1.applyObjectAttributes(d3.select(d["__node"]), false);
        });
      });
    }

    function makeNewGroup(d, i) {
      var tabs = selection.select('.' + utils.str.hypenate(namespace, 'tab-list')),
          panes = selection.select('.' + utils.str.hypenate(namespace, 'tab-content')),
          n = newGroupNumber();

      if (tabs.selectAll('.' + utils.str.hypenate(namespace, 'tab')).size() == maxNumberOfGroups) {
        onError('only ' + maxNumberOfGroups + ' allowed.', 'warning');
        return;
      }

      var tab = insertTab(tabs, n),
          pane = makeTabPane(panes, n);
      populatePane(pane, n);
      togglePaneById(pane.attr('id'));
    }

    function newGroupNumber() {
      var tabs = selection.select('.' + utils.str.hypenate(namespace, 'tab-list')); //,

      var n = tabs.selectAll('li').size() - 3,
          // minus 1 for plus tab
      s = 'Group ' + n,
          gs = [];
      tabs.each(function (d, i) {
        return gs.push(d3.select(this).select("a").text());
      });

      while (gs.includes(s)) {
        n += 1;
        s = 'Group ' + n;
      }

      return n;
    }

    function updateTabAndPaneNumbers() {
      var tabs = selection.select('.' + utils.str.hypenate(namespace, 'tab-list')),
          panes = selection.select('.' + utils.str.hypenate(namespace, 'tab-content'));
      tabs = tabs.selectAll('.' + utils.str.hypenate(namespace, 'tab'));
      panes = panes.selectAll('.' + utils.str.hypenate(namespace, 'tab', 'pane'));
      tabs.each(function (d, i) {
        d3.select(this).datum(i).attr("id", utils.str.hypenate(namespace, 'tab', i)).select('a').attr('href', '#' + utils.str.hypenate(namespace, 'tab', 'pane', i)).text(function (dd, ii) {
          var curText = d3.select(this).text();

          if (curText.split(' ')[0] == 'Group') {
            return 'Group ' + i;
          }

          return curText;
        });
      });
      panes.each(function (d, i) {
        d3.select(this).datum(i).attr('id', utils.str.hypenate(namespace, 'tab', 'pane', i));
        utils.sel.safeSelect(d3.select(this), 'p', 'lead').text(function (dd, ii) {
          var curText = d3.select(this).text();

          if (curText.split(' ')[0] == 'Group') {
            return 'Group ' + i;
          }

          return curText;
        });
        utils.sel.safeSelect(d3.select(this), 'button', 'remove-btn').on('click', removeBtnClickToRemove);
      });
    }

    function gatherDataLists() {
      var tabs = selection.select('.' + utils.str.hypenate(namespace, 'tab-list'));
      var panes = selection.select('.' + utils.str.hypenate(namespace, 'tab-content'));
      var tables = panes.selectAll('.' + utils.str.hypenate(namespace, "data-table"));
      var data = {};
      var textGroups = tabs.selectAll('li.' + utils.str.hypenate(namespace, 'tab') + ' > a').nodes().map(function (d, i) {
        return d3.select(d).text();
      });
      textGroups.map(function (e, i) {
        data[e] = [];
      });
      tables.each(function (d, i) {
        var table = d3.select(this).select("tbody");
        data[textGroups[i]] = table.selectAll('tr').data();
      });
      return data;
    }

    function showRemainingPanes() {
      var tabs = selection.select('.' + utils.str.hypenate(namespace, 'tab-list')),
          panes = selection.select('.' + utils.str.hypenate(namespace, 'tab-content')),
          remainingTabs = tabs.selectAll('.' + utils.str.hypenate(namespace, 'tab'));

      if (remainingTabs.size() == 0) {
        panes.select('.' + utils.str.hypenate(namespace, 'default-tab')).classed("active", true).classed('text-left', true);
      } else {
        var lastTab = remainingTabs.nodes()[remainingTabs.size() - 1],
            lastPaneId = d3.select(lastTab).select('a').attr('href');
        panes.select(lastPaneId).classed("active", true).classed('text-left', true);
      }
    }

    return lassoWidget;
  }

  /*******************************************************************************

  **                                                                            **
  **                                                                            **
  **                                PLOTZOOM                                    **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates an plotZoom instance, which can handle drag and scroll events
   * @constructor plotZoom
   * @param {function} chart a function instance of one of the d3sm plots (e.g. bar, boxwhisker, bubbleHeatmap, violin, etc)
   * @param {axis}  xAxis the axis instance responsible for the x axis
   * @param {axis} yAxis the axis instance responsible for the y axis
   * @namespace plotZoom
   * @returns {function} zoom
   */

  function multiPlotZoom(chart) {
    var
    /**
    * The event on which to fire
    * (see {@link plotZoom#eventType})
    * @param {string} eventType which event it should handle. Currently supports scroll and wheel
    * @memberof plotZoom#
    * @property
    */
    eventType,

    /**
    * A scaling factor for the wheel "speed"
    * (see {@link plotZoom#wheelSpeed})
    * @param {number} [wheelSpeed=20] scales the wheel translation by wheelSpeed
    * @memberof plotZoom#
    * @property
    */
    wheelSpeed = 20,

    /**
    * The orientation in which to allow scrolling: 'horizontal', 'vertical', or '2D'
    * (see {@link plotZoom#orient})
    * @param {string} [orient=chart.orient() || 'horizontal']
    * @memberof plotZoom#
    * @property
    */
    orient = chart.orient == undefined ? 'horizontal' : chart.orient(),

    /**
    * The max distance allowed to scroll in the x direction
    * (see {@link plotZoom#xLock})
    * @param {number} [xLock=chart.spaceX()] ideally chart.overflowQ() == true and this value is the
    * bounding rect across all elements in the chart  minus the space in which to show.
    * @memberof plotZoom#
    * @property
    */
    xLock = chart.spaceX(),

    /**
    * The max distance allowed to scroll in the y direction
    * (see {@link plotZoom#yLock})
    * @param {number} [yLock=chart.spaceY()] ideally chart.overflowQ() == true and this value is the
    * bounding rect across all elements in the chart  minus the space in which to show.
    * @memberof plotZoom#
    * @property
    */
    yLock = chart.spaceY(),
        chartSel = chart.selection(),
        svg = d3.select(utils.sel.getContainingSVG(chartSel.node())),
        //.thisSVG()),
    xComponents = [],
        yComponents = [];
    /**
     * Gets or sets the event type in which to respond
     * (see {@link plotZoom#eventType})
     * @param {string} [_=none] should be 'drag' or 'wheel'
     * @returns {zoom | string}
     * @memberof plotZoom
     * @property
     * by default plotZoom=undefined
     */

    zoom.eventType = function (_) {
      return arguments.length ? (eventType = _, zoom) : eventType;
    };
    /**
     * Gets or sets the wheel speed in which to scale the wheel scroll transform
     * (see {@link plotZoom#wheelSpeed})
     * @param {number} [_=none]
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default wheelSpeed=20
     */


    zoom.wheelSpeed = function (_) {
      return arguments.length ? (wheelSpeed = _, zoom) : wheelSpeed;
    };
    /**
     * Gets or sets the orientation in which items are manipulated
     * (see {@link plotZoom#orient})
     * @param {string} [_=none] should be horizontal, vertical, or 2D
     * @returns {zoom | string}
     * @memberof plotZoom
     * @property
     * by default orient=chart.orient() || 'horizontal'
     */


    zoom.orient = function (_) {
      return arguments.length ? (orient = _, zoom) : orient;
    };
    /**
     * Gets or sets the max distance in which to scroll X
     * (see {@link plotZoom#xLock})
     * @param {number} [_=none] should be a positive value
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default xLock=chart.spaceX()
     */


    zoom.xLock = function (_) {
      return arguments.length ? (xLock = _, zoom) : xLock;
    };
    /**
     * Gets or sets the max distance in which to scroll Y
     * (see {@link plotZoom#yLock})
     * @param {number} [_=none]  should be a positive value
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default yLock=chart.spaceY()
     */


    zoom.yLock = function (_) {
      return arguments.length ? (yLock = _, zoom) : yLock;
    };

    zoom.xComponents = function (_) {
      return arguments.length ? (xComponents = _, zoom) : xComponents;
    };

    zoom.yComponents = function (_) {
      return arguments.length ? (yComponents = _, zoom) : yComponents;
    };

    function setLocks() {
      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'));
      var cos = chartObjSel.attr('transform', 'translate(0,0)');
      xLock = chartSel.node().getBBox().width - chart.spaceX(); // * .9

      yLock = chartSel.node().getBBox().height - chart.spaceY(); // * .9

      cos.attr('transform', 'translate(' + chartObjTrans[0] + ',' + chartObjTrans[1] + ')');
      utils.con.log('plotZoom', 'setLocks', {
        xLock: xLock,
        yLock: yLock
      });
    }
    /**
     * Sets the x and y locks (how far one can scroll)
     * (see {@link plotZoom#xLock} and {@link plotZoom#yLock})
     * @function plotZoom.setLocks
     * @returns {undefined}
     * @memberof plotZoom
     * @property
     */


    zoom.setLocks = setLocks;

    function zoom() {
      setLocks();
      var xComponentsSel = xComponents.map(function (d, i) {
        return d.selection();
      }),
          yComponentsSel = yComponents.map(function (d, i) {
        return d.selection();
      });
      var horizontalQ, verticalQ;

      if (orient == '2D') {
        horizontalQ = true;
        verticalQ = true;
      }

      if (orient == 'horizontal') {
        horizontalQ = true;
        verticalQ = false;
      }

      if (orient == 'vertical') {
        verticalQ = true;
        horizontalQ = false;
      } // capture transform event


      var transform = d3.event.transform;
      var chartBox = chartSel.node().getBBox();
      var xComponentsBox = xComponentsSel.map(function (d, i) {
        return d.node().getBBox();
      });
      var yComponentsBox = xComponentsSel.map(function (d, i) {
        return d.node().getBBox();
      });
      var chartWidth = chartBox.width - chartBox.x;
      var chartHeight = chartBox.height - chartBox.y; // enable wheel event

      if (eventType == "wheel") {
        var e = d3.event; // prevent page scrolling

        e.preventDefault(); // event delta is very very slow, so speed it up with wheel speed

        var w = d3.event.deltaY * wheelSpeed;
        var shiftQ = d3.event.shiftKey; // d3 has no way to make custom transform, so make an object and add
        // the necessary functions to make wheel event compatible with drag events

        if (orient == '2D') {
          transform = shiftQ ? {
            k: 1,
            x: w,
            y: 0
          } : {
            k: 1,
            x: 0,
            y: w
          };
        } else {
          transform = horizontalQ ? {
            k: 1,
            x: w,
            y: 0
          } : {
            k: 1,
            x: 0,
            y: w
          };
        } // * -1 is invert


        transform.applyX = function (x) {
          return x * this.k + this.x * -1;
        };

        transform.applyY = function (y) {
          return y * this.k + this.y * -1;
        };
      }

      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var xComponentObjSel = xComponentsSel.map(function (d, i) {
        return d.select('.' + utils.str.hypenate(xComponents[i].namespace(), 'object-container'));
      });
      var yComponentObjSel = yComponentsSel.map(function (d, i) {
        return d.select('.' + utils.str.hypenate(yComponents[i].namespace(), 'object-container'));
      });
      var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'));
      var xComponentsObjTrans = xComponentObjSel.map(function (d, i) {
        return utils.sel.getTranslation(d.attr('transform'));
      });
      var yComponentsObjTrans = yComponentObjSel.map(function (d, i) {
        return utils.sel.getTranslation(d.attr('transform'));
      });
      var x = horizontalQ ? transform.applyX(chartObjTrans[0]) : 0;

      if (horizontalQ) {
        x = x < -xLock ? (transform.x = 0, -xLock) : (transform.x = 0, Math.min(x, 0));
      }

      var y = verticalQ ? transform.applyY(chartObjTrans[1]) : 0;

      if (verticalQ) {
        y = y < -yLock ? (transform.y = 0, -yLock) : (transform.y = 0, Math.min(y, 0));
      }

      chartObjSel.attr('transform', 'translate(' + x + ',' + y + ')');

      if (horizontalQ) {
        // xAxisObjSel.attr('transform', 'translate('+x+','+0+')')
        xComponentObjSel.map(function (d, i) {
          d.attr('transform', 'translate(' + x + ',' + 0 + ')');
        });
      }

      if (verticalQ) {
        // yAxisObjSel.attr('transform', 'translate('+0+','+y+')')
        yComponentObjSel.map(function (d, i) {
          d.attr('transform', 'translate(' + 0 + ',' + y + ')');
        });
      }
    }

    zoom.reset = function () {

      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var xAxisObjSel = xAxisSel.select('.' + utils.str.hypenate(xAxis.namespace(), 'object-container'));
      var yAxisObjSel = yAxisSel.select('.' + utils.str.hypenate(yAxis.namespace(), 'object-container'));
      chartObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
      xAxisObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
      yAxisObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
    };

    return zoom;
  }

  /*******************************************************************************

  **                                                                            **
  **                                                                            **
  **                                PLOTZOOM                                    **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates an plotZoom instance, which can handle drag and scroll events
   * @constructor plotZoom
   * @param {function} chart a function instance of one of the d3sm plots (e.g. bar, boxwhisker, bubbleHeatmap, violin, etc)
   * @param {axis}  xAxis the axis instance responsible for the x axis
   * @param {axis} yAxis the axis instance responsible for the y axis
   * @namespace plotZoom
   * @returns {function} zoom
   */

  function plotZoom(chart, xAxis, yAxis) {
    var
    /**
    * The event on which to fire
    * (see {@link plotZoom#eventType})
    * @param {string} eventType which event it should handle. Currently supports scroll and wheel
    * @memberof plotZoom#
    * @property
    */
    eventType,

    /**
    * A scaling factor for the wheel "speed"
    * (see {@link plotZoom#wheelSpeed})
    * @param {number} [wheelSpeed=20] scales the wheel translation by wheelSpeed
    * @memberof plotZoom#
    * @property
    */
    wheelSpeed = 20,

    /**
    * The orientation in which to allow scrolling: 'horizontal', 'vertical', or '2D'
    * (see {@link plotZoom#orient})
    * @param {string} [orient=chart.orient() || 'horizontal']
    * @memberof plotZoom#
    * @property
    */
    orient = chart.orient == undefined ? 'horizontal' : chart.orient(),

    /**
    * The max distance allowed to scroll in the x direction
    * (see {@link plotZoom#xLock})
    * @param {number} [xLock=chart.spaceX()] ideally chart.overflowQ() == true and this value is the
    * bounding rect across all elements in the chart  minus the space in which to show.
    * @memberof plotZoom#
    * @property
    */
    xLock = chart.spaceX(),

    /**
    * The max distance allowed to scroll in the y direction
    * (see {@link plotZoom#yLock})
    * @param {number} [yLock=chart.spaceY()] ideally chart.overflowQ() == true and this value is the
    * bounding rect across all elements in the chart  minus the space in which to show.
    * @memberof plotZoom#
    * @property
    */
    yLock = chart.spaceY(),
        chartSel = chart.selection(),
        xAxisSel = xAxis.selection(),
        yAxisSel = yAxis.selection(),
        svg = d3.select(utils.sel.getContainingSVG(chartSel.node())); //).thisSVG()

    /**
     * Gets or sets the event type in which to respond
     * (see {@link plotZoom#eventType})
     * @param {string} [_=none] should be 'drag' or 'wheel'
     * @returns {zoom | string}
     * @memberof plotZoom
     * @property
     * by default plotZoom=undefined
     */

    zoom.eventType = function (_) {
      return arguments.length ? (eventType = _, zoom) : eventType;
    };
    /**
     * Gets or sets the wheel speed in which to scale the wheel scroll transform
     * (see {@link plotZoom#wheelSpeed})
     * @param {number} [_=none]
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default wheelSpeed=20
     */


    zoom.wheelSpeed = function (_) {
      return arguments.length ? (wheelSpeed = _, zoom) : wheelSpeed;
    };
    /**
     * Gets or sets the orientation in which items are manipulated
     * (see {@link plotZoom#orient})
     * @param {string} [_=none] should be horizontal, vertical, or 2D
     * @returns {zoom | string}
     * @memberof plotZoom
     * @property
     * by default orient=chart.orient() || 'horizontal'
     */


    zoom.orient = function (_) {
      return arguments.length ? (orient = _, zoom) : orient;
    };
    /**
     * Gets or sets the max distance in which to scroll X
     * (see {@link plotZoom#xLock})
     * @param {number} [_=none] should be a positive value
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default xLock=chart.spaceX()
     */


    zoom.xLock = function (_) {
      return arguments.length ? (xLock = _, zoom) : xLock;
    };
    /**
     * Gets or sets the max distance in which to scroll Y
     * (see {@link plotZoom#yLock})
     * @param {number} [_=none]  should be a positive value
     * @returns {zoom | number}
     * @memberof plotZoom
     * @property
     * by default yLock=chart.spaceY()
     */


    zoom.yLock = function (_) {
      return arguments.length ? (yLock = _, zoom) : yLock;
    };

    function setLocks() {
      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'));
      var cos = chartObjSel.attr('transform', 'translate(0,0)');
      xLock = chartSel.node().getBBox().width - chart.spaceX() * .9;
      yLock = chartSel.node().getBBox().height - chart.spaceY() * .9;
      cos.attr('transform', 'translate(' + chartObjTrans[0] + ',' + chartObjTrans[1] + ')');
      utils.con.log('plotZoom', 'setLocks', {
        xLock: xLock,
        yLock: yLock
      });
    }
    /**
     * Sets the x and y locks (how far one can scroll)
     * (see {@link plotZoom#xLock} and {@link plotZoom#yLock})
     * @function plotZoom.setLocks
     * @returns {undefined}
     * @memberof plotZoom
     * @property
     */


    zoom.setLocks = setLocks;

    function zoom() {
      setLocks();
      var horizontalQ, verticalQ;

      if (orient == '2D') {
        horizontalQ = true;
        verticalQ = true;
      }

      if (orient == 'horizontal') {
        horizontalQ = true;
        verticalQ = false;
      }

      if (orient == 'vertical') {
        verticalQ = true;
        horizontalQ = false;
      } // capture transform event


      var transform = d3.event.transform;
      var chartBox = chartSel.node().getBBox();
      var xAxisBox = xAxisSel.node().getBBox();
      var yAxisBox = xAxisSel.node().getBBox();
      var chartWidth = chartBox.width - chartBox.x;
      var chartHeight = chartBox.height - chartBox.y;
      var xAxisWidth = xAxisBox.width; // - xAxisBox.x

      var xAxisHeight = xAxisBox.height; // - xAxisBox.y

      var yAxisWidth = yAxisBox.width; // - yAxisBox.x

      var yAxisHeight = yAxisBox.height; // -yAxisBox.y
      // enable wheel event

      if (eventType == "wheel") {
        var e = d3.event; // prevent page scrolling

        e.preventDefault(); // event delta is very very slow, so speed it up with wheel speed

        var w = d3.event.deltaY * wheelSpeed;
        var shiftQ = d3.event.shiftKey; // d3 has no way to make custom transform, so make an object and add
        // the necessary functions to make wheel event compatible with drag events

        if (orient == '2D') {
          transform = shiftQ ? {
            k: 1,
            x: w,
            y: 0
          } : {
            k: 1,
            x: 0,
            y: w
          };
        } else {
          transform = horizontalQ ? {
            k: 1,
            x: w,
            y: 0
          } : {
            k: 1,
            x: 0,
            y: w
          };
        } // the * -1 inverts the direction


        transform.applyX = function (x) {
          return x * this.k + this.x * -1;
        };

        transform.applyY = function (y) {
          return y * this.k + this.y * -1;
        };
      }

      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var xAxisObjSel = xAxisSel.select('.' + utils.str.hypenate(xAxis.namespace(), 'object-container'));
      var yAxisObjSel = yAxisSel.select('.' + utils.str.hypenate(yAxis.namespace(), 'object-container')); // xLock = chartSel.node().getBBox().width - chart.spaceX() - chartSel.node().getBBox().x
      // yLock = chartSel.node().getBBox().height - chart.spaceY()
      // console.table({'xLock':xLock, "yLock":yLock})
      // bhm.selection().node().getBBox().width - bhm.spaceX()

      var chartObjTrans = utils.sel.getTranslation(chartObjSel.attr('transform'));
      var xAxisObjTrans = utils.sel.getTranslation(xAxisObjSel.attr('transform'));
      var yAxisObjTrans = utils.sel.getTranslation(yAxisObjSel.attr('transform'));
      var x = horizontalQ ? transform.applyX(chartObjTrans[0]) : 0;

      if (horizontalQ) {
        x = x < -xLock ? (transform.x = 0, -xLock) : (transform.x = 0, Math.min(x, 0));
      }

      var y = verticalQ ? transform.applyY(chartObjTrans[1]) : 0;

      if (verticalQ) {
        y = y < -yLock ? (transform.y = 0, -yLock) : (transform.y = 0, Math.min(y, 0));
      }

      chartObjSel.attr('transform', 'translate(' + x + ',' + y + ')');

      if (horizontalQ) {
        xAxisObjSel.attr('transform', 'translate(' + x + ',' + 0 + ')');
      }

      if (verticalQ) {
        yAxisObjSel.attr('transform', 'translate(' + 0 + ',' + y + ')');
      } // var lasso = svg.select(".lasso-container")
      // if (!lasso.empty()) {
      //   lasso.attr('transform', 'translate('+x+','+y+')')
      // }

    }

    zoom.reset = function () {

      var chartObjSel = chartSel.select('.' + utils.str.hypenate(chart.namespace(), 'object-container'));
      var xAxisObjSel = xAxisSel.select('.' + utils.str.hypenate(xAxis.namespace(), 'object-container'));
      var yAxisObjSel = yAxisSel.select('.' + utils.str.hypenate(yAxis.namespace(), 'object-container'));
      chartObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
      xAxisObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
      yAxisObjSel.attr('transform', 'translate(' + 0 + ',' + 0 + ')');
    };

    return zoom;
  }

  function selectFilter(selection) {
    var data,
        namespace = 'd3sm-select-filter',
        selectionName = 'Select options:',
        defaultValue = undefined;
    var lastValue = undefined;

    selectFilter.data = function (_) {
      return arguments.length ? (data = _, selectFilter) : data;
    };

    selectFilter.namespace = function (_) {
      return arguments.length ? (namespace = _, selectFilter) : namespace;
    };

    selectFilter.selectionName = function (_) {
      return arguments.length ? (selectionName = _, selectFilter) : selectionName;
    };

    selectFilter.defaultValue = function (_) {
      return arguments.length ? (defaultValue = _, selectFilter) : defaultValue;
    };

    selectFilter.currentOption = currentOption;

    function selectFilter() {
      var container = utils.sel.safeSelect(selection, 'div', 'input-group').classed(utils.str.hypenate(namespace, 'container'), true),
          selectPrepend = utils.sel.safeSelect(container, 'div', 'select-prepend').classed('input-group-prepend', true),
          selectPrependSpan = utils.sel.safeSelect(selectPrepend, 'span', 'input-group-text').text(selectionName),
          select = utils.sel.safeSelect(container, 'select', 'custom-select').classed(utils.str.hypenate(namespace, 'select'), true),
          selectAppend = utils.sel.safeSelect(container, 'div', 'select-append').classed('input-group-prepend', true),
          selectAppendButton = utils.sel.safeSelect(selectAppend, 'a', 'filter-button').classed('btn btn-outline-secondary', true),
          filterButtonIcon = utils.sel.safeSelect(selectAppendButton, 'i', 'fa fa-filter'),
          inputGroup = utils.sel.safeSelect(container, 'div', 'filter-input-group').classed('input-group', true).classed('d-none', true),
          inputPrepend = utils.sel.safeSelect(inputGroup, 'div', 'input-group-prepend'),
          inputPrependSpan = utils.sel.safeSelect(inputPrepend, 'span', 'input-group-text').classed('search-button', true),
          inputPrependSpanIcon = utils.sel.safeSelect(inputPrependSpan, 'i', 'fa fa-search'),
          input = utils.sel.safeSelect(inputGroup, 'input', 'form-control').attr('placeholder', 'all').attr('type', 'text'),
          inputAppend = utils.sel.safeSelect(inputGroup, 'div', 'input-group-append'),
          inputAppendButton = utils.sel.safeSelect(inputAppend, 'a', 'close-button').classed('btn btn-outline-secondary', true),
          inputAppendButtonIcon = utils.sel.safeSelect(inputAppendButton, 'i', 'fa fa-close');
      var keys = d3.keys(data),
          options = select.selectAll('option');
      options = options.data(d3.keys(data));
      options = options.merge(options.enter().append('option')).attr('value', function (d, i) {
        return d;
      }).text(function (d, i) {
        return d;
      });
      var filterButton = selectAppendButton,
          closeButton = inputAppendButton;
      filterButton.on('click', function (d, i) {
        var currentStyle = inputGroup.classed('d-none');
        inputGroup.classed('d-none', !currentStyle);
      });
      closeButton.on('click', function (d, i) {
        input.property('value', '').dispatch('input');
      });
      input.on('input', function (d, i) {
        var val = input.property('value'),
            reg = new RegExp(val, 'gi'),
            use;

        if (val == '') {
          use = keys;
        } else {
          use = [];
          d3.keys(data).map(function (option, j) {
            var match = option.match(reg);

            if (match == null || match.join('') == '') ; else {
              use.push(option);
            }
          });
        }

        options = select.selectAll('option');
        options = options.data(use);
        options.exit().remove();
        options = options.merge(options.enter().append('option')).attr('value', function (d, i) {
          return d;
        }).text(function (d, i) {
          return d;
        });
        var current = currentOption();

        if (lastValue != current) {
          lastValue = current;
          select.dispatch('change');
        }
      });
    }

    function currentOption() {
      var val = selection.select("select").property('value');
      return val == undefined || val == '' ? defaultValue == undefined ? d3.keys(data)[0] : defaultValue : val;
    }

    return selectFilter;
  }

  /*******************************************************************************
  **                                                                            **
  **                                                                            **
  **                                DATATOGGLE                                  **
  **                                                                            **
  **                                                                            **
  *******************************************************************************/

  /**
   * Creates a datatoggle
   * @constructor datatoggle
   * @param {d3.selection} selection
   * @namespace datatoggle
   * @returns {function} datatoggle
   */

  function datatoggle(selection) {
    var
    /**
    * Keys to make toggle-able options
    * (see {@link datatoggle#keys})
    * @param {string[]} [keys=undefined]
    * @memberof datatoggle#
    * @property
    */
    keys,

    /**
    * What to do when a different key is clicked
    * (see {@link datatoggle#updateFunction})
    * @param {function} [updateFunction=function(){}]
    * @memberof datatoggle#
    * @property
    */
    updateFunction = function updateFunction() {},

    /**
    * Namespace for all items made by this instance of datatoggle
    * @param {string} [namespace="d3sm-databar"]
    * @memberof datatoggle#
    * @property
    */
    namespace = 'd3sm-databar',

    /**
    * Currently toggled key
    * @param {string} [currentKey=undefined]
    * @memberof datatoggle#
    * @property
    */
    currentKey,
        xAxisSelectQ = false,
        xAxisOptions,
        yAxisSelectQ = false,
        yAxisOptions,
        data;

    toggle.xAxisSelectQ = function (_) {
      return arguments.length ? (xAxisSelectQ = _, toggle) : xAxisSelectQ;
    };

    toggle.yAxisSelectQ = function (_) {
      return arguments.length ? (yAxisSelectQ = _, toggle) : yAxisSelectQ;
    };

    toggle.xAxisOptions = function (_) {
      return arguments.length ? (xAxisOptions = _, toggle) : xAxisOptions;
    };

    toggle.yAxisOptions = function (_) {
      return arguments.length ? (yAxisOptions = _, toggle) : yAxisOptions;
    };

    toggle.data = function (_) {
      return arguments.length ? (data = _, toggle) : data;
    };

    toggle.keys = function (_) {
      return arguments.length ? (keys = _, toggle) : keys;
    };

    toggle.currentKey = function (_) {
      return arguments.length ? (currentKey = _, toggle) : currentKey;
    };
    /**
     * Gets / sets the updateFunction
     * (see {@link datatoggle#updateFunction})
     * @function datatoggle.updateFunction
     * @param {function} [_=none]
     * @returns {datatoggle | function}
     * @memberof datatoggle
     * @property
     * by default updateFunction = function(){}
     */


    toggle.updateFunction = function (_) {
      return arguments.length ? (updateFunction = _, toggle) : updateFunction;
    };
    /**
     * Gets / sets the namespace
     * (see {@link datatoggle#namespace})
     * @function datatoggle.namespace
     * @param {string} [_=none]
     * @returns {datatoggle | string}
     * @memberof datatoggle
     * @property
     * by default namespace = 'd3sm-databar'
     */


    toggle.namespace = function (_) {
      return arguments.length ? (namespace = _, toggle) : namespace;
    };
    /**
     * Gets / sets the currentKey
     * (see {@link datatoggle#currentKey})
     * @function datatoggle.currentKey
     * @param {string} [_=none]
     * @returns {datatoggle | string}
     * @memberof datatoggle
     * @property
     * by default currentKey = undefined
     */


    toggle.currentKeys = function () {
      var vals = {};
      d3.keys(filterSelects).map(function (k, i) {
        vals[k] = filterSelects[k].currentOption();
      });
      return vals;
    };

    var filterSelects = {};

    function toggle() {
      // selection options
      // selection.classed('d-flex flex-row', true)
      // var filterButton = utils.sel.safeSelect(selection, 'a', 'slider-buttons')
      // var filterI = utils.sel.safeSelect(filterButton, 'i', 'fa fa-sliders')

      /*BUG: unexpected behavior.
        - when using bootstrap-eque way for collapse, clicking button submits to
        the same page in applications (but not in demo), so using anchor (<a>)
        - when using anchor, cause page to jump to that location
        - when using show, the first open works, but the close does not.
      */
      // filterButton.attr('class', 'btn btn-secondary')
      // .attr('data-toggle', 'collapse')
      // .attr('href', '#'+utils.str.hypenate(namespace, 'data-toggle'))
      // .attr('target', '_blank')
      // .html(filterButton.html()+' Filters')
      // .on('click', function(d, i){
      //   d3.event.preventDefault()
      //   d3.event.stopPropagation()
      //   var dt = d3.select("#"+utils.str.hypenate(namespace, 'data-toggle'))
      //   dt.classed('show', !dt.classed('show'))
      //   dt.classed('d-inline-flex', dt.classed('show'))
      //
      //   filterButton.classed('btn-primary', dt.classed('show'))
      //   filterButton.classed('btn-secondary', !dt.classed('show'))
      // })
      // .classed('d-inline-flex', true)
      // .style("margin-right", '10px')
      // var datatoggleCollapse = utils.sel.safeSelect(selection, 'div', utils.str.hypenate(namespace,'collapse'))
      // .attr('id', utils.str.hypenate(namespace, 'data-toggle'))
      // .classed('collapse', true)
      // var flexRow = utils.sel.safeSelect(datatoggleCollapse, 'div', 'd-inline-flex flex-row flex-wrap')
      var flexRow = utils.sel.safeSelect(selection, 'div', 'd-inline-flex flex-row flex-wrap');
      var dataopts = flexRow.selectAll('div.' + utils.str.hypenate(namespace, 'select-filter')); // remove excess

      dataopts.exit().remove(); // bind data

      dataopts = dataopts.data(d3.keys(data)); //enter

      var doEnter = dataopts.enter().append('div').attr('class', 'select-filter');
      dataopts = dataopts.merge(doEnter).style('margin-right', "10px");
      dataopts.each(function (d, i) {
        var t = d3.select(this);
        var sf = selectFilter(t).data(data[d]).namespace(utils.str.hypenate(namespace, d)).selectionName(d);
        sf();
        filterSelects[d] = sf;
      });
      selection.selectAll('select').on('change', function () {
        updateFunction();
      }); // bind update function
      // d3.selectAll

      return toggle;
    }

    return toggle;
  }

  var aux = {
    lasso: lasso,
    plotZoom: plotZoom,
    multiPlotZoom: multiPlotZoom,
    datatoggle: datatoggle,
    selectFilter: selectFilter,
    lassoWidget: lassoWidget
  };

  var d3sm = {
    aux: aux,
    axis: axis,
    charts: charts,
    colorFunction: colorFunction,
    debugQ: false,
    groupingSpacer: groupingSpacer,
    legends: legends,
    tooltip: tooltip,
    utils: utils
  };

  if (typeof window !== 'undefined') {
    window.d3sm = d3sm;
  }

  exports.default = d3sm;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
